﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace CommonTool
{
    public static class TypeConvert
    {
        public static byte[] StructToBytes(object structObj)
        {
            // 得到结构体的大小
            int size = Marshal.SizeOf(structObj);
            // 创建byte数组
            byte[] bytes = new byte[size];
            // 分配结构体大小的内存空间
            IntPtr structPtr = Marshal.AllocHGlobal(size);
            // 将结构体拷到分配好的内存空间
            Marshal.StructureToPtr(structObj, structPtr, false);
            //从 内存空间拷到byte数组
            Marshal.Copy(structPtr, bytes, 0, size);
            // 释放内存空间
            Marshal.FreeHGlobal(structPtr);
            // 返回byte数组
            return bytes;
        }

        public static T BytesToStuct<T>(byte[] bytes)
        {
            // 得到结构体的大小
            int size = Marshal.SizeOf(typeof(T));
            // byte数组长度小于结构体的大小
            if (size > bytes.Length)
            {
                // 返回空
                return default(T);
            }
            // 分配结构体大小的内存空间
            IntPtr structPtr = Marshal.AllocHGlobal(size);
            // 将byte数组拷到分配好的内存空间
            Marshal.Copy(bytes, 0, structPtr, size);
            // 将内存空间转换为目标结构体
            T obj = (T)Marshal.PtrToStructure(structPtr, typeof(T));
            // 释放内存空间
            Marshal.FreeHGlobal(structPtr);
            // 返回结构体
            return obj;
        }

        public static ushort[] BytesToUShorts(byte[] bytes)
        {
            int length = bytes.Length / 2;
            List<ushort> tmpList = new List<ushort>();
            for (int i = 0; i < length; i++)
            {
                tmpList.Add(BitConverter.ToUInt16(bytes, i * 2));
            }
            return tmpList.ToArray();
        }

        public static byte[] UShortsToBytes(ushort[] list)
        {
            List<byte> tmpList = new List<byte>();
            for (int i = 0; i < list.Length; i++)
            {
                tmpList.AddRange(BitConverter.GetBytes(list[i]));
            }
            return tmpList.ToArray();
        }

        public static byte[] ObjectToBytes(object obj)
        {
            MemoryStream ms = new MemoryStream();
            IFormatter formatter = new BinaryFormatter();
            formatter.Serialize(ms, obj);
            return ms.ToArray();
        }

        public static object BytesToObject(byte[] bytes)
        {
            MemoryStream ms = new MemoryStream(bytes);
            IFormatter formatter = new BinaryFormatter();
            return formatter.Deserialize(ms);
        }

        /// <summary>
        /// bool数组转换成0/1的byte数组
        /// </summary>
        /// <param name="datas"></param>
        /// <returns></returns>
        public static byte[] BoolArrayToByteArray(bool[] datas)
        {
            byte[] list = new byte[datas.Length];
            for (int i = 0; i < datas.Length; i++)
            {
                list[i] = Convert.ToByte(datas[i]);
            }
            return list;
        }

        /// <summary>
        /// 0/1的byte数组转换成bool数组
        /// </summary>
        /// <param name="datas"></param>
        /// <returns></returns>
        public static bool[] ByteArrayToBoolArray(byte[] datas)
        {
            bool[] list = new bool[datas.Length];
            for (int i = 0; i < datas.Length; i++)
            {
                list[i] = Convert.ToBoolean(datas[i]);
            }
            return list;
        }
    }
}
