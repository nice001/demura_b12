﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonTool
{
	public static class DataCompare
	{
		public static void CompareBool(bool[] new_datas, bool[] old_datas, ref List<int> true_datas, ref List<int> false_datas, int offer = 0)
		{
			if (new_datas.Length == 0)
				return;
			if (old_datas == null || old_datas.Length == 0)
				old_datas = new bool[new_datas.Length];
			for (int i = 0; i < new_datas.Length; i++)
			{
				if (new_datas[i] && !old_datas[i])
					true_datas.Add(offer + i);
				else if (!new_datas[i] && old_datas[i])
					false_datas.Add(offer + i);
			}
		}
	}
}
