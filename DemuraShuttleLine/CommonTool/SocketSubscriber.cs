﻿using HYC.HTCommunication.SocketWrapper;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace CommonTool
{
    public class SocketSubscriber:IDisposable
    {
        HTCOMMSocketServer server;
        string ip = "";
        int port = 30000;
        Dictionary<string, Action<string, byte[]>> subscribe_channels = new Dictionary<string, Action<string, byte[]>>();
        Dictionary<string, Socket> publish_socket = new Dictionary<string, Socket>();

        public SocketSubscriber(string _ip, int _port)
        {
            server = new HTCOMMSocketServer();
            server.ClientConnected += Server_ClientConnected;
            server.ClientDisconnected += Server_ClientDisconnected;
            server.DataReceived += Server_DataReceived;
            server.Listen(_port);
            ip = _ip;
        }

        private void Server_DataReceived(object sender, SocketEventArgs e)
        {
            byte[] tmps = e.Datas;
            string msg = Encoding.ASCII.GetString(tmps);
            JObject jObject = JObject.Parse(msg);
            string channel = jObject.GetValue("channel").ToString();
            if (subscribe_channels.ContainsKey(channel))
            {
                subscribe_channels[channel].Invoke(channel, (byte[])jObject.GetValue("data"));
            }
        }

        private void Server_ClientDisconnected(object sender, SocketEventArgs e)
        {
          
        }

        private void Server_ClientConnected(object sender, SocketEventArgs e)
        {
           
        }

        public void Subscribe(string channel, Action<string,byte[]> handler)
        {
            subscribe_channels.Add(channel, handler);
        }

        public void Publish(string channel, byte[] datas)
        {
            try
            {
                Socket socket;
                if (publish_socket.ContainsKey(channel)&& publish_socket[channel].Connected)
                    socket = publish_socket[channel];
                else
                {
                    socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
                    socket.Connect(ip, port);
                    publish_socket.Add(channel, socket);
                }
                JObject jObject = new JObject();
                jObject.Add("channel", channel);
                jObject.Add("data", datas);
                byte[] tmps = Encoding.ASCII.GetBytes(jObject.ToString());
                try
                {
                    socket.Send(tmps);
                }
                catch (SocketException)
                {
                    publish_socket.Remove(channel);
                    Publish(channel, datas);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Dispose()
        {
            foreach (Socket item in publish_socket.Values)
            {
                item.Close();
            }
            server.Dispose();
        }
    }
}
