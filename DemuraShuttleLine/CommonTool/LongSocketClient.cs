﻿using HYC.HTCommunication.SocketWrapper;
using HYC.HTLog.Core;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace CommonTool
{
    public class LongSocketClient
    {
        public event Action<bool> SocketStatus;
        string socket_ip;
        int socket_port;
        ILogNet logNet;
        public Socket socketClient;
        HTCOMMSocketClient client;
        public event EventHandler<byte[]> DataReceived;
        public LongSocketClient(string _ip,int _port, ILogNet _logNet)
        {
            socket_ip = _ip;
            socket_port = _port;
            logNet = _logNet;
        }

        void SocketConnectSync()
        {
            Task.Factory.StartNew(() =>
            {
                Thread.Sleep(3000);
                SocketConnect();
            });
        }

        public void SocketConnect()
        {
            try
            {
                client = new HTCOMMSocketClient();
                client.DataReceived += Client_DataReceived;
                client.ClientDisconnected += Client_ClientDisconnected;
                client.ClientConnected += Client_ClientConnected;
                client.Connect(socket_ip, socket_port);
            }
            catch (Exception ex)
            {
                HandleSocketException(ex);
            }
        }

        private void Client_ClientConnected(object sender, SocketEventArgs e)
        {
            SocketStatus?.BeginInvoke(true, null, null);
            socketClient = e.Socket;
            logNet.WriteInfo("LongSocketClient", socket_ip + ":" + socket_port + "建立连接");
        }

        private void Client_ClientDisconnected(object sender, SocketEventArgs e)
        {
            SocketStatus?.BeginInvoke(false, null, null);
            socketClient = null;
            logNet.WriteInfo("LongSocketClient", socket_ip + ":" + socket_port + "断开连接");
            SocketConnectSync();
        }

        private void Client_DataReceived(object sender, SocketEventArgs e)
        {
            DataReceived?.Invoke(this, e.Datas);
        }

        object send_lock = new object();
        public void SendSocket(byte[] datas)
        {
            if (socketClient == null|| !socketClient.Connected)
                return;
            lock (send_lock)
            {
                try
                {
                    List<byte> sendDatas = new List<byte>();
                    sendDatas.AddRange(datas);
                    while (true)
                    {
                        int num = socketClient.Send(sendDatas.ToArray());
                        if (num < sendDatas.Count)
                        {
                            sendDatas.RemoveRange(0, num);
                        }
                        else
                            break;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        private void HandleSocketException(Exception ex)
        {
            if (ex.GetType() == typeof(ObjectDisposedException))
            {
                logNet.WriteInfo("SocketConnect", "Socket连接失败，对象已经被清理");
                SocketConnectSync();
            }
            else if (ex.GetType() == typeof(SocketException))
            {
                SocketException se = (SocketException)ex;
                client.Dispose();
                logNet.WriteInfo("SocketConnect", "Socket连接失败:" + se.ErrorCode + ":" + se.Message);
                SocketConnectSync();
            }
            else if (ex.GetType() == typeof(InvalidOperationException))
            {
                logNet.WriteInfo("SocketConnect", "Socket连接操作无效:" + ex.Message);
                client.Dispose();
                SocketConnectSync();
            }
            else
                logNet.WriteException("SocketConnect", ex);
        }

    }
}
