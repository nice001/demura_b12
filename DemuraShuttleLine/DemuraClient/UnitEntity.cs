﻿using System;
using System.Runtime.InteropServices;

namespace DemuraClient
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    internal struct UnitEntity
    {
        /// <summary>
        /// 0.OFF_Line 1, ON_LINE, 2 Busy
        /// </summary>
        public ushort ChanelStatus;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
        public string PanelID;
        /// <summary>
        /// 0 Normal, 1 Rework1,  2 Rework2 ，3 Ok, 4 NG，5Cancel
        /// </summary>
        public ushort PanelStatus;
        /// <summary>
        /// 已在哪些工位测试过
        /// </summary>
        public ushort TestStations;
        /// <summary>
        /// Tester4-测试次数设定
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public ushort[] TestCountSV;
        /// <summary>
        /// 测试机号码
        /// </summary>
        public ushort TesterNo;
        public ushort zUserDefine1;
        public ushort zUserDefine2;
        public ushort zUserDefine3;
        public ushort zUserDefine4;
        public ushort zUserDefine5;
        public ushort zUserDefine6;
        public ushort zUserDefine7;
        public ushort zUserDefine8;
        public ushort zUserDefine9;
        public ushort zUserDefine10;
        /// <summary>
        /// 备用
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public ushort[] Spare;
    }

    public class UnitObject
    {
        /// <summary>
        /// 0.OFF_Line 1, ON_LINE, 2 Busy
        /// </summary>
        public short ChanelStatus=-1;
        public string PanelID=null;
        /// <summary>
        /// 0 Normal, 1 Rework1,  2 Rework2 ，3 Ok, 4 NG，5Cancel
        /// </summary>
        public short PanelStatus=-1;
        /// <summary>
        /// 已在哪些工位测试过
        /// </summary>
        public short TestStations=-1;
        /// <summary>
        /// Tester4-测试次数设定
        /// </summary>
        public ushort[] TestCountSV=null;
        /// <summary>
        /// 测试机号码
        /// </summary>
        public short TesterNo=-1;
        public short zUserDefine1 = -1;
        public short zUserDefine2 = -1;
        public short zUserDefine3 = -1;
        public short zUserDefine4 = -1;
        public short zUserDefine5 = -1;
        public short zUserDefine6 = -1;
        public short zUserDefine7 = -1;
        public short zUserDefine8 = -1;
        public short zUserDefine9 = -1;
        public short zUserDefine10 = -1;
        /// <summary>
        /// 备用
        /// </summary>
        public ushort[] Spare=null;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct PLCToUnitEntity
    {
        /// <summary>
        /// 0.OFF_Line 1, ON_LINE, 2 Busy
        /// </summary>
        public ushort ChanelStatus;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
        public string PanelID;
        /// <summary>
        /// 0 Normal, 1 Rework1,  2 Rework2 ，3 Ok, 4 NG，5Cancel
        /// </summary>
        public ushort PanelStatus;
        /// <summary>
        /// 已在哪些工位测试过
        /// </summary>
        public ushort TestStations;
        /// <summary>
        /// Tester4-测试次数设定
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public ushort[] TestOkCount;
        /// <summary>
        /// Tester4-测试次数设定
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public ushort[] TestNgCount;
        /// <summary>
        /// Tester4-测试次数设定
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public ushort[] TestCountSV;
        /// <summary>
        /// 测试机号码
        /// </summary>
        public ushort TesterNo;
        public ushort zUserDefine1;
        public ushort zUserDefine2;
        public ushort zUserDefine3;
        public ushort zUserDefine4;
        public ushort zUserDefine5;
        public ushort zUserDefine6;
        public ushort zUserDefine7;
        public ushort zUserDefine8;
        public ushort zUserDefine9;
        public ushort zUserDefine10;
        /// <summary>
        /// 备用
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public ushort[] Spare;
    }
}
