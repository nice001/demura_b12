﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CommonTool;
using Newtonsoft.Json.Linq;

namespace DemuraClient
{
    /// <summary>
    /// 使用步骤
    /// 1、实例化对象，注册事件
    /// 2、初始化客户端 InitClient
    /// 3、获取正在进行的工作 GetDoingSignal
    /// </summary>
    public class MessageClient:IDisposable
    {
        private string socket_ip;
        private int socket_port =Properties.Settings.Default.server_port;
        Socket client;
        string unitChannel = "channel_unit";//信号动作频道
        readonly string plcStatus = "PLCStatus";//PLC状态频道
        readonly string plcConnect = "PLCConnect"; // PLC 连接频道
        readonly string plcChannel = "PLCChannel";//写panel数据频道
        readonly string dataChannel = "SingleDataChannel";//写单个数据频道
        readonly string ackChannel = "ACKChannel";//写M区ACK频道
        readonly string cimStatusChannel = "CIMStatusChannel";//CIM状态频道
        readonly string commonChannel = "CommonChannel";//普通信息更新频道
        string panelStatusChannel = "PanelStatusChannel";//panel状态频道
        MQClient mqClient;
        int currentUnitIndex = 0;

        private bool serverConnected;

        /// <summary>
        /// 线体连接状态
        /// </summary>
        public bool ServerConnected { get { return serverConnected; } }
        /// <summary>
        /// 服务器连接事件
        /// </summary>
        public event Action<bool> ServerConnectedEvent;

        /// <summary>
        /// CIM连接事件
        /// </summary>
        public event Action<bool> CIMConnectedEvent;

        /// <summary>
        /// PLC连接事件
        /// </summary>
        public event Action<int> PLCConnectedEvent;

        /// <summary>
        /// 信号事件
        /// </summary>
        public event Action<bool[]> DoWorkEvent;

        /// <summary>
        /// 时间更新
        /// </summary>
        public event Action<DateTime> TimeUpdateEvent;

        /// <summary>
        /// panel状态更新
        /// </summary>
        public event Action<int[],int[]> PanelStatusEvent;

        public event Action<string> MessageNoticeEvent;

        /// <summary>
        /// 初始化Client，可能会长时间阻塞
        /// </summary>
        /// <param name="_ip"></param>
        public MessageClient(int unit_index, string _ip)
        {
            socket_ip = _ip;
            mqClient = new MQClient(_ip,Properties.Settings.Default.mq_port);
            panelStatusChannel += unit_index;            
            currentUnitIndex = unit_index;
        }

        /// <summary>
        /// 初始化Client，可能会长时间阻塞
        /// </summary>
        public void InitClient()
        {
            mqClient.MessageNoticeEvent += MessageNoticeEvent;
            ConnectMQ();
            SocketConnect();
        }

        void ConnectMQ()
        {
            try
            {
                mqClient.Subscribe(unitChannel, (channel, message) =>
                {
                    bool[] cmds= TypeConvert.ByteArrayToBoolArray((byte[])message);
                    DoWorkEvent?.Invoke(cmds);
                });
                mqClient.Subscribe(plcStatus, (channel, message) =>
                {
                    //ClientGlobal.logNet.WriteInfo("MessageClient", "PLC状态更新，当前状态：" + message);
                    PLCConnectedEvent?.Invoke((int)message);
                });
                mqClient.Subscribe(commonChannel, (channel, message) =>
                {
                    KeyValuePair<string, object> keyValuePair = (KeyValuePair<string, object>)TypeConvert.BytesToObject((byte[])message);
                    if (keyValuePair.Key == "Time")
                    {
                        ClientGlobal.logNet.WriteInfo("MessageClient", "时间更新，更新时间为：" + (string)keyValuePair.Value);
                        TimeUpdateEvent?.Invoke(DateTime.Parse((string)keyValuePair.Value));
                    }
                });
                mqClient.Subscribe(panelStatusChannel, (channel, message) =>
                {
                    Tuple<int[], int[]> tuple = (Tuple<int[], int[]>)TypeConvert.BytesToObject((byte[])message);
                    ClientGlobal.logNet.WriteInfo("MessageClient", "panel状态更新");
                    PanelStatusEvent?.Invoke(tuple.Item1, tuple.Item2);
                });
                mqClient.Subscribe(cimStatusChannel, (channel, message) =>
                {
                    KeyValuePair<string, bool> keyValuePair = (KeyValuePair<string, bool>)TypeConvert.BytesToObject((byte[])message);
                    if (keyValuePair.Key == "CIM")
                    {
                        CIMConnectedEvent?.Invoke(keyValuePair.Value);
                    }
                });
            }
            catch(SocketException se)
            {
                ClientGlobal.logNet.WriteInfo("ConnectMQ", "通道订阅出错：" + se.Message);
                Thread.Sleep(3000);
                ConnectMQ();
            }
        }

        void SocketConnectSync()
        {
            Task.Factory.StartNew(() =>
            {
                UpdateServerStatus();
                Thread.Sleep(3000);
                SocketConnect();
            });
        }
        bool socketReseting = false;//防止多次连接
        void SocketConnect()
        {
            if (socketReseting)
                return;
            socketReseting = true;
            try
            {
                client = new Socket(SocketType.Stream, ProtocolType.Tcp);
                client.ReceiveTimeout = 5000;
                client.SendTimeout = 5000;
                client.Connect(socket_ip, socket_port);
                Task.Factory.StartNew(KeepLiveServer, TaskCreationOptions.LongRunning);
                Console.WriteLine(DateTime.Now.ToString("HH:mm:ss:fff") + ":" + client.LocalEndPoint.ToString() + "建立连接");
                GeSystemTime();
                UpdateServerStatus();
                socketReseting = false;
            }
            catch (Exception ex)
            {
                socketReseting = false;
                HandleSocketException(ex);
            }
        }

        /// <summary>
        /// 发送M区信号ACK
        /// </summary>
        /// <param name="address">信号地址</param>
        public void SendSignal(int address,bool result=true)
        {
            if (!serverConnected)
                throw new Exception("与线体电脑连接已断开!");
            JObject jObject = new JObject
            {
                { "operate", ackChannel },
                { "address", address },
                { "result", result }
            };
            byte[] datas = Encoding.ASCII.GetBytes(jObject.ToString());
            try
            {
                SendSocket(datas, false);
            }
            catch (Exception ex)
            {
                ClientGlobal.logNet.WriteInfo("MessageClient", "发送信号失败，原因："+ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// 发送工位Panel数据
        /// </summary>
        /// <param name="unit_index">当前单体索引：1-10</param>
        /// <param name="panel_index">当前单体中panel索引：1-4，上层1：1；上层2：2；下层1：3；下层2：4</param>
        /// <param name="obj">数据</param>
        public void SendPanelData(int unit_index, int panel_index, UnitObject obj)
        {
            if (!serverConnected)
                throw new Exception("与线体电脑连接已断开!");
            int panelIndex = GetPanelIndex(unit_index, panel_index);
            UnitEntity lastUnit = GetPanelData(panelIndex);
            if (obj.ChanelStatus >= 0)
                lastUnit.ChanelStatus = (ushort)obj.ChanelStatus;
            if (obj.PanelID != null)
                lastUnit.PanelID = obj.PanelID;
            if (obj.PanelStatus >= 0)
                lastUnit.PanelStatus = (ushort)obj.PanelStatus;
            if (obj.Spare != null)
                lastUnit.Spare = obj.Spare;
            if (obj.TestCountSV != null)
                lastUnit.TestCountSV = obj.TestCountSV;
            if (obj.TesterNo >= 0)
                lastUnit.TesterNo = (ushort)obj.TesterNo;
            if (obj.TestStations >= 0)
                lastUnit.TestStations = (ushort)obj.TestStations;
            if (obj.zUserDefine1 >= 0)
                lastUnit.zUserDefine1 = (ushort)obj.zUserDefine1;
            if (obj.zUserDefine2 >= 0)
                lastUnit.zUserDefine2 = (ushort)obj.zUserDefine2;
            if (obj.zUserDefine3 >= 0)
                lastUnit.zUserDefine3 = (ushort)obj.zUserDefine3;
            if (obj.zUserDefine4 >= 0)
                lastUnit.zUserDefine4 = (ushort)obj.zUserDefine4;
            if (obj.zUserDefine5 >= 0)
                lastUnit.zUserDefine5 = (ushort)obj.zUserDefine5;
            if (obj.zUserDefine6 >= 0)
                lastUnit.zUserDefine6 = (ushort)obj.zUserDefine6;
            if (obj.zUserDefine7 >= 0)
                lastUnit.zUserDefine7 = (ushort)obj.zUserDefine7;
            if (obj.zUserDefine8 >= 0)
                lastUnit.zUserDefine8 = (ushort)obj.zUserDefine8;
            if (obj.zUserDefine9 >= 0)
                lastUnit.zUserDefine9 = (ushort)obj.zUserDefine9;
            if (obj.zUserDefine10 >= 0)
                lastUnit.zUserDefine10 = (ushort)obj.zUserDefine10;
            JObject jObject = new JObject
            {
                { "operate", plcChannel },
                { "panelIndex", panelIndex },
                { "panelDatas",  TypeConvert.StructToBytes(lastUnit) }
            };
            byte[] datas = Encoding.ASCII.GetBytes(jObject.ToString());
            try
            {
                SendSocket(datas, false);
            }
            catch (Exception ex)
            {
                ClientGlobal.logNet.WriteInfo("MessageClient", "发送Panel数据失败，原因：" + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// 发送单个数据
        /// </summary>
        /// <typeparam name="T">数据类型，目前包含string、ushort[]与byte[]</typeparam>
        /// <param name="address">地址</param>
        /// <param name="data">数据</param>
        public void SendSingleData<T>(int address, T data, int length = 0)
        {
            if (!serverConnected)
                throw new Exception("与线体电脑连接已断开!");
            JObject jObject = new JObject();
            jObject.Add("address", address);
            if (typeof(T) == typeof(string))
                jObject.Add("data", Encoding.ASCII.GetBytes(data.ToString()));
            else if (typeof(T) == typeof(ushort[]))
                jObject.Add("data", TypeConvert.UShortsToBytes((ushort[])Convert.ChangeType(data, typeof(short[]))));
            else if (typeof(T) == typeof(byte[]))
                jObject.Add("data", (byte[])Convert.ChangeType(data, typeof(byte[])));
            else
                throw new NotSupportedException("不支持该数据类型");
            jObject.Add("type", typeof(T).ToString());
            jObject.Add("length", length);
            jObject.Add("operate", dataChannel);
            byte[] datas = Encoding.ASCII.GetBytes(jObject.ToString());
            try
            {
                SendSocket(datas, false);
            }
            catch (Exception ex)
            {
                ClientGlobal.logNet.WriteInfo("MessageClient", "发送单个数据失败，原因：" + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// 获取工位Panel信息
        /// </summary>
        /// <param name="unit_index">当前单体索引：1-10</param>
        /// <param name="index">当前单体中panel索引：1-4，上层1：1；上层2：2；下层1：3；下层2：4</param>
        /// <returns></returns>
        public PLCToUnitEntity GetPLCPanelData(int unit_index, int panel_index)
        {
            if (!serverConnected)
                throw new Exception("与线体电脑连接已断开!");
            int panelIndex = GetPanelIndex(unit_index, panel_index);
            JObject jObject = new JObject();
            jObject.Add("operate", "GetPLCPanelData");
            jObject.Add("panelIndex", panelIndex);
            byte[] datas = Encoding.ASCII.GetBytes(jObject.ToString());
            try
            {
                byte[] tmpList = SendSocket(datas, true);
                if(tmpList==null)
                    throw new Exception("与线体电脑连接已断开!");
                return TypeConvert.BytesToStuct<PLCToUnitEntity>(tmpList);
            }
            catch (Exception ex)
            {
                ClientGlobal.logNet.WriteInfo("MessageClient", "获取工位信息失败，原因：" + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// 获取单个地址的数据
        /// </summary>
        /// <typeparam name="T">数据类型，目前包含string、bool[]、ushort[]与byte[]</typeparam>
        /// <param name="address">数据地址</param>
        /// <param name="length">数据长度</param>
        /// <returns></returns>
        public T GetSingleData<T>(int address, int length, OperateArea area)
        {
            if (!serverConnected)
                return default(T);
            JObject jObject = new JObject();
            jObject.Add("operate", "GetSingleData");
            jObject.Add("address", address);
            jObject.Add("length", length);
            jObject.Add("area", area.ToString());
            byte[] datas = Encoding.ASCII.GetBytes(jObject.ToString());
            try
            {
                byte[] receiveDatas = SendSocket(datas, true);
                if(receiveDatas==null)
                    return default(T);
                if (typeof(T) == typeof(string))
                    return (T)Convert.ChangeType(Encoding.ASCII.GetString(receiveDatas).Trim(), typeof(T));
                else if (typeof(T) == typeof(ushort[]))
                    return (T)Convert.ChangeType(TypeConvert.BytesToUShorts(receiveDatas), typeof(T));
                else if (typeof(T) == typeof(bool[]))
                    return (T)Convert.ChangeType(TypeConvert.ByteArrayToBoolArray(receiveDatas), typeof(T));
                else if (typeof(T) == typeof(byte[]))
                    return (T)Convert.ChangeType(receiveDatas, typeof(T));
                else
                    return default(T);
            }
            catch (Exception ex)
            {
                ClientGlobal.logNet.WriteInfo("MessageClient", "获取地址数据失败，原因：" + ex.Message);
                return default(T);
            }
        }

        /// <summary>
        /// 发送ReDemura数据
        /// </summary>
        /// <param name="panelId"></param>
        /// <returns>响应数据</returns>
        public byte[] SendRemuraData(string panelId)
        {
            if (!serverConnected)
                return null;
            JObject jObject = new JObject();
            jObject.Add("operate", "ReDemura");
            jObject.Add("panelId", panelId);
            byte[] datas = Encoding.ASCII.GetBytes(jObject.ToString());
            try {
                return SendSocket(datas, true);
            }
            catch (Exception ex)
            {
                ClientGlobal.logNet.WriteInfo("MessageClient", "发送Redemura失败，原因：" + ex.Message);
                return null;
            }
        }

        object send_lock = new object();
        private byte[] SendSocket(byte[] datas, bool isReceive = false)
        {
            lock (send_lock)
            {
                try
                {
                    List<byte> sendDatas = new List<byte>();
                    sendDatas.AddRange(datas);
                    while (true)
                    {
                        int num = client.Send(sendDatas.ToArray());
                        if (num < sendDatas.Count)
                        {
                            sendDatas.RemoveRange(0, num);
                        }
                        else
                            break;
                    }
                    if (isReceive)
                    {
                        List<byte> receiveDatas = new List<byte>();
                        byte[] receiveBuffer;
                        while (true)
                        {
                            receiveBuffer = new byte[1024];
                            int length = client.Receive(receiveBuffer, 0, 1024, SocketFlags.None);
                            receiveDatas.AddRange(receiveBuffer.Take(length));
                            if (length < 1024)
                                break;
                        }
                        return receiveDatas.ToArray();
                    }
                    else
                        return null;
                }
                catch (Exception ex)
                {
                    HandleSocketException(ex);
                    throw;
                }
            }
        }

        private void HandleSocketException(Exception ex)
        {
            if (ex.GetType() == typeof(ObjectDisposedException))
            {
                ClientGlobal.logNet.WriteInfo("SocketConnect", "Socket连接失败，对象已经被清理");
                SocketConnectSync();
            }
            else if (ex.GetType() == typeof(SocketException))
            {
                SocketException se = (SocketException)ex;
                client.Close();
                ClientGlobal.logNet.WriteInfo("SocketConnect", "Socket连接失败:" + se.ErrorCode + ":" + se.Message);
                SocketConnectSync();
            }
            else if (ex.GetType() == typeof(InvalidOperationException))
            {
                ClientGlobal.logNet.WriteInfo("SocketConnect", "Socket连接操作无效:" + ex.Message);
                client.Close();
                SocketConnectSync();
            }
            else
                ClientGlobal.logNet.WriteException("SocketConnect", ex);
        }

        /// <summary>
        /// 与server保活
        /// </summary>
        private void KeepLiveServer()
        {
            while (true)
            {
                try
                {
                    JObject jObject = new JObject();
                    jObject.Add("operate", "KeepLive");
                    jObject.Add("unitIndex", currentUnitIndex);
                    byte[] datas = Encoding.ASCII.GetBytes(jObject.ToString());
                    SendSocket(datas);
                    if (!client.Connected)
                    {
                        break;
                    }
                    Thread.Sleep(1000);
                }
                catch
                {
                    break;
                }
            }
        }

        /// <summary>
        /// 获取已写入的工位Panel信息
        /// </summary>
        /// <param name="panelIndex">Panel的索引：1-40</param>
        /// <returns></returns>
        private UnitEntity GetPanelData(int panelIndex)
        {
            JObject jObject = new JObject();
            jObject.Add("operate", "GetPanelData");
            jObject.Add("panelIndex", panelIndex);
            byte[] datas = Encoding.ASCII.GetBytes(jObject.ToString());
            try
            {
                byte[] tmpList = SendSocket(datas, true);
                if(tmpList==null)
                    throw new Exception("与线体电脑连接已断开!");
                return TypeConvert.BytesToStuct<UnitEntity>(tmpList);
            }
            catch (Exception ex)
            {
                ClientGlobal.logNet.WriteException("SocketConnect", ex);
                throw;
            }
        }

        /// <summary>
        /// 获取系统时间
        /// </summary>
        private void GeSystemTime()
        {
            JObject jObject = new JObject();
            jObject.Add("operate", "GeSystemTime");
            byte[] datas = Encoding.ASCII.GetBytes(jObject.ToString());
            try
            {
                byte[] tmpList = SendSocket(datas, true);
                if (tmpList == null)
                    throw new Exception("与线体电脑连接已断开!");
                TimeUpdateEvent?.Invoke(DateTime.Parse(Encoding.ASCII.GetString(tmpList)));
            }
            catch (Exception ex)
            {
                ClientGlobal.logNet.WriteException("SocketConnect", ex);
                throw;
            }
        }

        /// <summary>
        /// 获取Panel的索引：1-40
        /// </summary>
        /// <param name="unit_index">当前单体索引：1-10</param>
        /// <param name="index">当前单体中panel索引：1-4，上层1：1；上层2：2；下层1：3；下层2：4</param>
        /// <returns></returns>
        int GetPanelIndex(int unit_index, int panel_index)
        {
            int panelIndex = (unit_index - 1) * 4 + panel_index;
            if (unit_index > 10)//预留，防止后续增加工位时panel有误差，因为第10工位为redemura工位，只有两个panel，其他为4个。
                panelIndex -= 2;
            return panelIndex;
        }

        void UpdateServerStatus()
        {
            if (client != null && client.Connected)
                serverConnected = true;
            else
            {
                serverConnected = false;
                PLCConnectedEvent?.Invoke(0);
            }
            ClientGlobal.logNet.WriteInfo("SocketConnect", "服务器状态更新："+serverConnected);
            ServerConnectedEvent?.Invoke(serverConnected);
        }

        public enum OperateArea
        {
            D,
            M,
            W
        }

        public void Dispose()
        {
            if (client != null)
                client.Dispose();
            if (mqClient != null)
                mqClient.Dispose();
        }

        ~MessageClient()
        {
            this.Dispose();
        }
    }
}
