﻿using System;
using System.Text;
using CommonTool;
using Newtonsoft.Json.Linq;

namespace DemuraClient
{
    public class CIMClient
    {
        LongSocketClient client;
        string cim_ip;
        int cim_port = Properties.Settings.Default.cim_port;

        /// <summary>
        /// CIMServer状态
        /// </summary>
        public event Action<bool> CIMServerStatus;
        /// <summary>
        /// CIM 下发文本信息
        /// </summary>
        public event Action<string> TerminalTextEvent;
        /// <summary>
        /// CIM 下发 OperatorCall，需要取消作业
        /// </summary>
        public event Action<string> OperatorCallEvent;
        /// <summary>
        /// 报警
        /// </summary>
        public event Action<ushort> LDBuzzONEvent;
        /// <summary>
        /// CIM下发时间
        /// </summary>
        public event Action<string> DateTimeSetEvent;
        /// <summary>
        ///扫码上报CIM结果，参数：位置1/2，判定结果，扫码panelId
        /// </summary>
        public event Action<int,bool,string> VCRReportACKEvent;
        /// <summary>
        ///出站上报结果，参数：位置1/2，判定结果，扫码panelId
        /// </summary>
        public event Action<int, bool, string> JobJudgeACKEvent;

        public CIMClient(string ip)
        {
            cim_ip = ip;
            client = new LongSocketClient(cim_ip, cim_port, ClientGlobal.logNet);
        }

        public void StartWork()
        {
            client.DataReceived += Client_DataReceived;
            client.SocketStatus += CIMServerStatus;
            client.SocketConnect();
        }

        private void Client_DataReceived(object sender, byte[] datas)
        {
            HandleCmd(datas);
        }

        private void HandleCmd(byte[] revDatas)
        {
            string strMsg = Encoding.UTF8.GetString(revDatas);
            ClientGlobal.logNet.WriteInfo("CIMClient", strMsg);
            string[] revList = strMsg.Split(new char[] { '{', '}' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string msg in revList)
            {
                JObject jObject = JObject.Parse("{" + msg.Trim() + "}");
                if (!jObject.ContainsKey("operate"))
                    return;
                string operateTag = jObject.GetValue("operate").ToString();
                switch (operateTag)
                {
                    case "VCRReportACK":
                        bool result = (bool)jObject.GetValue("result");
                        int no = (int)jObject.GetValue("no");
                        string panelId = jObject.GetValue("panelId").ToString();
                        VCRReportACKEvent?.BeginInvoke(no,result, panelId, null, null);
                        break;
                    case "JobJudgeACK":
                        result = (bool)jObject.GetValue("result");
                        no = (int)jObject.GetValue("no");
                        panelId = jObject.GetValue("panelId").ToString();
                        JobJudgeACKEvent?.BeginInvoke(no, result, panelId, null, null);
                        break;
                    case "TerminalText":
                        string text = jObject.GetValue("text").ToString();
                        TerminalTextEvent?.BeginInvoke(text, null, null);
                        break;
                    case "OperatorCall":
                        text = jObject.GetValue("text").ToString();
                        OperatorCallEvent?.Invoke(text);
                        break;
                    case "LDBuzzON":
                        ushort code = (ushort)jObject.GetValue("code");
                        LDBuzzONEvent?.BeginInvoke(code, null, null);
                        break;
                    case "DateTimeSet":
                        string time = jObject.GetValue("time").ToString();
                        DateTimeSetEvent?.BeginInvoke(time, null, null);
                        break;
                }
            }
        }

        /// <summary>
        /// PLC状态更新
        /// </summary>
        /// <param name="status">状态</param>
        public void MachineStatusChangeReport(int status)
        {
            JObject jObject = new JObject();
            jObject.Add("operate", "PlcStatus");
            jObject.Add("status", status);
            client.SendSocket(Encoding.ASCII.GetBytes(jObject.ToString()));
        }

        /// <summary>
        /// 设备报警时
        /// </summary>
        /// <param name="alarmStatus">报警状态，1: AlarmSet；2: AlarmClear</param>
        /// <param name="alarmId">报警Id</param>
        /// <param name="alarmCode"></param>
        /// <param name="alarmLevel">报警等级，1: Light；2: Serious</param>
        /// <param name="alarmText">报警内容，为字母格式</param>
        public void AlarmStatusReport(short alarmStatus, int alarmId, byte alarmCode, short alarmLevel, string alarmText)
        {
            JObject jObject = new JObject();
            jObject.Add("operate", "AlarmReport");
            jObject.Add("alarmId", alarmId);
            jObject.Add("alarmLevel", alarmLevel);
            jObject.Add("alarmCode", alarmCode);
            jObject.Add("alarmStatus", alarmStatus);
            jObject.Add("alarmText", alarmText);
            client.SendSocket(Encoding.ASCII.GetBytes(jObject.ToString()));
            ClientGlobal.logNet.WriteInfo("CIMClient", "上报CIM报警信息完成");
        }

        /// <summary>
        /// 拔片
        /// </summary>
        /// <param name="panelId"></param>
        /// <param name="flag">等级</param>
        public void RemovedJobReport(string panelId, int flag)
        {
            JObject jObject = new JObject();
            jObject.Add("operate", "RemoveReport");
            jObject.Add("panelId", panelId);
            jObject.Add("flag", flag);
            client.SendSocket(Encoding.ASCII.GetBytes(jObject.ToString()));
            ClientGlobal.logNet.WriteInfo("CIMClient", "上报CIM拔片完成");
        }

        /// <summary>
        /// 出站时 OK，NG判级
        /// </summary>
        /// <param name="no">位置1/2</param>
        /// <param name="panelId"></param>
        /// <param name="result">1:OK/2:NG</param>
        public void JobJudgeChangeReport(int no, string panelId, int result)
        {
            JObject jObject = new JObject();
            jObject.Add("operate", "JobJudgeReport");
            jObject.Add("panelId", panelId);
            jObject.Add("no", no);
            jObject.Add("result", result);
            client.SendSocket(Encoding.ASCII.GetBytes(jObject.ToString()));
            ClientGlobal.logNet.WriteInfo("CIMClient", "上报CIM出站信息完成");
        }

        /// <summary>
        /// 扫码枪状态
        /// </summary>
        /// <param name="no">扫码枪编号1/2</param>
        /// <param name="status">扫码枪状态</param>
        public void VCRStatusChangeReport(int no, bool status)
        {
            JObject jObject = new JObject();
            jObject.Add("operate", "VCRStatus");
            jObject.Add("no", no);
            jObject.Add("status", status);
            client.SendSocket(Encoding.ASCII.GetBytes(jObject.ToString()));
        }

        /// <summary>
        /// 扫码结果
        /// </summary>
        /// <param name="no">扫码枪编号1/2</param>
        /// <param name="postion">panel 位置</param>
        /// <param name="panelId">新扫码panelId</param>
        /// <param name="oldPanelId">已有panelId</param>
        public void VCRReadResultReport(int no,int postion, string panelId, string oldPanelId)
        {
            JObject jObject = new JObject();
            jObject.Add("operate", "VCRResultReport");
            jObject.Add("panelId", panelId);
            jObject.Add("no", no);
            jObject.Add("postion", postion);
            jObject.Add("oldPanelId", oldPanelId);
            client.SendSocket(Encoding.ASCII.GetBytes(jObject.ToString()));
            ClientGlobal.logNet.WriteInfo("CIMClient", "上报CIM扫码信息完成");
        }
    }
}
