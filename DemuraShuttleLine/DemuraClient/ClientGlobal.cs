﻿using HYC.HTLog.Core;
using HYC.HTLog.Logs;

namespace DemuraClient
{
    public static class ClientGlobal
    {
        public static ILogNet logNet = new LogManagerDateTime("log\\DemuraClient", GenerateMode.ByEveryDay);
    }
}
