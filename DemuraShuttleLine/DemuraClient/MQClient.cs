﻿using HYC.HTCommunication.SocketWrapper;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DemuraClient
{
    public class MQClient : IDisposable
    {
        string ip = "";
        int port = 30000;
        Dictionary<string, Action<string, JToken>> subscribe_channels = new Dictionary<string, Action<string, JToken>>();
        Dictionary<string, HTCOMMSocketClient> publish_socket = new Dictionary<string, HTCOMMSocketClient>();
        public event Action<string> MessageNoticeEvent;
        public MQClient(string _ip, int _port = 30000)
        {
            ip = _ip;
            port = _port;
        }

        public void Subscribe(string channel, Action<string, JToken> handler)
        {
            HTCOMMSocketClient client = new HTCOMMSocketClient();
            try
            {
                if (subscribe_channels.ContainsKey(channel))
                {
                    subscribe_channels[channel] = handler;
                    return;
                }
                client.Connect(ip, port);
                client.DataReceived += Client_DataReceived;
                client.ClientDisconnected += Client_ClientDisconnected;
                JObject jObject = new JObject();
                jObject.Add("channel", channel);
                byte[] tmps = Encoding.ASCII.GetBytes(jObject.ToString());
                client.Send(tmps);
                subscribe_channels.Add(channel, handler);
                publish_socket.Add(channel, client);
                MessageNoticeEvent?.Invoke(channel + "通道建立连接");
                Console.WriteLine(DateTime.Now.ToString("HH:mm:ss:fff") + ":" + channel + "建立连接");
            }
            catch (SocketException se)
            {
                ClientGlobal.logNet.WriteInfo("MQClient", channel + "通道连接失败:" + se.Message);
                client.Dispose();
                Task.Factory.StartNew(() =>
                {
                    Thread.Sleep(3000);
                    try
                    {
                        Subscribe(channel, handler);
                    }
                    catch { }
                });
            }
            catch (Exception ex)
            { ClientGlobal.logNet.WriteException("MQClient", ex); throw; }
        }

        private void Client_DataReceived(object sender, SocketEventArgs e)
        {
            try
            {
                byte[] tmps = e.Datas;
                string strMsg = Encoding.ASCII.GetString(tmps);
                string[] revList = strMsg.Split(new char[] { '{', '}' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string msg in revList)
                {
                    JObject jObject = JObject.Parse("{" + msg.Trim() + "}");
                    string channel = jObject.GetValue("channel").ToString();
                    if (subscribe_channels.ContainsKey(channel))
                    {
                        subscribe_channels[channel].Invoke(channel, jObject.GetValue("data"));
                    }
                }
            }
            catch (Exception ex)
            {
                ClientGlobal.logNet.WriteException("MQClient", ex); throw;
            }
        }

        object publish_socket_lock = new object();
        private void Client_ClientDisconnected(object sender, SocketEventArgs e)
        {
            lock (publish_socket_lock)
            {
                foreach (KeyValuePair<string, HTCOMMSocketClient> item in publish_socket)
                {
                    try
                    {
                        if (sender.Equals(item.Value))
                        {
                            Action<string, JToken> handler = subscribe_channels[item.Key];
                            subscribe_channels.Remove(item.Key);
                            publish_socket.Remove(item.Key);
                            e.Socket.Dispose();
                            Task.Factory.StartNew(() =>
                            {
                                MessageNoticeEvent?.Invoke(item.Key + "通道断开连接");
                                Thread.Sleep(3000);
                                Subscribe(item.Key, handler);
                            });
                            return;
                        }
                    }
                    catch (Exception ex) { ClientGlobal.logNet.WriteException("MQClient", ex); }
                }
            }
        }

        public void Dispose()
        {
            foreach (HTCOMMSocketClient item in publish_socket.Values)
            {
                item.Dispose();
            }
        }
    }
}
