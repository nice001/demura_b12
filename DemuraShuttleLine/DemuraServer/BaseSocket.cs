﻿using HYC.HTCommunication.SocketWrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DemuraServer
{
    public class BaseSocket : IDisposable
    {
        private Socket socketWatch = null;

        private int sendTimeout = 1000;

        public event EventHandler<SocketEventArgs> ClientConnected;

        public event EventHandler<SocketEventArgs> ClientDisconnected;

        public event EventHandler<SocketEventArgs> DataReceived;

        public void Listen(int port, string ip = "")
        {
            try
            {
                socketWatch = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socketWatch.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, sendTimeout);
                IPEndPoint iPEndPoint = null;
                iPEndPoint = ((!string.IsNullOrEmpty(ip)) ? new IPEndPoint(IPAddress.Parse(ip), port) : new IPEndPoint(IPAddress.Any, port));
                socketWatch.Bind(iPEndPoint);
                socketWatch.Listen(200);
                StartAccept(null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Accept_Completed(object sender, SocketAsyncEventArgs e)
        {
            try
            {
                this.ClientConnected?.BeginInvoke(this, new SocketEventArgs(e.AcceptSocket, null), null, null);
                Console.WriteLine(DateTime.Now.ToString("HH:mm:ss:fff") + ":" + e.AcceptSocket.RemoteEndPoint.ToString() + "建立连接");
                if (!e.AcceptSocket.Connected)
                {
                    this.ClientDisconnected?.BeginInvoke(this, new SocketEventArgs(e.AcceptSocket, null), null, null);
                    return;
                }
                ReceiveData(sender, null, e.AcceptSocket);
                StartAccept(e);
            }
            catch (Exception ex)
            {
                try
                {
                    Console.WriteLine(ex.Message);
                    this.ClientDisconnected?.BeginInvoke(this, new SocketEventArgs(e.AcceptSocket, null), null, null);
                    e.AcceptSocket.Dispose();
                }
                catch{ }
            }
        }

        void ReceiveData(object sender, SocketAsyncEventArgs dataArgs,Socket acceptSocket)
        {
            if (dataArgs == null)
            {
                dataArgs = new SocketAsyncEventArgs();
                dataArgs.AcceptSocket = acceptSocket;
                dataArgs.Completed += HandleReceive;
                dataArgs.SetBuffer(new byte[1024 * 1024], 0, 1024 * 1024);
            }
            bool result = acceptSocket.ReceiveAsync(dataArgs);
            if (!result)
            {
                HandleReceive(sender, dataArgs);
            }
        }

        private void HandleReceive(object sender, SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.Success && e.BytesTransferred > 0)
            {
                this.DataReceived?.BeginInvoke(this, new SocketEventArgs(e.AcceptSocket, (byte[])e.Buffer.Clone()), null, null);
                Array.Clear(e.Buffer, 0, e.Buffer.Length);
                ReceiveData(sender, e, e.AcceptSocket);
            }
            else
                this.ClientDisconnected?.BeginInvoke(this, new SocketEventArgs(e.AcceptSocket, null), null, null);
        }

        private void StartAccept(SocketAsyncEventArgs args)
        {
            try
            {
                if (args == null)
                {
                    args = new SocketAsyncEventArgs();
                    args.Completed += Accept_Completed;
                }
                else
                {
                    args.AcceptSocket = null;
                }
                bool result = socketWatch.AcceptAsync(args);
                if (!result)
                {
                    Accept_Completed(this, args);
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception)
            {
            }
        }

        public void Dispose()
        {
            if (socketWatch != null)
            {
                socketWatch.Close();
                socketWatch = null;
            }
        }
    }
}
