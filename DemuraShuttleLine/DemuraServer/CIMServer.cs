﻿using DemuraServer.Entity;
using HYC.HTCommunication.SocketWrapper;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace DemuraServer
{
    public class CIMServer
    {
        /// <summary>
        /// CIM Client状态
        /// </summary>
        public event Action<string,bool> CIMClientStatus;
        /// <summary>
        /// 设备状态改变
        /// </summary>
        public event Action<int> MachineStatusChangeReport;//PLC状态
        /// <summary>
        /// 设备报警时
        /// </summary>
        public event Action<AlarmInfo> AlarmStatusReport;
        /// <summary>
        /// 拔片
        /// </summary>
        public event Action<string,int> RemovedJobReport;//panelId和等级flag
        /// <summary>
        /// 出站时 OK，NG判级，参数：位置1/2，panelId，1:OK/2:NG，上报后响应回调(操作结果，扫码panelId，位置1/2)
        /// </summary>
        public event Action<int,string,int, Action<bool, string, int>> JobJudgeChangeReport;
        /// <summary>
        /// 扫码枪状态
        /// </summary>
        public event Action<int,bool> VCRStatusChangeReport;//扫码枪编号1/2，扫码枪状态
        /// <summary>
        /// 扫码结果,参数：扫码枪编号1/2，位置1/2，新扫码panelId，已有panelId，上报后响应回调(判定结果，扫码panelId，位置1/2)
        /// </summary>
        public event Action<int,int,string,string,Action<bool,string,int>> VCRReadResultReport;

        const string plcStatus = "PlcStatus";
        const string alarmReport = "AlarmReport";
        const string removeReport = "RemoveReport";
        const string jobJudgeReport = "JobJudgeReport";
        const string vcrStatus = "VCRStatus";
        const string vcrResultReport = "VCRResultReport";
        HTCOMMSocketServer server;
        Socket socketClient;


        public CIMServer()
        {

        }
        /// <summary>
        /// 开启服务监听，当有连接进入时，则认为有CIM需要进行交互
        /// </summary>
        public void StartServer()
        {
            server = new HTCOMMSocketServer();
            server.ClientConnected += Server_ClientConnected;
            server.ClientDisconnected += Server_ClientDisconnected;
            server.DataReceived += Server_DataReceived;
            server.Listen(Properties.Settings.Default.cim_port);
        }

        private void Server_DataReceived(object sender, SocketEventArgs e)
        {
            CmdHandle(e.Datas);
        }

        private void Server_ClientDisconnected(object sender, SocketEventArgs e)
        {
            socketClient = null;
            CIMClientStatus?.BeginInvoke(e.RemoteIP, false, null, null);
            ServerGlobal.logNet.WriteWarn("CIMServer", "下游客户端" + e.RemoteIP + "断开网络连接！");
        }

        private void Server_ClientConnected(object sender, SocketEventArgs e)
        {
            socketClient = e.Socket;
            CIMClientStatus?.BeginInvoke(e.RemoteIP, true, null, null);
            ServerGlobal.logNet.WriteInfo("CIMServer", "下游客户端"+e.RemoteIP+"建立网络连接！");
        }

        private void CmdHandle(byte[] revDatas)
        {
            string strMsg = Encoding.UTF8.GetString(revDatas);
            ServerGlobal.logNet.WriteInfo("CIMServer", strMsg);
            string[] revList = strMsg.Split(new char[] { '{', '}' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string msg in revList)
            {
                JObject jObject = JObject.Parse("{" + msg.Trim() + "}");
                if (!jObject.ContainsKey("operate"))
                    return;
                string operateTag = jObject.GetValue("operate").ToString();
                switch (operateTag)
                {
                    case plcStatus:
                        int status = (int)jObject.GetValue("status");
                        MachineStatusChangeReport?.BeginInvoke(status, null, null);
                        break;
                    case alarmReport:
                        AlarmInfo alarm = new AlarmInfo();
                        alarm.AlarmId = (int)jObject.GetValue("alarmId");
                        alarm.AlarmLevel = (short)jObject.GetValue("alarmLevel");
                        alarm.AlarmCode = (byte)jObject.GetValue("alarmCode");
                        alarm.AlarmStatus = (short)jObject.GetValue("alarmStatus");
                        alarm.AlarmText = jObject.GetValue("alarmText").ToString();
                        AlarmStatusReport?.BeginInvoke(alarm, null, null);
                        break;
                    case removeReport:
                        string panelId = (string)jObject.GetValue("panelId");
                        int flag = (int)jObject.GetValue("flag");
                        RemovedJobReport?.BeginInvoke(panelId, flag, null, null);
                        break;
                    case jobJudgeReport:
                        int no = (int)jObject.GetValue("no");
                        panelId = (string)jObject.GetValue("panelId");
                        int result = (int)jObject.GetValue("result");
                        JobJudgeChangeReport?.Invoke(no, panelId, result,JobJudgeACK);
                        break;
                    case vcrStatus:
                        no = (int)jObject.GetValue("no");
                        bool tmpstatus = (bool)jObject.GetValue("status");
                        VCRStatusChangeReport?.BeginInvoke(no, tmpstatus, null, null);
                        break;
                    case vcrResultReport:
                        no = (int)jObject.GetValue("no");
                        int postion = (int)jObject.GetValue("postion");
                        panelId = (string)jObject.GetValue("panelId");
                        string oldPanelId = (string)jObject.GetValue("oldPanelId");
                        VCRReadResultReport?.Invoke(no, postion, panelId, oldPanelId, VCRReportACK);
                        break;
                }
            }
        }

        /// <summary>
        /// 扫码比对结果
        /// </summary>
        /// <param name="result">结果</param>
        /// <param name="panelId"></param>
        /// <param name="no">扫码枪编号</param>
        private void VCRReportACK(bool result, string panelId,int no)
        {
            if (socketClient == null)
                return;
            JObject jObject = new JObject();
            jObject.Add("operate", "VCRReportACK");
            jObject.Add("result", result);
            jObject.Add("panelId", panelId);
            jObject.Add("no", no);
            socketClient.Send(Encoding.ASCII.GetBytes(jObject.ToString()));
        }

        /// <summary>
        /// 出站上报结果
        /// </summary>
        /// <param name="result"></param>
        /// <param name="panelId"></param>
        /// <param name="no">出料编号</param>
        private void JobJudgeACK(bool result, string panelId, int no)
        {
            if (socketClient == null)
                return;
            JObject jObject = new JObject();
            jObject.Add("operate", "JobJudgeACK");
            jObject.Add("result", result);
            jObject.Add("panelId", panelId);
            jObject.Add("no", no);
            socketClient.Send(Encoding.ASCII.GetBytes(jObject.ToString()));
        }

        /// <summary>
        /// CIM 下发文本信息
        /// </summary>
        /// <param name="text">文本信息</param>
        /// <returns>1: OK，2: NG</returns>
        public int TerminalText(string text)
        {
            JObject jObject = new JObject();
            jObject.Add("operate", "TerminalText");
            jObject.Add("text", text);
            return SendDatas(Encoding.ASCII.GetBytes(jObject.ToString()));
        }

        /// <summary>
        /// CIM 下发 OperatorCall，需要取消作业
        /// </summary>
        /// <param name="text">文本信息</param>
        /// <returns>1: OK，2: NG</returns>
        public int OperatorCall(string text)
        {
            JObject jObject = new JObject();
            jObject.Add("operate", "OperatorCall");
            jObject.Add("text", text);
            return SendDatas(Encoding.ASCII.GetBytes(jObject.ToString()));
        }

        /// <summary>
        /// 报警
        /// </summary>
        /// <param name="code">报警编号</param>
        /// <returns>1: OK，2: NG</returns>
        public int LDBuzzON(ushort code)
        {
            JObject jObject = new JObject();
            jObject.Add("operate", "LDBuzzON");
            jObject.Add("code", code);
            return SendDatas(Encoding.ASCII.GetBytes(jObject.ToString()));
        }

        /// <summary>
        /// CIM下发时间
        /// </summary>
        /// <param name="time">时间</param>
        /// <returns>1: OK，2: NG</returns>
        public int DateTimeSet(string time)
        {
            JObject jObject = new JObject();
            jObject.Add("operate", "DateTimeSet");
            jObject.Add("time", time);
            return SendDatas(Encoding.ASCII.GetBytes(jObject.ToString()));
        }

        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="datas"></param>
        /// <returns>1: OK，2: NG</returns>
        private int SendDatas(byte[] datas)
        {
            if (socketClient == null)
                return 2;
            if (!socketClient.Connected)
                return 2;
            try
            {
                while (true)
                {
                    int len = socketClient.Send(datas);
                    if (len < datas.Length)
                    {
                        datas = datas.Skip(len).ToArray();
                    }
                    else
                        break;
                }
                return 1;
            }
            catch (Exception ex)
            {
                ServerGlobal.logNet.WriteInfo("CIMServer", "发送CIM数据失败，原因：" + ex.Message);
                return 2;
            }
        }
    }
}
