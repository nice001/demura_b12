﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using CommonTool;
using DemuraServer.Properties;
using HYC.HTCommunication.SocketWrapper;
using HYC.HTLog.Core;
using Newtonsoft.Json.Linq;

namespace DemuraServer
{
    public class MessageServer
    {
        BaseSocket server;
        string channelName="channel_unit"; //信号动作频道
        string plcStatus = "PLCStatus";//PLC状态频道
        string plcConnect = "PLCConnect"; // PLC 连接频道
        const string plcChannel = "PLCChannel";//写panel数据频道
        const string dataChannel = "SingleDataChannel";//写单个数据频道
        const string ackChannel = "ACKChannel";//写M区ACK频道
        const string cimStatusChannel = "CIMStatusChannel";//CIM状态频道
        readonly string commonChannel = "CommonChannel";//普通信息更新频道
        string panelStatusChannel = "PanelStatusChannel";//panel状态频道
        Dictionary<IntPtr, Socket> clients = new Dictionary<IntPtr, Socket>();
        /// <summary>
        /// 写PLC工位Panel数据事件
        /// </summary>
        public event Action<int,byte[]> UnitWritePanelEvent;
        /// <summary>
        /// Unit保活事件
        /// </summary>
        public event Action<int> UnitKeepLiveEvent;
        /// <summary>
        /// Unit信号ACK事件
        /// </summary>
        public event Action<int,bool> UnitACKEvent;
        /// <summary>
        /// Unit写PLC事件
        /// </summary>
        public event Action<JObject> UnitWriteDataEvent;
        /// <summary>
        /// Unit读PLC数据事件
        /// </summary>
        public event Action<Socket,int, ushort,string> UnitReadDataEvent;
        /// <summary>
        /// Unit读PLCPanel数据事件
        /// </summary>
        public event Action<Socket, int> UnitReadPLCPanelEvent;
        /// <summary>
        /// Unit读Panel数据事件
        /// </summary>
        public event Action<Socket, int> UnitReadPanelEvent;
        /// <summary>
        /// ReDemura触发事件
        /// </summary>
        public event Action<string> ReDemuraHappenEvent;
        private MQServer mqServer = new MQServer(Settings.Default.mq_port);

        public MessageServer(ILogNet logNet)
        {
            if (logNet != null)
                ServerGlobal.logNet = logNet;
        }

        public void Init()
        {
            StartServer();
        }

        /// <summary>
        /// 开启服务监听，当有连接进入时，则认为有CIM需要进行交互
        /// </summary>
        private void StartServer()
        {
            server = new BaseSocket();
            server.ClientConnected += Server_ClientConnected;
            server.ClientDisconnected += Server_ClientDisconnected;
            server.DataReceived += Server_DataReceived;
            server.Listen(Settings.Default.line_port);
            ServerGlobal.logNet.WriteWarn("SocketServer", "服务器监听已经开启！");
        }

        public void StartAction(bool[] cmds)
        {
            mqServer.Publish(channelName, TypeConvert.BoolArrayToByteArray(cmds));
        }

        public void SendSocketData(Socket client, byte[] datas)
        {
            if (!client.Connected)
                return;
            try
            {
                int length = client.Send(datas);
                if (length < datas.Length)
                {
                    byte[] tmpDatas = new byte[datas.Length - length];
                    Array.Copy(datas, length, tmpDatas, 0, tmpDatas.Length);
                    SendSocketData(client, tmpDatas);
                }
            }
            catch (Exception ex)
            {
                ServerGlobal.logNet.WriteInfo("DemuraServer", "数据发送失败，原因:" + ex.Message);
            }
        }

        private void Server_DataReceived(object sender, SocketEventArgs e)
        {
            CmdHandle(e.Socket,e.Datas);
        }

        private void Server_ClientDisconnected(object sender, SocketEventArgs e)
        {
            lock (clients)
            {
                clients.Remove(e.Socket.Handle);
            }
            ServerGlobal.logNet.WriteWarn("SocketServer", "下游客户端断开网络连接！"+e.RemoteIP);
        }

        private void Server_ClientConnected(object sender, SocketEventArgs e)
        {
            lock (clients)
            {
                clients.Add(e.Socket.Handle, e.Socket);
            }
            ServerGlobal.logNet.WriteInfo("SocketServer", "下游客户端" + e.RemoteIP + ":" + e.RemotePort + "建立网络连接！");
        }

        private void CmdHandle(Socket client, byte[] revDatas)
        {
            string strMsg = Encoding.ASCII.GetString(revDatas);
            string[] revList = strMsg.Split(new char[] { '{', '}' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string msg in revList)
            {
                JObject jObject = JObject.Parse("{" + msg.Trim() + "}");
                if (!jObject.ContainsKey("operate"))
                    return;
                string operateTag = jObject.GetValue("operate").ToString();
                if (operateTag != "KeepLive")
                {
                    Console.WriteLine(DateTime.Now.ToString("HH:mm:ss:fff") + ":收到Unit" + client.Handle + "操作" + operateTag + "，信息为" + msg);
                    ServerGlobal.logNet.WriteInfo("MessageServer", "收到Unit" + client.Handle + "操作" + operateTag + "，信息为" + msg);
                }
                switch (operateTag)
                {
                    case "GetPLCPanelData":
                        int panelIndex = (int)jObject.GetValue("panelIndex");
                        UnitReadPLCPanelEvent?.Invoke(client, panelIndex);
                        break;
                    case "GetSingleData":
                        int address = (int)jObject.GetValue("address");
                        ushort length = (ushort)jObject.GetValue("length");
                        string area = (string)jObject.GetValue("area");
                        ServerGlobal.logNet.WriteInfo("接收到GetSingleData请求" + address);
                        UnitReadDataEvent?.Invoke(client, address, length, area);
                        break;
                    case "ReDemura":
                        string panelId = (string)jObject.GetValue("panelId");
                        ReDemuraHappenEvent?.Invoke(panelId);
                        break;
                    case "GetPanelData":
                        panelIndex = (int)jObject.GetValue("panelIndex");
                        UnitReadPanelEvent?.Invoke(client, panelIndex);
                        break;
                    case "KeepLive":
                        int unitIndex = (int)jObject.GetValue("unitIndex");
                        UnitKeepLiveEvent?.Invoke(unitIndex);
                        break;
                    case ackChannel:
                        UnitACKEvent?.Invoke((int)jObject.GetValue("address"), (bool)jObject.GetValue("result"));
                        break;
                    case plcChannel:
                        UnitWritePanelEvent?.Invoke((int)jObject.GetValue("panelIndex"), (byte[])jObject.GetValue("panelDatas"));
                        break;
                    case dataChannel:
                        UnitWriteDataEvent?.Invoke(jObject);
                        break;
                    case "GeSystemTime":
                        //Console.WriteLine(DateTime.Now.ToString() + "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
                        SendSocketData(client, Encoding.ASCII.GetBytes(DateTime.Now.ToString()));
                        break;
                }
            }
        }

        /// <summary>
        /// 发送CIM状态
        /// </summary>
        /// <param name="statusType"></param>
        /// <param name="status"></param>
        public void SendStatus(string statusType,bool status)
        {
            KeyValuePair<string, bool> keyValuePair = new KeyValuePair<string, bool>(statusType, status);
            mqServer.Publish(cimStatusChannel, TypeConvert.ObjectToBytes(keyValuePair));
        }

        /// <summary>
        /// 更新PLC状态
        /// </summary>
        /// <param name="status"></param>
        public void UpdatePLCStatus(int status)
        {
            mqServer.Publish(plcStatus, status);
        }
        /// <summary>
        ///  更新PLC连接状态
        /// </summary>
        /// <param name="connect"></param>
        public void UpdatePLCConnect(bool connect)
        {
            mqServer.Publish(plcConnect, connect);
        }

        /// <summary>
        /// 更新时间
        /// </summary>
        /// <param name="strTime">时间</param>
        public void UpdateTime(string strTime)
        {
            KeyValuePair<string, object> keyValuePair =new  KeyValuePair<string, object>("Time",strTime);
            mqServer.Publish(commonChannel, TypeConvert.ObjectToBytes(keyValuePair));
        }

        public void SendPanelStatus(List<int> trueList, List<int> falseList)
        {
            int testStartOffer = 17;
            for (int i = 1; i <= 10; i++)
            {
                var trueTesult = from tmp in trueList where tmp > testStartOffer && tmp <= (testStartOffer + 4) select tmp - testStartOffer;
                var falseTesult = from tmp in falseList where tmp > testStartOffer && tmp <= (testStartOffer + 4) select tmp - testStartOffer;
                int[] trueStatus = trueTesult.ToArray();
                int[] falseStatus = falseTesult.ToArray();
                testStartOffer += 4;
                if (trueStatus.Length == 0 && falseStatus.Length == 0)
                    continue;
                mqServer.Publish(panelStatusChannel+i, TypeConvert.ObjectToBytes(new Tuple<int[], int[]>(trueStatus, falseStatus)));
            }
        }
    }
}
