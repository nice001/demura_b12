﻿using HYC.HTCommunication.SocketWrapper;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace DemuraServer
{
    public class MQServer: IDisposable
    {
        BaseSocket server;
        Dictionary<string, Dictionary<IntPtr,Socket>> publish_socket = new Dictionary<string, Dictionary<IntPtr, Socket>>();
        public MQServer(int _port=30000)
        {
            server = new BaseSocket();
            server.ClientConnected += Server_ClientConnected;
            server.ClientDisconnected += Server_ClientDisconnected;
            server.DataReceived += Server_DataReceived;
            server.Listen(_port);
        }

        object publish_socket_lock = new object();
        private void Server_DataReceived(object sender, SocketEventArgs e)
        {
            lock (publish_socket_lock)
            {
                byte[] tmps = e.Datas;
                string strMsg = Encoding.ASCII.GetString(tmps).Trim();
                JObject jObject = JObject.Parse(strMsg);
                string channel = jObject.GetValue("channel").ToString();
                if (publish_socket.ContainsKey(channel))
                {
                    publish_socket[channel].Add(e.Socket.Handle,e.Socket);
                }
                else
                {
                    publish_socket.Add(channel, new Dictionary<IntPtr, Socket> { { e.Socket.Handle,e.Socket} });
                }
                ServerGlobal.logNet.WriteWarn("MQServer", channel+"通道已创建，socket="+e.Socket.RemoteEndPoint.ToString());
                Console.WriteLine(DateTime.Now.ToString() +":"+ channel+ "通道请求完成，socket=" + e.Socket.RemoteEndPoint.ToString());
            }
            
        }

        private void Server_ClientDisconnected(object sender, SocketEventArgs e)
        {
            lock (publish_socket_lock)
            {
                foreach (KeyValuePair<string, Dictionary<IntPtr, Socket>> item in publish_socket)
                {
                    ServerGlobal.logNet.WriteInfo("MQServer", "通道" + item.Key + "的Socket断开连接");
                    item.Value.Remove(e.Socket.Handle);
                    return;
                }
            }
        }

        private void Server_ClientConnected(object sender, SocketEventArgs e)
        {
        }

        public void Publish(string channel, JToken datas)
        {
            try
            {
                lock (publish_socket_lock)
                {
                    if (!publish_socket.ContainsKey(channel))
                        return;
                    Dictionary<IntPtr, Socket> socketList = publish_socket[channel];
                    foreach (KeyValuePair<IntPtr, Socket> item in socketList)
                    {
                        Task.Factory.StartNew(() =>
                        {
                            Socket socket = item.Value;
                            JObject jObject = new JObject();
                            jObject.Add("channel", channel);
                            jObject.Add("data", datas);
                            byte[] tmps = Encoding.ASCII.GetBytes(jObject.ToString());
                            try
                            {
                                socket.Send(tmps);
                            }
                            catch(Exception ex)
                            {
                                publish_socket[channel].Remove(item.Key);
                                ServerGlobal.logNet.WriteInfo("MQServer", channel + "信息推送失败:" +ex.Message);
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Dispose()
        {
            foreach (Dictionary<IntPtr, Socket> item in publish_socket.Values)
            {
                foreach (var obj in item)
                {
                    obj.Value.Close();
                }
            }
            server.Dispose();
        }
    }
}
