﻿using HYC.HTLog.Core;
using HYC.HTLog.Logs;

namespace DemuraServer
{
    public static class ServerGlobal
    {
        public static ILogNet logNet = new LogManagerDateTime("DemuraServer", GenerateMode.ByEveryDay);
    }
}
