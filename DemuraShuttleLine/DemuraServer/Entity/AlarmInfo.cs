﻿namespace DemuraServer.Entity
{
    public  class AlarmInfo
    {
        short alarmStatus;
        int alarmId;
        byte alarmCode;
        short alarmLevel;
        string alarmText;

        /// <summary>
        /// 报警状态，1: AlarmSet；2: AlarmClear
        /// </summary>
        public short AlarmStatus { get { return alarmStatus; } set { alarmStatus = value; } }
        /// <summary>
        /// 报警Id,demura报警为15001-20000之间
        /// </summary>
        public int AlarmId { get { return alarmId; } set { alarmId = value; } }
        /// <summary>
        /// 
        /// </summary>
        public byte AlarmCode { get { return alarmCode; } set { alarmCode = value; } }
        /// <summary>
        /// 报警等级，1: Light；2: Serious
        /// </summary>
        public short AlarmLevel { get { return alarmLevel; }
            set { alarmLevel = value; }
        }
        /// <summary>
        /// 报警内容，为字母格式
        /// </summary>
        public string AlarmText { get { return alarmText; } set { alarmText = value; } }
    }
}
