﻿using HYC.HTDB;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace DemuraDB
{
    public class DemuraBaseDB
    {
        static HTDBHelper db;
        static string unit_table_name = "D2_DemuraData";
        static string data_source, db_name, db_user, db_pwd, connStr;
        static DataBaseType db_type = DataBaseType.SQLServer;
        /// <summary>
        /// 单体数据表名
        /// </summary>
        public string UnitTableName { get { return unit_table_name; } set { unit_table_name = value; } }
        public string DataSource { get { return data_source; } set { data_source = value; } }
        public string DbName { get { return db_name; } set { db_name = value; } }
        public string DbUser { get { return db_user; } set { db_user = value; } }
        public string DbPwd { get { return db_pwd; } set { db_pwd = value; } }
        public string ConnStr { get { return connStr; } set { connStr = value; } }


        static DemuraBaseDB db_object;

        private DemuraBaseDB(string _data_source, string _db_name, string _db_user, string _db_pwd)
        {
            data_source = _data_source;
            db_name = _db_name;
            db_user = _db_user;
            db_pwd = _db_pwd;
            connStr = @"Data Source=" + data_source + ";Initial Catalog=" + db_name + ";UID=" + db_user + ";PWD=" + db_pwd;
            if (db_type == DataBaseType.MySQL)
                db = HTDBHelper.CreateMySQL();
            else if (db_type == DataBaseType.SQLServer)
                db = HTDBHelper.CreateSQLServer();
            else if (db_type == DataBaseType.SQLite)
                db = HTDBHelper.CreateSQLite();
            else
                throw new NotSupportedException("暂不支持该数据库类型!");
            db.ConnectionString = connStr;
        }

        /// <summary>
        /// 执行查询，返回第一行第一列
        /// </summary>
        /// <param name="sqlStr"></param>
        /// <returns></returns>
        public object ExecuteScalar(string sqlStr)
        {
            MySqlCommand cmd;
            MySqlConnection con;
            MySqlDataAdapter msda;
            con = new MySqlConnection(connStr);
            con.Open();
            //MessageBox.Show("连接数据库成功");
            cmd = new MySqlCommand(sqlStr, con);
            cmd.CommandType = CommandType.Text;
            return cmd.ExecuteScalar();
        }
        /// <summary>
        /// 执行查询，返回数据表
        /// </summary>
        /// <param name="sqlStr"></param>
        /// <returns></returns>
        public DataTable ExecuteQuery(string sqlStr)
        {
            MySqlCommand cmd;
            MySqlConnection con;
            MySqlDataAdapter msda;
            con = new MySqlConnection(connStr);
            con.Open();
            //MessageBox.Show("连接数据库成功");
            cmd = new MySqlCommand(sqlStr, con);
            cmd.CommandType = CommandType.Text;
            DataTable dt = new DataTable();
            msda = new MySqlDataAdapter(cmd);
            msda.Fill(dt);
            con.Close();

            return dt;
        }

        /// <summary>
        /// 设置连接参数
        /// </summary>
        /// <param name="_data_source"></param>
        /// <param name="_db_name"></param>
        /// <param name="_db_user"></param>
        /// <param name="_db_pwd"></param>
        public static void SetDBConnectParam(string _data_source, string _db_name, string _db_user, string _db_pwd, string unit_data_table, DataBaseType _dbType)
        {
            data_source = _data_source;
            db_name = _db_name;
            db_user = _db_user;
            db_pwd = _db_pwd;
            unit_table_name = unit_data_table;
            db_type = _dbType;
        }

        static object initObj = new object();
        public static DemuraBaseDB GetDemuraDB()
        {
            lock (initObj)
            {
                if (data_source == null || db_name == null)
                    throw new NotImplementedException("请先设置连接参数！");
                if (db_object == null)
                    db_object = new DemuraBaseDB(data_source, db_name, db_user, db_pwd);
                return db_object;
            }
        }

        /// <summary>
        /// 插入报警
        /// </summary>
        /// <param name="alarmId">报警Code</param>
        /// <param name="unitId">PLC编号</param>
        public virtual void InsertAlarm(int alarmId, int plcId)
        {
            try
            {
                string strSql = "insert into ALARMHISTORYLIST (ALARMID,ALARMTIME,EQUIPID) values (@ALARMID,@ALARMTIME,@EQUIPID)";
                db.ExecuteNonQuery(strSql, CommandType.Text,
                    new KeyValuePair<string, object>(@"ALARMID", alarmId),
                    new KeyValuePair<string, object>(@"ALARMTIME", DateTime.Now),
                    new KeyValuePair<string, object>(@"EQUIPID", plcId));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 取消报警
        /// </summary>
        /// <param name="alarmId">报警Code</param>
        /// <param name="plcId">PLC编号</param>
        /// <param name="cancelMode">取消模式</param>
        public virtual void CancelAlarm(int alarmId, int plcId, byte cancelMode = 0)
        {
            try
            {
                string strSql = "update ALARMHISTORYLIST  set ALARMCANCLEMODE=@ALARMCANCLEMODE,ALARMCANCELTIME=GETDATE() where ALARMID=@ALARMID and EQUIPID=@EQUIPID and ALARMCANCELTIME is NULL";
                db.ExecuteNonQuery(strSql, CommandType.Text,
                    new KeyValuePair<string, object>(@"ALARMCANCLEMODE", cancelMode),
                    new KeyValuePair<string, object>(@"ALARMID", alarmId),
                    new KeyValuePair<string, object>(@"EQUIPID", plcId));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 查询报警信息
        /// </summary>
        /// <param name="filterParam">查询条件</param>
        /// <param name="plcId">PLC编号</param>
        /// <param name="startTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">显示条数</param>
        /// <returns></returns>
        public virtual DataSet GetAlarmInfoList(Dictionary<string, object> filterParam, int plcId, DateTime startTime, DateTime endTime, int pageIndex, int pageSize)
        {
            string strWhere = "";
            if (filterParam != null)
            {
                foreach (var item in filterParam)
                {
                    strWhere += " and " + item.Key + "=" + item.Value;
                }
            }
            // string strSql = "select top " + pageSize + " * from(select row_number() over(order by ALARMTIME asc) as rownumber,count(1) over() as totalsize, his.*,DATEDIFF(second,ALARMTIME,ALARMCANCELTIME) as ALIVETIME ,detail.LEVEL,detail.DESCRIPTION,detail.DESCRIPTIONCHS from ALARMHISTORYLIST his left join ALARMDETAIL detail on his.ALARMID=detail.ALARMID  where  EQUIPID=" + plcId + " and ALARMTIME >='" + startTime + "' and ALARMTIME <='" + endTime + "' " + strWhere + ") temp_row where rownumber >" + ((pageIndex - 1) * pageSize);
             string strSql = "select his.*,detail.LEVEL,detail.DESCRIPTION,detail.DESCRIPTIONCHS from ALARMHISTORYLIST his left join ALARMDETAIL detail on his.ALARMID=detail.ALARMID  where  ALARMTIME >='" + startTime + "' and ALARMTIME <='" + endTime + "' " + strWhere;

            DataSet dataSet = db.ExecuteDataSet(strSql, CommandType.Text);
            return dataSet;
        }

        /// <summary>
        /// 查询前10报警信息
        /// </summary>
        /// <param name="filterParam">查询条件</param>
        /// <param name="startTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        public virtual DataSet GetAlarmStatisticsTop10(Dictionary<string, object> filterParam, DateTime startTime, DateTime endTime)
        {
            string strWhere = "";
            if (filterParam != null)
            {
                foreach (var item in filterParam)
                {
                    strWhere += " and " + item.Key + "=" + item.Value;
                }
            }
            string strSql = "select tmp.total,tmp.ALARMID,tmp.EQUIPID,detail.LEVEL,detail.DESCRIPTION,detail.DESCRIPTIONCHS from (select  count(1) total ,his.ALARMID,his.EQUIPID from ALARMHISTORYLIST his left join ALARMDETAIL detail on his.ALARMID = detail.ALARMID where ALARMTIME >='" + startTime + "' and ALARMTIME <='" + endTime + "'" + strWhere + " group  by his.ALARMID,his.EQUIPID order by total desc) tmp left join ALARMDETAIL detail on tmp.ALARMID = detail.ALARMID";
            DataSet dataSet = db.ExecuteDataSet(strSql, CommandType.Text);
            return dataSet;
        }

        /// <summary>
        /// 查询报警描述信息
        /// </summary>
        /// <param name="alarmId">报警id</param>
        /// <returns></returns>
        public virtual DataSet GetAlarmMsg(int alarmId)
        {
            string strSql = "select * from ALARMDETAIL where ALARMID=" + alarmId;
            DataSet dataSet = db.ExecuteDataSet(strSql, CommandType.Text);
            return dataSet;
        }

        /// <summary>
        ///  插入检测数据
        /// </summary>
        /// <param name="param">插入的字段</param>
        public virtual void InsertDemuraData(Dictionary<string, object> param)
        {
            if (param.Count == 0)
                return;
            try
            {
                string strSql = "insert into " + UnitTableName + " (";
                foreach (KeyValuePair<string, object> item in param)
                {
                    strSql += item.Key + ",";
                }
                strSql = strSql.Remove(strSql.Length - 1);
                strSql += ") valuses (";
                foreach (KeyValuePair<string, object> item in param)
                {
                    strSql += item.Value + ",";
                }
                strSql = strSql.Remove(strSql.Length - 1);
                strSql += ")";
                db.ExecuteNonQuery(strSql, CommandType.Text);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 更新检测数据
        /// </summary>
        /// <param name="filterParam">更新的信息</param>
        /// <param name="panelId">panelId</param>
        /// <returns></returns>
        public virtual void UpdateDemuraDatas(Dictionary<string, object> filterParam, string panelId)
        {
            string updateInfo = "";
            if (filterParam != null)
            {
                foreach (var item in filterParam)
                {
                    updateInfo += item.Key + "=" + item.Value + ",";
                }
                updateInfo = updateInfo.Remove(updateInfo.Length - 1);
            }
            string strSql = "update " + UnitTableName + " set " + updateInfo + " where panelId=" + panelId;
            db.ExecuteDataSet(strSql, CommandType.Text);
        }

        /// <summary>
        /// 查询检测数据
        /// </summary>
        /// <param name="filterParam">查询条件</param>
        /// <param name="startTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">显示条数</param>
        /// <returns></returns>
        public virtual DataSet GetDemuraDatas(Dictionary<string, object> filterParam, DateTime startTime, DateTime endTime, int pageIndex, int pageSize)
        {
            string strWhere = "";
            if (filterParam != null)
            {
                foreach (var item in filterParam)
                {
                    strWhere += " and " + item.Key + "=" + item.Value;
                }
            }
            string strSql = "select top " + pageSize + " *,DATEDIFF(second,STARTTIME,ENDTIME) as CheckTime from(select row_number() over(order by ENDTIME asc) as rownumber,count(1) over() as totalsize, * from " + UnitTableName + " where ENDTIME >='" + startTime + "' and ENDTIME <='" + endTime + "' " + strWhere + ") temp_row where rownumber >" + ((pageIndex - 1) * pageSize);
            DataSet dataSet = db.ExecuteDataSet(strSql, CommandType.Text);
            return dataSet;
        }

        /// <summary>
        /// 获取数据统计信息
        /// </summary>
        /// <param name="ngList">NG列表</param>
        /// <param name="groupbyColumn">排序列</param>
        /// <param name="dtStart">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        public DataTable GetDataStatistics(Dictionary<string, string> ngList, string groupbyColumn, string onew, DateTime dtStart, DateTime endTime)
        {
            string des = "";
            string searchText = "";
            if (onew == "CameraID")
                des = "相机编号,";
            else if (onew == "TesterID")
                des = "治具编号,";
            else
            {
                onew = "UnitID";
                des = "单体编号,";
            }
            string strSql = "select " + (onew != null ? onew + " as " + des : "") + $"count(1) as 总数量";
            strSql += ",sum(case when ErrorCode = '0' then 1 else 0 end) 待测试"; //待测试数量
            strSql += ",sum(case when  ErrorCode= '0' then 1 else 0 end) * 1 / count(1) 待测试比例";//待测试比例          

            foreach (KeyValuePair<string, string> item in ngList)
            {
                strSql += ",sum(case when ErrorCode=" + item.Key + " then 1 else 0 end) " + item.Value;
                strSql += ",sum(case when ErrorCode=" + item.Key + " then 1 else 0 end) * 1 / count(1) " + item.Value + "比例";
                searchText += "," + item.Value + "," + item.Value + "比例";
                searchText += "," + "concat(" + item.Value + "," + "'('" + "," + item.Value + "比例*100" + "," + "'%)') as " + item.Value + "比率";
            }
            strSql += " from " + UnitTableName + " where ENDTIME>='" + dtStart + "' and ENDTIME<='" + endTime + "'" + (groupbyColumn != null ? groupbyColumn : "") + (onew != null ? " group by " + onew : "");
            strSql = "select " + (string.IsNullOrEmpty(des) ? "" : des + "") + " 总数量,待测试,待测试比例,CONCAT(待测试, '(',待测试* 1.0 /总数量*100, '%)') AS 待测试比率" + searchText + " from (" + strSql + ") tmp";//检测NG总数量

            DataTable dt = new DataTable();
            dt = ExecuteQuery(strSql.ToString());
            double[] zxc = new double[dt.Columns.Count];
            DataRow newRow = dt.NewRow();
            double[] sumInfo = new double[dt.Columns.Count];
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    for (int j = 1; j < dt.Columns.Count; j++)
                    {
                        if (dt.Rows[i][j].ToString() == "")//判断获取到的某行某列数据是否为空
                        {
                            Console.WriteLine("出错了");
                            continue;//跳出循环
                        }
                        else if (dt.Rows[i][j].ToString().Contains("(")) ///  比率列
                        {
                            sumInfo[j] += double.Parse(dt.Rows[i][j].ToString().Split('(')[0]);
                        }
                        else
                        {
                            sumInfo[j] += double.Parse(dt.Rows[i][j].ToString());
                        }
                    }
                }
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    newRow[j] = sumInfo[j];
                }
                for (int i = 3; i < sumInfo.Length; i = i + 3)
                {
                    double total = sumInfo[1];
                    newRow[i] = (sumInfo[i-1] / total).ToString("F4");
                }
                for (int k = 4; k < sumInfo.Length; k = k + 3)
                {
                    double total = sumInfo[1];
                    newRow[k] = sumInfo[k-2] + "(" + (sumInfo[k-2] / total*100).ToString("F2") + "%)";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("查询数据为空，请调整查询时间", "提示!!!!");
            }
            finally
            {
                newRow[0] = "汇总:";
                dt.Rows.Add(newRow);
            }
            return dt;
        }

        /// <summary>
        /// 根据panelId获取检测状态
        /// </summary>
        /// <param name="panelId"></param>
        /// <returns></returns>
        public virtual int GetDemuraErrorCodeByPanelId(string panelId)
        {
            string strSql = "select ErrorCode from " + UnitTableName + " where PanelID= '" + panelId + "' order by EndTime desc";
            DataSet dataSet = db.ExecuteDataSet(strSql, CommandType.Text);
            if (dataSet.Tables.Count > 0)
            {
                return int.Parse(dataSet.Tables[0].Rows[0]["ErrorCode"].ToString());
            }
            return -1;
        }
        /// <summary>
        ///  获取PG num 根据UnitID 偏移计算
        /// </summary>
        /// <param name="panelId"></param>
        /// <returns></returns>
        public virtual int GetDemuraPGIndex(string panelId)
        {
            string strSql = "select UnitID,TesterID from " + UnitTableName + " where PanelID= '" + panelId + "' order by EndTime desc";
            DataSet dataSet = db.ExecuteDataSet(strSql, CommandType.Text);
            if (dataSet.Tables.Count > 0)
            {
                int unitID = int.Parse(dataSet.Tables[0].Rows[0]["UnitID"].ToString());
                int TesterID = int.Parse(dataSet.Tables[0].Rows[0]["TesterID"].ToString());
                return (unitID - 1) * 4 + TesterID;
            }
            return -1;
        }

        /// <summary>
        /// 根据panelId获取检测数据
        /// </summary>
        /// <param name="panelId"></param>
        /// <returns></returns>
        public virtual DataSet GetDemuraDataByPanelId(string panelId)
        {
            string strSql = "select * from " + UnitTableName + " where PANELID=" + panelId;
            DataSet dataSet = db.ExecuteDataSet(strSql, CommandType.Text);
            return dataSet;
        }
    }
    public enum DataBaseType
    {
        SQLServer,
        MySQL,
        SQLite
    }

}
