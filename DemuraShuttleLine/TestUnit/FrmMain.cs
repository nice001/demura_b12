﻿using DemuraClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestUnit
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }


        CIMClient cim_client;
        private void FrmMain_Load(object sender, EventArgs e)
        {
            cim_client = new CIMClient("192.168.1.100");
            cim_client.CIMServerStatus += Cim_client_CIMServerStatus;
            cim_client.DateTimeSetEvent += Cim_client_DateTimeSetEvent;
            cim_client.JobJudgeACKEvent += Cim_client_JobJudgeACKEvent;
            cim_client.LDBuzzONEvent += Cim_client_LDBuzzONEvent;
            cim_client.OperatorCallEvent += Cim_client_OperatorCallEvent;
            cim_client.TerminalTextEvent += Cim_client_TerminalTextEvent;
            cim_client.VCRReportACKEvent += Cim_client_VCRReportACKEvent;
        }

        private void Cim_client_VCRReportACKEvent(int arg1, bool arg2, string arg3)
        {
            throw new NotImplementedException();
        }

        private void Cim_client_TerminalTextEvent(string obj)
        {
            throw new NotImplementedException();
        }

        private void Cim_client_OperatorCallEvent(string obj)
        {
            throw new NotImplementedException();
        }

        private void Cim_client_LDBuzzONEvent(ushort obj)
        {
            throw new NotImplementedException();
        }

        private void Cim_client_JobJudgeACKEvent(int arg1, bool arg2, string arg3)
        {
            throw new NotImplementedException();
        }

        private void Cim_client_DateTimeSetEvent(string obj)
        {
            throw new NotImplementedException();
        }

        private void Cim_client_CIMServerStatus(bool obj)
        {
            throw new NotImplementedException();
        }

        int i = 1;
        private void 增加单体ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 frm = new Form1();
            //frm.MdiParent = this;
            frm.ShowDialog();
            return;
            Task.Factory.StartNew(() =>
            {
                frm.Init(i);
                i++;
            });
        }
    }
}
