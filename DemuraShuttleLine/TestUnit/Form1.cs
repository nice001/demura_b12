﻿using DemuraClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestUnit
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        MessageClient client;
        private void Form1_Load(object sender, EventArgs e)
        {
           // Init(2);
        }

        public void Init(int index)
        {
            try
            {
                string time = DateTime.Now.ToString("MM-dd HH:mm:ss");
                client = new MessageClient(index, "127.0.0.1");
                client.DoWorkEvent += Client_DoWorkEvent;
                client.PLCConnectedEvent += Client_PLCConnectedEvent;
                client.TimeUpdateEvent += Client_TimeUpdateEvent;
                client.MessageNoticeEvent += Client_MessageNoticeEvent;
                client.InitClient();
                this.Invoke(new Action(() => {
                    timer1.Enabled = true;
                }));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void Client_MessageNoticeEvent(string obj)
        {
            tbLog.Invoke(new Action(() => {
                tbLog.AppendText(DateTime.Now.ToShortTimeString() + ":"+obj+ "\r\n");
            }));
        }

        private void Client_TimeUpdateEvent(DateTime obj)
        {
            tbLog.Invoke(new Action(() => {
                tbLog.AppendText(DateTime.Now.ToShortTimeString() + ":时间同步\r\n");
            }));
        }

        private void Client_PLCConnectedEvent(int obj)
        {
            if (this.IsDisposed || this.Disposing)
                return;
            if (obj > 0)
                try
                {
                    plcStatus.Invoke(new Action(() =>
                {
                    plcStatus.BackColor = Color.Yellow;
                    plcStatus.Tag = DateTime.Now;
                }));
                }
                catch { }
        }

        private void Client_DoWorkEvent(bool[] obj)
        {
            if (this.IsDisposed || this.Disposing)
                return;
            try
            {
                rtbShow.Invoke(new Action(() => { rtbShow.AppendText("收到信号："); }));
            }
            catch { }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UnitObject unitObject = new UnitObject();
            unitObject.ChanelStatus = 1;
            unitObject.PanelID = "123456789";
            unitObject.PanelStatus = 1;
            unitObject.TestCountSV = new ushort[10];
            unitObject.TesterNo = 23456;
            unitObject.TestStations = 1;
            unitObject.zUserDefine1 = 10;
            client.SendPanelData(1, 1, unitObject);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            client.SendSignal(int.Parse(tbAddress.Text),true);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            client.SendSingleData(int.Parse(textBox1.Text), textBox2.Text);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            byte[] nums = Encoding.ASCII.GetBytes(textBox4.Text);
            client.SendSingleData(int.Parse(textBox3.Text), nums);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (plcStatus.Tag == null)
                return;
            DateTime now = DateTime.Now;
            DateTime lastTime = (DateTime)plcStatus.Tag;
            if (now.Subtract(lastTime).TotalSeconds > 3)
            {
                plcStatus.Invoke(new Action(() => { plcStatus.BackColor = Color.Red; }));
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            client.Dispose();
            client = null;
        }
    }
}
