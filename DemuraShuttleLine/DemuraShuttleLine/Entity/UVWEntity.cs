﻿using System.Runtime.InteropServices;

namespace DemuraShuttleLine.Entity
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct UVWEntity
    {
        public ushort StationStatus;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
        public string PanelID;
        public ushort PanelStatus;
        public ushort TestStations;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public ushort[] TestOkCount;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public ushort[] TestNgCount;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
        public ushort[] TestCountSV;
        public ushort UVWNum;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 19)]
        public ushort[] Spare;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct InArmEntity
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
        public string PanelID;
        public ushort PanelStatus;
        public ushort DeviceStatus;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public ushort[] Spare;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct DVInfo
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 140)]
        public string pgInfo;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 140)]
        public string preInfo;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 140)]
        public string lastInfo;
    }

    /// <summary>
    /// D区下料平台对位写PLC地址
    /// </summary>
   [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct UVWMoveInfo
    {
        public ushort X;
        public ushort Y;
        public ushort R;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
        public ushort[] Spare;
    }
}
