﻿using CommonTool;
using HYC.HTPLC;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DemuraShuttleLine.Entity
{
    public class PLCStatusEntity : PLCBaseEntity
    {
        /// <summary>
        /// PLC状态事件
        /// </summary>
        public event Action<int> PLCStatusEvent;

        /// <summary>
        /// Panel状态事件
        /// </summary>
        public event Action<List<int>, List<int>> PanelStatusEvent;

        /// <summary>
        /// 时间更新
        /// </summary>
        public event Action<string> TimeUpdateEvent;

        public event Action<bool> ConnectEvent;

        public override void Start()
        {
            Task.Factory.StartNew(() =>
            {
                GetPLCStatus();
            }, TaskCreationOptions.LongRunning);
            Task.Factory.StartNew(() =>
            {
                KeepLivePLC();
            }, TaskCreationOptions.LongRunning);
            Task.Factory.StartNew(() =>
            {
                GetPanelStatus();
            }, TaskCreationOptions.LongRunning);
            Task.Factory.StartNew(() =>
            {
                TimeSynchronization();
            });
        }

        /// <summary>
        /// 更新单体状态
        /// </summary>
        /// <param name="address">单体地址</param>
        /// <param name="status">单体状态</param>
        public void UpdateUnitStatus(int unitIndex, bool status)
        {
            OperateResult result = plcObject.Write("D" + (GlobalParameters.D_HandShanking_StartAddress + (unitIndex - 1)), BitConverter.GetBytes(Convert.ToUInt16(status)));
            if (result.IsSuccess)
            { }
        }

        bool liveTag = false;
        /// <summary>
        /// 与PLC进行保活
        /// </summary>
        private void KeepLivePLC()
        {
            while (true)
            {
                liveTag = !liveTag;
                try
                {
                    OperateResult result = plcObject.Write(GlobalParameters.D_Line_HandShanking_Address, BitConverter.GetBytes(Convert.ToUInt16(liveTag)));
                    if (result.IsSuccess)
                    { }
                }
                catch (Exception)
                {
                    liveTag = !liveTag;
                    ConnectEvent?.BeginInvoke(false, null, null);
                }
                finally
                {
                    Thread.Sleep(1000);
                }
            }
        }

        bool[] last_panel_status;
        /// <summary>
        /// 获取panel状态
        /// </summary>
        private void GetPanelStatus()
        {
            while (true)
            {
                try
                {
                    OperateResult<bool[]> datas = plcObject.ReadBoolByWord(GlobalParameters.D_Panel_Status, GlobalParameters.D_Panel_Status_Length);
                    if (datas.IsSuccess)
                    {
                        bool[] status = datas.Content;
                        List<int> true_datas = new List<int>();
                        List<int> false_datas = new List<int>();
                        DataCompare.CompareBool(status, last_panel_status, ref true_datas, ref false_datas, 0);
                        last_panel_status = status;
                        if (true_datas.Count > 0 || false_datas.Count > 0)
                            PanelStatusEvent?.Invoke(true_datas, false_datas);
                    }

                }
                catch
                {
                    ConnectEvent?.BeginInvoke(false, null, null);
                }
                finally
                {
                    Thread.Sleep(1000);
                }
            }
        }

        int lastStatus;
        /// <summary>
        /// 获取PLC状态
        /// </summary>
        private void GetPLCStatus()
        {
            while (true)
            {
                try
                {
                    OperateResult<byte[]> datas = plcObject.Read(GlobalParameters.D_PLC_Status, 1);
                    if (datas.IsSuccess)
                    {
                        ushort status = BitConverter.ToUInt16(datas.Content, 0);
                        if (lastStatus != status)
                            PLCStatusEvent?.BeginInvoke(status, null, null);
                        lastStatus = status;
                        ConnectEvent?.BeginInvoke(true, null, null);
                    }
                    else
                    {
                        PLCStatusEvent?.BeginInvoke(0, null, null);
                        ConnectEvent?.BeginInvoke(false, null, null);
                        lastStatus = 0;
                    }
                }
                catch (Exception)
                {
                    PLCStatusEvent?.BeginInvoke(0, null, null);
                    ConnectEvent?.BeginInvoke(false, null, null);
                    lastStatus = 0;
                }
                finally
                {
                    Thread.Sleep(1000);
                }
            }
        }

        /// <summary>
        /// 时间同步
        /// </summary>
        public void TimeSynchronization()
        {
            while (true)
            {
                DateTime dt = DateTime.Now;
                TimeUpdateEvent?.Invoke(dt.ToString());
                TimeEntity timeEntity = new TimeEntity
                {
                    Date_Year = (ushort)dt.Year,
                    Date_Month = (ushort)dt.Month,
                    Date_Day = (ushort)dt.Day,
                    Date_Hour = (ushort)dt.Hour,
                    Date_Minute = (ushort)dt.Minute,
                    Date_Second = (ushort)dt.Second
                };
                byte[] tmpList = TypeConvert.StructToBytes(timeEntity);
                OperateResult result = plcObject.Write(GlobalParameters.D_Time_StartAddress, tmpList);
                if (result.IsSuccess)
                {
                    PLCObject.Write(GlobalParameters.D_Time_Req, BitConverter.GetBytes((ushort)1));//通知PLC时间同步
                    Program.logNet.WriteInfo("TimeSynchronization", "完成与PLC时间同步");
                    return;
                }
            }
        }
    }
}
