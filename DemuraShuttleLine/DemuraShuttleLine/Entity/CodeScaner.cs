﻿using DemuraShuttleLine.Interface;
using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DemuraShuttleLine.Entity
{
    public class CodeScaner : ICodeScaner
    {
        private string scanerIP;
        private int scanerPort;
        private bool status;
        private string scanerNo;
        private int connectTimeOut;
        private int scanTimeOut;
        private TcpClient scanerClient;
        NetworkStream networkStream;
        public CodeScaner(string _scanerNo, string _scanerIP, int _scanerPort)
        {
            scanerNo = _scanerNo;
            scanerIP = _scanerIP;
            scanerPort = _scanerPort;
            scanerClient = new TcpClient();
        }

        public event Action<object, bool> ScanerStatus;

        public bool Status { get { return this.status; } set { this.status = value; } }
        public string ScanerNo { get { return this.scanerNo; } set { this.scanerNo = value; } }
        public string ScanerIP { get { return this.scanerIP; } set { this.scanerIP = value; } }
        public int ScanerPort { get { return this.scanerPort; } set { this.scanerPort = value; } }
        public int ConnectTimeOut { get { return this.connectTimeOut; } set { this.connectTimeOut = value; } }
        public int ScanTimeOut { get { return this.scanTimeOut; } set { this.scanTimeOut = value; } }

        public bool Connect()
        {
            try
            {
                scanerClient.Connect(scanerIP, scanerPort);
                this.status = true;
                networkStream = scanerClient.GetStream();
                Task.Factory.StartNew(() => { UpdateStatus(); }, TaskCreationOptions.LongRunning);
                return true;
            }
            catch (Exception ex)
            {
                Program.logNet.WriteException("CodeScaner" + this.scanerNo, ex);
                return false;
            }
        }

        void UpdateStatus()
        {
            while (true)
            {
                this.status = scanerClient.Connected;
                ScanerStatus?.Invoke(this, this.status);
                Thread.Sleep(1000);
            }
        }

        public void DisConnect()
        {
            this.status = false;
            scanerClient.Close();
        }

        public string ScanCode(char sign)
        {
            try
            {
                if (networkStream == null)
                    return "";
                byte[] datas = BitConverter.GetBytes(sign);
                networkStream.Write(datas, 0, datas.Length);
                byte[] result = new byte[1024];
                networkStream.Read(result, 0, 1024);
                return Encoding.ASCII.GetString(result);
            }
            catch (Exception ex)
            {
                Program.logNet.WriteException("CodeScaner" + this.scanerNo, ex);
                return "";
            }
        }
    }
}
