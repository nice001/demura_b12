﻿using CommonTool;
using HYC.HTPLC;
using HYC.HTPLC.Profinet.Melsec;
using System;
using System.Runtime.InteropServices;

namespace DemuraShuttleLine.Entity
{
    public class PLCBaseEntity
    {
        private const int PLCConnectTimeOut = 5000;
        internal MelsecMcNet plcObject;
        internal string plcAddress;
        internal int port;
        internal bool isConnected;
        internal string plcType;
        /// <summary>
        /// PLC IP地址
        /// </summary>
        public string PlcAddress { get { return this.plcAddress; } set { this.plcAddress = value; } }

        /// <summary>
        /// PLC端口号
        /// </summary>
        public int Port { get { return this.port; } set { this.port = value; } }

        /// <summary>
        /// PLC索引
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// PLC类型
        /// </summary>
        public string PlcType { get { return this.plcType; } set { this.plcType = value; } }

        /// <summary>
        /// PLC对象
        /// </summary>
        public MelsecMcNet PLCObject { get { return this.plcObject; } }

        /// <summary>
        /// PLC连接状态
        /// </summary>
        public bool IsConnected { get { return isConnected; } set { isConnected = value; } }

        /// <summary>
        /// 开始工作
        /// </summary>
        public virtual void Start()
        { }

        /// <summary>
        /// 转换对象
        /// </summary>
        public virtual void Clone(PLCBaseEntity entity)
        {
            this.Index = entity.Index;
            this.plcAddress = entity.plcAddress;
            this.port = entity.port;
            this.plcType = entity.plcType;
        }

        /// <summary>
        /// 初始化PLC对象
        /// </summary>
        /// <returns></returns>
        public bool InitPLC()
        {
            plcObject = new MelsecMcNet();
            plcObject.IpAddress = plcAddress;
            plcObject.Port = port;
            plcObject.ConnectTimeOut = PLCConnectTimeOut;
            OperateResult result = plcObject.ConnectServer();
            isConnected = result.IsSuccess;
            if (!result.IsSuccess)
                throw new TimeoutException("无法连接到PLC；" + result.ToMessageShowString());
            return true;
        }

        /// <summary>
        /// 写M区信号，需捕获InvalidOperationException异常
        /// </summary>
        /// <param name="address">地址</param>
        /// <param name="writeNum">写入重试次数</param>
        public void WriteActionCmd(string address, bool value = true, int writeNum = 3)
        {
            OperateResult result = plcObject.Write(address, value);
            writeNum--;
            if (!result.IsSuccess)
            {
                if (writeNum == 0)
                {
                    throw new InvalidOperationException(address + "写命令失败：" + result.ToMessageShowString());
                }
                else
                {
                    WriteActionCmd(address, value, writeNum);
                }
            }
        }

        /// <summary>
        /// 写D区数据，需捕获InvalidOperationException异常
        /// </summary>
        /// <param name="address">地址</param>
        /// <param name="datas">数据</param>
        /// <param name="writeNum">写入重试次数</param>
        public void WriteData(string address, byte[] datas, int writeNum = 3)
        {
            OperateResult result = plcObject.Write(address, datas);
            writeNum--;
            if (!result.IsSuccess)
            {
                if (writeNum == 0)
                {
                    throw new InvalidOperationException(address + "写数据失败：" + result.ToMessageShowString());
                }
                else
                {
                    WriteData(address, datas, writeNum);
                }
            }
        }

        /// <summary>
        ///  写D区字符串，需捕获InvalidOperationException异常
        /// </summary>
        /// <param name="address"></param>
        /// <param name="datas"></param>
        /// <param name="writeNum"></param>
        public void WriteStringData(string address, string datas, int length = 0, int writeNum = 3)
        {
            OperateResult result;
            if (length > 0)
                result = plcObject.Write(address, datas, length);
            else
                result = plcObject.Write(address, datas);
            writeNum--;
            if (!result.IsSuccess)
            {
                if (writeNum == 0)
                {
                    throw new InvalidOperationException(address + "写数据失败：" + result.ToMessageShowString());
                }
                else
                {
                    WriteStringData(address, datas, writeNum);
                }
            }
        }

        /// <summary>
        /// 读取D区数据转换成结构体，需捕获InvalidOperationException异常
        /// </summary>
        /// <typeparam name="T">结构体类型</typeparam>
        /// <param name="startAddress">开始地址</param>
        /// <returns></returns>
        public T ReadDataToStuct<T>(int startAddress)
        {
            Type tmpType = typeof(T);
            if (startAddress == 0)
                throw new ArgumentException("D区开始位置不能为0");
            OperateResult<byte[]> datas = plcObject.Read("D" + startAddress, (ushort)(Marshal.SizeOf(tmpType) / 2));
            if (!datas.IsSuccess)
                throw new InvalidOperationException(datas.ToMessageShowString());
            return TypeConvert.BytesToStuct<T>(datas.Content);
        }

        /// <summary>
        /// 读取D区数据，需捕获InvalidOperationException异常
        /// </summary>
        /// <param name="startAddress">开始地址</param>
        /// <param name="length">读取长度</param>
        /// <returns></returns>
        public byte[] ReadData(string startAddress, ushort length)
        {
            OperateResult<byte[]> datas = plcObject.Read(startAddress, length);
            if (datas.IsSuccess)
                return datas.Content;
            else
                throw new InvalidOperationException(datas.ToMessageShowString());
        }

        /// <summary>
        /// 读取M区信号，需捕获InvalidOperationException异常
        /// </summary>
        /// <param name="startAddress">开始地址</param>
        /// <param name="length"></param>
        /// <returns></returns>
        public bool[] ReadCmd(int startAddress, ushort length)
        {
            OperateResult<bool[]> datas = plcObject.ReadBool("M" + startAddress, length);
            if (datas.IsSuccess)
                return datas.Content;
            else
                throw new InvalidOperationException(datas.ToMessageShowString());
        }


    }
}
