﻿using HYC.HTPLC;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DemuraShuttleLine.Entity
{
    public class PLCSignalEntity : PLCBaseEntity
    {
        private const int PLCReceiveTimeOut = 5000;

        /// <summary>
        /// 信号事件
        /// </summary>
        public event Action<bool[]> PLCSignalEvent;

        /// <summary>
        /// 开始监听PLC信息
        /// </summary>
        public override void Start()
        {
            if (plcObject == null)
                throw new NotImplementedException("PLC对象没有进行实例化！");
            plcObject.ReceiveTimeOut = PLCReceiveTimeOut;
            Task.Factory.StartNew(() =>
                {
                    while (true)
                    {
                        try
                        {
                            OperateResult<bool[]> result = plcObject.ReadBool("M" + GlobalParameters.Action_Address, GlobalParameters.Action_Length);
                            if (result.IsSuccess)
                            {
                                PLCSignalEvent?.Invoke(result.Content);
                            }
                            Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            Program.logNet.WriteException("PLCSignalEntity", ex);
                        }
                    }
                }, TaskCreationOptions.LongRunning);
        }
    }
}
