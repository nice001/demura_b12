﻿using System.Runtime.InteropServices;

namespace DemuraShuttleLine.Entity
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct TimeEntity
    {
        public ushort Date_Year;
        public ushort Date_Month;
        public ushort Date_Day;
        public ushort Date_Hour;
        public ushort Date_Minute;
        public ushort Date_Second;
    }
}
