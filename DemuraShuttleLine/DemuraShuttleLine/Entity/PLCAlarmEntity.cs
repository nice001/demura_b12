﻿using CommonTool;
using HYC.HTPLC;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DemuraShuttleLine.Entity
{
    public class PLCAlarmEntity : PLCBaseEntity
    {
        //private const int W_Weight_Alarm = 0;
        //private const ushort W_Weight_length = 110;
        //private const int W_Light_Alarm = 70;
        //private const ushort W_Light_length = 271;

        /// <summary>
        /// 重报警发生
        /// </summary>
        public event Action<List<int>> WeightAlarmHappenEvent;
        /// <summary>
        /// 重报警消除
        /// </summary>
        public event Action<List<int>> WeightAlarmOverEvent;
        /// <summary>
        /// 轻报警发生
        /// </summary>
        public event Action<List<int>> LightAlarmHappenEvent;
        /// <summary>
        /// 轻报警消除
        /// </summary>
        public event Action<List<int>> LightAlarmOverEvent;

        public override void Start()
        {
            Task.Factory.StartNew(() =>
            {
                GetAlarm();
            }, TaskCreationOptions.LongRunning);
        }

        //记录上一次信号状态，用于判断是否发生变化
        bool[] lastWeightAlarmStatus;
        //记录上一次信号状态，用于判断是否发生变化
        bool[] lastLightAlarmStatus;
        private void GetAlarm()
        {
            while (true)
            {
                OperateResult<bool[]> weight_result = plcObject.ReadBoolByWord("W" + GlobalParameters.W_Weight_Alarm, GlobalParameters.W_Weight_length);
                if (weight_result.IsSuccess)
                {
                    List<int> happenList1 = new List<int>();
                    List<int> overList1 = new List<int>();
                    DataCompare.CompareBool(weight_result.Content, lastWeightAlarmStatus, ref happenList1, ref overList1,24576);
                    if (happenList1.Count > 0)
                        WeightAlarmHappenEvent?.Invoke(happenList1);
                    if (overList1.Count > 0)
                        WeightAlarmOverEvent?.Invoke(overList1);
                    lastWeightAlarmStatus = weight_result.Content;
                }

                OperateResult<bool[]> light_result = plcObject.ReadBoolByWord("W" + GlobalParameters.W_Light_Alarm, GlobalParameters.W_Light_length);
                if (light_result.IsSuccess)
                {
                    List<int> happenList2 = new List<int>();
                    List<int> overList2 = new List<int>();
                    DataCompare.CompareBool(light_result.Content, lastLightAlarmStatus, ref happenList2, ref overList2);
                    if (happenList2.Count > 0)
                        LightAlarmHappenEvent?.Invoke(happenList2);
                    if (overList2.Count > 0)
                        LightAlarmOverEvent?.Invoke(overList2);
                    lastLightAlarmStatus = light_result.Content;
                }
                Thread.Sleep(3000);
            }
        }

        /// <summary>
        /// 报警地址转换
        /// </summary>
        /// <param name="datas">索引数组</param>
        /// <returns>报警地址</returns>
        private List<string> AlarmConvertAddress(List<int> datas)
        {
            List<string> tmpList = new List<string>();
            for (int i = 0; i < datas.Count; i++)
            {
                string str = "W" + i.ToString("x").PadLeft(3, '0');
                str.Insert(str.Length - 1, ".");
                tmpList.Add(str);
            }
            return tmpList;
        }
    }
}
