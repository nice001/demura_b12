﻿namespace DemuraShuttleLine.Entity
{
    public class GlobalParameters
    {
        #region PC-PLC
        public const int D_Unit_StartAddress = 20000;//PC-PLC Unit开始地址
        public const int D_Unit_Buffer_Length = 200;//PC-PLC Unit每组数据长度
        public const int D_HandShanking_StartAddress = 19101;//单体握手开始地址 
        public const string D_Line_HandShanking_Address = "D19100";//线体握手地址 
        public const string D_Time_StartAddress = "D30234";//时间同步地址 todo暂无
        public const string D_Out_UVW1_Address = "D29000";//下料对位平台1数据 
        public const string D_Out_UVW2_Address = "D29020";//下料对位平台2数据  
        public const string D_Out_Tray_Address = "D29050";//下料Tray盘
        #endregion

        #region PLC-PC
        public const int D_Read_Unit_StartAddress = 10000;//PLC-PC Unit开始地址
        public const int D_Read_Unit_Buffer_Length = 200;//PLC-PC Unit每组数据长度
        public const int unitPanelAddress = 11000;///PLC-PC 单体中PanelId字段开始地址
        public const int inPanelAddress = 12400;//PLC-PC 上料机中PanelId字段开始地址
        public const int lineOfferLength = 103;//PLC-PC 线体中每组数据偏移长度
        public const int outPanelAddress = 12400;//PLC-PC 下料机中PanelId字段开始地址
        #endregion

        #region M区信号
        public const int Action_Address = 100;//M区信号 开始地址
        public const int Action_Length = 2000;//M区信号长度
        #endregion

        #region CIM数据
        public const string CIM_AlarmCode = "D50059";//CIM 报警地址
        public const string CIM_Stauts = "D50058";//CIM 状态地址
        public const string RemovePanelLevel = "D50070";//拔片等级地址
        public const int scan1Status = 50063;//写扫码枪1状态地址
        public const int scan2Status = 50064;//写扫码枪2状态地址
        public const string D_PLC_Status = "D16199";//"D50056";//PLC状态地址 1：Run，2：down，6：IDLE，7：PM，Other：Unkown
        public const string D_Panel_Status = "D19140"; //"D50044";//Panel状态开始地址 一个D对应4个地址
        public const ushort D_Panel_Status_Length = 64; //12;//Panel状态长度
        public const string D_Time_Req = "D50061";//要求PLC时间同步请求
        #endregion

        #region 报警
        public const int W_Weight_Alarm = 600;//重报警开始地址
        public const ushort W_Weight_length = 32;//重报警地址长度
        public const int W_Light_Alarm = 0;//轻报警开始地址
        public const ushort W_Light_length = 251;//轻报警地址长度
        #endregion

        #region 其他
        public const int dv1Datas = 100100;//写DV1数据地址
        public const int dv2Datas = 100400;//写DV2数据地址


        public const int Arm_InA_TargetPos = 13399;
        public const int Arm_InB_TargetPos = 14199;
        public const int Arm_OutA_TargetPos = 13599;
        public const int Arm_OutB_TargetPos = 14399;

        #endregion
    }

    public enum ArmType
    {
       In_A,
       In_B,
       Out_A,
       Out_B
    }
    /// <summary>
    /// CIM状态码
    /// </summary>
    public enum CIMStatusEnum
    {
        Online = 1,
        OffLine = 2,
        OperatorCallTag = 3
    }

    /// <summary>
    /// 进料手臂请求
    /// </summary>
    public class InArmCmd_Req
    {
        /// <summary>
        /// 进料手臂JobCheckReq
        /// </summary>
        public const int JobCheckReq = 40000;
        /// <summary>
        /// 进料手臂JobReportReq
        /// </summary>
        public const int JobReportReq = 40001;
        /// <summary>
        /// 进料手臂扫码1请求
        /// </summary>
        public const int MCR1Req = 40016;
        /// <summary>
        /// 进料手臂扫码2请求
        /// </summary>
        public const int MCR2Req = 40018;
    }

    /// <summary>
    /// 进料手臂响应
    /// </summary>
    public class InArmCmd_Ack
    {
        public const int JobCheckAck = 41000;
        public const int JobReportAck = 41001;
        public const int MCR1OK = 41016;
        public const int MCR1Fail = 41017;
        public const int MCR2OK = 41018;
        public const int MCR2Fail = 41019;
    }

    /// <summary>
    /// 中转平台请求
    /// </summary>
    public class TransitPlat_Req
    {
        public const int DVReq1 = 40438;
        public const int DVReq2 = 40439;
    }

    /// <summary>
    /// 中转平台响应
    /// </summary>
    public class TransitPlat_Ack
    {
        public const int DVAck1 = 41438;
        public const int DVAck2 = 41439;
    }

    /// <summary>
    /// 对位平台A请求
    /// </summary>
    public class UVW_A_Req
    {
        public const int Org1Req = 400;//A侧对位平台1回原点
        public const int ADJ1Req = 401;//A侧对位平台1调整请求
        public const int Org2Req = 410;//A侧对位平台2回原点
        public const int ADJ2Req = 411;//A侧对位平台2调整请求
        public const int Clib1Req = 402;//A侧对位平台1标定请求
        public const int Clib2Req = 412;//A侧对位平台2标定请求
    }

    /// <summary>
    /// 对位平台A响应
    /// </summary>
    public class UVW_A_Ack
    {
        public const int Org1Comp = 2400;
        public const int Org1Fail = 2401;
        public const int ADJ1Comp = 2402;
        public const int ADJ1Fail = 2403;
        public const int Ready1 = 2404;
        public const int InZeroPos1 = 2405;
        public const int Org2Comp = 2410;
        public const int Org2Fail = 2411;
        public const int ADJ2Comp = 2412;
        public const int ADJ2Fail = 2413;
        public const int Ready2 = 2414;
        public const int InZeroPos2 = 2415;
        public const int Clib1Comp = 2406;
        public const int Clib1Fail = 2407;
        public const int Clib2Comp = 2416;
        public const int Clib2Fail = 2417;
    }

    /// <summary>
    /// 对位平台B请求
    /// </summary>
    public class UVW_B_Req
    {
        public const int Org1Req = 480;//B侧对位平台1回原点
        public const int ADJ1Req = 481;//B侧对位平台1调整请求
        public const int Org2Req = 490;//B侧对位平台2回原点
        public const int ADJ2Req = 491;//B侧对位平台2调整请求
        public const int Clib1Req = 482;//B侧对位平台1标定请求
        public const int Clib2Req = 492;//B侧对位平台2标定请求
    }

    /// <summary>
    /// 对位平台B响应
    /// </summary>
    public class UVW_B_Ack
    {
        public const int Org1Comp = 2480;
        public const int Org1Fail = 2481;
        public const int ADJ1Comp = 2482;
        public const int ADJ1Fail = 2483;
        public const int Ready1 = 2484;
        public const int InZeroPos1 = 2485;
        public const int Org2Comp = 2490;
        public const int Org2Fail = 2491;
        public const int ADJ2Comp = 2492;
        public const int ADJ2Fail = 2493;
        public const int Ready2 = 2494;
        public const int InZeroPos2 = 2495;
        public const int Clib1Comp = 2486;
        public const int Clib1Fail = 2487;
        public const int Clib2Comp = 2496;
        public const int Clib2Fail = 2497;
    }

    /// <summary>
    /// 下料对位平台请求
    /// </summary>
    public class Out_UVW_Req
    {
        public const int Snap1Req = 540;//平台1拍照
        public const int Snap1Clib = 541;// 平台1标定

        public const int Snap2Req = 550;//平台2拍照
        public const int Snap2Clib = 551;// 平台2标定
    }

    /// <summary>
    /// 下料对位平台响应
    /// </summary>
    public class Out_UVW_Ack
    {
        public const int Snap1OK = 2540;//平台1拍照OK
        public const int Snap1Fail = 2541;//平台1拍照NG
        public const int Snap1Clib1Comp = 2542; // 平台1标定OK
        public const int Snap1Clib1FFail = 2543; //平台1标定NG

        public const int Snap2OK = 2550;//平台2拍照OK
        public const int Snap2Fail = 2551;//平台2拍照NG
        public const int Snap2Clib1Comp = 2552; // 平台2标定OK
        public const int Snap2Clib1FFail = 2553; //平台2标定NG
    }

    /// <summary>
    /// 下料对位平台请求
    /// </summary>
    public class Out_Tray_Req
    {
        public const int SnapReq = 680;//Tray拍照
    }

    /// <summary>
    /// 下料对位平台响应
    /// </summary>
    public class Out_Tray_Ack
    {
        public const int SnapOK = 2680;//Tray拍照OK
        public const int SnapFail = 2681;//Tray拍照NG
    }

    /// <summary>
    /// D区下料平台对位读PLC地址
    /// </summary>
    public class D_Out_UVW_Read
    {
        public const int UVW_1 = 19000;
        public const int UVW_2 = 19020;
    }

    /// <summary>
    /// D区对位平台读PLC地址
    /// </summary>
    public class D_UVW_Address
    {
        public const int UVW_A1 = 19070;
        public const int UVW_A2 = 19071;
        public const int UVW_B1 = 19072;
        public const int UVW_B2 = 19073;
    }

    /// <summary>
    /// D区对位平台标定数据读PLC地址
    /// </summary>
    public class D_UVW_Clib_Address
    {
        public const int UVW_A = 200;
        public const int UVW_B = 201;
    }


    /// <summary>
    /// D区PLC to PC 对位地址
    /// </summary>
    public class D_InArm_PLC_Address
    {
        public const int InArm_1 = 25001;
        public const int InArm_2 = 25104;
    }

    /// <summary>
    /// D区写PLC对位信息地址
    /// </summary>
    public class D_InArm_Address
    {
        public const int InArm_1 = 35000;
        public const int InArm_2 = 35072;
    }
}
