﻿using CommonTool;
using HYC.HTPLC;
using HYC.HTPLC.Profinet.Melsec;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace DemuraShuttleLine.Entity
{
    public class PLCEntity
	{
		private const int D_Action_Address = 40000;
		private const int PLCConnectTimeOut = 5000;
		private const int PLCReceiveTimeOut = 5000;
		private const int W_Weight_Alarm = 0;
		private const ushort W_Weight_length = 106;
		private const int W_Light_Alarm = 106;
		private const ushort W_Light_length = 106;
		private MelsecMcNet plcObject;
		private string plcAddress;
		private int port;
		private bool isConnected;
		/// <summary>
		/// PLC IP地址
		/// </summary>
		public string PlcAddress { get => this.plcAddress; set => this.plcAddress = value; }

		/// <summary>
		/// PLC端口号
		/// </summary>
		public int Port { get => this.port; set => this.port = value; }

		/// <summary>
		/// PLC索引
		/// </summary>
		public int Index { get; set; }

		/// <summary>
		/// PLC对象
		/// </summary>
		public MelsecMcNet PLCObject { get => this.plcObject; }
		public bool IsConnected { get => isConnected; set => isConnected = value; }

		/// <summary>
		/// 动作执行事件
		/// </summary>
		public event Action<PLCEntity, List<int>> PLCActionEvent;
		/// <summary>
		/// 动作执行结束事件
		/// </summary>
		public event Action<PLCEntity, List<int>> PLCActionOverEvent;
		/// <summary>
		/// 重报警发生
		/// </summary>
		public event Action<PLCEntity, List<string>> WeightAlarmHappenEvent;
		/// <summary>
		/// 重报警消除
		/// </summary>
		public event Action<PLCEntity, List<string>> WeightAlarmOverEvent;
		/// <summary>
		/// 轻报警发生
		/// </summary>
		public event Action<PLCEntity, List<string>> LightAlarmHappenEvent;
		/// <summary>
		/// 轻报警消除
		/// </summary>
		public event Action<PLCEntity, List<string>> LightAlarmOverEvent;

		/// <summary>
		/// 初始化PLC对象
		/// </summary>
		/// <returns></returns>
		public bool InitPLC()
		{
			plcObject = new MelsecMcNet();
			plcObject.IpAddress = plcAddress;
			plcObject.Port = port;
			plcObject.ConnectTimeOut = PLCConnectTimeOut;
			OperateResult result = plcObject.ConnectServer();
			isConnected = result.IsSuccess;
			if (!result.IsSuccess)
				throw new TimeoutException("无法连接到PLC；" + result.ToMessageShowString());
			return true;
		}

		/// <summary>
		/// 开始监听PLC信息
		/// </summary>
		public void Listen()
		{
			if (plcObject == null)
				throw new NotImplementedException("PLC对象没有进行实例化！");
			plcObject.ReceiveTimeOut = PLCReceiveTimeOut;
			Task.Factory.StartNew(() =>
				{
					while (true)
					{
						OperateResult<bool[]> result = plcObject.ReadBool("M"+ D_Action_Address, 1000);
						if (result.IsSuccess)
						{
							Task.Factory.StartNew(() => { CompareCmdChange(result.Content); });
						}
						Thread.Sleep(19);
					}
				}, TaskCreationOptions.LongRunning);
			//todo
			/*Task.Factory.StartNew(() =>
			{
				GetAlarm();
			},TaskCreationOptions.LongRunning);*/
		}

		//记录上一次信号状态，用于判断是否发生变化
		bool[] lastActions=new bool[1000];

		private void CompareCmdChange(bool[] datas)
		{
			List<int> indexList = new List<int>();
			List<int> overList = new List<int>();
			DataCompare.CompareBool(datas, lastActions,ref indexList,ref overList, D_Action_Address);
			lastActions = datas;
			if (indexList.Count > 0)
				PLCActionEvent(this, indexList);
			if (overList.Count > 0)
				PLCActionOverEvent(this, overList);
		}

		/// <summary>
		/// 写M区信号，需捕获InvalidOperationException异常
		/// </summary>
		/// <param name="address">地址</param>
		/// <param name="writeNum">写入重试次数</param>
		public void WriteActionCmd(string address, int writeNum = 3)
		{
			OperateResult result = plcObject.Write(address, true);
			writeNum--;
			if (!result.IsSuccess)
			{
				if (writeNum == 0)
				{
					throw new InvalidOperationException(address + "写命令失败：" + result.ToMessageShowString());
				}
				else
				{
					WriteActionCmd(address, writeNum);
				}
			}
		}

		/// <summary>
		/// 写D区数据，需捕获InvalidOperationException异常
		/// </summary>
		/// <param name="address">地址</param>
		/// <param name="datas">数据</param>
		/// <param name="writeNum">写入重试次数</param>
		public void WriteData(string address, byte[] datas,int writeNum=3)
		{
			OperateResult result= plcObject.Write(address, datas);
			writeNum--;
			if (!result.IsSuccess)
			{
				if (writeNum == 0)
				{
					throw new InvalidOperationException(address + "写数据失败：" +result.ToMessageShowString());
				}
				else
				{
					WriteData(address, datas, writeNum);
				}
			}
		}

		/// <summary>
		///  写D区字符串，需捕获InvalidOperationException异常
		/// </summary>
		/// <param name="address"></param>
		/// <param name="datas"></param>
		/// <param name="writeNum"></param>
		public void WriteStringData(string address,string datas, int writeNum = 3)
		{
			OperateResult result = plcObject.Write(address, datas);
			writeNum--;
			if (!result.IsSuccess)
			{
				if (writeNum == 0)
				{
					throw new InvalidOperationException(address + "写数据失败：" + result.ToMessageShowString());
				}
				else
				{
					WriteStringData(address, datas, writeNum);
				}
			}
		}

		/// <summary>
		/// 读取D区数据转换成结构体，需捕获InvalidOperationException异常
		/// </summary>
		/// <typeparam name="T">结构体类型</typeparam>
		/// <param name="startAddress">开始地址</param>
		/// <returns></returns>
		public T ReadDataToStuct<T>(int startAddress)
		{
			Type tmpType = typeof(T);
			if (startAddress == 0)
				throw new ArgumentException("D区开始位置不能为0");
			OperateResult<byte[]> datas = plcObject.Read("D" + startAddress, (ushort)(Marshal.SizeOf(tmpType) /2));
			if (!datas.IsSuccess)
				throw new InvalidOperationException(datas.ToMessageShowString());
			return (T)TypeConvert.BytesToStuct(datas.Content);
		}

		/// <summary>
		/// 读取D区数据，需捕获InvalidOperationException异常
		/// </summary>
		/// <param name="startAddress">开始地址</param>
		/// <param name="length">读取长度</param>
		/// <returns></returns>
		public byte[] ReadData(int startAddress, ushort length)
		{
			OperateResult<byte[]> datas = plcObject.Read("D" + startAddress, length);
			if (datas.IsSuccess)
				return datas.Content;
			else
				throw new InvalidOperationException(datas.ToMessageShowString());
		}

		/// <summary>
		/// 读取M区信号，需捕获InvalidOperationException异常
		/// </summary>
		/// <param name="startAddress">开始地址</param>
		/// <param name="length"></param>
		/// <returns></returns>
		public bool[] ReadCmd(int startAddress, ushort length)
		{
			OperateResult<bool[]> datas = plcObject.ReadBool("M" + startAddress, length);
			if (datas.IsSuccess)
				return datas.Content;
			else
				throw new InvalidOperationException(datas.ToMessageShowString());
		}

		//记录上一次信号状态，用于判断是否发生变化
		bool[] lastWeightAlarmStatus;
		//记录上一次信号状态，用于判断是否发生变化
		bool[] lastLightAlarmStatus;
		private void GetAlarm()
		{
			while (true)
			{
				OperateResult<bool[]> weight_result = plcObject.ReadBool("W"+W_Weight_Alarm, W_Weight_length);
				List<int> happenList1 = new List<int>();
				List<int> overList1 = new List<int>();
				DataCompare.CompareBool(weight_result.Content, lastWeightAlarmStatus, ref happenList1, ref overList1);
				if (happenList1.Count > 0)
					WeightAlarmHappenEvent?.Invoke(this, AlarmConvertAddress(happenList1));
				if (overList1.Count > 0)
					WeightAlarmOverEvent?.Invoke(this, AlarmConvertAddress(overList1));
				lastWeightAlarmStatus = weight_result.Content;
				OperateResult<bool[]> light_result = plcObject.ReadBool("W" + W_Light_Alarm, W_Light_length);
				List<int> happenList2 = new List<int>();
				List<int> overList2 = new List<int>();
				DataCompare.CompareBool(light_result.Content, lastLightAlarmStatus, ref happenList2, ref overList2);
				if (happenList2.Count > 0)
					LightAlarmHappenEvent?.Invoke(this, AlarmConvertAddress(happenList2));
				if (overList2.Count > 0)
					LightAlarmOverEvent?.Invoke(this, AlarmConvertAddress(overList2));
				lastLightAlarmStatus = light_result.Content;
				Thread.Sleep(3000);
			}
		}

		/// <summary>
		/// 报警地址转换
		/// </summary>
		/// <param name="datas">索引数组</param>
		/// <returns>报警地址</returns>
		private List<string> AlarmConvertAddress(List<int> datas)
		{
			List<string> tmpList = new List<string>();
			for (int i = 0; i < datas.Count; i++)
			{
				string str = "W" + i.ToString("x").PadLeft(3, '0');
				str.Insert(str.Length - 1, ".");
				tmpList.Add(str);
			}
			return tmpList;
		}
	}
}
