﻿using System;
using System.ComponentModel;
using System.Configuration;

namespace DemuraShuttleLine.Entity
{
    /// <summary>
    /// 软件运行参数
    /// </summary>
    public class SoftRunParamaters
    {
        Configuration configFile;
        KeyValueConfigurationCollection settings;
        public SoftRunParamaters()
        {
            configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            settings = configFile.AppSettings.Settings;
        }
        [Category("通信参数"), DisplayName("IsAddLine"), Description("是否作为增线设备")]
        public bool IsAddLine
        {
            get { return bool.Parse(settings["is_add_line"].Value); }
            set { AddUpdateAppSettings("is_add_line", value.ToString()); }
        }
        [Category("扫码枪参数"), DisplayName("IsShieldScaner"), Description("是否屏蔽扫码枪")]
        public bool IsShieldScaner { get { return bool.Parse(settings["shield_scaner"].Value); } set { AddUpdateAppSettings("shield_scaner", value.ToString()); } }
        [Category("单体配置参数"), DisplayName("UnitTimeout"), Description("单体保活超时时间")]
        public int UnitTimeout { get { return int.Parse(settings["unit_timeout"].Value); } set { AddUpdateAppSettings("unit_timeout", value.ToString()); } }
        [Category("扫码枪参数"), DisplayName("ScanerSign"), Description("扫码枪记号")]
        public char ScanerSign { get { return char.Parse(settings["scaner_sign"].Value); } set { AddUpdateAppSettings("scaner_sign", value.ToString()); } }

        public void AddUpdateAppSettings(string key, string value)
        {
            try
            {
                if (settings[key] == null)
                {
                    settings.Add(key, value);
                }
                else
                {
                    settings[key].Value = value;
                }
                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            }
            catch (ConfigurationErrorsException)
            {
                Console.WriteLine("Error writing app settings");
            }
        }
    }
}
