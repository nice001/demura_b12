﻿using DemuraDB;
using DemuraShuttleLine.Entity;
using HYC.HTLog.Core;
using HYC.HTLog.Logs;
using System;
using System.Windows.Forms;

namespace DemuraShuttleLine
{
    static class Program
    {
        public static ILogNet logNet = new LogManagerDateTime("log\\DemuraShuttle", GenerateMode.ByEveryDay);
        public static string configPath = Application.StartupPath + "\\Config\\";
        public static SoftRunParamaters softRunParamaters = new SoftRunParamaters();
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                DataBaseType dbType= (DataBaseType)Enum.Parse(typeof(DataBaseType), Properties.Settings.Default.databaseType);
                DemuraBaseDB.SetDBConnectParam(Properties.Settings.Default.data_source, Properties.Settings.Default.db_name, Properties.Settings.Default.db_user, Properties.Settings.Default.db_pwd, Properties.Settings.Default.unit_table_name, dbType);
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new FrmMain());
            }
            catch (Exception ex)
            {
                logNet.WriteException("程序异常退出", ex);
                MessageBox.Show(ex.StackTrace);
            }
        }
    }
}
