﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace DemuraShuttleLine
{
    public static class FileOperate
    {
        public static void ExportGridToExcel(DataGridView tmpGrid)
        {
            try
            {
                if (tmpGrid.RowCount <= 0 || tmpGrid == null)
                {
                    MessageBox.Show("此数据表格无数据！", "提示");
                    return;
                }
                string filePath = "";
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    filePath = fbd.SelectedPath;
                }
                else
                {
                    return;
                }
                string fileName = filePath + "\\" + DateTime.Now.ToString("yyyyMMddHHss") + ".csv";
                if (!File.Exists(fileName))
                {
                    File.Create(fileName).Close();
                }
                FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate);
                StreamWriter sw = new StreamWriter(fs, Encoding.UTF8);
                string strLine = "";

                for (int i = 0; i < tmpGrid.ColumnCount; i++)
                {
                    strLine += tmpGrid.Columns[i].HeaderText + ',';
                }
                strLine = strLine.Remove(strLine.Length - 1);
                sw.WriteLine(strLine);
                for (int i = 0; i < tmpGrid.RowCount; i++)
                {
                    strLine = "";
                    for (int j = 0; j < tmpGrid.ColumnCount; j++)
                    {
                        strLine += tmpGrid.Rows[i].Cells[j].Value.ToString() + ',';
                    }
                    strLine = strLine.Remove(strLine.Length - 1);
                    sw.WriteLine(strLine);
                }
                sw.Close();
                fs.Close();
                sw.Dispose();
            }
            catch
            {
                throw;
            }
        }
    }
}
