﻿namespace DemuraShuttleLine
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.btnPLCTest = new HYC.WindowsControls.HTButton();
            this.pnlTop = new HYC.WindowsControls.HTPanel();
            this.btnParaSet = new HYC.WindowsControls.HTButton();
            this.btnAutoPos = new HYC.WindowsControls.HTButton();
            this.btnWork = new HYC.WindowsControls.HTButton();
            this.btnLog = new HYC.WindowsControls.HTButton();
            this.btnDataBroadcast = new HYC.WindowsControls.HTButton();
            this.btnDataSearch = new HYC.WindowsControls.HTButton();
            this.btnAlarm = new HYC.WindowsControls.HTButton();
            this.pnlHost = new HYC.WindowsControls.HTPanel();
            this.ucplcTest = new DemuraShuttleLine.Controls.UCPLCTest();
            this.htStyleManager1 = new HYC.WindowsControls.HTStyleManager(this.components);
            this.ucDevciesStatus = new DemuraShuttleLine.Controls.UCDevciesStatus();
            this.pnlTop.SuspendLayout();
            this.pnlHost.SuspendLayout();
            this.SuspendLayout();
            // 
            // FORM_PNL_TITLE
            // 
            this.FORM_PNL_TITLE.Location = new System.Drawing.Point(3, 1);
            this.FORM_PNL_TITLE.Size = new System.Drawing.Size(1767, 40);
            // 
            // btnPLCTest
            // 
            this.btnPLCTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPLCTest.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPLCTest.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnPLCTest.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnPLCTest.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnPLCTest.Font = new System.Drawing.Font("微软雅黑", 14F);
            this.btnPLCTest.Location = new System.Drawing.Point(932, 5);
            this.btnPLCTest.Margin = new System.Windows.Forms.Padding(0, 4, 4, 4);
            this.btnPLCTest.Name = "btnPLCTest";
            this.btnPLCTest.RadiusSides = ((HYC.WindowsControls.HTCornerRadiusSides)((HYC.WindowsControls.HTCornerRadiusSides.LeftTop | HYC.WindowsControls.HTCornerRadiusSides.RightTop)));
            this.btnPLCTest.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnPLCTest.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnPLCTest.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnPLCTest.Size = new System.Drawing.Size(150, 60);
            this.btnPLCTest.Style = HYC.WindowsControls.HTStyle.Gray;
            this.btnPLCTest.TabIndex = 13;
            this.btnPLCTest.Text = "手动调试";
            this.btnPLCTest.Click += new System.EventHandler(this.btnPLCTest_Click);
            // 
            // pnlTop
            // 
            this.pnlTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.pnlTop.Controls.Add(this.btnParaSet);
            this.pnlTop.Controls.Add(this.btnAutoPos);
            this.pnlTop.Controls.Add(this.btnWork);
            this.pnlTop.Controls.Add(this.btnLog);
            this.pnlTop.Controls.Add(this.btnDataBroadcast);
            this.pnlTop.Controls.Add(this.btnDataSearch);
            this.pnlTop.Controls.Add(this.btnAlarm);
            this.pnlTop.Controls.Add(this.btnPLCTest);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.pnlTop.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.pnlTop.Location = new System.Drawing.Point(291, 41);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.pnlTop.Size = new System.Drawing.Size(1479, 65);
            this.pnlTop.Style = HYC.WindowsControls.HTStyle.Gray;
            this.pnlTop.TabIndex = 4;
            this.pnlTop.Text = null;
            // 
            // btnParaSet
            // 
            this.btnParaSet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnParaSet.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnParaSet.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnParaSet.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnParaSet.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnParaSet.Font = new System.Drawing.Font("微软雅黑", 14F);
            this.btnParaSet.Location = new System.Drawing.Point(778, 5);
            this.btnParaSet.Margin = new System.Windows.Forms.Padding(0, 4, 4, 4);
            this.btnParaSet.Name = "btnParaSet";
            this.btnParaSet.RadiusSides = ((HYC.WindowsControls.HTCornerRadiusSides)((HYC.WindowsControls.HTCornerRadiusSides.LeftTop | HYC.WindowsControls.HTCornerRadiusSides.RightTop)));
            this.btnParaSet.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnParaSet.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnParaSet.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnParaSet.Size = new System.Drawing.Size(150, 60);
            this.btnParaSet.Style = HYC.WindowsControls.HTStyle.Gray;
            this.btnParaSet.TabIndex = 15;
            this.btnParaSet.Text = "参数设置";
            this.btnParaSet.Click += new System.EventHandler(this.btnParaSet_Click);
            // 
            // btnAutoPos
            // 
            this.btnAutoPos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAutoPos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAutoPos.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnAutoPos.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnAutoPos.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnAutoPos.Font = new System.Drawing.Font("微软雅黑", 14F);
            this.btnAutoPos.Location = new System.Drawing.Point(162, 5);
            this.btnAutoPos.Margin = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.btnAutoPos.Name = "btnAutoPos";
            this.btnAutoPos.RadiusSides = ((HYC.WindowsControls.HTCornerRadiusSides)((HYC.WindowsControls.HTCornerRadiusSides.LeftTop | HYC.WindowsControls.HTCornerRadiusSides.RightTop)));
            this.btnAutoPos.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnAutoPos.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnAutoPos.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnAutoPos.Size = new System.Drawing.Size(150, 60);
            this.btnAutoPos.Style = HYC.WindowsControls.HTStyle.Gray;
            this.btnAutoPos.TabIndex = 14;
            this.btnAutoPos.Text = "对位";
            this.btnAutoPos.Click += new System.EventHandler(this.btnAutoPos_Click);
            // 
            // btnWork
            // 
            this.btnWork.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnWork.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnWork.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnWork.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnWork.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnWork.Font = new System.Drawing.Font("微软雅黑", 14F);
            this.btnWork.Location = new System.Drawing.Point(8, 5);
            this.btnWork.Margin = new System.Windows.Forms.Padding(4);
            this.btnWork.Name = "btnWork";
            this.btnWork.RadiusSides = ((HYC.WindowsControls.HTCornerRadiusSides)((HYC.WindowsControls.HTCornerRadiusSides.LeftTop | HYC.WindowsControls.HTCornerRadiusSides.RightTop)));
            this.btnWork.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnWork.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnWork.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnWork.Size = new System.Drawing.Size(150, 60);
            this.btnWork.Style = HYC.WindowsControls.HTStyle.Gray;
            this.btnWork.TabIndex = 13;
            this.btnWork.Text = "流程监控";
            this.btnWork.Click += new System.EventHandler(this.btnWork_Click);
            // 
            // btnLog
            // 
            this.btnLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLog.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLog.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnLog.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnLog.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnLog.Font = new System.Drawing.Font("微软雅黑", 14F);
            this.btnLog.Location = new System.Drawing.Point(1086, 5);
            this.btnLog.Margin = new System.Windows.Forms.Padding(0, 4, 4, 4);
            this.btnLog.Name = "btnLog";
            this.btnLog.RadiusSides = ((HYC.WindowsControls.HTCornerRadiusSides)((HYC.WindowsControls.HTCornerRadiusSides.LeftTop | HYC.WindowsControls.HTCornerRadiusSides.RightTop)));
            this.btnLog.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnLog.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnLog.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnLog.Size = new System.Drawing.Size(150, 60);
            this.btnLog.Style = HYC.WindowsControls.HTStyle.Gray;
            this.btnLog.TabIndex = 13;
            this.btnLog.Text = "运行日志";
            this.btnLog.Click += new System.EventHandler(this.btnLog_Click);
            // 
            // btnDataBroadcast
            // 
            this.btnDataBroadcast.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDataBroadcast.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDataBroadcast.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnDataBroadcast.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnDataBroadcast.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnDataBroadcast.Font = new System.Drawing.Font("微软雅黑", 14F);
            this.btnDataBroadcast.Location = new System.Drawing.Point(470, 5);
            this.btnDataBroadcast.Margin = new System.Windows.Forms.Padding(0, 4, 4, 4);
            this.btnDataBroadcast.Name = "btnDataBroadcast";
            this.btnDataBroadcast.RadiusSides = ((HYC.WindowsControls.HTCornerRadiusSides)((HYC.WindowsControls.HTCornerRadiusSides.LeftTop | HYC.WindowsControls.HTCornerRadiusSides.RightTop)));
            this.btnDataBroadcast.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnDataBroadcast.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnDataBroadcast.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnDataBroadcast.Size = new System.Drawing.Size(150, 60);
            this.btnDataBroadcast.Style = HYC.WindowsControls.HTStyle.Gray;
            this.btnDataBroadcast.TabIndex = 13;
            this.btnDataBroadcast.Text = "数据看板";
            this.btnDataBroadcast.Click += new System.EventHandler(this.btnDataBroadcast_Click);
            // 
            // btnDataSearch
            // 
            this.btnDataSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDataSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDataSearch.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnDataSearch.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnDataSearch.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnDataSearch.Font = new System.Drawing.Font("微软雅黑", 14F);
            this.btnDataSearch.Location = new System.Drawing.Point(316, 5);
            this.btnDataSearch.Margin = new System.Windows.Forms.Padding(0, 4, 4, 4);
            this.btnDataSearch.Name = "btnDataSearch";
            this.btnDataSearch.RadiusSides = ((HYC.WindowsControls.HTCornerRadiusSides)((HYC.WindowsControls.HTCornerRadiusSides.LeftTop | HYC.WindowsControls.HTCornerRadiusSides.RightTop)));
            this.btnDataSearch.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnDataSearch.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnDataSearch.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnDataSearch.Size = new System.Drawing.Size(150, 60);
            this.btnDataSearch.Style = HYC.WindowsControls.HTStyle.Gray;
            this.btnDataSearch.TabIndex = 13;
            this.btnDataSearch.Text = "数据查询";
            this.btnDataSearch.Click += new System.EventHandler(this.btnDataSearch_Click);
            // 
            // btnAlarm
            // 
            this.btnAlarm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAlarm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAlarm.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnAlarm.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnAlarm.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnAlarm.Font = new System.Drawing.Font("微软雅黑", 14F);
            this.btnAlarm.Location = new System.Drawing.Point(624, 5);
            this.btnAlarm.Margin = new System.Windows.Forms.Padding(0, 4, 4, 4);
            this.btnAlarm.Name = "btnAlarm";
            this.btnAlarm.RadiusSides = ((HYC.WindowsControls.HTCornerRadiusSides)((HYC.WindowsControls.HTCornerRadiusSides.LeftTop | HYC.WindowsControls.HTCornerRadiusSides.RightTop)));
            this.btnAlarm.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnAlarm.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnAlarm.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnAlarm.Size = new System.Drawing.Size(150, 60);
            this.btnAlarm.Style = HYC.WindowsControls.HTStyle.Gray;
            this.btnAlarm.TabIndex = 13;
            this.btnAlarm.Text = "报警信息";
            this.btnAlarm.Click += new System.EventHandler(this.btnAlarm_Click);
            // 
            // pnlHost
            // 
            this.pnlHost.Controls.Add(this.ucplcTest);
            this.pnlHost.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlHost.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.pnlHost.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.pnlHost.Location = new System.Drawing.Point(291, 106);
            this.pnlHost.Name = "pnlHost";
            this.pnlHost.Padding = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.pnlHost.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.pnlHost.Size = new System.Drawing.Size(1479, 828);
            this.pnlHost.Style = HYC.WindowsControls.HTStyle.Gray;
            this.pnlHost.TabIndex = 5;
            this.pnlHost.Text = null;
            // 
            // ucplcTest
            // 
            this.ucplcTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucplcTest.Location = new System.Drawing.Point(10, 3);
            this.ucplcTest.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.ucplcTest.Name = "ucplcTest";
            this.ucplcTest.Size = new System.Drawing.Size(1459, 822);
            this.ucplcTest.TabIndex = 0;
            this.ucplcTest.Visible = false;
            // 
            // htStyleManager1
            // 
            this.htStyleManager1.Style = HYC.WindowsControls.HTStyle.White;
            // 
            // ucDevciesStatus
            // 
            this.ucDevciesStatus.Dock = System.Windows.Forms.DockStyle.Left;
            this.ucDevciesStatus.Location = new System.Drawing.Point(3, 41);
            this.ucDevciesStatus.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ucDevciesStatus.Name = "ucDevciesStatus";
            this.ucDevciesStatus.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.ucDevciesStatus.Size = new System.Drawing.Size(288, 893);
            this.ucDevciesStatus.TabIndex = 0;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1771, 980);
            this.Controls.Add(this.pnlHost);
            this.Controls.Add(this.pnlTop);
            this.Controls.Add(this.ucDevciesStatus);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 7, 4, 7);
            this.Name = "FrmMain";
            this.Padding = new System.Windows.Forms.Padding(3, 1, 1, 1);
            this.StatusBarForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(34)))), ((int)(((byte)(34)))));
            this.Text = "Demura线体";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.Controls.SetChildIndex(this.FORM_PNL_TITLE, 0);
            this.Controls.SetChildIndex(this.ucDevciesStatus, 0);
            this.Controls.SetChildIndex(this.pnlTop, 0);
            this.Controls.SetChildIndex(this.pnlHost, 0);
            this.pnlTop.ResumeLayout(false);
            this.pnlHost.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private HYC.WindowsControls.HTButton btnPLCTest;
        private Controls.UCPLCTest ucplcTest;
        private HYC.WindowsControls.HTPanel pnlTop;
        private HYC.WindowsControls.HTPanel pnlHost;
        private Controls.UCDevciesStatus ucDevciesStatus;
        private HYC.WindowsControls.HTButton btnAlarm;
        private HYC.WindowsControls.HTButton btnDataSearch;
        private HYC.WindowsControls.HTButton btnDataBroadcast;
        private HYC.WindowsControls.HTButton btnLog;
        private HYC.WindowsControls.HTButton btnWork;
        private HYC.WindowsControls.HTStyleManager htStyleManager1;
        private HYC.WindowsControls.HTButton btnParaSet;
        private HYC.WindowsControls.HTButton btnAutoPos;
    }
}