﻿using DemuraShuttleLine.Business;
using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DemuraShuttleLine.Controls
{
    public partial class UCPLCTest : UserControl
    {
        public UCPLCTest()
        {
            InitializeComponent();
        }
        PLCManage plcManage;
        public void SetPlcManage(PLCManage _plcManage)
        {
            plcManage = _plcManage;
        }

        private void btnWrite_Click(object sender, EventArgs e)
        {
            try
            {
                if (tbAddress.Text.Contains('M'))
                {
                    if (cmbType.SelectedIndex == 3)
                    {
                        plcManage.WriteCmd(tbAddress.Text, false);
                    }
                    else
                    {
                        plcManage.WriteCmd(tbAddress.Text);
                    }
                }
                else
                {
                    if (cmbType.SelectedIndex == 0)//string
                    {
                        plcManage.WriteStringDatas(tbAddress.Text, tbDatas.Text, int.Parse(tbLength.Text));
                    }
                    else if (cmbType.SelectedIndex == 1)//ushort
                    {
                        plcManage.WriteDatas(tbAddress.Text, BitConverter.GetBytes(ushort.Parse(tbDatas.Text)));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            try
            {
                //UnitEntity entity = plcManage.ReadStruct<UnitEntity>(30340);
                ushort length = ushort.Parse(tbLength.Text);
                int address = int.Parse(tbAddress.Text.Remove(0, 1));
                if (tbAddress.Text.Contains('M'))
                {
                    bool[] cmds = plcManage.ReadCmd(tbAddress.Text, length);
                    string showCmd = "";
                    for (int i = 0; i < length; i++)
                    {
                        showCmd += "M" + (address + i) + ":" + cmds[i] + "\r\n";
                    }
                    lbShow.Text = showCmd;
                }
                else
                {
                    if (cmbType.SelectedIndex == 0)//string
                    {
                        byte[] datas = plcManage.ReadDatas(tbAddress.Text, ushort.Parse(tbLength.Text));
                        lbShow.Text = Encoding.ASCII.GetString(datas);
                    }
                    else if (cmbType.SelectedIndex == 1)//ushort
                    {
                        byte[] datas = plcManage.ReadDatas(tbAddress.Text, 1);
                        lbShow.Text = BitConverter.ToUInt16(datas, 0).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnScan_Click(object sender, EventArgs e)
        {
            try
            {
                tbScanResult.Text = plcManage.ScanCode(char.Parse(tbSign.Text));
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
