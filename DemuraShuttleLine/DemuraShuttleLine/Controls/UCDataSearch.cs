﻿using DemuraDB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace DemuraShuttleLine.Controls
{
    public partial class UCDataSearch : UserControl
    {
        public UCDataSearch()
        {
            InitializeComponent();
            tpStartTime.Value = DateTime.Now.AddDays(-1);
            tpEndTime.Value = DateTime.Now;
            ucPageView.PageIndexChanged += UcPageView_PageIndexChanged;
            dgvData.AutoGenerateColumns = false;
            dgvData.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.Beige;
            dgvData.RowsDefaultCellStyle.BackColor = System.Drawing.Color.Bisque;
        }

        private void UcPageView_PageIndexChanged()
        {
            btnSearch_Click(btnSearch, null);
        }

        DemuraBaseDB dbManage = DemuraBaseDB.GetDemuraDB();
        Dictionary<string, object> pairs = new Dictionary<string, object>();

        private void btnSearch_Click(object sender, EventArgs e)
        {

            dgvData.DataSource = null;
            bool flag = false;
            DateTime startTime = tpStartTime.Value;
            DateTime endTime = tpEndTime.Value;
            TimeSpan time = endTime - startTime;
            int Pagea = ucPageView.PageSize;//每页条数
            int index = ucPageView.PageNum;//总页数
            int sd = ucPageView.PageIndex - 1;//当前页码
            if (time.Days > 30)//判断天数
            {
                MessageBox.Show("数据查询时间跨度不能大于30天！", "提示");
                return;
            }
            if (tbPanelId.Text == "" && tbTestId.Text == "" && tbUnitId.Text == "")
            {

                StringBuilder sb = new StringBuilder();
                sb.Append("select UnitID,testerID,TestResult,StartTime,EndTime,ErrorCode,ErrorMessage,CameraID,PANELID,MODULETYPE from DemuraUnitTestInfo ");
                sb.Append($"where StartTime >= '{startTime} ' and EndTime <= '{ endTime}' LIMIT " + (sd * Pagea) + "," + Pagea);
                string yudex = "select COUNT(*) from DemuraUnitTestInfo " + $"where StartTime >= '{startTime} ' and EndTime <= '{ endTime}'";
                dgvData.DataSource = dbManage.ExecuteQuery(sb.ToString());
                string cvx = "select count(*) from DemuraUnitTestInfo" + $" where StartTime >= '{startTime} ' and EndTime <= '{ endTime}'";
                ucPageView.TotalSize = int.Parse(dbManage.ExecuteScalar(cvx).ToString());

            }
            // ucPageView.PageIndex = 1;
            if (!string.IsNullOrWhiteSpace(tbPanelId.Text))
                pairs.Add("PANELID", tbPanelId.Text);
            if (!string.IsNullOrWhiteSpace(tbTestId.Text))
                pairs.Add("TESTERID", tbTestId.Text);
            if (!string.IsNullOrWhiteSpace(tbUnitId.Text.ToString()))
                pairs.Add("UNITID", tbUnitId.Text.ToString());
            if (cbOK.Checked != cbNG.Checked)
            {
                if (cbOK.Checked)
                {
                    pairs.Add("TestResult", "'PASS'");
                }
                else
                {
                    pairs.Add("TestResult", "'FAIL'");
                }
                Exeq(pairs, startTime, endTime, Pagea);
            }
            else
            {
                //  ucPageView.PageIndex = 1;
                Exeq(pairs, startTime, endTime, Pagea);
            }
            dgvData.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvData.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvData.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCells;
        }
        public void Exeq(object litm, DateTime startTime, DateTime endTime, int Pagea)
        {
            int sd = ucPageView.PageIndex - 1;//当前页码
            string strWhere = "";
            foreach (var item in pairs)
            {
                try
                {
                    strWhere += " and " + item.Key + "=" + item.Value;
                    if (strWhere != "")
                    {
                        string strsq3 = "select UnitID,testerID,TestResult,StartTime,EndTime,ErrorCode,ErrorMessage,CameraID,PANELID,MODULETYPE from DemuraUnitTestInfo " + $"where StartTime >= '{startTime} ' and EndTime <= '{ endTime}' " + strWhere + " LIMIT " + (sd * Pagea) + "," + Pagea;
                        dgvData.DataSource = dbManage.ExecuteQuery(strsq3.ToString());
                        string cvx1 = "select count(*) from DemuraUnitTestInfo" + $" where StartTime >= '{startTime} ' and EndTime <= '{ endTime}' " + strWhere;
                        ucPageView.TotalSize = int.Parse(dbManage.ExecuteScalar(cvx1).ToString());

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("未查询到数据" + ex.Message);
                }
            }
            pairs.Clear();
            //  ucPageView.PageIndex = 1;
        }
        private void SetBtnEnable(bool enable)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action(() =>
                {
                    SetBtnEnable(enable);
                }));
            }
            else
            {
                cbOK.Enabled = enable;
                cbNG.Enabled = enable;
            }
        }
        private void tpStartTime_ValueChanged(object sender, EventArgs e)
        {
            if (tpStartTime.Value > tpEndTime.Value)
                return;
        }

        private void tpEndTime_ValueChanged(object sender, EventArgs e)
        {
            if (tpEndTime.Value < tpStartTime.Value)
                return;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            FileOperate.ExportGridToExcel(dgvData);
        }
    }
}
