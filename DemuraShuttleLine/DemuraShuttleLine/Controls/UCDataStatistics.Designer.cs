﻿
namespace DemuraShuttleLine.Controls
{
    partial class UCDataStatistics
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.htPanel1 = new HYC.WindowsControls.HTPanel();
            this.btnExport = new HYC.WindowsControls.HTButton();
            this.tpEndTime = new System.Windows.Forms.DateTimePicker();
            this.tpStartTime = new System.Windows.Forms.DateTimePicker();
            this.btnSearch = new HYC.WindowsControls.HTButton();
            this.htGroupBox3 = new HYC.WindowsControls.HTGroupBox();
            this.rbfirstNight = new HYC.WindowsControls.HTRadioButton();
            this.rbfirstDay = new HYC.WindowsControls.HTRadioButton();
            this.rbDay = new HYC.WindowsControls.HTRadioButton();
            this.rbNight = new HYC.WindowsControls.HTRadioButton();
            this.rbTimeSpan = new HYC.WindowsControls.HTRadioButton();
            this.htGroupBox2 = new HYC.WindowsControls.HTGroupBox();
            this.rbShowNum = new HYC.WindowsControls.HTRadioButton();
            this.rbShowPercent = new HYC.WindowsControls.HTRadioButton();
            this.htGroupBox1 = new HYC.WindowsControls.HTGroupBox();
            this.htLabel4 = new HYC.WindowsControls.HTLabel();
            this.tbUnitid = new HYC.WindowsControls.HTComboBox();
            this.rbAll = new HYC.WindowsControls.HTRadioButton();
            this.rbCameraID = new HYC.WindowsControls.HTRadioButton();
            this.rbPGID = new HYC.WindowsControls.HTRadioButton();
            this.htLabel2 = new HYC.WindowsControls.HTLabel();
            this.htLabel1 = new HYC.WindowsControls.HTLabel();
            this.dgvData = new HYC.WindowsControls.HTDataGridViewEx();
            this.rbShowPercentAndNum = new HYC.WindowsControls.HTRadioButton();
            this.htPanel1.SuspendLayout();
            this.htGroupBox3.SuspendLayout();
            this.htGroupBox2.SuspendLayout();
            this.htGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.SuspendLayout();
            // 
            // htPanel1
            // 
            this.htPanel1.Controls.Add(this.btnExport);
            this.htPanel1.Controls.Add(this.tpEndTime);
            this.htPanel1.Controls.Add(this.tpStartTime);
            this.htPanel1.Controls.Add(this.btnSearch);
            this.htPanel1.Controls.Add(this.htGroupBox3);
            this.htPanel1.Controls.Add(this.htGroupBox2);
            this.htPanel1.Controls.Add(this.htGroupBox1);
            this.htPanel1.Controls.Add(this.htLabel2);
            this.htPanel1.Controls.Add(this.htLabel1);
            this.htPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.htPanel1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htPanel1.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htPanel1.Location = new System.Drawing.Point(0, 0);
            this.htPanel1.Name = "htPanel1";
            this.htPanel1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htPanel1.Size = new System.Drawing.Size(1351, 139);
            this.htPanel1.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htPanel1.TabIndex = 1;
            this.htPanel1.Text = null;
            // 
            // btnExport
            // 
            this.btnExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExport.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnExport.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnExport.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnExport.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.btnExport.Location = new System.Drawing.Point(1210, 45);
            this.btnExport.Name = "btnExport";
            this.btnExport.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnExport.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnExport.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnExport.Size = new System.Drawing.Size(138, 38);
            this.btnExport.Style = HYC.WindowsControls.HTStyle.Gray;
            this.btnExport.TabIndex = 10;
            this.btnExport.Text = "导出数据";
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // tpEndTime
            // 
            this.tpEndTime.CustomFormat = "yyyy/MM/dd HH:mm:ss";
            this.tpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpEndTime.Location = new System.Drawing.Point(820, 79);
            this.tpEndTime.Name = "tpEndTime";
            this.tpEndTime.Size = new System.Drawing.Size(180, 26);
            this.tpEndTime.TabIndex = 9;
            this.tpEndTime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tpEndTime_KeyPress);
            // 
            // tpStartTime
            // 
            this.tpStartTime.CustomFormat = "yyyy/MM/dd HH:mm:ss";
            this.tpStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpStartTime.Location = new System.Drawing.Point(820, 34);
            this.tpStartTime.Name = "tpStartTime";
            this.tpStartTime.Size = new System.Drawing.Size(180, 26);
            this.tpStartTime.TabIndex = 9;
            this.tpStartTime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tpStartTime_KeyPress);
            // 
            // btnSearch
            // 
            this.btnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearch.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnSearch.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnSearch.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnSearch.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.btnSearch.Location = new System.Drawing.Point(1035, 45);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnSearch.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnSearch.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnSearch.Size = new System.Drawing.Size(138, 38);
            this.btnSearch.Style = HYC.WindowsControls.HTStyle.Gray;
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Text = "查询";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // htGroupBox3
            // 
            this.htGroupBox3.Controls.Add(this.rbfirstNight);
            this.htGroupBox3.Controls.Add(this.rbfirstDay);
            this.htGroupBox3.Controls.Add(this.rbDay);
            this.htGroupBox3.Controls.Add(this.rbNight);
            this.htGroupBox3.Controls.Add(this.rbTimeSpan);
            this.htGroupBox3.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htGroupBox3.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htGroupBox3.Location = new System.Drawing.Point(498, 4);
            this.htGroupBox3.Name = "htGroupBox3";
            this.htGroupBox3.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.htGroupBox3.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htGroupBox3.Size = new System.Drawing.Size(211, 135);
            this.htGroupBox3.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htGroupBox3.TabIndex = 6;
            this.htGroupBox3.Text = "时间条件";
            // 
            // rbfirstNight
            // 
            this.rbfirstNight.BackColor = System.Drawing.Color.Transparent;
            this.rbfirstNight.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbfirstNight.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.rbfirstNight.Location = new System.Drawing.Point(24, 99);
            this.rbfirstNight.Name = "rbfirstNight";
            this.rbfirstNight.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.rbfirstNight.RadioButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.rbfirstNight.Size = new System.Drawing.Size(90, 29);
            this.rbfirstNight.Style = HYC.WindowsControls.HTStyle.Gray;
            this.rbfirstNight.TabIndex = 2;
            this.rbfirstNight.Text = "前一夜班";
            this.rbfirstNight.ValueChanged += new HYC.WindowsControls.HTRadioButton.OnValueChanged(this.rbfirstNight_ValueChanged);
            // 
            // rbfirstDay
            // 
            this.rbfirstDay.BackColor = System.Drawing.Color.Transparent;
            this.rbfirstDay.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbfirstDay.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.rbfirstDay.Location = new System.Drawing.Point(22, 64);
            this.rbfirstDay.Name = "rbfirstDay";
            this.rbfirstDay.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.rbfirstDay.RadioButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.rbfirstDay.Size = new System.Drawing.Size(92, 29);
            this.rbfirstDay.Style = HYC.WindowsControls.HTStyle.Gray;
            this.rbfirstDay.TabIndex = 1;
            this.rbfirstDay.Text = "前一白班";
            this.rbfirstDay.ValueChanged += new HYC.WindowsControls.HTRadioButton.OnValueChanged(this.rbfirstDay_ValueChanged);
            // 
            // rbDay
            // 
            this.rbDay.BackColor = System.Drawing.Color.Transparent;
            this.rbDay.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbDay.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.rbDay.Location = new System.Drawing.Point(118, 64);
            this.rbDay.Name = "rbDay";
            this.rbDay.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.rbDay.RadioButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.rbDay.Size = new System.Drawing.Size(70, 29);
            this.rbDay.Style = HYC.WindowsControls.HTStyle.Gray;
            this.rbDay.TabIndex = 0;
            this.rbDay.Text = "白班";
            this.rbDay.ValueChanged += new HYC.WindowsControls.HTRadioButton.OnValueChanged(this.rbDay_ValueChanged);
            // 
            // rbNight
            // 
            this.rbNight.BackColor = System.Drawing.Color.Transparent;
            this.rbNight.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbNight.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.rbNight.Location = new System.Drawing.Point(118, 99);
            this.rbNight.Name = "rbNight";
            this.rbNight.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.rbNight.RadioButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.rbNight.Size = new System.Drawing.Size(70, 29);
            this.rbNight.Style = HYC.WindowsControls.HTStyle.Gray;
            this.rbNight.TabIndex = 0;
            this.rbNight.Text = "夜班";
            this.rbNight.ValueChanged += new HYC.WindowsControls.HTRadioButton.OnValueChanged(this.rbNight_ValueChanged);
            // 
            // rbTimeSpan
            // 
            this.rbTimeSpan.BackColor = System.Drawing.Color.Transparent;
            this.rbTimeSpan.Checked = true;
            this.rbTimeSpan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbTimeSpan.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.rbTimeSpan.Location = new System.Drawing.Point(22, 29);
            this.rbTimeSpan.Name = "rbTimeSpan";
            this.rbTimeSpan.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.rbTimeSpan.RadioButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.rbTimeSpan.Size = new System.Drawing.Size(115, 29);
            this.rbTimeSpan.Style = HYC.WindowsControls.HTStyle.Gray;
            this.rbTimeSpan.TabIndex = 0;
            this.rbTimeSpan.Text = "时间区间";
            this.rbTimeSpan.ValueChanged += new HYC.WindowsControls.HTRadioButton.OnValueChanged(this.rbTimeSpan_ValueChanged);
            // 
            // htGroupBox2
            // 
            this.htGroupBox2.Controls.Add(this.rbShowPercentAndNum);
            this.htGroupBox2.Controls.Add(this.rbShowNum);
            this.htGroupBox2.Controls.Add(this.rbShowPercent);
            this.htGroupBox2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htGroupBox2.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htGroupBox2.Location = new System.Drawing.Point(294, 4);
            this.htGroupBox2.Name = "htGroupBox2";
            this.htGroupBox2.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.htGroupBox2.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htGroupBox2.Size = new System.Drawing.Size(171, 135);
            this.htGroupBox2.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htGroupBox2.TabIndex = 6;
            this.htGroupBox2.Text = "显示方式";
            // 
            // rbShowNum
            // 
            this.rbShowNum.BackColor = System.Drawing.Color.Transparent;
            this.rbShowNum.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbShowNum.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.rbShowNum.Location = new System.Drawing.Point(25, 67);
            this.rbShowNum.Name = "rbShowNum";
            this.rbShowNum.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.rbShowNum.RadioButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.rbShowNum.Size = new System.Drawing.Size(128, 29);
            this.rbShowNum.Style = HYC.WindowsControls.HTStyle.Gray;
            this.rbShowNum.TabIndex = 0;
            this.rbShowNum.Text = "按数量显示";
            this.rbShowNum.ValueChanged += new HYC.WindowsControls.HTRadioButton.OnValueChanged(this.rbShowNum_ValueChanged);
            // 
            // rbShowPercent
            // 
            this.rbShowPercent.BackColor = System.Drawing.Color.Transparent;
            this.rbShowPercent.Checked = true;
            this.rbShowPercent.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbShowPercent.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.rbShowPercent.Location = new System.Drawing.Point(25, 35);
            this.rbShowPercent.Name = "rbShowPercent";
            this.rbShowPercent.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.rbShowPercent.RadioButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.rbShowPercent.Size = new System.Drawing.Size(128, 29);
            this.rbShowPercent.Style = HYC.WindowsControls.HTStyle.Gray;
            this.rbShowPercent.TabIndex = 0;
            this.rbShowPercent.Text = "按比例显示";
            this.rbShowPercent.ValueChanged += new HYC.WindowsControls.HTRadioButton.OnValueChanged(this.rbShowPercent_ValueChanged);
            // 
            // htGroupBox1
            // 
            this.htGroupBox1.Controls.Add(this.htLabel4);
            this.htGroupBox1.Controls.Add(this.tbUnitid);
            this.htGroupBox1.Controls.Add(this.rbAll);
            this.htGroupBox1.Controls.Add(this.rbCameraID);
            this.htGroupBox1.Controls.Add(this.rbPGID);
            this.htGroupBox1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htGroupBox1.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htGroupBox1.Location = new System.Drawing.Point(12, 1);
            this.htGroupBox1.Name = "htGroupBox1";
            this.htGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.htGroupBox1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htGroupBox1.Size = new System.Drawing.Size(255, 138);
            this.htGroupBox1.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htGroupBox1.TabIndex = 6;
            this.htGroupBox1.Text = "统计维度";
            // 
            // htLabel4
            // 
            this.htLabel4.AutoSize = true;
            this.htLabel4.BackColor = System.Drawing.Color.Transparent;
            this.htLabel4.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htLabel4.Location = new System.Drawing.Point(8, 67);
            this.htLabel4.Name = "htLabel4";
            this.htLabel4.Size = new System.Drawing.Size(107, 20);
            this.htLabel4.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htLabel4.TabIndex = 7;
            this.htLabel4.Text = "选择单体序号：";
            this.htLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbUnitid
            // 
            this.tbUnitid.BackColor = System.Drawing.SystemColors.Control;
            this.tbUnitid.FillColor = System.Drawing.Color.White;
            this.tbUnitid.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.tbUnitid.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13"});
            this.tbUnitid.Location = new System.Drawing.Point(121, 67);
            this.tbUnitid.MinimumSize = new System.Drawing.Size(63, 0);
            this.tbUnitid.Name = "tbUnitid";
            this.tbUnitid.Padding = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.tbUnitid.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.tbUnitid.Size = new System.Drawing.Size(97, 27);
            this.tbUnitid.Style = HYC.WindowsControls.HTStyle.Gray;
            this.tbUnitid.TabIndex = 17;
            this.tbUnitid.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rbAll
            // 
            this.rbAll.BackColor = System.Drawing.Color.Transparent;
            this.rbAll.Checked = true;
            this.rbAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbAll.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.rbAll.Location = new System.Drawing.Point(12, 29);
            this.rbAll.Name = "rbAll";
            this.rbAll.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.rbAll.RadioButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.rbAll.Size = new System.Drawing.Size(108, 29);
            this.rbAll.Style = HYC.WindowsControls.HTStyle.Gray;
            this.rbAll.TabIndex = 0;
            this.rbAll.Text = "统计所有";
            this.rbAll.ValueChanged += new HYC.WindowsControls.HTRadioButton.OnValueChanged(this.rbAll_ValueChanged);
            // 
            // rbCameraID
            // 
            this.rbCameraID.BackColor = System.Drawing.Color.Transparent;
            this.rbCameraID.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbCameraID.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.rbCameraID.Location = new System.Drawing.Point(121, 102);
            this.rbCameraID.Name = "rbCameraID";
            this.rbCameraID.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.rbCameraID.RadioButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.rbCameraID.Size = new System.Drawing.Size(123, 29);
            this.rbCameraID.Style = HYC.WindowsControls.HTStyle.Gray;
            this.rbCameraID.TabIndex = 0;
            this.rbCameraID.Text = "按相机统计";
            this.rbCameraID.ValueChanged += new HYC.WindowsControls.HTRadioButton.OnValueChanged(this.rbUnitID_ValueChanged);
            // 
            // rbPGID
            // 
            this.rbPGID.BackColor = System.Drawing.Color.Transparent;
            this.rbPGID.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbPGID.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.rbPGID.Location = new System.Drawing.Point(12, 102);
            this.rbPGID.Name = "rbPGID";
            this.rbPGID.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.rbPGID.RadioButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.rbPGID.Size = new System.Drawing.Size(108, 29);
            this.rbPGID.Style = HYC.WindowsControls.HTStyle.Gray;
            this.rbPGID.TabIndex = 0;
            this.rbPGID.Text = "按治具统计";
            this.rbPGID.ValueChanged += new HYC.WindowsControls.HTRadioButton.OnValueChanged(this.rbPGID_ValueChanged);
            // 
            // htLabel2
            // 
            this.htLabel2.AutoSize = true;
            this.htLabel2.BackColor = System.Drawing.Color.Transparent;
            this.htLabel2.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htLabel2.Location = new System.Drawing.Point(735, 84);
            this.htLabel2.Name = "htLabel2";
            this.htLabel2.Size = new System.Drawing.Size(79, 20);
            this.htLabel2.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htLabel2.TabIndex = 4;
            this.htLabel2.Text = "结束时间：";
            this.htLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // htLabel1
            // 
            this.htLabel1.AutoSize = true;
            this.htLabel1.BackColor = System.Drawing.Color.Transparent;
            this.htLabel1.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htLabel1.Location = new System.Drawing.Point(735, 39);
            this.htLabel1.Name = "htLabel1";
            this.htLabel1.Size = new System.Drawing.Size(79, 20);
            this.htLabel1.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htLabel1.TabIndex = 5;
            this.htLabel1.Text = "开始时间：";
            this.htLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToAddRows = false;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvData.Location = new System.Drawing.Point(0, 139);
            this.dgvData.Name = "dgvData";
            this.dgvData.RowTemplate.Height = 23;
            this.dgvData.Size = new System.Drawing.Size(1351, 603);
            this.dgvData.TabIndex = 1;
            this.dgvData.DataSourceChanged += new System.EventHandler(this.dgvData_DataSourceChanged);
            this.dgvData.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvData_CellFormatting);
            // 
            // rbShowPercentAndNum
            // 
            this.rbShowPercentAndNum.BackColor = System.Drawing.Color.Transparent;
            this.rbShowPercentAndNum.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbShowPercentAndNum.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.rbShowPercentAndNum.Location = new System.Drawing.Point(25, 99);
            this.rbShowPercentAndNum.Name = "rbShowPercentAndNum";
            this.rbShowPercentAndNum.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.rbShowPercentAndNum.RadioButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.rbShowPercentAndNum.Size = new System.Drawing.Size(128, 29);
            this.rbShowPercentAndNum.Style = HYC.WindowsControls.HTStyle.Gray;
            this.rbShowPercentAndNum.TabIndex = 1;
            this.rbShowPercentAndNum.Text = "数量/比例显示";
            this.rbShowPercentAndNum.ValueChanged += new HYC.WindowsControls.HTRadioButton.OnValueChanged(this.rbShowPercentAndNum_ValueChanged);
            // 
            // UCDataStatistics
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.dgvData);
            this.Controls.Add(this.htPanel1);
            this.Name = "UCDataStatistics";
            this.Size = new System.Drawing.Size(1351, 742);
            this.htPanel1.ResumeLayout(false);
            this.htPanel1.PerformLayout();
            this.htGroupBox3.ResumeLayout(false);
            this.htGroupBox2.ResumeLayout(false);
            this.htGroupBox1.ResumeLayout(false);
            this.htGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private HYC.WindowsControls.HTPanel htPanel1;
        private HYC.WindowsControls.HTLabel htLabel2;
        private HYC.WindowsControls.HTLabel htLabel1;
        private HYC.WindowsControls.HTGroupBox htGroupBox1;
        private HYC.WindowsControls.HTRadioButton rbPGID;
        private HYC.WindowsControls.HTRadioButton rbCameraID;
        private HYC.WindowsControls.HTButton btnSearch;
        private System.Windows.Forms.DateTimePicker tpStartTime;
        private System.Windows.Forms.DateTimePicker tpEndTime;
        private HYC.WindowsControls.HTGroupBox htGroupBox2;
        private HYC.WindowsControls.HTRadioButton rbShowNum;
        private HYC.WindowsControls.HTRadioButton rbShowPercent;
        private HYC.WindowsControls.HTGroupBox htGroupBox3;
        private HYC.WindowsControls.HTRadioButton rbDay;
        private HYC.WindowsControls.HTRadioButton rbNight;
        private HYC.WindowsControls.HTRadioButton rbTimeSpan;
        private HYC.WindowsControls.HTRadioButton rbAll;
        private HYC.WindowsControls.HTButton btnExport;
        private HYC.WindowsControls.HTDataGridViewEx dgvData;
        private HYC.WindowsControls.HTComboBox tbUnitid;
        private HYC.WindowsControls.HTLabel htLabel4;
        private HYC.WindowsControls.HTRadioButton rbfirstNight;
        private HYC.WindowsControls.HTRadioButton rbfirstDay;
        private HYC.WindowsControls.HTRadioButton rbShowPercentAndNum;
    }
}
