﻿
namespace DemuraShuttleLine.Controls
{
    partial class UCPageView
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolbtnFirst = new System.Windows.Forms.ToolStripButton();
            this.toolbtnFront = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tooltbPage = new System.Windows.Forms.ToolStripTextBox();
            this.toollbTotalPage = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolbtnAfter = new System.Windows.Forms.ToolStripButton();
            this.toolbtnLast = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toollbTotal = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel5 = new System.Windows.Forms.ToolStripLabel();
            this.toolcmbPerNum = new System.Windows.Forms.ToolStripComboBox();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolbtnFirst,
            this.toolbtnFront,
            this.toolStripSeparator1,
            this.tooltbPage,
            this.toollbTotalPage,
            this.toolStripSeparator2,
            this.toolbtnAfter,
            this.toolbtnLast,
            this.toolStripSeparator3,
            this.toolStripLabel2,
            this.toollbTotal,
            this.toolStripLabel3,
            this.toolStripLabel5,
            this.toolcmbPerNum});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1234, 28);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolbtnFirst
            // 
            this.toolbtnFirst.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolbtnFirst.Enabled = false;
            this.toolbtnFirst.Image = global::DemuraShuttleLine.Properties.Resources.L_p_1;
            this.toolbtnFirst.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbtnFirst.Name = "toolbtnFirst";
            this.toolbtnFirst.Size = new System.Drawing.Size(29, 25);
            this.toolbtnFirst.Text = "toolStripButton1";
            this.toolbtnFirst.ToolTipText = "第一页";
            this.toolbtnFirst.Click += new System.EventHandler(this.toolbtnFirst_Click);
            this.toolbtnFirst.MouseDown += new System.Windows.Forms.MouseEventHandler(this.toolbtnFirst_MouseDown);
            this.toolbtnFirst.MouseUp += new System.Windows.Forms.MouseEventHandler(this.toolbtnFirst_MouseUp);
            // 
            // toolbtnFront
            // 
            this.toolbtnFront.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolbtnFront.Enabled = false;
            this.toolbtnFront.Image = global::DemuraShuttleLine.Properties.Resources.L_p;
            this.toolbtnFront.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbtnFront.Name = "toolbtnFront";
            this.toolbtnFront.Size = new System.Drawing.Size(29, 25);
            this.toolbtnFront.Text = "toolStripButton2";
            this.toolbtnFront.ToolTipText = "上一页";
            this.toolbtnFront.Click += new System.EventHandler(this.toolbtnFront_Click);
            this.toolbtnFront.MouseDown += new System.Windows.Forms.MouseEventHandler(this.toolbtnFront_MouseDown);
            this.toolbtnFront.MouseUp += new System.Windows.Forms.MouseEventHandler(this.toolbtnFront_MouseUp);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // tooltbPage
            // 
            this.tooltbPage.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F);
            this.tooltbPage.Name = "tooltbPage";
            this.tooltbPage.Size = new System.Drawing.Size(50, 28);
            this.tooltbPage.Text = "1";
            this.tooltbPage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tooltbPage_KeyPress);
            this.tooltbPage.TextChanged += new System.EventHandler(this.tooltbPage_TextChanged);
            // 
            // toollbTotalPage
            // 
            this.toollbTotalPage.Name = "toollbTotalPage";
            this.toollbTotalPage.Size = new System.Drawing.Size(18, 25);
            this.toollbTotalPage.Text = "1";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 28);
            // 
            // toolbtnAfter
            // 
            this.toolbtnAfter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolbtnAfter.Enabled = false;
            this.toolbtnAfter.Image = global::DemuraShuttleLine.Properties.Resources.R_P;
            this.toolbtnAfter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbtnAfter.Name = "toolbtnAfter";
            this.toolbtnAfter.Size = new System.Drawing.Size(29, 25);
            this.toolbtnAfter.Text = "toolStripButton3";
            this.toolbtnAfter.ToolTipText = "下一页";
            this.toolbtnAfter.Click += new System.EventHandler(this.toolbtnAfter_Click);
            this.toolbtnAfter.MouseDown += new System.Windows.Forms.MouseEventHandler(this.toolbtnAfter_MouseDown);
            this.toolbtnAfter.MouseUp += new System.Windows.Forms.MouseEventHandler(this.toolbtnAfter_MouseUp);
            // 
            // toolbtnLast
            // 
            this.toolbtnLast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolbtnLast.Enabled = false;
            this.toolbtnLast.Image = global::DemuraShuttleLine.Properties.Resources.R_p_1;
            this.toolbtnLast.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolbtnLast.Name = "toolbtnLast";
            this.toolbtnLast.Size = new System.Drawing.Size(29, 25);
            this.toolbtnLast.Text = "toolStripButton4";
            this.toolbtnLast.ToolTipText = "最后一页";
            this.toolbtnLast.Click += new System.EventHandler(this.toolbtnLast_Click);
            this.toolbtnLast.MouseDown += new System.Windows.Forms.MouseEventHandler(this.toolbtnLast_MouseDown);
            this.toolbtnLast.MouseUp += new System.Windows.Forms.MouseEventHandler(this.toolbtnLast_MouseUp);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 28);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(39, 25);
            this.toolStripLabel2.Text = "总计";
            // 
            // toollbTotal
            // 
            this.toollbTotal.Name = "toollbTotal";
            this.toollbTotal.Size = new System.Drawing.Size(18, 25);
            this.toollbTotal.Text = "1";
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(39, 25);
            this.toolStripLabel3.Text = "条，";
            // 
            // toolStripLabel5
            // 
            this.toolStripLabel5.Name = "toolStripLabel5";
            this.toolStripLabel5.Size = new System.Drawing.Size(84, 25);
            this.toolStripLabel5.Text = "每页条数：";
            // 
            // toolcmbPerNum
            // 
            this.toolcmbPerNum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolcmbPerNum.Items.AddRange(new object[] {
            "10",
            "20",
            "50",
            "100"});
            this.toolcmbPerNum.Name = "toolcmbPerNum";
            this.toolcmbPerNum.Size = new System.Drawing.Size(80, 28);
            this.toolcmbPerNum.SelectedIndexChanged += new System.EventHandler(this.toolcmbPerNum_SelectedIndexChanged);
            // 
            // ucPageView
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.Controls.Add(this.toolStrip1);
            this.Name = "ucPageView";
            this.Size = new System.Drawing.Size(1234, 31);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolbtnFirst;
        private System.Windows.Forms.ToolStripButton toolbtnFront;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox tooltbPage;
        private System.Windows.Forms.ToolStripLabel toollbTotalPage;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolbtnAfter;
        private System.Windows.Forms.ToolStripButton toolbtnLast;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripLabel toollbTotal;
        private System.Windows.Forms.ToolStripLabel toolStripLabel5;
        private System.Windows.Forms.ToolStripComboBox toolcmbPerNum;
    }
}
