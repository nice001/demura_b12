﻿
namespace DemuraShuttleLine.Controls
{
    partial class UCStatusMonitor
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabMonitor = new HYC.WindowsControls.HTTabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.labTarget_OutA_2 = new System.Windows.Forms.Label();
            this.labTarget_OutA_1 = new System.Windows.Forms.Label();
            this.labTarget_OutB_1 = new System.Windows.Forms.Label();
            this.labTarget_OutB_2 = new System.Windows.Forms.Label();
            this.labTarget_InA_2 = new System.Windows.Forms.Label();
            this.labTarget_InA_1 = new System.Windows.Forms.Label();
            this.labTarget_InB_1 = new System.Windows.Forms.Label();
            this.labTarget_InB_2 = new System.Windows.Forms.Label();
            this.Arm1_A_1 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel150 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel151 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel48 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel62 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel49 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel61 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel60 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel59 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel58 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel1 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel21 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel56 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel5 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel57 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel149 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel147 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel146 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel143 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel142 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel139 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel22 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel55 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel148 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel23 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel145 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel52 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel141 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel9 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel144 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel138 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel140 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel54 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel24 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel53 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel6 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel51 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel25 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel13 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel3 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel26 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel50 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel2 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel27 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel47 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel28 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel46 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel17 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel29 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel40 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel10 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel45 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel30 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel4 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel7 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel44 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel31 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel20 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel14 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel41 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel32 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel43 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel8 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel39 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel33 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel42 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel18 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel19 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel34 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel38 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel11 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel16 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel35 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel37 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel12 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel15 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel36 = new HYC.WindowsControls.HTTitlePanel();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.htTitlePanel152 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel153 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel95 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel63 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel64 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel65 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel66 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel67 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel68 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel69 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel70 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel71 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel72 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel137 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel135 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel131 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel127 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel134 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel130 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel73 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel74 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel75 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel76 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel77 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel136 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel133 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel129 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel126 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel78 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel132 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel128 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel79 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel80 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel81 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel82 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel83 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel84 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel85 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel86 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel87 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel88 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel89 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel90 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel91 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel92 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel93 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel94 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel96 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel97 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel98 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel99 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel100 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel101 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel102 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel103 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel104 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel105 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel106 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel107 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel108 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel109 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel110 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel111 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel112 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel113 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel114 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel115 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel116 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel117 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel118 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel119 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel120 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel121 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel122 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel123 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel124 = new HYC.WindowsControls.HTTitlePanel();
            this.htTitlePanel125 = new HYC.WindowsControls.HTTitlePanel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tabMonitor.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMonitor
            // 
            this.tabMonitor.Controls.Add(this.tabPage1);
            this.tabMonitor.Controls.Add(this.tabPage2);
            this.tabMonitor.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabMonitor.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.tabMonitor.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.tabMonitor.ItemSize = new System.Drawing.Size(80, 40);
            this.tabMonitor.Location = new System.Drawing.Point(0, 0);
            this.tabMonitor.MenuStyle = HYC.WindowsControls.HTMenuStyle.Custom;
            this.tabMonitor.Name = "tabMonitor";
            this.tabMonitor.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabMonitor.RightToLeftLayout = true;
            this.tabMonitor.SelectedIndex = 0;
            this.tabMonitor.Size = new System.Drawing.Size(1608, 701);
            this.tabMonitor.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabMonitor.Style = HYC.WindowsControls.HTStyle.Custom;
            this.tabMonitor.StyleCustomMode = true;
            this.tabMonitor.TabBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(254)))), ((int)(((byte)(244)))));
            this.tabMonitor.TabIndex = 13;
            this.tabMonitor.TabPosition = HYC.WindowsControls.HTTabControl.UITabPosition.Right;
            this.tabMonitor.TabSelectedColor = System.Drawing.Color.Gray;
            this.tabMonitor.TabSelectedForeColor = System.Drawing.Color.Black;
            this.tabMonitor.TabUnSelectedForeColor = System.Drawing.Color.Black;
            this.tabMonitor.SelectedIndexChanged += new System.EventHandler(this.tabMonitor_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.tabPage1.BackgroundImage = global::DemuraShuttleLine.Properties.Resources.demura_design;
            this.tabPage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.tabPage1.Controls.Add(this.labTarget_OutA_2);
            this.tabPage1.Controls.Add(this.labTarget_OutA_1);
            this.tabPage1.Controls.Add(this.labTarget_OutB_1);
            this.tabPage1.Controls.Add(this.labTarget_OutB_2);
            this.tabPage1.Controls.Add(this.labTarget_InA_2);
            this.tabPage1.Controls.Add(this.labTarget_InA_1);
            this.tabPage1.Controls.Add(this.labTarget_InB_1);
            this.tabPage1.Controls.Add(this.labTarget_InB_2);
            this.tabPage1.Controls.Add(this.Arm1_A_1);
            this.tabPage1.Controls.Add(this.htTitlePanel150);
            this.tabPage1.Controls.Add(this.htTitlePanel151);
            this.tabPage1.Controls.Add(this.htTitlePanel48);
            this.tabPage1.Controls.Add(this.htTitlePanel62);
            this.tabPage1.Controls.Add(this.htTitlePanel49);
            this.tabPage1.Controls.Add(this.htTitlePanel61);
            this.tabPage1.Controls.Add(this.htTitlePanel60);
            this.tabPage1.Controls.Add(this.htTitlePanel59);
            this.tabPage1.Controls.Add(this.htTitlePanel58);
            this.tabPage1.Controls.Add(this.htTitlePanel1);
            this.tabPage1.Controls.Add(this.htTitlePanel21);
            this.tabPage1.Controls.Add(this.htTitlePanel56);
            this.tabPage1.Controls.Add(this.htTitlePanel5);
            this.tabPage1.Controls.Add(this.htTitlePanel57);
            this.tabPage1.Controls.Add(this.htTitlePanel149);
            this.tabPage1.Controls.Add(this.htTitlePanel147);
            this.tabPage1.Controls.Add(this.htTitlePanel146);
            this.tabPage1.Controls.Add(this.htTitlePanel143);
            this.tabPage1.Controls.Add(this.htTitlePanel142);
            this.tabPage1.Controls.Add(this.htTitlePanel139);
            this.tabPage1.Controls.Add(this.htTitlePanel22);
            this.tabPage1.Controls.Add(this.htTitlePanel55);
            this.tabPage1.Controls.Add(this.htTitlePanel148);
            this.tabPage1.Controls.Add(this.htTitlePanel23);
            this.tabPage1.Controls.Add(this.htTitlePanel145);
            this.tabPage1.Controls.Add(this.htTitlePanel52);
            this.tabPage1.Controls.Add(this.htTitlePanel141);
            this.tabPage1.Controls.Add(this.htTitlePanel9);
            this.tabPage1.Controls.Add(this.htTitlePanel144);
            this.tabPage1.Controls.Add(this.htTitlePanel138);
            this.tabPage1.Controls.Add(this.htTitlePanel140);
            this.tabPage1.Controls.Add(this.htTitlePanel54);
            this.tabPage1.Controls.Add(this.htTitlePanel24);
            this.tabPage1.Controls.Add(this.htTitlePanel53);
            this.tabPage1.Controls.Add(this.htTitlePanel6);
            this.tabPage1.Controls.Add(this.htTitlePanel51);
            this.tabPage1.Controls.Add(this.htTitlePanel25);
            this.tabPage1.Controls.Add(this.htTitlePanel13);
            this.tabPage1.Controls.Add(this.htTitlePanel3);
            this.tabPage1.Controls.Add(this.htTitlePanel26);
            this.tabPage1.Controls.Add(this.htTitlePanel50);
            this.tabPage1.Controls.Add(this.htTitlePanel2);
            this.tabPage1.Controls.Add(this.htTitlePanel27);
            this.tabPage1.Controls.Add(this.htTitlePanel47);
            this.tabPage1.Controls.Add(this.htTitlePanel28);
            this.tabPage1.Controls.Add(this.htTitlePanel46);
            this.tabPage1.Controls.Add(this.htTitlePanel17);
            this.tabPage1.Controls.Add(this.htTitlePanel29);
            this.tabPage1.Controls.Add(this.htTitlePanel40);
            this.tabPage1.Controls.Add(this.htTitlePanel10);
            this.tabPage1.Controls.Add(this.htTitlePanel45);
            this.tabPage1.Controls.Add(this.htTitlePanel30);
            this.tabPage1.Controls.Add(this.htTitlePanel4);
            this.tabPage1.Controls.Add(this.htTitlePanel7);
            this.tabPage1.Controls.Add(this.htTitlePanel44);
            this.tabPage1.Controls.Add(this.htTitlePanel31);
            this.tabPage1.Controls.Add(this.htTitlePanel20);
            this.tabPage1.Controls.Add(this.htTitlePanel14);
            this.tabPage1.Controls.Add(this.htTitlePanel41);
            this.tabPage1.Controls.Add(this.htTitlePanel32);
            this.tabPage1.Controls.Add(this.htTitlePanel43);
            this.tabPage1.Controls.Add(this.htTitlePanel8);
            this.tabPage1.Controls.Add(this.htTitlePanel39);
            this.tabPage1.Controls.Add(this.htTitlePanel33);
            this.tabPage1.Controls.Add(this.htTitlePanel42);
            this.tabPage1.Controls.Add(this.htTitlePanel18);
            this.tabPage1.Controls.Add(this.htTitlePanel19);
            this.tabPage1.Controls.Add(this.htTitlePanel34);
            this.tabPage1.Controls.Add(this.htTitlePanel38);
            this.tabPage1.Controls.Add(this.htTitlePanel11);
            this.tabPage1.Controls.Add(this.htTitlePanel16);
            this.tabPage1.Controls.Add(this.htTitlePanel35);
            this.tabPage1.Controls.Add(this.htTitlePanel37);
            this.tabPage1.Controls.Add(this.htTitlePanel12);
            this.tabPage1.Controls.Add(this.htTitlePanel15);
            this.tabPage1.Controls.Add(this.htTitlePanel36);
            this.tabPage1.Location = new System.Drawing.Point(0, 40);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1608, 661);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "镜像2";
            // 
            // labTarget_OutA_2
            // 
            this.labTarget_OutA_2.BackColor = System.Drawing.Color.Lime;
            this.labTarget_OutA_2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labTarget_OutA_2.ForeColor = System.Drawing.Color.Black;
            this.labTarget_OutA_2.Location = new System.Drawing.Point(1140, 311);
            this.labTarget_OutA_2.Name = "labTarget_OutA_2";
            this.labTarget_OutA_2.Size = new System.Drawing.Size(60, 20);
            this.labTarget_OutA_2.TabIndex = 23;
            this.labTarget_OutA_2.Tag = "69";
            this.labTarget_OutA_2.Text = "目标-0";
            this.labTarget_OutA_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labTarget_OutA_2.Visible = false;
            // 
            // labTarget_OutA_1
            // 
            this.labTarget_OutA_1.BackColor = System.Drawing.Color.Lime;
            this.labTarget_OutA_1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labTarget_OutA_1.ForeColor = System.Drawing.Color.Black;
            this.labTarget_OutA_1.Location = new System.Drawing.Point(1061, 311);
            this.labTarget_OutA_1.Name = "labTarget_OutA_1";
            this.labTarget_OutA_1.Size = new System.Drawing.Size(60, 20);
            this.labTarget_OutA_1.TabIndex = 22;
            this.labTarget_OutA_1.Tag = "68";
            this.labTarget_OutA_1.Text = "目标-0";
            this.labTarget_OutA_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labTarget_OutA_1.Visible = false;
            // 
            // labTarget_OutB_1
            // 
            this.labTarget_OutB_1.BackColor = System.Drawing.Color.Lime;
            this.labTarget_OutB_1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labTarget_OutB_1.ForeColor = System.Drawing.Color.Black;
            this.labTarget_OutB_1.Location = new System.Drawing.Point(919, 341);
            this.labTarget_OutB_1.Name = "labTarget_OutB_1";
            this.labTarget_OutB_1.Size = new System.Drawing.Size(60, 20);
            this.labTarget_OutB_1.TabIndex = 21;
            this.labTarget_OutB_1.Tag = "84";
            this.labTarget_OutB_1.Text = "目标-0";
            this.labTarget_OutB_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labTarget_OutB_1.Visible = false;
            // 
            // labTarget_OutB_2
            // 
            this.labTarget_OutB_2.BackColor = System.Drawing.Color.Lime;
            this.labTarget_OutB_2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labTarget_OutB_2.ForeColor = System.Drawing.Color.Black;
            this.labTarget_OutB_2.Location = new System.Drawing.Point(840, 341);
            this.labTarget_OutB_2.Name = "labTarget_OutB_2";
            this.labTarget_OutB_2.Size = new System.Drawing.Size(60, 20);
            this.labTarget_OutB_2.TabIndex = 20;
            this.labTarget_OutB_2.Tag = "85";
            this.labTarget_OutB_2.Text = "目标-0";
            this.labTarget_OutB_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labTarget_OutB_2.Visible = false;
            // 
            // labTarget_InA_2
            // 
            this.labTarget_InA_2.BackColor = System.Drawing.Color.Lime;
            this.labTarget_InA_2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labTarget_InA_2.ForeColor = System.Drawing.Color.Black;
            this.labTarget_InA_2.Location = new System.Drawing.Point(642, 311);
            this.labTarget_InA_2.Name = "labTarget_InA_2";
            this.labTarget_InA_2.Size = new System.Drawing.Size(60, 20);
            this.labTarget_InA_2.TabIndex = 19;
            this.labTarget_InA_2.Tag = "65";
            this.labTarget_InA_2.Text = "目标-0";
            this.labTarget_InA_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labTarget_InA_2.Visible = false;
            // 
            // labTarget_InA_1
            // 
            this.labTarget_InA_1.BackColor = System.Drawing.Color.Lime;
            this.labTarget_InA_1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labTarget_InA_1.ForeColor = System.Drawing.Color.Black;
            this.labTarget_InA_1.Location = new System.Drawing.Point(565, 311);
            this.labTarget_InA_1.Name = "labTarget_InA_1";
            this.labTarget_InA_1.Size = new System.Drawing.Size(60, 20);
            this.labTarget_InA_1.TabIndex = 18;
            this.labTarget_InA_1.Tag = "64";
            this.labTarget_InA_1.Text = "目标-0";
            this.labTarget_InA_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labTarget_InA_1.Visible = false;
            // 
            // labTarget_InB_1
            // 
            this.labTarget_InB_1.BackColor = System.Drawing.Color.Lime;
            this.labTarget_InB_1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labTarget_InB_1.ForeColor = System.Drawing.Color.Black;
            this.labTarget_InB_1.Location = new System.Drawing.Point(449, 341);
            this.labTarget_InB_1.Name = "labTarget_InB_1";
            this.labTarget_InB_1.Size = new System.Drawing.Size(60, 20);
            this.labTarget_InB_1.TabIndex = 17;
            this.labTarget_InB_1.Tag = "80";
            this.labTarget_InB_1.Text = "目标-0";
            this.labTarget_InB_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labTarget_InB_1.Visible = false;
            // 
            // labTarget_InB_2
            // 
            this.labTarget_InB_2.BackColor = System.Drawing.Color.Lime;
            this.labTarget_InB_2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labTarget_InB_2.ForeColor = System.Drawing.Color.Black;
            this.labTarget_InB_2.Location = new System.Drawing.Point(367, 341);
            this.labTarget_InB_2.Name = "labTarget_InB_2";
            this.labTarget_InB_2.Size = new System.Drawing.Size(60, 20);
            this.labTarget_InB_2.TabIndex = 16;
            this.labTarget_InB_2.Tag = "81";
            this.labTarget_InB_2.Text = "目标-0";
            this.labTarget_InB_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labTarget_InB_2.Visible = false;
            // 
            // Arm1_A_1
            // 
            this.Arm1_A_1.BackColor = System.Drawing.Color.Transparent;
            this.Arm1_A_1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.Arm1_A_1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.Arm1_A_1.ForeColor = System.Drawing.Color.White;
            this.Arm1_A_1.Location = new System.Drawing.Point(565, 331);
            this.Arm1_A_1.Name = "Arm1_A_1";
            this.Arm1_A_1.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.Arm1_A_1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.Arm1_A_1.Size = new System.Drawing.Size(60, 50);
            this.Arm1_A_1.Style = HYC.WindowsControls.HTStyle.Custom;
            this.Arm1_A_1.TabIndex = 1;
            this.Arm1_A_1.Tag = "64";
            this.Arm1_A_1.Text = "上料_A_1";
            this.Arm1_A_1.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.Arm1_A_1.TitleHeight = 25;
            // 
            // htTitlePanel150
            // 
            this.htTitlePanel150.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel150.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel150.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel150.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel150.Location = new System.Drawing.Point(156, 459);
            this.htTitlePanel150.Name = "htTitlePanel150";
            this.htTitlePanel150.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel150.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel150.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel150.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel150.TabIndex = 13;
            this.htTitlePanel150.Tag = "57";
            this.htTitlePanel150.Text = "InArm_A_2";
            this.htTitlePanel150.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel150.TitleHeight = 25;
            // 
            // htTitlePanel151
            // 
            this.htTitlePanel151.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel151.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel151.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel151.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel151.Location = new System.Drawing.Point(222, 459);
            this.htTitlePanel151.Name = "htTitlePanel151";
            this.htTitlePanel151.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel151.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel151.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel151.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel151.TabIndex = 14;
            this.htTitlePanel151.Tag = "56";
            this.htTitlePanel151.Text = "InArm_A_1";
            this.htTitlePanel151.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel151.TitleHeight = 25;
            // 
            // htTitlePanel48
            // 
            this.htTitlePanel48.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel48.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel48.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel48.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel48.Location = new System.Drawing.Point(170, 208);
            this.htTitlePanel48.Name = "htTitlePanel48";
            this.htTitlePanel48.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel48.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel48.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel48.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel48.TabIndex = 6;
            this.htTitlePanel48.Tag = "77";
            this.htTitlePanel48.Text = "UVW_B_2";
            this.htTitlePanel48.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel48.TitleHeight = 25;
            // 
            // htTitlePanel62
            // 
            this.htTitlePanel62.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel62.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel62.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel62.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel62.Location = new System.Drawing.Point(1505, 302);
            this.htTitlePanel62.Name = "htTitlePanel62";
            this.htTitlePanel62.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel62.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel62.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel62.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel62.TabIndex = 12;
            this.htTitlePanel62.Tag = "100";
            this.htTitlePanel62.Text = "下料机Arm";
            this.htTitlePanel62.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel62.TitleHeight = 25;
            // 
            // htTitlePanel49
            // 
            this.htTitlePanel49.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel49.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel49.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel49.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel49.Location = new System.Drawing.Point(236, 208);
            this.htTitlePanel49.Name = "htTitlePanel49";
            this.htTitlePanel49.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel49.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel49.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel49.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel49.TabIndex = 5;
            this.htTitlePanel49.Tag = "76";
            this.htTitlePanel49.Text = "UVW_B_1";
            this.htTitlePanel49.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel49.TitleHeight = 25;
            // 
            // htTitlePanel61
            // 
            this.htTitlePanel61.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel61.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel61.Font = new System.Drawing.Font("微软雅黑", 7F);
            this.htTitlePanel61.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel61.ForeDisableColor = System.Drawing.SystemColors.HotTrack;
            this.htTitlePanel61.Location = new System.Drawing.Point(1532, 461);
            this.htTitlePanel61.Name = "htTitlePanel61";
            this.htTitlePanel61.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel61.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel61.RectDisableColor = System.Drawing.Color.Blue;
            this.htTitlePanel61.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel61.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel61.TabIndex = 12;
            this.htTitlePanel61.Tag = "96";
            this.htTitlePanel61.Text = "下料中转_1";
            this.htTitlePanel61.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel61.TitleHeight = 25;
            // 
            // htTitlePanel60
            // 
            this.htTitlePanel60.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel60.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel60.Font = new System.Drawing.Font("微软雅黑", 7F);
            this.htTitlePanel60.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel60.Location = new System.Drawing.Point(1466, 461);
            this.htTitlePanel60.Name = "htTitlePanel60";
            this.htTitlePanel60.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel60.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel60.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel60.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel60.TabIndex = 11;
            this.htTitlePanel60.Tag = "97";
            this.htTitlePanel60.Text = "下料中转_2";
            this.htTitlePanel60.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel60.TitleHeight = 25;
            // 
            // htTitlePanel59
            // 
            this.htTitlePanel59.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel59.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel59.Font = new System.Drawing.Font("微软雅黑", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.htTitlePanel59.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel59.Location = new System.Drawing.Point(1300, 440);
            this.htTitlePanel59.Name = "htTitlePanel59";
            this.htTitlePanel59.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel59.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel59.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel59.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel59.TabIndex = 12;
            this.htTitlePanel59.Tag = "92";
            this.htTitlePanel59.Text = "出料Arm_1";
            this.htTitlePanel59.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel59.TitleHeight = 25;
            // 
            // htTitlePanel58
            // 
            this.htTitlePanel58.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel58.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel58.Font = new System.Drawing.Font("微软雅黑", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.htTitlePanel58.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel58.Location = new System.Drawing.Point(1225, 440);
            this.htTitlePanel58.Name = "htTitlePanel58";
            this.htTitlePanel58.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel58.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel58.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel58.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel58.TabIndex = 11;
            this.htTitlePanel58.Tag = "93";
            this.htTitlePanel58.TagString = "";
            this.htTitlePanel58.Text = "出料Arm_2";
            this.htTitlePanel58.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel58.TitleHeight = 25;
            // 
            // htTitlePanel1
            // 
            this.htTitlePanel1.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel1.FillColor = System.Drawing.SystemColors.Control;
            this.htTitlePanel1.FillDisableColor = System.Drawing.SystemColors.Control;
            this.htTitlePanel1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel1.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel1.Location = new System.Drawing.Point(1021, 460);
            this.htTitlePanel1.Name = "htTitlePanel1";
            this.htTitlePanel1.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel1.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel1.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel1.TabIndex = 0;
            this.htTitlePanel1.Tag = "17";
            this.htTitlePanel1.Text = "PG18";
            this.htTitlePanel1.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel1.TitleHeight = 25;
            // 
            // htTitlePanel21
            // 
            this.htTitlePanel21.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel21.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel21.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel21.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel21.Location = new System.Drawing.Point(1021, 387);
            this.htTitlePanel21.Name = "htTitlePanel21";
            this.htTitlePanel21.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel21.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel21.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel21.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel21.TabIndex = 0;
            this.htTitlePanel21.Tag = "19";
            this.htTitlePanel21.Text = "PG20";
            this.htTitlePanel21.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel21.TitleHeight = 25;
            // 
            // htTitlePanel56
            // 
            this.htTitlePanel56.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel56.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel56.Font = new System.Drawing.Font("微软雅黑", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.htTitlePanel56.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel56.Location = new System.Drawing.Point(1225, 235);
            this.htTitlePanel56.Name = "htTitlePanel56";
            this.htTitlePanel56.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel56.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel56.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel56.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel56.TabIndex = 9;
            this.htTitlePanel56.Tag = "89";
            this.htTitlePanel56.Text = "下料平台_2";
            this.htTitlePanel56.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel56.TitleHeight = 25;
            // 
            // htTitlePanel5
            // 
            this.htTitlePanel5.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel5.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel5.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel5.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel5.Location = new System.Drawing.Point(884, 460);
            this.htTitlePanel5.Name = "htTitlePanel5";
            this.htTitlePanel5.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel5.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel5.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel5.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel5.TabIndex = 0;
            this.htTitlePanel5.Tag = "13";
            this.htTitlePanel5.Text = "PG14";
            this.htTitlePanel5.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel5.TitleHeight = 25;
            // 
            // htTitlePanel57
            // 
            this.htTitlePanel57.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel57.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel57.Font = new System.Drawing.Font("微软雅黑", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.htTitlePanel57.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel57.Location = new System.Drawing.Point(1300, 235);
            this.htTitlePanel57.Name = "htTitlePanel57";
            this.htTitlePanel57.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel57.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel57.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel57.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel57.TabIndex = 10;
            this.htTitlePanel57.Tag = "88";
            this.htTitlePanel57.Text = "下料平台_1";
            this.htTitlePanel57.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel57.TitleHeight = 25;
            // 
            // htTitlePanel149
            // 
            this.htTitlePanel149.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel149.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel149.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel149.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel149.Location = new System.Drawing.Point(1416, 302);
            this.htTitlePanel149.Name = "htTitlePanel149";
            this.htTitlePanel149.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel149.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel149.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel149.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel149.TabIndex = 0;
            this.htTitlePanel149.Tag = "104";
            this.htTitlePanel149.Text = "PG49";
            this.htTitlePanel149.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel149.TitleHeight = 25;
            // 
            // htTitlePanel147
            // 
            this.htTitlePanel147.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel147.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel147.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel147.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel147.Location = new System.Drawing.Point(1157, 222);
            this.htTitlePanel147.Name = "htTitlePanel147";
            this.htTitlePanel147.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel147.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel147.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel147.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel147.TabIndex = 0;
            this.htTitlePanel147.Tag = "46";
            this.htTitlePanel147.Text = "PG47";
            this.htTitlePanel147.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel147.TitleHeight = 25;
            // 
            // htTitlePanel146
            // 
            this.htTitlePanel146.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel146.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel146.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel146.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel146.Location = new System.Drawing.Point(1157, 162);
            this.htTitlePanel146.Name = "htTitlePanel146";
            this.htTitlePanel146.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel146.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel146.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel146.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel146.TabIndex = 0;
            this.htTitlePanel146.Tag = "44";
            this.htTitlePanel146.Text = "PG45";
            this.htTitlePanel146.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel146.TitleHeight = 25;
            // 
            // htTitlePanel143
            // 
            this.htTitlePanel143.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel143.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel143.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel143.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel143.Location = new System.Drawing.Point(1021, 222);
            this.htTitlePanel143.Name = "htTitlePanel143";
            this.htTitlePanel143.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel143.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel143.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel143.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel143.TabIndex = 0;
            this.htTitlePanel143.Tag = "42";
            this.htTitlePanel143.Text = "PG43";
            this.htTitlePanel143.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel143.TitleHeight = 25;
            // 
            // htTitlePanel142
            // 
            this.htTitlePanel142.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel142.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel142.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel142.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel142.Location = new System.Drawing.Point(1021, 162);
            this.htTitlePanel142.Name = "htTitlePanel142";
            this.htTitlePanel142.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel142.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel142.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel142.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel142.TabIndex = 0;
            this.htTitlePanel142.Tag = "40";
            this.htTitlePanel142.Text = "PG41";
            this.htTitlePanel142.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel142.TitleHeight = 25;
            // 
            // htTitlePanel139
            // 
            this.htTitlePanel139.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel139.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel139.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel139.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel139.Location = new System.Drawing.Point(884, 222);
            this.htTitlePanel139.Name = "htTitlePanel139";
            this.htTitlePanel139.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel139.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel139.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel139.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel139.TabIndex = 0;
            this.htTitlePanel139.Tag = "38";
            this.htTitlePanel139.Text = "PG39";
            this.htTitlePanel139.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel139.TitleHeight = 25;
            // 
            // htTitlePanel22
            // 
            this.htTitlePanel22.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel22.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel22.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel22.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel22.Location = new System.Drawing.Point(884, 162);
            this.htTitlePanel22.Name = "htTitlePanel22";
            this.htTitlePanel22.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel22.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel22.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel22.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel22.TabIndex = 0;
            this.htTitlePanel22.Tag = "36";
            this.htTitlePanel22.Text = "PG37";
            this.htTitlePanel22.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel22.TitleHeight = 25;
            // 
            // htTitlePanel55
            // 
            this.htTitlePanel55.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel55.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel55.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel55.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel55.Location = new System.Drawing.Point(103, 153);
            this.htTitlePanel55.Name = "htTitlePanel55";
            this.htTitlePanel55.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel55.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel55.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel55.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel55.TabIndex = 7;
            this.htTitlePanel55.Tag = "73";
            this.htTitlePanel55.Text = "InArm_B_2";
            this.htTitlePanel55.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel55.TitleHeight = 25;
            // 
            // htTitlePanel148
            // 
            this.htTitlePanel148.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel148.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel148.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel148.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel148.Location = new System.Drawing.Point(1350, 302);
            this.htTitlePanel148.Name = "htTitlePanel148";
            this.htTitlePanel148.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel148.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel148.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel148.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel148.TabIndex = 0;
            this.htTitlePanel148.Tag = "105";
            this.htTitlePanel148.Text = "PG50";
            this.htTitlePanel148.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel148.TitleHeight = 25;
            // 
            // htTitlePanel23
            // 
            this.htTitlePanel23.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel23.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel23.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel23.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel23.Location = new System.Drawing.Point(661, 222);
            this.htTitlePanel23.Name = "htTitlePanel23";
            this.htTitlePanel23.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel23.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel23.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel23.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel23.TabIndex = 0;
            this.htTitlePanel23.Tag = "34";
            this.htTitlePanel23.Text = "PG35";
            this.htTitlePanel23.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel23.TitleHeight = 25;
            // 
            // htTitlePanel145
            // 
            this.htTitlePanel145.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel145.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel145.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel145.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel145.Location = new System.Drawing.Point(1096, 222);
            this.htTitlePanel145.Name = "htTitlePanel145";
            this.htTitlePanel145.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel145.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel145.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel145.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel145.TabIndex = 0;
            this.htTitlePanel145.Tag = "47";
            this.htTitlePanel145.Text = "PG48";
            this.htTitlePanel145.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel145.TitleHeight = 25;
            // 
            // htTitlePanel52
            // 
            this.htTitlePanel52.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel52.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel52.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel52.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel52.Location = new System.Drawing.Point(173, 278);
            this.htTitlePanel52.Name = "htTitlePanel52";
            this.htTitlePanel52.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel52.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel52.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel52.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel52.TabIndex = 7;
            this.htTitlePanel52.Tag = "52";
            this.htTitlePanel52.Text = "扫码_1";
            this.htTitlePanel52.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel52.TitleHeight = 25;
            // 
            // htTitlePanel141
            // 
            this.htTitlePanel141.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel141.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel141.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel141.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel141.Location = new System.Drawing.Point(958, 222);
            this.htTitlePanel141.Name = "htTitlePanel141";
            this.htTitlePanel141.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel141.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel141.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel141.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel141.TabIndex = 0;
            this.htTitlePanel141.Tag = "43";
            this.htTitlePanel141.Text = "PG44";
            this.htTitlePanel141.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel141.TitleHeight = 25;
            // 
            // htTitlePanel9
            // 
            this.htTitlePanel9.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel9.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel9.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel9.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel9.Location = new System.Drawing.Point(661, 460);
            this.htTitlePanel9.Name = "htTitlePanel9";
            this.htTitlePanel9.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel9.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel9.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel9.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel9.TabIndex = 0;
            this.htTitlePanel9.Tag = "9";
            this.htTitlePanel9.Text = "PG10";
            this.htTitlePanel9.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel9.TitleHeight = 25;
            // 
            // htTitlePanel144
            // 
            this.htTitlePanel144.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel144.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel144.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel144.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel144.Location = new System.Drawing.Point(1096, 162);
            this.htTitlePanel144.Name = "htTitlePanel144";
            this.htTitlePanel144.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel144.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel144.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel144.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel144.TabIndex = 0;
            this.htTitlePanel144.Tag = "45";
            this.htTitlePanel144.Text = "PG46";
            this.htTitlePanel144.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel144.TitleHeight = 25;
            // 
            // htTitlePanel138
            // 
            this.htTitlePanel138.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel138.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel138.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel138.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel138.Location = new System.Drawing.Point(821, 222);
            this.htTitlePanel138.Name = "htTitlePanel138";
            this.htTitlePanel138.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel138.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel138.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel138.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel138.TabIndex = 0;
            this.htTitlePanel138.Tag = "39";
            this.htTitlePanel138.Text = "PG40";
            this.htTitlePanel138.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel138.TitleHeight = 25;
            // 
            // htTitlePanel140
            // 
            this.htTitlePanel140.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel140.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel140.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel140.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel140.Location = new System.Drawing.Point(958, 162);
            this.htTitlePanel140.Name = "htTitlePanel140";
            this.htTitlePanel140.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel140.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel140.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel140.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel140.TabIndex = 0;
            this.htTitlePanel140.Tag = "41";
            this.htTitlePanel140.Text = "PG42";
            this.htTitlePanel140.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel140.TitleHeight = 25;
            // 
            // htTitlePanel54
            // 
            this.htTitlePanel54.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel54.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel54.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel54.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel54.Location = new System.Drawing.Point(170, 153);
            this.htTitlePanel54.Name = "htTitlePanel54";
            this.htTitlePanel54.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel54.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel54.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel54.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel54.TabIndex = 8;
            this.htTitlePanel54.Tag = "72";
            this.htTitlePanel54.Text = "InArm_B_1";
            this.htTitlePanel54.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel54.TitleHeight = 25;
            // 
            // htTitlePanel24
            // 
            this.htTitlePanel24.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel24.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel24.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel24.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel24.Location = new System.Drawing.Point(821, 162);
            this.htTitlePanel24.Name = "htTitlePanel24";
            this.htTitlePanel24.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel24.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel24.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel24.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel24.TabIndex = 0;
            this.htTitlePanel24.Tag = "37";
            this.htTitlePanel24.Text = "PG38";
            this.htTitlePanel24.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel24.TitleHeight = 25;
            // 
            // htTitlePanel53
            // 
            this.htTitlePanel53.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel53.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel53.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel53.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel53.Location = new System.Drawing.Point(107, 278);
            this.htTitlePanel53.Name = "htTitlePanel53";
            this.htTitlePanel53.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel53.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel53.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel53.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel53.TabIndex = 8;
            this.htTitlePanel53.Tag = "53";
            this.htTitlePanel53.Text = "扫码_2";
            this.htTitlePanel53.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel53.TitleHeight = 25;
            // 
            // htTitlePanel6
            // 
            this.htTitlePanel6.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel6.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel6.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel6.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel6.Location = new System.Drawing.Point(884, 387);
            this.htTitlePanel6.Name = "htTitlePanel6";
            this.htTitlePanel6.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel6.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel6.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel6.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel6.TabIndex = 0;
            this.htTitlePanel6.Tag = "15";
            this.htTitlePanel6.Text = "PG16";
            this.htTitlePanel6.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel6.TitleHeight = 25;
            // 
            // htTitlePanel51
            // 
            this.htTitlePanel51.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel51.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel51.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel51.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel51.Location = new System.Drawing.Point(90, 459);
            this.htTitlePanel51.Name = "htTitlePanel51";
            this.htTitlePanel51.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel51.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel51.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel51.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel51.TabIndex = 5;
            this.htTitlePanel51.Tag = "48";
            this.htTitlePanel51.Text = "接料_1";
            this.htTitlePanel51.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel51.TitleHeight = 25;
            // 
            // htTitlePanel25
            // 
            this.htTitlePanel25.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel25.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel25.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel25.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel25.Location = new System.Drawing.Point(524, 222);
            this.htTitlePanel25.Name = "htTitlePanel25";
            this.htTitlePanel25.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel25.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel25.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel25.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel25.TabIndex = 0;
            this.htTitlePanel25.Tag = "30";
            this.htTitlePanel25.Text = "PG31";
            this.htTitlePanel25.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel25.TitleHeight = 25;
            // 
            // htTitlePanel13
            // 
            this.htTitlePanel13.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel13.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel13.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel13.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel13.Location = new System.Drawing.Point(524, 460);
            this.htTitlePanel13.Name = "htTitlePanel13";
            this.htTitlePanel13.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel13.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel13.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel13.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel13.TabIndex = 0;
            this.htTitlePanel13.Tag = "5";
            this.htTitlePanel13.Text = "PG6";
            this.htTitlePanel13.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel13.TitleHeight = 25;
            // 
            // htTitlePanel3
            // 
            this.htTitlePanel3.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel3.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel3.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel3.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel3.Location = new System.Drawing.Point(245, 394);
            this.htTitlePanel3.Name = "htTitlePanel3";
            this.htTitlePanel3.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel3.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel3.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel3.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel3.TabIndex = 5;
            this.htTitlePanel3.Tag = "61";
            this.htTitlePanel3.Text = "UVW_A_2";
            this.htTitlePanel3.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel3.TitleHeight = 25;
            // 
            // htTitlePanel26
            // 
            this.htTitlePanel26.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel26.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel26.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel26.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel26.Location = new System.Drawing.Point(661, 162);
            this.htTitlePanel26.Name = "htTitlePanel26";
            this.htTitlePanel26.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel26.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel26.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel26.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel26.TabIndex = 0;
            this.htTitlePanel26.Tag = "32";
            this.htTitlePanel26.Text = "PG33";
            this.htTitlePanel26.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel26.TitleHeight = 25;
            // 
            // htTitlePanel50
            // 
            this.htTitlePanel50.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel50.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel50.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel50.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel50.Location = new System.Drawing.Point(24, 459);
            this.htTitlePanel50.Name = "htTitlePanel50";
            this.htTitlePanel50.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel50.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel50.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel50.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel50.TabIndex = 6;
            this.htTitlePanel50.Tag = "49";
            this.htTitlePanel50.Text = "接料_2";
            this.htTitlePanel50.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel50.TitleHeight = 25;
            // 
            // htTitlePanel2
            // 
            this.htTitlePanel2.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel2.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel2.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel2.Location = new System.Drawing.Point(958, 460);
            this.htTitlePanel2.Name = "htTitlePanel2";
            this.htTitlePanel2.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel2.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel2.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel2.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel2.TabIndex = 0;
            this.htTitlePanel2.Tag = "16";
            this.htTitlePanel2.Text = "PG17";
            this.htTitlePanel2.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel2.TitleHeight = 25;
            // 
            // htTitlePanel27
            // 
            this.htTitlePanel27.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel27.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel27.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel27.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel27.Location = new System.Drawing.Point(388, 222);
            this.htTitlePanel27.Name = "htTitlePanel27";
            this.htTitlePanel27.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel27.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel27.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel27.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel27.TabIndex = 0;
            this.htTitlePanel27.Tag = "26";
            this.htTitlePanel27.Text = "PG27";
            this.htTitlePanel27.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel27.TitleHeight = 25;
            // 
            // htTitlePanel47
            // 
            this.htTitlePanel47.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel47.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel47.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel47.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel47.Location = new System.Drawing.Point(179, 394);
            this.htTitlePanel47.Name = "htTitlePanel47";
            this.htTitlePanel47.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel47.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel47.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel47.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel47.TabIndex = 6;
            this.htTitlePanel47.Tag = "60";
            this.htTitlePanel47.Text = "UVW_A_1";
            this.htTitlePanel47.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel47.TitleHeight = 25;
            // 
            // htTitlePanel28
            // 
            this.htTitlePanel28.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel28.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel28.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel28.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel28.Location = new System.Drawing.Point(958, 387);
            this.htTitlePanel28.Name = "htTitlePanel28";
            this.htTitlePanel28.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel28.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel28.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel28.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel28.TabIndex = 0;
            this.htTitlePanel28.Tag = "18";
            this.htTitlePanel28.Text = "PG19";
            this.htTitlePanel28.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel28.TitleHeight = 25;
            // 
            // htTitlePanel46
            // 
            this.htTitlePanel46.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel46.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel46.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel46.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel46.Location = new System.Drawing.Point(367, 292);
            this.htTitlePanel46.Name = "htTitlePanel46";
            this.htTitlePanel46.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel46.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel46.Size = new System.Drawing.Size(60, 50);
            this.htTitlePanel46.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel46.TabIndex = 1;
            this.htTitlePanel46.Tag = "81";
            this.htTitlePanel46.Text = "上料_B_2";
            this.htTitlePanel46.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel46.TitleHeight = 25;
            // 
            // htTitlePanel17
            // 
            this.htTitlePanel17.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel17.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel17.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel17.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel17.Location = new System.Drawing.Point(388, 459);
            this.htTitlePanel17.Name = "htTitlePanel17";
            this.htTitlePanel17.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel17.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel17.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel17.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel17.TabIndex = 0;
            this.htTitlePanel17.Tag = "1";
            this.htTitlePanel17.Text = "PG2";
            this.htTitlePanel17.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel17.TitleHeight = 25;
            // 
            // htTitlePanel29
            // 
            this.htTitlePanel29.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel29.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel29.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel29.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel29.Location = new System.Drawing.Point(1157, 460);
            this.htTitlePanel29.Name = "htTitlePanel29";
            this.htTitlePanel29.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel29.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel29.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel29.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel29.TabIndex = 0;
            this.htTitlePanel29.Tag = "21";
            this.htTitlePanel29.Text = "PG22";
            this.htTitlePanel29.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel29.TitleHeight = 25;
            // 
            // htTitlePanel40
            // 
            this.htTitlePanel40.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel40.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel40.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel40.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel40.Location = new System.Drawing.Point(1096, 460);
            this.htTitlePanel40.Name = "htTitlePanel40";
            this.htTitlePanel40.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel40.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel40.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel40.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel40.TabIndex = 0;
            this.htTitlePanel40.Tag = "20";
            this.htTitlePanel40.Text = "PG21";
            this.htTitlePanel40.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel40.TitleHeight = 25;
            // 
            // htTitlePanel10
            // 
            this.htTitlePanel10.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel10.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel10.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel10.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel10.Location = new System.Drawing.Point(661, 387);
            this.htTitlePanel10.Name = "htTitlePanel10";
            this.htTitlePanel10.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel10.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel10.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel10.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel10.TabIndex = 0;
            this.htTitlePanel10.Tag = "11";
            this.htTitlePanel10.Text = "PG12";
            this.htTitlePanel10.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel10.TitleHeight = 25;
            // 
            // htTitlePanel45
            // 
            this.htTitlePanel45.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel45.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel45.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel45.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel45.Location = new System.Drawing.Point(840, 292);
            this.htTitlePanel45.Name = "htTitlePanel45";
            this.htTitlePanel45.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel45.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel45.Size = new System.Drawing.Size(60, 50);
            this.htTitlePanel45.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel45.TabIndex = 2;
            this.htTitlePanel45.Tag = "85";
            this.htTitlePanel45.Text = "下料_B_2";
            this.htTitlePanel45.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel45.TitleHeight = 25;
            // 
            // htTitlePanel30
            // 
            this.htTitlePanel30.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel30.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel30.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel30.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel30.Location = new System.Drawing.Point(524, 162);
            this.htTitlePanel30.Name = "htTitlePanel30";
            this.htTitlePanel30.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel30.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel30.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel30.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel30.TabIndex = 0;
            this.htTitlePanel30.Tag = "28";
            this.htTitlePanel30.Text = "PG29";
            this.htTitlePanel30.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel30.TitleHeight = 25;
            // 
            // htTitlePanel4
            // 
            this.htTitlePanel4.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel4.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel4.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel4.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel4.Location = new System.Drawing.Point(1061, 331);
            this.htTitlePanel4.Name = "htTitlePanel4";
            this.htTitlePanel4.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel4.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel4.Size = new System.Drawing.Size(60, 50);
            this.htTitlePanel4.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel4.TabIndex = 2;
            this.htTitlePanel4.Tag = "68";
            this.htTitlePanel4.Text = "下料_A_1";
            this.htTitlePanel4.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel4.TitleHeight = 25;
            // 
            // htTitlePanel7
            // 
            this.htTitlePanel7.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel7.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel7.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel7.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel7.Location = new System.Drawing.Point(821, 387);
            this.htTitlePanel7.Name = "htTitlePanel7";
            this.htTitlePanel7.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel7.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel7.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel7.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel7.TabIndex = 0;
            this.htTitlePanel7.Tag = "14";
            this.htTitlePanel7.Text = "PG15";
            this.htTitlePanel7.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel7.TitleHeight = 25;
            // 
            // htTitlePanel44
            // 
            this.htTitlePanel44.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel44.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel44.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel44.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel44.Location = new System.Drawing.Point(919, 292);
            this.htTitlePanel44.Name = "htTitlePanel44";
            this.htTitlePanel44.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel44.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel44.Size = new System.Drawing.Size(60, 50);
            this.htTitlePanel44.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel44.TabIndex = 3;
            this.htTitlePanel44.Tag = "84";
            this.htTitlePanel44.Text = "下料_B_1";
            this.htTitlePanel44.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel44.TitleHeight = 25;
            // 
            // htTitlePanel31
            // 
            this.htTitlePanel31.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel31.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel31.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel31.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel31.Location = new System.Drawing.Point(600, 162);
            this.htTitlePanel31.Name = "htTitlePanel31";
            this.htTitlePanel31.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel31.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel31.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel31.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel31.TabIndex = 0;
            this.htTitlePanel31.Tag = "33";
            this.htTitlePanel31.Text = "PG34";
            this.htTitlePanel31.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel31.TitleHeight = 25;
            // 
            // htTitlePanel20
            // 
            this.htTitlePanel20.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel20.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel20.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel20.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel20.Location = new System.Drawing.Point(327, 459);
            this.htTitlePanel20.Name = "htTitlePanel20";
            this.htTitlePanel20.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel20.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel20.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel20.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel20.TabIndex = 0;
            this.htTitlePanel20.Tag = "0";
            this.htTitlePanel20.Text = "PG1";
            this.htTitlePanel20.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel20.TitleHeight = 25;
            // 
            // htTitlePanel14
            // 
            this.htTitlePanel14.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel14.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel14.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel14.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel14.Location = new System.Drawing.Point(524, 387);
            this.htTitlePanel14.Name = "htTitlePanel14";
            this.htTitlePanel14.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel14.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel14.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel14.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel14.TabIndex = 0;
            this.htTitlePanel14.Tag = "7";
            this.htTitlePanel14.Text = "PG8";
            this.htTitlePanel14.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel14.TitleHeight = 25;
            // 
            // htTitlePanel41
            // 
            this.htTitlePanel41.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel41.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel41.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel41.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel41.Location = new System.Drawing.Point(1140, 331);
            this.htTitlePanel41.Name = "htTitlePanel41";
            this.htTitlePanel41.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel41.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel41.Size = new System.Drawing.Size(60, 50);
            this.htTitlePanel41.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel41.TabIndex = 3;
            this.htTitlePanel41.Tag = "69";
            this.htTitlePanel41.Text = "下料_A_2";
            this.htTitlePanel41.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel41.TitleHeight = 25;
            // 
            // htTitlePanel32
            // 
            this.htTitlePanel32.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel32.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel32.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel32.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel32.Location = new System.Drawing.Point(388, 162);
            this.htTitlePanel32.Name = "htTitlePanel32";
            this.htTitlePanel32.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel32.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel32.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel32.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel32.TabIndex = 0;
            this.htTitlePanel32.Tag = "24";
            this.htTitlePanel32.Text = "PG25";
            this.htTitlePanel32.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel32.TitleHeight = 25;
            // 
            // htTitlePanel43
            // 
            this.htTitlePanel43.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel43.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel43.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel43.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel43.Location = new System.Drawing.Point(449, 292);
            this.htTitlePanel43.Name = "htTitlePanel43";
            this.htTitlePanel43.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel43.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel43.Size = new System.Drawing.Size(60, 50);
            this.htTitlePanel43.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel43.TabIndex = 4;
            this.htTitlePanel43.Tag = "80";
            this.htTitlePanel43.Text = "上料_B_1";
            this.htTitlePanel43.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel43.TitleHeight = 25;
            // 
            // htTitlePanel8
            // 
            this.htTitlePanel8.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel8.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel8.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel8.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel8.Location = new System.Drawing.Point(821, 460);
            this.htTitlePanel8.Name = "htTitlePanel8";
            this.htTitlePanel8.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel8.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel8.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel8.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel8.TabIndex = 0;
            this.htTitlePanel8.Tag = "12";
            this.htTitlePanel8.Text = "PG13";
            this.htTitlePanel8.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel8.TitleHeight = 25;
            // 
            // htTitlePanel39
            // 
            this.htTitlePanel39.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel39.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel39.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel39.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel39.Location = new System.Drawing.Point(1096, 387);
            this.htTitlePanel39.Name = "htTitlePanel39";
            this.htTitlePanel39.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel39.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel39.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel39.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel39.TabIndex = 0;
            this.htTitlePanel39.Tag = "22";
            this.htTitlePanel39.Text = "PG23";
            this.htTitlePanel39.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel39.TitleHeight = 25;
            // 
            // htTitlePanel33
            // 
            this.htTitlePanel33.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel33.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel33.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel33.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel33.Location = new System.Drawing.Point(600, 222);
            this.htTitlePanel33.Name = "htTitlePanel33";
            this.htTitlePanel33.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel33.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel33.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel33.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel33.TabIndex = 0;
            this.htTitlePanel33.Tag = "35";
            this.htTitlePanel33.Text = "PG36";
            this.htTitlePanel33.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel33.TitleHeight = 25;
            // 
            // htTitlePanel42
            // 
            this.htTitlePanel42.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel42.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel42.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel42.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel42.Location = new System.Drawing.Point(642, 331);
            this.htTitlePanel42.Name = "htTitlePanel42";
            this.htTitlePanel42.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel42.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel42.Size = new System.Drawing.Size(60, 50);
            this.htTitlePanel42.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel42.TabIndex = 4;
            this.htTitlePanel42.Tag = "65";
            this.htTitlePanel42.Text = "上料_A_2";
            this.htTitlePanel42.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel42.TitleHeight = 25;
            // 
            // htTitlePanel18
            // 
            this.htTitlePanel18.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel18.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel18.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel18.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel18.Location = new System.Drawing.Point(388, 387);
            this.htTitlePanel18.Name = "htTitlePanel18";
            this.htTitlePanel18.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel18.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel18.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel18.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel18.TabIndex = 0;
            this.htTitlePanel18.Tag = "3";
            this.htTitlePanel18.Text = "PG4";
            this.htTitlePanel18.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel18.TitleHeight = 25;
            // 
            // htTitlePanel19
            // 
            this.htTitlePanel19.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel19.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel19.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel19.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel19.Location = new System.Drawing.Point(327, 387);
            this.htTitlePanel19.Name = "htTitlePanel19";
            this.htTitlePanel19.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel19.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel19.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel19.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel19.TabIndex = 0;
            this.htTitlePanel19.Tag = "2";
            this.htTitlePanel19.Text = "PG3";
            this.htTitlePanel19.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel19.TitleHeight = 25;
            // 
            // htTitlePanel34
            // 
            this.htTitlePanel34.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel34.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel34.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel34.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel34.Location = new System.Drawing.Point(1157, 387);
            this.htTitlePanel34.Name = "htTitlePanel34";
            this.htTitlePanel34.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel34.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel34.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel34.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel34.TabIndex = 0;
            this.htTitlePanel34.Tag = "23";
            this.htTitlePanel34.Text = "PG24";
            this.htTitlePanel34.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel34.TitleHeight = 25;
            // 
            // htTitlePanel38
            // 
            this.htTitlePanel38.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel38.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel38.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel38.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel38.Location = new System.Drawing.Point(327, 222);
            this.htTitlePanel38.Name = "htTitlePanel38";
            this.htTitlePanel38.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel38.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel38.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel38.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel38.TabIndex = 0;
            this.htTitlePanel38.Tag = "27";
            this.htTitlePanel38.Text = "PG28";
            this.htTitlePanel38.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel38.TitleHeight = 25;
            // 
            // htTitlePanel11
            // 
            this.htTitlePanel11.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel11.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel11.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel11.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel11.Location = new System.Drawing.Point(600, 387);
            this.htTitlePanel11.Name = "htTitlePanel11";
            this.htTitlePanel11.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel11.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel11.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel11.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel11.TabIndex = 0;
            this.htTitlePanel11.Tag = "10";
            this.htTitlePanel11.Text = "PG11";
            this.htTitlePanel11.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel11.TitleHeight = 25;
            // 
            // htTitlePanel16
            // 
            this.htTitlePanel16.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel16.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel16.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel16.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel16.Location = new System.Drawing.Point(463, 459);
            this.htTitlePanel16.Name = "htTitlePanel16";
            this.htTitlePanel16.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel16.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel16.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel16.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel16.TabIndex = 0;
            this.htTitlePanel16.Tag = "4";
            this.htTitlePanel16.Text = "PG5";
            this.htTitlePanel16.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel16.TitleHeight = 25;
            // 
            // htTitlePanel35
            // 
            this.htTitlePanel35.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel35.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel35.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel35.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel35.Location = new System.Drawing.Point(463, 162);
            this.htTitlePanel35.Name = "htTitlePanel35";
            this.htTitlePanel35.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel35.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel35.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel35.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel35.TabIndex = 0;
            this.htTitlePanel35.Tag = "29";
            this.htTitlePanel35.Text = "PG30";
            this.htTitlePanel35.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel35.TitleHeight = 25;
            // 
            // htTitlePanel37
            // 
            this.htTitlePanel37.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel37.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel37.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel37.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel37.Location = new System.Drawing.Point(327, 162);
            this.htTitlePanel37.Name = "htTitlePanel37";
            this.htTitlePanel37.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel37.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel37.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel37.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel37.TabIndex = 0;
            this.htTitlePanel37.Tag = "25";
            this.htTitlePanel37.Text = "PG26";
            this.htTitlePanel37.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel37.TitleHeight = 25;
            // 
            // htTitlePanel12
            // 
            this.htTitlePanel12.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel12.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel12.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel12.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel12.Location = new System.Drawing.Point(600, 460);
            this.htTitlePanel12.Name = "htTitlePanel12";
            this.htTitlePanel12.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel12.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel12.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel12.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel12.TabIndex = 0;
            this.htTitlePanel12.Tag = "8";
            this.htTitlePanel12.Text = "PG9";
            this.htTitlePanel12.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel12.TitleHeight = 25;
            // 
            // htTitlePanel15
            // 
            this.htTitlePanel15.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel15.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel15.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel15.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel15.Location = new System.Drawing.Point(463, 387);
            this.htTitlePanel15.Name = "htTitlePanel15";
            this.htTitlePanel15.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel15.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel15.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel15.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel15.TabIndex = 0;
            this.htTitlePanel15.Tag = "6";
            this.htTitlePanel15.Text = "PG7";
            this.htTitlePanel15.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel15.TitleHeight = 25;
            // 
            // htTitlePanel36
            // 
            this.htTitlePanel36.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel36.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel36.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel36.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel36.Location = new System.Drawing.Point(463, 222);
            this.htTitlePanel36.Name = "htTitlePanel36";
            this.htTitlePanel36.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel36.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel36.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel36.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel36.TabIndex = 0;
            this.htTitlePanel36.Tag = "31";
            this.htTitlePanel36.Text = "PG32";
            this.htTitlePanel36.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel36.TitleHeight = 25;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.tabPage2.BackgroundImage = global::DemuraShuttleLine.Properties.Resources.demura_design_rev;
            this.tabPage2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.htTitlePanel152);
            this.tabPage2.Controls.Add(this.htTitlePanel153);
            this.tabPage2.Controls.Add(this.htTitlePanel95);
            this.tabPage2.Controls.Add(this.htTitlePanel63);
            this.tabPage2.Controls.Add(this.htTitlePanel64);
            this.tabPage2.Controls.Add(this.htTitlePanel65);
            this.tabPage2.Controls.Add(this.htTitlePanel66);
            this.tabPage2.Controls.Add(this.htTitlePanel67);
            this.tabPage2.Controls.Add(this.htTitlePanel68);
            this.tabPage2.Controls.Add(this.htTitlePanel69);
            this.tabPage2.Controls.Add(this.htTitlePanel70);
            this.tabPage2.Controls.Add(this.htTitlePanel71);
            this.tabPage2.Controls.Add(this.htTitlePanel72);
            this.tabPage2.Controls.Add(this.htTitlePanel137);
            this.tabPage2.Controls.Add(this.htTitlePanel135);
            this.tabPage2.Controls.Add(this.htTitlePanel131);
            this.tabPage2.Controls.Add(this.htTitlePanel127);
            this.tabPage2.Controls.Add(this.htTitlePanel134);
            this.tabPage2.Controls.Add(this.htTitlePanel130);
            this.tabPage2.Controls.Add(this.htTitlePanel73);
            this.tabPage2.Controls.Add(this.htTitlePanel74);
            this.tabPage2.Controls.Add(this.htTitlePanel75);
            this.tabPage2.Controls.Add(this.htTitlePanel76);
            this.tabPage2.Controls.Add(this.htTitlePanel77);
            this.tabPage2.Controls.Add(this.htTitlePanel136);
            this.tabPage2.Controls.Add(this.htTitlePanel133);
            this.tabPage2.Controls.Add(this.htTitlePanel129);
            this.tabPage2.Controls.Add(this.htTitlePanel126);
            this.tabPage2.Controls.Add(this.htTitlePanel78);
            this.tabPage2.Controls.Add(this.htTitlePanel132);
            this.tabPage2.Controls.Add(this.htTitlePanel128);
            this.tabPage2.Controls.Add(this.htTitlePanel79);
            this.tabPage2.Controls.Add(this.htTitlePanel80);
            this.tabPage2.Controls.Add(this.htTitlePanel81);
            this.tabPage2.Controls.Add(this.htTitlePanel82);
            this.tabPage2.Controls.Add(this.htTitlePanel83);
            this.tabPage2.Controls.Add(this.htTitlePanel84);
            this.tabPage2.Controls.Add(this.htTitlePanel85);
            this.tabPage2.Controls.Add(this.htTitlePanel86);
            this.tabPage2.Controls.Add(this.htTitlePanel87);
            this.tabPage2.Controls.Add(this.htTitlePanel88);
            this.tabPage2.Controls.Add(this.htTitlePanel89);
            this.tabPage2.Controls.Add(this.htTitlePanel90);
            this.tabPage2.Controls.Add(this.htTitlePanel91);
            this.tabPage2.Controls.Add(this.htTitlePanel92);
            this.tabPage2.Controls.Add(this.htTitlePanel93);
            this.tabPage2.Controls.Add(this.htTitlePanel94);
            this.tabPage2.Controls.Add(this.htTitlePanel96);
            this.tabPage2.Controls.Add(this.htTitlePanel97);
            this.tabPage2.Controls.Add(this.htTitlePanel98);
            this.tabPage2.Controls.Add(this.htTitlePanel99);
            this.tabPage2.Controls.Add(this.htTitlePanel100);
            this.tabPage2.Controls.Add(this.htTitlePanel101);
            this.tabPage2.Controls.Add(this.htTitlePanel102);
            this.tabPage2.Controls.Add(this.htTitlePanel103);
            this.tabPage2.Controls.Add(this.htTitlePanel104);
            this.tabPage2.Controls.Add(this.htTitlePanel105);
            this.tabPage2.Controls.Add(this.htTitlePanel106);
            this.tabPage2.Controls.Add(this.htTitlePanel107);
            this.tabPage2.Controls.Add(this.htTitlePanel108);
            this.tabPage2.Controls.Add(this.htTitlePanel109);
            this.tabPage2.Controls.Add(this.htTitlePanel110);
            this.tabPage2.Controls.Add(this.htTitlePanel111);
            this.tabPage2.Controls.Add(this.htTitlePanel112);
            this.tabPage2.Controls.Add(this.htTitlePanel113);
            this.tabPage2.Controls.Add(this.htTitlePanel114);
            this.tabPage2.Controls.Add(this.htTitlePanel115);
            this.tabPage2.Controls.Add(this.htTitlePanel116);
            this.tabPage2.Controls.Add(this.htTitlePanel117);
            this.tabPage2.Controls.Add(this.htTitlePanel118);
            this.tabPage2.Controls.Add(this.htTitlePanel119);
            this.tabPage2.Controls.Add(this.htTitlePanel120);
            this.tabPage2.Controls.Add(this.htTitlePanel121);
            this.tabPage2.Controls.Add(this.htTitlePanel122);
            this.tabPage2.Controls.Add(this.htTitlePanel123);
            this.tabPage2.Controls.Add(this.htTitlePanel124);
            this.tabPage2.Controls.Add(this.htTitlePanel125);
            this.tabPage2.Location = new System.Drawing.Point(0, 40);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(450, 230);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "镜像1";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Lime;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(1202, 317);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 20);
            this.label4.TabIndex = 85;
            this.label4.Tag = "64";
            this.label4.Text = "目标-0";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label4.Visible = false;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Lime;
            this.label12.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(1124, 316);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 20);
            this.label12.TabIndex = 84;
            this.label12.Tag = "65";
            this.label12.Text = "目标-0";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label12.Visible = false;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Lime;
            this.label15.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(1018, 346);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(60, 20);
            this.label15.TabIndex = 83;
            this.label15.Tag = "81";
            this.label15.Text = "目标-0";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label15.Visible = false;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Lime;
            this.label16.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(939, 346);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(60, 20);
            this.label16.TabIndex = 82;
            this.label16.Tag = "80";
            this.label16.Text = "目标-0";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label16.Visible = false;
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.Lime;
            this.label17.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(771, 316);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(60, 20);
            this.label17.TabIndex = 81;
            this.label17.Tag = "68";
            this.label17.Text = "目标-0";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label17.Visible = false;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.Lime;
            this.label18.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(692, 316);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(60, 20);
            this.label18.TabIndex = 80;
            this.label18.Tag = "69";
            this.label18.Text = "目标-0";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label18.Visible = false;
            // 
            // label19
            // 
            this.label19.BackColor = System.Drawing.Color.Lime;
            this.label19.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(514, 346);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(60, 20);
            this.label19.TabIndex = 79;
            this.label19.Tag = "85";
            this.label19.Text = "目标-0";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label19.Visible = false;
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.Color.Lime;
            this.label20.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(435, 346);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(60, 20);
            this.label20.TabIndex = 78;
            this.label20.Tag = "84";
            this.label20.Text = "目标-0";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label20.Visible = false;
            // 
            // htTitlePanel152
            // 
            this.htTitlePanel152.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel152.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel152.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel152.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel152.Location = new System.Drawing.Point(1387, 454);
            this.htTitlePanel152.Name = "htTitlePanel152";
            this.htTitlePanel152.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel152.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel152.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel152.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel152.TabIndex = 76;
            this.htTitlePanel152.Tag = "57";
            this.htTitlePanel152.Text = "InArm_A_2";
            this.htTitlePanel152.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel152.TitleHeight = 25;
            // 
            // htTitlePanel153
            // 
            this.htTitlePanel153.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel153.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel153.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel153.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel153.Location = new System.Drawing.Point(1324, 454);
            this.htTitlePanel153.Name = "htTitlePanel153";
            this.htTitlePanel153.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel153.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel153.Size = new System.Drawing.Size(60, 49);
            this.htTitlePanel153.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel153.TabIndex = 77;
            this.htTitlePanel153.Tag = "56";
            this.htTitlePanel153.Text = "InArm_A_1";
            this.htTitlePanel153.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel153.TitleHeight = 25;
            // 
            // htTitlePanel95
            // 
            this.htTitlePanel95.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel95.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel95.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel95.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel95.Location = new System.Drawing.Point(1248, 463);
            this.htTitlePanel95.Name = "htTitlePanel95";
            this.htTitlePanel95.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel95.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel95.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel95.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel95.TabIndex = 25;
            this.htTitlePanel95.Tag = "0";
            this.htTitlePanel95.Text = "PG1";
            this.htTitlePanel95.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel95.TitleHeight = 25;
            // 
            // htTitlePanel63
            // 
            this.htTitlePanel63.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel63.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel63.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel63.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel63.Location = new System.Drawing.Point(57, 333);
            this.htTitlePanel63.Name = "htTitlePanel63";
            this.htTitlePanel63.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel63.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel63.Size = new System.Drawing.Size(57, 49);
            this.htTitlePanel63.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel63.TabIndex = 75;
            this.htTitlePanel63.Tag = "100";
            this.htTitlePanel63.Text = "下料机Arm";
            this.htTitlePanel63.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel63.TitleHeight = 25;
            // 
            // htTitlePanel64
            // 
            this.htTitlePanel64.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel64.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel64.Font = new System.Drawing.Font("微软雅黑", 7F);
            this.htTitlePanel64.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel64.Location = new System.Drawing.Point(41, 454);
            this.htTitlePanel64.Name = "htTitlePanel64";
            this.htTitlePanel64.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel64.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel64.Size = new System.Drawing.Size(57, 49);
            this.htTitlePanel64.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel64.TabIndex = 74;
            this.htTitlePanel64.Tag = "96";
            this.htTitlePanel64.Text = "下料中转_1";
            this.htTitlePanel64.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel64.TitleHeight = 25;
            // 
            // htTitlePanel65
            // 
            this.htTitlePanel65.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel65.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel65.Font = new System.Drawing.Font("微软雅黑", 7F);
            this.htTitlePanel65.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel65.Location = new System.Drawing.Point(104, 454);
            this.htTitlePanel65.Name = "htTitlePanel65";
            this.htTitlePanel65.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel65.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel65.Size = new System.Drawing.Size(57, 49);
            this.htTitlePanel65.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel65.TabIndex = 72;
            this.htTitlePanel65.Tag = "97";
            this.htTitlePanel65.Text = "下料中转_2";
            this.htTitlePanel65.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel65.TitleHeight = 25;
            // 
            // htTitlePanel66
            // 
            this.htTitlePanel66.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel66.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel66.Font = new System.Drawing.Font("微软雅黑", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.htTitlePanel66.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel66.Location = new System.Drawing.Point(280, 443);
            this.htTitlePanel66.Name = "htTitlePanel66";
            this.htTitlePanel66.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel66.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel66.Size = new System.Drawing.Size(57, 49);
            this.htTitlePanel66.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel66.TabIndex = 73;
            this.htTitlePanel66.Tag = "92";
            this.htTitlePanel66.Text = "出料Arm_1";
            this.htTitlePanel66.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel66.TitleHeight = 25;
            // 
            // htTitlePanel67
            // 
            this.htTitlePanel67.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel67.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel67.Font = new System.Drawing.Font("微软雅黑", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.htTitlePanel67.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel67.Location = new System.Drawing.Point(343, 443);
            this.htTitlePanel67.Name = "htTitlePanel67";
            this.htTitlePanel67.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel67.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel67.Size = new System.Drawing.Size(57, 49);
            this.htTitlePanel67.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel67.TabIndex = 71;
            this.htTitlePanel67.Tag = "93";
            this.htTitlePanel67.Text = "出料Arm_2";
            this.htTitlePanel67.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel67.TitleHeight = 25;
            // 
            // htTitlePanel68
            // 
            this.htTitlePanel68.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel68.FillColor = System.Drawing.SystemColors.Control;
            this.htTitlePanel68.FillDisableColor = System.Drawing.SystemColors.Control;
            this.htTitlePanel68.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel68.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel68.Location = new System.Drawing.Point(617, 463);
            this.htTitlePanel68.Name = "htTitlePanel68";
            this.htTitlePanel68.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel68.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel68.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel68.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel68.TabIndex = 37;
            this.htTitlePanel68.Tag = "16";
            this.htTitlePanel68.Text = "PG17";
            this.htTitlePanel68.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel68.TitleHeight = 25;
            // 
            // htTitlePanel69
            // 
            this.htTitlePanel69.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel69.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel69.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel69.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel69.Location = new System.Drawing.Point(617, 396);
            this.htTitlePanel69.Name = "htTitlePanel69";
            this.htTitlePanel69.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel69.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel69.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel69.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel69.TabIndex = 38;
            this.htTitlePanel69.Tag = "18";
            this.htTitlePanel69.Text = "PG19";
            this.htTitlePanel69.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel69.TitleHeight = 25;
            // 
            // htTitlePanel70
            // 
            this.htTitlePanel70.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel70.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel70.Font = new System.Drawing.Font("微软雅黑", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.htTitlePanel70.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel70.Location = new System.Drawing.Point(343, 231);
            this.htTitlePanel70.Name = "htTitlePanel70";
            this.htTitlePanel70.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel70.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel70.Size = new System.Drawing.Size(57, 49);
            this.htTitlePanel70.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel70.TabIndex = 69;
            this.htTitlePanel70.Tag = "89";
            this.htTitlePanel70.Text = "下料平台_2";
            this.htTitlePanel70.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel70.TitleHeight = 25;
            // 
            // htTitlePanel71
            // 
            this.htTitlePanel71.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel71.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel71.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel71.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel71.Location = new System.Drawing.Point(754, 463);
            this.htTitlePanel71.Name = "htTitlePanel71";
            this.htTitlePanel71.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel71.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel71.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel71.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel71.TabIndex = 41;
            this.htTitlePanel71.Tag = "12";
            this.htTitlePanel71.Text = "PG13";
            this.htTitlePanel71.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel71.TitleHeight = 25;
            // 
            // htTitlePanel72
            // 
            this.htTitlePanel72.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel72.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel72.Font = new System.Drawing.Font("微软雅黑", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.htTitlePanel72.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel72.Location = new System.Drawing.Point(280, 231);
            this.htTitlePanel72.Name = "htTitlePanel72";
            this.htTitlePanel72.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel72.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel72.Size = new System.Drawing.Size(57, 49);
            this.htTitlePanel72.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel72.TabIndex = 70;
            this.htTitlePanel72.Tag = "88";
            this.htTitlePanel72.Text = "下料平台_1";
            this.htTitlePanel72.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel72.TitleHeight = 25;
            // 
            // htTitlePanel137
            // 
            this.htTitlePanel137.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel137.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel137.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel137.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel137.Location = new System.Drawing.Point(229, 296);
            this.htTitlePanel137.Name = "htTitlePanel137";
            this.htTitlePanel137.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel137.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel137.Size = new System.Drawing.Size(57, 49);
            this.htTitlePanel137.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel137.TabIndex = 42;
            this.htTitlePanel137.Tag = "105";
            this.htTitlePanel137.Text = "PG50";
            this.htTitlePanel137.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel137.TitleHeight = 25;
            // 
            // htTitlePanel135
            // 
            this.htTitlePanel135.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel135.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel135.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel135.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel135.Location = new System.Drawing.Point(480, 231);
            this.htTitlePanel135.Name = "htTitlePanel135";
            this.htTitlePanel135.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel135.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel135.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel135.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel135.TabIndex = 42;
            this.htTitlePanel135.Tag = "47";
            this.htTitlePanel135.Text = "PG48";
            this.htTitlePanel135.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel135.TitleHeight = 25;
            // 
            // htTitlePanel131
            // 
            this.htTitlePanel131.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel131.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel131.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel131.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel131.Location = new System.Drawing.Point(617, 231);
            this.htTitlePanel131.Name = "htTitlePanel131";
            this.htTitlePanel131.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel131.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel131.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel131.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel131.TabIndex = 42;
            this.htTitlePanel131.Tag = "43";
            this.htTitlePanel131.Text = "PG44";
            this.htTitlePanel131.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel131.TitleHeight = 25;
            // 
            // htTitlePanel127
            // 
            this.htTitlePanel127.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel127.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel127.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel127.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel127.Location = new System.Drawing.Point(754, 231);
            this.htTitlePanel127.Name = "htTitlePanel127";
            this.htTitlePanel127.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel127.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel127.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel127.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel127.TabIndex = 42;
            this.htTitlePanel127.Tag = "39";
            this.htTitlePanel127.Text = "PG40";
            this.htTitlePanel127.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel127.TitleHeight = 25;
            // 
            // htTitlePanel134
            // 
            this.htTitlePanel134.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel134.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel134.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel134.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel134.Location = new System.Drawing.Point(480, 163);
            this.htTitlePanel134.Name = "htTitlePanel134";
            this.htTitlePanel134.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel134.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel134.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel134.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel134.TabIndex = 42;
            this.htTitlePanel134.Tag = "45";
            this.htTitlePanel134.Text = "PG46";
            this.htTitlePanel134.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel134.TitleHeight = 25;
            // 
            // htTitlePanel130
            // 
            this.htTitlePanel130.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel130.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel130.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel130.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel130.Location = new System.Drawing.Point(617, 163);
            this.htTitlePanel130.Name = "htTitlePanel130";
            this.htTitlePanel130.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel130.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel130.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel130.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel130.TabIndex = 42;
            this.htTitlePanel130.Tag = "41";
            this.htTitlePanel130.Text = "PG42";
            this.htTitlePanel130.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel130.TitleHeight = 25;
            // 
            // htTitlePanel73
            // 
            this.htTitlePanel73.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel73.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel73.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel73.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel73.Location = new System.Drawing.Point(754, 163);
            this.htTitlePanel73.Name = "htTitlePanel73";
            this.htTitlePanel73.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel73.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel73.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel73.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel73.TabIndex = 42;
            this.htTitlePanel73.Tag = "37";
            this.htTitlePanel73.Text = "PG38";
            this.htTitlePanel73.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel73.TitleHeight = 25;
            // 
            // htTitlePanel74
            // 
            this.htTitlePanel74.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel74.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel74.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel74.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel74.Location = new System.Drawing.Point(1406, 163);
            this.htTitlePanel74.Name = "htTitlePanel74";
            this.htTitlePanel74.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel74.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel74.Size = new System.Drawing.Size(57, 49);
            this.htTitlePanel74.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel74.TabIndex = 65;
            this.htTitlePanel74.Tag = "72";
            this.htTitlePanel74.Text = "InArm_B_1";
            this.htTitlePanel74.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel74.TitleHeight = 25;
            // 
            // htTitlePanel75
            // 
            this.htTitlePanel75.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel75.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel75.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel75.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel75.Location = new System.Drawing.Point(974, 231);
            this.htTitlePanel75.Name = "htTitlePanel75";
            this.htTitlePanel75.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel75.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel75.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel75.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel75.TabIndex = 44;
            this.htTitlePanel75.Tag = "35";
            this.htTitlePanel75.Text = "PG36";
            this.htTitlePanel75.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel75.TitleHeight = 25;
            // 
            // htTitlePanel76
            // 
            this.htTitlePanel76.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel76.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel76.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel76.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel76.Location = new System.Drawing.Point(1406, 307);
            this.htTitlePanel76.Name = "htTitlePanel76";
            this.htTitlePanel76.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel76.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel76.Size = new System.Drawing.Size(57, 49);
            this.htTitlePanel76.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel76.TabIndex = 66;
            this.htTitlePanel76.Tag = "52";
            this.htTitlePanel76.Text = "扫码_1";
            this.htTitlePanel76.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel76.TitleHeight = 25;
            // 
            // htTitlePanel77
            // 
            this.htTitlePanel77.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel77.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel77.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel77.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel77.Location = new System.Drawing.Point(974, 463);
            this.htTitlePanel77.Name = "htTitlePanel77";
            this.htTitlePanel77.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel77.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel77.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel77.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel77.TabIndex = 46;
            this.htTitlePanel77.Tag = "8";
            this.htTitlePanel77.Text = "PG9";
            this.htTitlePanel77.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel77.TitleHeight = 25;
            // 
            // htTitlePanel136
            // 
            this.htTitlePanel136.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel136.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel136.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel136.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel136.Location = new System.Drawing.Point(163, 296);
            this.htTitlePanel136.Name = "htTitlePanel136";
            this.htTitlePanel136.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel136.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel136.Size = new System.Drawing.Size(57, 49);
            this.htTitlePanel136.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel136.TabIndex = 48;
            this.htTitlePanel136.Tag = "104";
            this.htTitlePanel136.Text = "PG49";
            this.htTitlePanel136.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel136.TitleHeight = 25;
            // 
            // htTitlePanel133
            // 
            this.htTitlePanel133.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel133.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel133.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel133.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel133.Location = new System.Drawing.Point(416, 231);
            this.htTitlePanel133.Name = "htTitlePanel133";
            this.htTitlePanel133.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel133.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel133.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel133.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel133.TabIndex = 48;
            this.htTitlePanel133.Tag = "46";
            this.htTitlePanel133.Text = "PG47";
            this.htTitlePanel133.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel133.TitleHeight = 25;
            // 
            // htTitlePanel129
            // 
            this.htTitlePanel129.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel129.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel129.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel129.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel129.Location = new System.Drawing.Point(553, 231);
            this.htTitlePanel129.Name = "htTitlePanel129";
            this.htTitlePanel129.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel129.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel129.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel129.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel129.TabIndex = 48;
            this.htTitlePanel129.Tag = "42";
            this.htTitlePanel129.Text = "PG43";
            this.htTitlePanel129.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel129.TitleHeight = 25;
            // 
            // htTitlePanel126
            // 
            this.htTitlePanel126.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel126.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel126.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel126.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel126.Location = new System.Drawing.Point(689, 231);
            this.htTitlePanel126.Name = "htTitlePanel126";
            this.htTitlePanel126.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel126.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel126.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel126.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel126.TabIndex = 48;
            this.htTitlePanel126.Tag = "38";
            this.htTitlePanel126.Text = "PG39";
            this.htTitlePanel126.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel126.TitleHeight = 25;
            // 
            // htTitlePanel78
            // 
            this.htTitlePanel78.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel78.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel78.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel78.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel78.Location = new System.Drawing.Point(1469, 163);
            this.htTitlePanel78.Name = "htTitlePanel78";
            this.htTitlePanel78.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel78.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel78.Size = new System.Drawing.Size(57, 49);
            this.htTitlePanel78.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel78.TabIndex = 68;
            this.htTitlePanel78.Tag = "73";
            this.htTitlePanel78.Text = "InArm_B_2";
            this.htTitlePanel78.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel78.TitleHeight = 25;
            // 
            // htTitlePanel132
            // 
            this.htTitlePanel132.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel132.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel132.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel132.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel132.Location = new System.Drawing.Point(416, 163);
            this.htTitlePanel132.Name = "htTitlePanel132";
            this.htTitlePanel132.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel132.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel132.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel132.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel132.TabIndex = 48;
            this.htTitlePanel132.Tag = "44";
            this.htTitlePanel132.Text = "PG45";
            this.htTitlePanel132.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel132.TitleHeight = 25;
            // 
            // htTitlePanel128
            // 
            this.htTitlePanel128.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel128.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel128.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel128.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel128.Location = new System.Drawing.Point(553, 163);
            this.htTitlePanel128.Name = "htTitlePanel128";
            this.htTitlePanel128.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel128.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel128.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel128.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel128.TabIndex = 48;
            this.htTitlePanel128.Tag = "40";
            this.htTitlePanel128.Text = "PG41";
            this.htTitlePanel128.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel128.TitleHeight = 25;
            // 
            // htTitlePanel79
            // 
            this.htTitlePanel79.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel79.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel79.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel79.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel79.Location = new System.Drawing.Point(689, 163);
            this.htTitlePanel79.Name = "htTitlePanel79";
            this.htTitlePanel79.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel79.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel79.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel79.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel79.TabIndex = 48;
            this.htTitlePanel79.Tag = "36";
            this.htTitlePanel79.Text = "PG37";
            this.htTitlePanel79.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel79.TitleHeight = 25;
            // 
            // htTitlePanel80
            // 
            this.htTitlePanel80.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel80.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel80.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel80.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel80.Location = new System.Drawing.Point(1469, 307);
            this.htTitlePanel80.Name = "htTitlePanel80";
            this.htTitlePanel80.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel80.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel80.Size = new System.Drawing.Size(57, 49);
            this.htTitlePanel80.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel80.TabIndex = 67;
            this.htTitlePanel80.Tag = "53";
            this.htTitlePanel80.Text = "扫码_2";
            this.htTitlePanel80.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel80.TitleHeight = 25;
            // 
            // htTitlePanel81
            // 
            this.htTitlePanel81.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel81.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel81.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel81.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel81.Location = new System.Drawing.Point(754, 396);
            this.htTitlePanel81.Name = "htTitlePanel81";
            this.htTitlePanel81.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel81.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel81.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel81.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel81.TabIndex = 31;
            this.htTitlePanel81.Tag = "14";
            this.htTitlePanel81.Text = "PG15";
            this.htTitlePanel81.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel81.TitleHeight = 25;
            // 
            // htTitlePanel82
            // 
            this.htTitlePanel82.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel82.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel82.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel82.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel82.Location = new System.Drawing.Point(1522, 454);
            this.htTitlePanel82.Name = "htTitlePanel82";
            this.htTitlePanel82.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel82.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel82.Size = new System.Drawing.Size(57, 49);
            this.htTitlePanel82.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel82.TabIndex = 61;
            this.htTitlePanel82.Tag = "49";
            this.htTitlePanel82.Text = "接料_2";
            this.htTitlePanel82.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel82.TitleHeight = 25;
            // 
            // htTitlePanel83
            // 
            this.htTitlePanel83.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel83.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel83.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel83.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel83.Location = new System.Drawing.Point(1111, 231);
            this.htTitlePanel83.Name = "htTitlePanel83";
            this.htTitlePanel83.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel83.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel83.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel83.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel83.TabIndex = 29;
            this.htTitlePanel83.Tag = "31";
            this.htTitlePanel83.Text = "PG32";
            this.htTitlePanel83.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel83.TitleHeight = 25;
            // 
            // htTitlePanel84
            // 
            this.htTitlePanel84.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel84.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel84.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel84.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel84.Location = new System.Drawing.Point(1387, 231);
            this.htTitlePanel84.Name = "htTitlePanel84";
            this.htTitlePanel84.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel84.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel84.Size = new System.Drawing.Size(57, 49);
            this.htTitlePanel84.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel84.TabIndex = 60;
            this.htTitlePanel84.Tag = "77";
            this.htTitlePanel84.Text = "UVW_B_2";
            this.htTitlePanel84.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel84.TitleHeight = 25;
            // 
            // htTitlePanel85
            // 
            this.htTitlePanel85.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel85.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel85.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel85.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel85.Location = new System.Drawing.Point(1111, 463);
            this.htTitlePanel85.Name = "htTitlePanel85";
            this.htTitlePanel85.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel85.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel85.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel85.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel85.TabIndex = 15;
            this.htTitlePanel85.Tag = "4";
            this.htTitlePanel85.Text = "PG5";
            this.htTitlePanel85.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel85.TitleHeight = 25;
            // 
            // htTitlePanel86
            // 
            this.htTitlePanel86.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel86.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel86.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel86.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel86.Location = new System.Drawing.Point(1387, 396);
            this.htTitlePanel86.Name = "htTitlePanel86";
            this.htTitlePanel86.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel86.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel86.Size = new System.Drawing.Size(57, 49);
            this.htTitlePanel86.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel86.TabIndex = 59;
            this.htTitlePanel86.Tag = "60";
            this.htTitlePanel86.Text = "UVW_A_1";
            this.htTitlePanel86.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel86.TitleHeight = 25;
            // 
            // htTitlePanel87
            // 
            this.htTitlePanel87.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel87.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel87.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel87.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel87.Location = new System.Drawing.Point(974, 163);
            this.htTitlePanel87.Name = "htTitlePanel87";
            this.htTitlePanel87.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel87.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel87.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel87.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel87.TabIndex = 17;
            this.htTitlePanel87.Tag = "33";
            this.htTitlePanel87.Text = "PG34";
            this.htTitlePanel87.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel87.TitleHeight = 25;
            // 
            // htTitlePanel88
            // 
            this.htTitlePanel88.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel88.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel88.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel88.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel88.Location = new System.Drawing.Point(1459, 454);
            this.htTitlePanel88.Name = "htTitlePanel88";
            this.htTitlePanel88.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel88.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel88.Size = new System.Drawing.Size(57, 49);
            this.htTitlePanel88.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel88.TabIndex = 64;
            this.htTitlePanel88.Tag = "48";
            this.htTitlePanel88.Text = "接料_1";
            this.htTitlePanel88.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel88.TitleHeight = 25;
            // 
            // htTitlePanel89
            // 
            this.htTitlePanel89.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel89.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel89.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel89.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel89.Location = new System.Drawing.Point(553, 463);
            this.htTitlePanel89.Name = "htTitlePanel89";
            this.htTitlePanel89.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel89.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel89.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel89.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel89.TabIndex = 19;
            this.htTitlePanel89.Tag = "17";
            this.htTitlePanel89.Text = "PG18";
            this.htTitlePanel89.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel89.TitleHeight = 25;
            // 
            // htTitlePanel90
            // 
            this.htTitlePanel90.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel90.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel90.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel90.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel90.Location = new System.Drawing.Point(1324, 231);
            this.htTitlePanel90.Name = "htTitlePanel90";
            this.htTitlePanel90.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel90.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel90.Size = new System.Drawing.Size(57, 49);
            this.htTitlePanel90.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel90.TabIndex = 63;
            this.htTitlePanel90.Tag = "76";
            this.htTitlePanel90.Text = "UVW_B_1";
            this.htTitlePanel90.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel90.TitleHeight = 25;
            // 
            // htTitlePanel91
            // 
            this.htTitlePanel91.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel91.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel91.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel91.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel91.Location = new System.Drawing.Point(1248, 231);
            this.htTitlePanel91.Name = "htTitlePanel91";
            this.htTitlePanel91.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel91.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel91.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel91.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel91.TabIndex = 21;
            this.htTitlePanel91.Tag = "27";
            this.htTitlePanel91.Text = "PG28";
            this.htTitlePanel91.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel91.TitleHeight = 25;
            // 
            // htTitlePanel92
            // 
            this.htTitlePanel92.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel92.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel92.Font = new System.Drawing.Font("微软雅黑", 8F);
            this.htTitlePanel92.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel92.Location = new System.Drawing.Point(1324, 396);
            this.htTitlePanel92.Name = "htTitlePanel92";
            this.htTitlePanel92.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel92.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel92.Size = new System.Drawing.Size(57, 49);
            this.htTitlePanel92.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel92.TabIndex = 62;
            this.htTitlePanel92.Tag = "61";
            this.htTitlePanel92.Text = "UVW_A_2";
            this.htTitlePanel92.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel92.TitleHeight = 25;
            // 
            // htTitlePanel93
            // 
            this.htTitlePanel93.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel93.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel93.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel93.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel93.Location = new System.Drawing.Point(553, 396);
            this.htTitlePanel93.Name = "htTitlePanel93";
            this.htTitlePanel93.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel93.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel93.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel93.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel93.TabIndex = 23;
            this.htTitlePanel93.Tag = "19";
            this.htTitlePanel93.Text = "PG20";
            this.htTitlePanel93.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel93.TitleHeight = 25;
            // 
            // htTitlePanel94
            // 
            this.htTitlePanel94.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel94.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel94.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel94.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel94.Location = new System.Drawing.Point(1018, 296);
            this.htTitlePanel94.Name = "htTitlePanel94";
            this.htTitlePanel94.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel94.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel94.Size = new System.Drawing.Size(60, 50);
            this.htTitlePanel94.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel94.TabIndex = 51;
            this.htTitlePanel94.Tag = "81";
            this.htTitlePanel94.Text = "上料_B_2";
            this.htTitlePanel94.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel94.TitleHeight = 25;
            // 
            // htTitlePanel96
            // 
            this.htTitlePanel96.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel96.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel96.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel96.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel96.Location = new System.Drawing.Point(1203, 336);
            this.htTitlePanel96.Name = "htTitlePanel96";
            this.htTitlePanel96.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel96.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel96.Size = new System.Drawing.Size(60, 50);
            this.htTitlePanel96.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel96.TabIndex = 52;
            this.htTitlePanel96.Tag = "64";
            this.htTitlePanel96.Text = "上料_A_1";
            this.htTitlePanel96.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel96.TitleHeight = 25;
            // 
            // htTitlePanel97
            // 
            this.htTitlePanel97.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel97.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel97.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel97.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel97.Location = new System.Drawing.Point(480, 463);
            this.htTitlePanel97.Name = "htTitlePanel97";
            this.htTitlePanel97.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel97.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel97.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel97.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel97.TabIndex = 27;
            this.htTitlePanel97.Tag = "20";
            this.htTitlePanel97.Text = "PG21";
            this.htTitlePanel97.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel97.TitleHeight = 25;
            // 
            // htTitlePanel98
            // 
            this.htTitlePanel98.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel98.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel98.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel98.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel98.Location = new System.Drawing.Point(416, 463);
            this.htTitlePanel98.Name = "htTitlePanel98";
            this.htTitlePanel98.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel98.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel98.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel98.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel98.TabIndex = 28;
            this.htTitlePanel98.Tag = "21";
            this.htTitlePanel98.Text = "PG22";
            this.htTitlePanel98.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel98.TitleHeight = 25;
            // 
            // htTitlePanel99
            // 
            this.htTitlePanel99.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel99.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel99.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel99.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel99.Location = new System.Drawing.Point(974, 396);
            this.htTitlePanel99.Name = "htTitlePanel99";
            this.htTitlePanel99.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel99.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel99.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel99.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel99.TabIndex = 40;
            this.htTitlePanel99.Tag = "10";
            this.htTitlePanel99.Text = "PG11";
            this.htTitlePanel99.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel99.TitleHeight = 25;
            // 
            // htTitlePanel100
            // 
            this.htTitlePanel100.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel100.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel100.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel100.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel100.Location = new System.Drawing.Point(514, 296);
            this.htTitlePanel100.Name = "htTitlePanel100";
            this.htTitlePanel100.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel100.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel100.Size = new System.Drawing.Size(60, 50);
            this.htTitlePanel100.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel100.TabIndex = 54;
            this.htTitlePanel100.Tag = "85";
            this.htTitlePanel100.Text = "下料_B_2";
            this.htTitlePanel100.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel100.TitleHeight = 25;
            // 
            // htTitlePanel101
            // 
            this.htTitlePanel101.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel101.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel101.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel101.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel101.Location = new System.Drawing.Point(1111, 163);
            this.htTitlePanel101.Name = "htTitlePanel101";
            this.htTitlePanel101.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel101.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel101.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel101.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel101.TabIndex = 50;
            this.htTitlePanel101.Tag = "29";
            this.htTitlePanel101.Text = "PG30";
            this.htTitlePanel101.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel101.TitleHeight = 25;
            // 
            // htTitlePanel102
            // 
            this.htTitlePanel102.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel102.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel102.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel102.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel102.Location = new System.Drawing.Point(771, 336);
            this.htTitlePanel102.Name = "htTitlePanel102";
            this.htTitlePanel102.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel102.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel102.Size = new System.Drawing.Size(60, 50);
            this.htTitlePanel102.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel102.TabIndex = 53;
            this.htTitlePanel102.Tag = "68";
            this.htTitlePanel102.Text = "下料_A_1";
            this.htTitlePanel102.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel102.TitleHeight = 25;
            // 
            // htTitlePanel103
            // 
            this.htTitlePanel103.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel103.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel103.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel103.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel103.Location = new System.Drawing.Point(689, 396);
            this.htTitlePanel103.Name = "htTitlePanel103";
            this.htTitlePanel103.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel103.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel103.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel103.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel103.TabIndex = 26;
            this.htTitlePanel103.Tag = "15";
            this.htTitlePanel103.Text = "PG16";
            this.htTitlePanel103.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel103.TitleHeight = 25;
            // 
            // htTitlePanel104
            // 
            this.htTitlePanel104.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel104.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel104.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel104.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel104.Location = new System.Drawing.Point(435, 296);
            this.htTitlePanel104.Name = "htTitlePanel104";
            this.htTitlePanel104.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel104.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel104.Size = new System.Drawing.Size(60, 50);
            this.htTitlePanel104.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel104.TabIndex = 55;
            this.htTitlePanel104.Tag = "84";
            this.htTitlePanel104.Text = "下料_B_1";
            this.htTitlePanel104.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel104.TitleHeight = 25;
            // 
            // htTitlePanel105
            // 
            this.htTitlePanel105.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel105.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel105.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel105.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel105.Location = new System.Drawing.Point(910, 163);
            this.htTitlePanel105.Name = "htTitlePanel105";
            this.htTitlePanel105.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel105.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel105.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel105.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel105.TabIndex = 22;
            this.htTitlePanel105.Tag = "32";
            this.htTitlePanel105.Text = "PG33";
            this.htTitlePanel105.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel105.TitleHeight = 25;
            // 
            // htTitlePanel106
            // 
            this.htTitlePanel106.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel106.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel106.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel106.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel106.Location = new System.Drawing.Point(1184, 463);
            this.htTitlePanel106.Name = "htTitlePanel106";
            this.htTitlePanel106.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel106.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel106.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel106.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel106.TabIndex = 20;
            this.htTitlePanel106.Tag = "1";
            this.htTitlePanel106.Text = "PG2";
            this.htTitlePanel106.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel106.TitleHeight = 25;
            // 
            // htTitlePanel107
            // 
            this.htTitlePanel107.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel107.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel107.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel107.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel107.Location = new System.Drawing.Point(1111, 396);
            this.htTitlePanel107.Name = "htTitlePanel107";
            this.htTitlePanel107.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel107.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel107.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel107.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel107.TabIndex = 18;
            this.htTitlePanel107.Tag = "6";
            this.htTitlePanel107.Text = "PG7";
            this.htTitlePanel107.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel107.TitleHeight = 25;
            // 
            // htTitlePanel108
            // 
            this.htTitlePanel108.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel108.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel108.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel108.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel108.Location = new System.Drawing.Point(692, 336);
            this.htTitlePanel108.Name = "htTitlePanel108";
            this.htTitlePanel108.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel108.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel108.Size = new System.Drawing.Size(60, 50);
            this.htTitlePanel108.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel108.TabIndex = 56;
            this.htTitlePanel108.Tag = "69";
            this.htTitlePanel108.Text = "下料_A_2";
            this.htTitlePanel108.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel108.TitleHeight = 25;
            // 
            // htTitlePanel109
            // 
            this.htTitlePanel109.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel109.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel109.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel109.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel109.Location = new System.Drawing.Point(1248, 163);
            this.htTitlePanel109.Name = "htTitlePanel109";
            this.htTitlePanel109.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel109.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel109.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel109.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel109.TabIndex = 16;
            this.htTitlePanel109.Tag = "25";
            this.htTitlePanel109.Text = "PG26";
            this.htTitlePanel109.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel109.TitleHeight = 25;
            // 
            // htTitlePanel110
            // 
            this.htTitlePanel110.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel110.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel110.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel110.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel110.Location = new System.Drawing.Point(939, 296);
            this.htTitlePanel110.Name = "htTitlePanel110";
            this.htTitlePanel110.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel110.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel110.Size = new System.Drawing.Size(60, 50);
            this.htTitlePanel110.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel110.TabIndex = 57;
            this.htTitlePanel110.Tag = "80";
            this.htTitlePanel110.Text = "上料_B_1";
            this.htTitlePanel110.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel110.TitleHeight = 25;
            // 
            // htTitlePanel111
            // 
            this.htTitlePanel111.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel111.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel111.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel111.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel111.Location = new System.Drawing.Point(689, 463);
            this.htTitlePanel111.Name = "htTitlePanel111";
            this.htTitlePanel111.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel111.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel111.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel111.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel111.TabIndex = 14;
            this.htTitlePanel111.Tag = "13";
            this.htTitlePanel111.Text = "PG14";
            this.htTitlePanel111.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel111.TitleHeight = 25;
            // 
            // htTitlePanel112
            // 
            this.htTitlePanel112.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel112.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel112.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel112.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel112.Location = new System.Drawing.Point(416, 396);
            this.htTitlePanel112.Name = "htTitlePanel112";
            this.htTitlePanel112.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel112.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel112.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel112.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel112.TabIndex = 30;
            this.htTitlePanel112.Tag = "23";
            this.htTitlePanel112.Text = "PG24";
            this.htTitlePanel112.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel112.TitleHeight = 25;
            // 
            // htTitlePanel113
            // 
            this.htTitlePanel113.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel113.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel113.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel113.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel113.Location = new System.Drawing.Point(910, 231);
            this.htTitlePanel113.Name = "htTitlePanel113";
            this.htTitlePanel113.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel113.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel113.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel113.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel113.TabIndex = 32;
            this.htTitlePanel113.Tag = "34";
            this.htTitlePanel113.Text = "PG35";
            this.htTitlePanel113.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel113.TitleHeight = 25;
            // 
            // htTitlePanel114
            // 
            this.htTitlePanel114.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel114.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel114.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel114.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel114.Location = new System.Drawing.Point(1124, 336);
            this.htTitlePanel114.Name = "htTitlePanel114";
            this.htTitlePanel114.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel114.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel114.Size = new System.Drawing.Size(60, 50);
            this.htTitlePanel114.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel114.TabIndex = 58;
            this.htTitlePanel114.Tag = "65";
            this.htTitlePanel114.Text = "上料_A_2";
            this.htTitlePanel114.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel114.TitleHeight = 25;
            // 
            // htTitlePanel115
            // 
            this.htTitlePanel115.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel115.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel115.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel115.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel115.Location = new System.Drawing.Point(1248, 396);
            this.htTitlePanel115.Name = "htTitlePanel115";
            this.htTitlePanel115.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel115.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel115.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel115.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel115.TabIndex = 47;
            this.htTitlePanel115.Tag = "2";
            this.htTitlePanel115.Text = "PG3";
            this.htTitlePanel115.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel115.TitleHeight = 25;
            // 
            // htTitlePanel116
            // 
            this.htTitlePanel116.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel116.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel116.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel116.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel116.Location = new System.Drawing.Point(1184, 396);
            this.htTitlePanel116.Name = "htTitlePanel116";
            this.htTitlePanel116.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel116.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel116.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel116.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel116.TabIndex = 45;
            this.htTitlePanel116.Tag = "3";
            this.htTitlePanel116.Text = "PG4";
            this.htTitlePanel116.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel116.TitleHeight = 25;
            // 
            // htTitlePanel117
            // 
            this.htTitlePanel117.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel117.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel117.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel117.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel117.Location = new System.Drawing.Point(480, 396);
            this.htTitlePanel117.Name = "htTitlePanel117";
            this.htTitlePanel117.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel117.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel117.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel117.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel117.TabIndex = 43;
            this.htTitlePanel117.Tag = "22";
            this.htTitlePanel117.Text = "PG23";
            this.htTitlePanel117.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel117.TitleHeight = 25;
            // 
            // htTitlePanel118
            // 
            this.htTitlePanel118.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel118.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel118.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel118.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel118.Location = new System.Drawing.Point(1184, 231);
            this.htTitlePanel118.Name = "htTitlePanel118";
            this.htTitlePanel118.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel118.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel118.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel118.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel118.TabIndex = 49;
            this.htTitlePanel118.Tag = "26";
            this.htTitlePanel118.Text = "PG27";
            this.htTitlePanel118.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel118.TitleHeight = 25;
            // 
            // htTitlePanel119
            // 
            this.htTitlePanel119.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel119.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel119.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel119.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel119.Location = new System.Drawing.Point(910, 396);
            this.htTitlePanel119.Name = "htTitlePanel119";
            this.htTitlePanel119.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel119.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel119.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel119.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel119.TabIndex = 39;
            this.htTitlePanel119.Tag = "11";
            this.htTitlePanel119.Text = "PG12";
            this.htTitlePanel119.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel119.TitleHeight = 25;
            // 
            // htTitlePanel120
            // 
            this.htTitlePanel120.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel120.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel120.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel120.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel120.Location = new System.Drawing.Point(1046, 463);
            this.htTitlePanel120.Name = "htTitlePanel120";
            this.htTitlePanel120.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel120.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel120.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel120.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel120.TabIndex = 36;
            this.htTitlePanel120.Tag = "5";
            this.htTitlePanel120.Text = "PG6";
            this.htTitlePanel120.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel120.TitleHeight = 25;
            // 
            // htTitlePanel121
            // 
            this.htTitlePanel121.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel121.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel121.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel121.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel121.Location = new System.Drawing.Point(1046, 163);
            this.htTitlePanel121.Name = "htTitlePanel121";
            this.htTitlePanel121.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel121.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel121.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel121.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel121.TabIndex = 35;
            this.htTitlePanel121.Tag = "28";
            this.htTitlePanel121.Text = "PG29";
            this.htTitlePanel121.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel121.TitleHeight = 25;
            // 
            // htTitlePanel122
            // 
            this.htTitlePanel122.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel122.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel122.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel122.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel122.Location = new System.Drawing.Point(1184, 163);
            this.htTitlePanel122.Name = "htTitlePanel122";
            this.htTitlePanel122.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel122.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel122.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel122.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel122.TabIndex = 34;
            this.htTitlePanel122.Tag = "24";
            this.htTitlePanel122.Text = "PG25";
            this.htTitlePanel122.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel122.TitleHeight = 25;
            // 
            // htTitlePanel123
            // 
            this.htTitlePanel123.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel123.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel123.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel123.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel123.Location = new System.Drawing.Point(910, 463);
            this.htTitlePanel123.Name = "htTitlePanel123";
            this.htTitlePanel123.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel123.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel123.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel123.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel123.TabIndex = 33;
            this.htTitlePanel123.Tag = "9";
            this.htTitlePanel123.Text = "PG10";
            this.htTitlePanel123.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel123.TitleHeight = 25;
            // 
            // htTitlePanel124
            // 
            this.htTitlePanel124.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel124.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel124.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel124.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel124.Location = new System.Drawing.Point(1046, 396);
            this.htTitlePanel124.Name = "htTitlePanel124";
            this.htTitlePanel124.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel124.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel124.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel124.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel124.TabIndex = 24;
            this.htTitlePanel124.Tag = "7";
            this.htTitlePanel124.Text = "PG8";
            this.htTitlePanel124.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel124.TitleHeight = 25;
            // 
            // htTitlePanel125
            // 
            this.htTitlePanel125.BackColor = System.Drawing.Color.Transparent;
            this.htTitlePanel125.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htTitlePanel125.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.htTitlePanel125.ForeColor = System.Drawing.Color.White;
            this.htTitlePanel125.Location = new System.Drawing.Point(1046, 231);
            this.htTitlePanel125.Name = "htTitlePanel125";
            this.htTitlePanel125.Padding = new System.Windows.Forms.Padding(0, 25, 0, 0);
            this.htTitlePanel125.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htTitlePanel125.Size = new System.Drawing.Size(55, 50);
            this.htTitlePanel125.Style = HYC.WindowsControls.HTStyle.Custom;
            this.htTitlePanel125.TabIndex = 13;
            this.htTitlePanel125.Tag = "30";
            this.htTitlePanel125.Text = "PG31";
            this.htTitlePanel125.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(81)))), ((int)(((byte)(123)))));
            this.htTitlePanel125.TitleHeight = 25;
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 10000;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.ReshowDelay = 100;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Green;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(103, 704);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 44);
            this.label1.TabIndex = 14;
            this.label1.Text = "有产品";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(1, 704);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 44);
            this.label2.TabIndex = 15;
            this.label2.Text = "无产品";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Red;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(307, 704);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 44);
            this.label3.TabIndex = 17;
            this.label3.Text = "AA区失败";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Blue;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(715, 704);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 44);
            this.label5.TabIndex = 21;
            this.label5.Text = "算法AA区NG";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Lime;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(1021, 704);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 44);
            this.label6.TabIndex = 20;
            this.label6.Text = "Demura OK";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.OrangeRed;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(511, 704);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 44);
            this.label7.TabIndex = 19;
            this.label7.Text = "擦除失败";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Brown;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(409, 704);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 44);
            this.label8.TabIndex = 18;
            this.label8.Text = "压接失败";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Fuchsia;
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(205, 704);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 44);
            this.label9.TabIndex = 25;
            this.label9.Text = "对位失败";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Goldenrod;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label10.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(613, 704);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 44);
            this.label10.TabIndex = 24;
            this.label10.Text = "写入失败";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label11.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.Location = new System.Drawing.Point(1123, 704);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 44);
            this.label11.TabIndex = 23;
            this.label11.Text = "Demura失败";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.Aqua;
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label13.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.Location = new System.Drawing.Point(817, 704);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 44);
            this.label13.TabIndex = 27;
            this.label13.Text = "前处理失败";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.BlueViolet;
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label14.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label14.Location = new System.Drawing.Point(919, 704);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 44);
            this.label14.TabIndex = 26;
            this.label14.Text = "后处理失败";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UCStatusMonitor
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabMonitor);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(1608, 912);
            this.MinimumSize = new System.Drawing.Size(1608, 912);
            this.Name = "UCStatusMonitor";
            this.Size = new System.Drawing.Size(1608, 912);
            this.tabMonitor.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private HYC.WindowsControls.HTTitlePanel htTitlePanel1;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel2;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel5;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel6;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel7;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel8;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel9;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel10;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel11;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel12;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel13;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel14;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel15;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel16;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel17;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel18;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel19;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel20;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel21;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel22;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel23;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel24;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel25;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel26;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel27;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel28;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel29;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel30;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel31;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel32;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel33;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel34;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel35;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel36;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel37;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel38;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel39;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel40;
        private HYC.WindowsControls.HTTitlePanel Arm1_A_1;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel4;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel41;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel42;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel43;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel44;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel45;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel46;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel3;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel47;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel48;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel49;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel50;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel51;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel52;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel53;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel54;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel55;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel56;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel57;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel58;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel59;
        private HYC.WindowsControls.HTTabControl tabMonitor;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel62;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel61;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel60;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel63;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel64;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel65;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel66;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel67;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel68;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel69;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel70;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel71;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel72;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel73;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel74;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel75;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel76;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel77;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel78;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel79;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel80;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel81;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel82;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel83;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel84;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel85;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel86;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel87;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel88;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel89;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel90;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel91;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel92;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel93;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel94;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel95;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel96;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel97;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel98;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel99;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel100;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel101;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel102;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel103;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel104;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel105;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel106;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel107;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel108;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel109;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel110;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel111;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel112;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel113;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel114;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel115;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel116;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel117;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel118;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel119;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel120;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel121;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel122;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel123;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel124;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel125;
        private System.Windows.Forms.ToolTip toolTip1;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel135;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel131;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel127;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel134;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel130;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel133;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel129;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel126;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel132;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel128;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel137;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel136;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel149;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel147;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel146;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel143;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel142;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel139;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel148;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel145;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel141;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel144;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel138;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel140;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel150;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel151;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel152;
        private HYC.WindowsControls.HTTitlePanel htTitlePanel153;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labTarget_InA_2;
        private System.Windows.Forms.Label labTarget_InA_1;
        private System.Windows.Forms.Label labTarget_InB_1;
        private System.Windows.Forms.Label labTarget_InB_2;
        private System.Windows.Forms.Label labTarget_OutA_2;
        private System.Windows.Forms.Label labTarget_OutA_1;
        private System.Windows.Forms.Label labTarget_OutB_1;
        private System.Windows.Forms.Label labTarget_OutB_2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
    }
}
