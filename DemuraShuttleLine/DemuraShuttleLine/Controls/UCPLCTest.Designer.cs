﻿
namespace DemuraShuttleLine.Controls
{
    partial class UCPLCTest
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.htLabel1 = new HYC.WindowsControls.HTLabel();
            this.tbAddress = new HYC.WindowsControls.HTTextBox();
            this.btnRead = new HYC.WindowsControls.HTButton();
            this.btnWrite = new HYC.WindowsControls.HTButton();
            this.htLabel2 = new HYC.WindowsControls.HTLabel();
            this.tbLength = new HYC.WindowsControls.HTTextBox();
            this.htLabel3 = new HYC.WindowsControls.HTLabel();
            this.htLabel4 = new HYC.WindowsControls.HTLabel();
            this.tbDatas = new HYC.WindowsControls.HTTextBox();
            this.cmbType = new HYC.WindowsControls.HTComboBox();
            this.htLabel5 = new HYC.WindowsControls.HTLabel();
            this.lbShow = new HYC.WindowsControls.HTTextBox();
            this.htGroupBox1 = new HYC.WindowsControls.HTGroupBox();
            this.htGroupBox2 = new HYC.WindowsControls.HTGroupBox();
            this.htLabel6 = new HYC.WindowsControls.HTLabel();
            this.btnScan = new HYC.WindowsControls.HTButton();
            this.htLabel10 = new HYC.WindowsControls.HTLabel();
            this.tbSign = new HYC.WindowsControls.HTTextBox();
            this.tbScanResult = new HYC.WindowsControls.HTTextBox();
            this.htGroupBox1.SuspendLayout();
            this.htGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // htLabel1
            // 
            this.htLabel1.BackColor = System.Drawing.Color.Transparent;
            this.htLabel1.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htLabel1.Location = new System.Drawing.Point(77, 59);
            this.htLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.htLabel1.Name = "htLabel1";
            this.htLabel1.Size = new System.Drawing.Size(85, 29);
            this.htLabel1.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htLabel1.TabIndex = 1;
            this.htLabel1.Text = "PLC地址：";
            this.htLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbAddress
            // 
            this.tbAddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbAddress.FillColor = System.Drawing.Color.White;
            this.tbAddress.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.tbAddress.Location = new System.Drawing.Point(182, 59);
            this.tbAddress.Margin = new System.Windows.Forms.Padding(4);
            this.tbAddress.Maximum = 2147483647D;
            this.tbAddress.Minimum = -2147483648D;
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.Padding = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.tbAddress.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.tbAddress.Size = new System.Drawing.Size(324, 26);
            this.tbAddress.Style = HYC.WindowsControls.HTStyle.Gray;
            this.tbAddress.TabIndex = 2;
            // 
            // btnRead
            // 
            this.btnRead.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRead.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnRead.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnRead.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnRead.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.btnRead.Location = new System.Drawing.Point(672, 223);
            this.btnRead.Margin = new System.Windows.Forms.Padding(4);
            this.btnRead.Name = "btnRead";
            this.btnRead.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnRead.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnRead.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnRead.Size = new System.Drawing.Size(133, 44);
            this.btnRead.Style = HYC.WindowsControls.HTStyle.Gray;
            this.btnRead.TabIndex = 3;
            this.btnRead.Text = "读取";
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // btnWrite
            // 
            this.btnWrite.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnWrite.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnWrite.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnWrite.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnWrite.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.btnWrite.Location = new System.Drawing.Point(672, 323);
            this.btnWrite.Margin = new System.Windows.Forms.Padding(4);
            this.btnWrite.Name = "btnWrite";
            this.btnWrite.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnWrite.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnWrite.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnWrite.Size = new System.Drawing.Size(133, 44);
            this.btnWrite.Style = HYC.WindowsControls.HTStyle.Gray;
            this.btnWrite.TabIndex = 3;
            this.btnWrite.Text = "写入";
            this.btnWrite.Click += new System.EventHandler(this.btnWrite_Click);
            // 
            // htLabel2
            // 
            this.htLabel2.BackColor = System.Drawing.Color.Transparent;
            this.htLabel2.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htLabel2.Location = new System.Drawing.Point(77, 291);
            this.htLabel2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.htLabel2.Name = "htLabel2";
            this.htLabel2.Size = new System.Drawing.Size(85, 29);
            this.htLabel2.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htLabel2.TabIndex = 1;
            this.htLabel2.Text = "操作结果：";
            this.htLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbLength
            // 
            this.tbLength.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbLength.FillColor = System.Drawing.Color.White;
            this.tbLength.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.tbLength.Location = new System.Drawing.Point(182, 132);
            this.tbLength.Margin = new System.Windows.Forms.Padding(4);
            this.tbLength.Maximum = 2147483647D;
            this.tbLength.Minimum = -2147483648D;
            this.tbLength.Name = "tbLength";
            this.tbLength.Padding = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.tbLength.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.tbLength.Size = new System.Drawing.Size(324, 26);
            this.tbLength.Style = HYC.WindowsControls.HTStyle.Gray;
            this.tbLength.TabIndex = 2;
            // 
            // htLabel3
            // 
            this.htLabel3.BackColor = System.Drawing.Color.Transparent;
            this.htLabel3.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htLabel3.Location = new System.Drawing.Point(74, 132);
            this.htLabel3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.htLabel3.Name = "htLabel3";
            this.htLabel3.Size = new System.Drawing.Size(94, 29);
            this.htLabel3.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htLabel3.TabIndex = 1;
            this.htLabel3.Text = "读写长度：";
            this.htLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // htLabel4
            // 
            this.htLabel4.BackColor = System.Drawing.Color.Transparent;
            this.htLabel4.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htLabel4.Location = new System.Drawing.Point(557, 59);
            this.htLabel4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.htLabel4.Name = "htLabel4";
            this.htLabel4.Size = new System.Drawing.Size(107, 29);
            this.htLabel4.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htLabel4.TabIndex = 1;
            this.htLabel4.Text = "写入数据：";
            this.htLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbDatas
            // 
            this.tbDatas.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbDatas.FillColor = System.Drawing.Color.White;
            this.tbDatas.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.tbDatas.Location = new System.Drawing.Point(672, 59);
            this.tbDatas.Margin = new System.Windows.Forms.Padding(4);
            this.tbDatas.Maximum = 2147483647D;
            this.tbDatas.Minimum = -2147483648D;
            this.tbDatas.Multiline = true;
            this.tbDatas.Name = "tbDatas";
            this.tbDatas.Padding = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.tbDatas.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.tbDatas.Size = new System.Drawing.Size(324, 86);
            this.tbDatas.Style = HYC.WindowsControls.HTStyle.Gray;
            this.tbDatas.TabIndex = 2;
            // 
            // cmbType
            // 
            this.cmbType.FillColor = System.Drawing.Color.White;
            this.cmbType.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.cmbType.Items.AddRange(new object[] {
            "string",
            "ushort",
            "True",
            "False"});
            this.cmbType.Location = new System.Drawing.Point(181, 203);
            this.cmbType.Margin = new System.Windows.Forms.Padding(4);
            this.cmbType.MinimumSize = new System.Drawing.Size(84, 0);
            this.cmbType.Name = "cmbType";
            this.cmbType.Padding = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.cmbType.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.cmbType.Size = new System.Drawing.Size(324, 37);
            this.cmbType.Style = HYC.WindowsControls.HTStyle.Gray;
            this.cmbType.TabIndex = 5;
            this.cmbType.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // htLabel5
            // 
            this.htLabel5.BackColor = System.Drawing.Color.Transparent;
            this.htLabel5.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htLabel5.Location = new System.Drawing.Point(74, 203);
            this.htLabel5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.htLabel5.Name = "htLabel5";
            this.htLabel5.Size = new System.Drawing.Size(85, 29);
            this.htLabel5.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htLabel5.TabIndex = 1;
            this.htLabel5.Text = "读写类型：";
            this.htLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbShow
            // 
            this.lbShow.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.lbShow.FillColor = System.Drawing.Color.White;
            this.lbShow.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.lbShow.Location = new System.Drawing.Point(179, 291);
            this.lbShow.Margin = new System.Windows.Forms.Padding(4);
            this.lbShow.Maximum = 2147483647D;
            this.lbShow.Minimum = -2147483648D;
            this.lbShow.Multiline = true;
            this.lbShow.Name = "lbShow";
            this.lbShow.Padding = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.lbShow.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.lbShow.Size = new System.Drawing.Size(324, 86);
            this.lbShow.Style = HYC.WindowsControls.HTStyle.Gray;
            this.lbShow.TabIndex = 6;
            // 
            // htGroupBox1
            // 
            this.htGroupBox1.Controls.Add(this.htLabel1);
            this.htGroupBox1.Controls.Add(this.lbShow);
            this.htGroupBox1.Controls.Add(this.htLabel3);
            this.htGroupBox1.Controls.Add(this.cmbType);
            this.htGroupBox1.Controls.Add(this.htLabel4);
            this.htGroupBox1.Controls.Add(this.btnWrite);
            this.htGroupBox1.Controls.Add(this.htLabel5);
            this.htGroupBox1.Controls.Add(this.btnRead);
            this.htGroupBox1.Controls.Add(this.htLabel2);
            this.htGroupBox1.Controls.Add(this.tbDatas);
            this.htGroupBox1.Controls.Add(this.tbAddress);
            this.htGroupBox1.Controls.Add(this.tbLength);
            this.htGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.htGroupBox1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htGroupBox1.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.htGroupBox1.Margin = new System.Windows.Forms.Padding(10);
            this.htGroupBox1.Name = "htGroupBox1";
            this.htGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.htGroupBox1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htGroupBox1.Size = new System.Drawing.Size(1041, 443);
            this.htGroupBox1.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htGroupBox1.TabIndex = 7;
            this.htGroupBox1.Text = "PLC测试";
            // 
            // htGroupBox2
            // 
            this.htGroupBox2.Controls.Add(this.htLabel6);
            this.htGroupBox2.Controls.Add(this.btnScan);
            this.htGroupBox2.Controls.Add(this.htLabel10);
            this.htGroupBox2.Controls.Add(this.tbSign);
            this.htGroupBox2.Controls.Add(this.tbScanResult);
            this.htGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.htGroupBox2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htGroupBox2.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htGroupBox2.Location = new System.Drawing.Point(0, 443);
            this.htGroupBox2.Margin = new System.Windows.Forms.Padding(10);
            this.htGroupBox2.Name = "htGroupBox2";
            this.htGroupBox2.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.htGroupBox2.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htGroupBox2.Size = new System.Drawing.Size(1041, 307);
            this.htGroupBox2.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htGroupBox2.TabIndex = 8;
            this.htGroupBox2.Text = "扫码枪测试";
            // 
            // htLabel6
            // 
            this.htLabel6.BackColor = System.Drawing.Color.Transparent;
            this.htLabel6.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htLabel6.Location = new System.Drawing.Point(74, 59);
            this.htLabel6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.htLabel6.Name = "htLabel6";
            this.htLabel6.Size = new System.Drawing.Size(85, 29);
            this.htLabel6.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htLabel6.TabIndex = 1;
            this.htLabel6.Text = "扫码标识：";
            this.htLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnScan
            // 
            this.btnScan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnScan.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnScan.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnScan.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnScan.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.btnScan.Location = new System.Drawing.Point(672, 59);
            this.btnScan.Margin = new System.Windows.Forms.Padding(4);
            this.btnScan.Name = "btnScan";
            this.btnScan.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnScan.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnScan.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnScan.Size = new System.Drawing.Size(133, 44);
            this.btnScan.Style = HYC.WindowsControls.HTStyle.Gray;
            this.btnScan.TabIndex = 3;
            this.btnScan.Text = "扫码";
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // htLabel10
            // 
            this.htLabel10.BackColor = System.Drawing.Color.Transparent;
            this.htLabel10.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htLabel10.Location = new System.Drawing.Point(75, 107);
            this.htLabel10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.htLabel10.Name = "htLabel10";
            this.htLabel10.Size = new System.Drawing.Size(84, 29);
            this.htLabel10.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htLabel10.TabIndex = 1;
            this.htLabel10.Text = "操作结果：";
            this.htLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbSign
            // 
            this.tbSign.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbSign.FillColor = System.Drawing.Color.White;
            this.tbSign.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.tbSign.Location = new System.Drawing.Point(182, 59);
            this.tbSign.Margin = new System.Windows.Forms.Padding(4);
            this.tbSign.Maximum = 2147483647D;
            this.tbSign.Minimum = -2147483648D;
            this.tbSign.Name = "tbSign";
            this.tbSign.Padding = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.tbSign.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.tbSign.Size = new System.Drawing.Size(324, 26);
            this.tbSign.Style = HYC.WindowsControls.HTStyle.Gray;
            this.tbSign.TabIndex = 2;
            this.tbSign.Text = "*";
            // 
            // tbScanResult
            // 
            this.tbScanResult.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbScanResult.FillColor = System.Drawing.Color.White;
            this.tbScanResult.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.tbScanResult.Location = new System.Drawing.Point(182, 110);
            this.tbScanResult.Margin = new System.Windows.Forms.Padding(4);
            this.tbScanResult.Maximum = 2147483647D;
            this.tbScanResult.Minimum = -2147483648D;
            this.tbScanResult.Name = "tbScanResult";
            this.tbScanResult.Padding = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.tbScanResult.ReadOnly = true;
            this.tbScanResult.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.tbScanResult.Size = new System.Drawing.Size(324, 26);
            this.tbScanResult.Style = HYC.WindowsControls.HTStyle.Gray;
            this.tbScanResult.TabIndex = 2;
            // 
            // UCPLCTest
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.htGroupBox2);
            this.Controls.Add(this.htGroupBox1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UCPLCTest";
            this.Size = new System.Drawing.Size(1041, 750);
            this.htGroupBox1.ResumeLayout(false);
            this.htGroupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private HYC.WindowsControls.HTLabel htLabel1;
        private HYC.WindowsControls.HTTextBox tbAddress;
        private HYC.WindowsControls.HTButton btnRead;
        private HYC.WindowsControls.HTButton btnWrite;
        private HYC.WindowsControls.HTLabel htLabel2;
        private HYC.WindowsControls.HTTextBox tbLength;
        private HYC.WindowsControls.HTLabel htLabel3;
        private HYC.WindowsControls.HTLabel htLabel4;
        private HYC.WindowsControls.HTTextBox tbDatas;
        private HYC.WindowsControls.HTComboBox cmbType;
        private HYC.WindowsControls.HTLabel htLabel5;
        private HYC.WindowsControls.HTTextBox lbShow;
        private HYC.WindowsControls.HTGroupBox htGroupBox1;
        private HYC.WindowsControls.HTGroupBox htGroupBox2;
        private HYC.WindowsControls.HTLabel htLabel6;
        private HYC.WindowsControls.HTButton btnScan;
        private HYC.WindowsControls.HTLabel htLabel10;
        private HYC.WindowsControls.HTTextBox tbSign;
        private HYC.WindowsControls.HTTextBox tbScanResult;
    }
}
