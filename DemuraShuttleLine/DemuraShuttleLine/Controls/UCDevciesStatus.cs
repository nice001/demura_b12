﻿using HYC.AutoPosition.UI.Controls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace DemuraShuttleLine.Controls
{
    public partial class UCDevciesStatus : UserControl
    {
        public UCDevciesStatus()
        {
            InitializeComponent();
            InitUnit();
        }

        Dictionary<int, DevStatusIndicator> unitList = new Dictionary<int, DevStatusIndicator>();
        void InitUnit()
        {
            int startY = 0;
            ControlCollection controlList = this.pnlMiddle.Controls;
            foreach (Control item in controlList)
            {
                if (item.Visible)
                    startY += item.Height + 1;
            }
            for (int i = 1; i <= Properties.Settings.Default.unit_num; i++)
            {
                DevStatusIndicator devStatueUnit = new DevStatusIndicator();
                devStatueUnit.Location = new Point(0, startY);
                devStatueUnit.DevName = "单体" + i;
                devStatueUnit.Tag = DateTime.Now;
                devStatueUnit.AutoScaleMode = AutoScaleMode.None;
                devStatueUnit.Status = false;
                startY = startY + devStatueUnit.Height + 1;
                this.pnlMiddle.Controls.Add(devStatueUnit);
                unitList.Add(i, devStatueUnit);
            }
            timerStatus.Enabled = true;
        }

        public void SetUnitStatus(int unit_index)
        {
            DevStatusIndicator tmpControl = unitList.ContainsKey(unit_index) ? unitList[unit_index] : null;
            if (tmpControl == null)
                return;
            tmpControl.Status = true;
            tmpControl.Tag = DateTime.Now;
        }

        public void SeLineStatus(bool status)
        {
            devStatusUPLine.Status = status;
        }
        private bool lastStatus = false;
        public void SetPLCConnect(bool status)
        {
            if (status != lastStatus)
            {
                if (!status)
                {
                    devStatusPLC.Status = false;
                    imbtnStatus.Text = "Down";
                    imbtnStatus.BackColor = Color.Red;
                }
                else
                {
                    devStatusPLC.Status = true;
                }
                lastStatus = status;
            }
        }
        public void SetPLCStatus(int status)
        {
           
            devStatusPLC.Status = true;
            string text = "";
            switch (status)
            {
                case 1:
                    text = "RUN";
                    imbtnStatus.BackColor = Color.Green;
                    break;
                case 2:
                    text = "DOWN";
                    imbtnStatus.BackColor = Color.Red;
                    break;
                case 6:
                    text = "IDLE";
                    imbtnStatus.BackColor = Color.Yellow;
                    break;
                case 7:
                    text = "PM";
                    imbtnStatus.BackColor = Color.Blue;
                    break;
                default:
                    text = "UnKown";
                    imbtnStatus.BackColor = Color.BlueViolet;
                    break;
                
                //case 1:
                //    imbtnStatus.Text = "有屏IDLE-Y";
                //    imbtnStatus.BackColor = Color.Yellow;
                //    break;
                //case 2:
                //    imbtnStatus.Text = "Run";
                //    imbtnStatus.BackColor = Color.Green;
                //    break;
                //case 3:
                //    imbtnStatus.Text = "Down";
                //    imbtnStatus.BackColor = Color.Red;
                //    break;
                //case 4:
                //    imbtnStatus.Text = "IDLEE-OP";
                //    imbtnStatus.BackColor = Color.Yellow;
                //    break;
                //case 5:
                //    imbtnStatus.Text = "无屏IDLE-N";
                //    imbtnStatus.BackColor = Color.Yellow;
                //    break;
                //default:
                //    imbtnStatus.Text = "状态未知";
                //    imbtnStatus.BackColor = Color.Blue;
                //    break;
            }
            imbtnStatus.Text = text + "(" + status + ")";           
            imbtnStatus.Tag = DateTime.Now;

        }

        public void SetScanerStatus(string scaner_no, bool status)
        {
            devStatusScaner.Status = status;
            devStatusScaner.Tag = DateTime.Now;
        }

        private void timerStatus_Tick(object sender, EventArgs e)
        {
            DateTime now = DateTime.Now;
            ControlCollection list = this.pnlMiddle.Controls;
            DateTime lastTime;
            foreach (Control item in list)
            {
                if (item.GetType() != typeof(DevStatusIndicator))
                    continue;
                DevStatusIndicator statusControl = (DevStatusIndicator)item;
                if (statusControl.Tag == null)
                    continue;
                if (!(bool)statusControl.Status)
                    continue;
                lastTime = (DateTime)statusControl.Tag;
                if (now.Subtract(lastTime).TotalSeconds > Program.softRunParamaters.UnitTimeout)
                {
                    this.Invoke(new Action(() => { statusControl.Status = false; }));
                }
            }
            //if (imbtnStatus.BackColor == Color.Red)
            //    return;
            //lastTime = (DateTime)imbtnStatus.Tag;
            //if (now.Subtract(lastTime).TotalSeconds > Program.softRunParamaters.UnitTimeout)
            //{
            //    this.Invoke(new Action(() =>
            //    {
            //        imbtnStatus.Text = "Down";
            //        imbtnStatus.BackColor = Color.Red;
            //    }));
            //}
        }

        private void UCDevciesStatus_Resize(object sender, EventArgs e)
        {
            this.Width = 288;
        }

        public void ShowOperateInfo(string msg)
        {
            string showMsg = DateTime.Now.ToString("MM-dd HH:mm:ss") + " " + msg;
            if (this.InvokeRequired)
                this.Invoke(new Action(() => mlistbShow.AddMessage(showMsg, Brushes.Black)));
            else
                mlistbShow.AddMessage(showMsg, Brushes.Black);
        }

        public void SetRecipelID(string id)
        {
            lbRecipelID.LabTextValue = id;
        }

        public void SetPPID(string id)
        {
            lbPPID.LabTextValue = id;
        }
    }
}
