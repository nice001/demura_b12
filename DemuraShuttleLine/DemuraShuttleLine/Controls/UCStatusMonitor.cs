﻿using DemuraDB;
using DemuraShuttleLine.Business;
using HYC.WindowsControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemuraShuttleLine.Controls
{
    public enum ResultType
    {
        [Description("待测试")]
        None = 0,
        [Description("OK")]
        DemuraOK,
        [Description("点灯NG")]
        PowerOnNG,
        [Description("调整NG")]
        AlignmentNG,
        [Description("UI AA区NG")]
        AAreaSizeNG,
        [Description("清晰度NG")]
        ArticulationNG,
        [Description("灰度NG")]
        GrayLevelNG,
        [Description("前处理NG")]
        PreProcessNG,
        [Description("算法AA区NG")]
        PreAlgAANG,
        [Description("算法Mark点NG")]
        PreAlgMarkNG,
        //[Description("算法错误NG(-1)")]
        //PreAlgErrorNG,
        //[Description("算法运行异常NG")]
        //PreAlgExceptionNG,
        [Description("后处理NG")]
        LastProcessNG,
        [Description("文件传输NG")]
        FileTransferNG,
        [Description("擦除NG")]
        ICEraseNG,
        [Description("烧录NG")]
        ICFlashNG,
        [Description("其它NG")]
        OthersNG,     //包括微调电机调整失败，微调电机异常
        [Description("流程异常NG")]
        ExceptionNG,
    }
    public partial class UCStatusMonitor : UserControl
    {
        PLCManage plcManage;
        DemuraBaseDB db = DemuraBaseDB.GetDemuraDB();
        private Dictionary<ResultType, Color> ShowColorDic = new Dictionary<ResultType, Color>
        {
            {ResultType.None,Color.Green },
            {ResultType.DemuraOK,Color.Lime },
            {ResultType.PowerOnNG,Color.Brown },
            {ResultType.AlignmentNG,Color.Fuchsia },
            {ResultType.AAreaSizeNG,Color.Red },
            {ResultType.PreProcessNG,Color.Aqua },
            {ResultType.PreAlgAANG,Color.Blue },
           // {ResultType.PreAlgMarkNG,Color.Khaki },
            {ResultType.LastProcessNG,Color.BlueViolet },
            {ResultType.ICEraseNG,Color.OrangeRed },
            {ResultType.ICFlashNG,Color.Goldenrod },
            //{ResultType.ExceptionNG,Color.Aqua },
            //{ResultType.OthersNG,Color.BlueViolet }

        };
        public UCStatusMonitor()
        {
            InitializeComponent();
            tabMonitor.SelectedIndex = 1;
        }

        public void SetPlcManage(PLCManage _plcManage)
        {
            plcManage = _plcManage;
            Task.Factory.StartNew(() => { MonitorTestResultTask(); });
        }

        private void MonitorTestResultTask()
        {
            while (true)
            {
                try
                {
                    this.Invoke(new Action(() =>
                    {
                        ControlCollection collection = tabMonitor.SelectedTab.Controls;
                        lock (lastChangeControls)
                        {
                            foreach (HTTitlePanel item in lastChangeControls)
                            {
                                if (item.GetType() != typeof(HTTitlePanel))
                                    continue;
                                string panelId = item.TagString.Trim('\0');
                                int index = int.Parse(item.Tag.ToString());
                                if (index < 48 || index == 104 || index == 105)   ///   Test工位需要刷新
                                {
                                    Color color = ShowColorDic[ResultType.None];
                                    int result = db.GetDemuraErrorCodeByPanelId(panelId);
                                    if (result >= 0) ///  有结果
                                    {
                                        ResultType type = (ResultType)result;
                                        if (ShowColorDic.ContainsKey(type))
                                        {
                                            color = ShowColorDic[type];
                                        }
                                        else
                                        {
                                            color = Color.FromArgb(192, 0, 0); //// NG
                                        }
                                    }
                                    item.Invoke(new Action(() => { item.FillColor = color; }));
                                    Thread.Sleep(5);
                                }
                                else
                                {
                                    continue;
                                }
                            }
                        }
                    }));
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    Thread.Sleep(5000);
                }
            }

        }
        List<HTTitlePanel> lastChangeControls = new List<HTTitlePanel>();
        public void UpdatePanelStatus(List<int> trueStatus, List<int> falseStatus)
        {
            if (plcManage == null)
                return;
            foreach (int tmp in falseStatus)
            {
                for (int i = 0; i < lastChangeControls.Count; i++)
                {
                    HTTitlePanel tmpPanel = lastChangeControls[i];
                    if (int.Parse(tmpPanel.Tag.ToString()) == tmp)
                    {
                        UpDataTargetPos(tmp, tmpPanel.TagString, false);
                        FillTitlePanel(tmpPanel, SystemColors.Control, "");
                        Console.WriteLine("状态移除，索引：" + tmp);
                        lastChangeControls.RemoveAt(i);
                        i--;
                        break;
                    }
                }
            }
            ControlCollection collection = tabMonitor.SelectedTab.Controls;
            foreach (int item in trueStatus)
            {
                foreach (Control tmp in collection)
                {
                    if (int.Parse(tmp.Tag.ToString()) == item)
                    {
                        if (tmp.GetType() != typeof(HTTitlePanel))
                            continue;
                        HTTitlePanel tmpPanel = (HTTitlePanel)tmp;
                        tmpPanel.FillColor = ShowColorDic[ResultType.None];
                        Console.WriteLine("状态新增，索引：" + tmp);
                        string panelId = plcManage.GetPanelIdByPosition(item);
                        tmpPanel.TagString = panelId;
                        toolTip1.SetToolTip(tmpPanel, panelId);
                        UpDataTargetPos(item, panelId, true);
                        Task.Factory.StartNew(() =>
                        {
                            try
                            {
                                Color color = ShowColorDic[ResultType.None];
                                int result = db.GetDemuraErrorCodeByPanelId(panelId);
                                if (result >= 0) ///  有结果
                                {
                                    ResultType type = (ResultType)result;
                                    if (ShowColorDic.ContainsKey(type))
                                    {
                                        color = ShowColorDic[type];
                                    }
                                    else
                                    {
                                        color = Color.FromArgb(192, 0, 0); //// NG
                                    }
                                }
                                tmpPanel.Invoke(new Action(() => { tmpPanel.FillColor = color; }));
                            }
                            catch { }
                        });
                        lastChangeControls.Add(tmpPanel);
                        break;
                    }
                }
            }
        }
        private void UpDataTargetPos(int posIndex, string panelID, bool visible)
        {
            try
            {
                int pos = 0;
                int pg = 0;
                Entity.ArmType armType = Entity.ArmType.In_A;
                if (posIndex == 64 || posIndex == 65)
                {
                    armType = Entity.ArmType.In_A;
                    pos = plcManage.GetArmTargetPos(Entity.ArmType.In_A);
                }
                else if (posIndex == 80 || posIndex == 81)
                {
                    armType = Entity.ArmType.In_B;
                    pos = plcManage.GetArmTargetPos(Entity.ArmType.In_B);
                }
                else if (posIndex == 68 || posIndex == 69)
                {
                    armType = Entity.ArmType.Out_A;

                }
                else if (posIndex == 84 || posIndex == 85)
                {
                    armType = Entity.ArmType.Out_B;
                }

                if (armType == Entity.ArmType.In_A || armType == Entity.ArmType.In_B)
                {
                    pg = pos * 2;
                    if (posIndex % 2 == 0) //  第一片料
                    {
                        pg = pos * 2 - 1;
                    }
                }
                else /// 出Arm 从数据库查找,PLC 获取不到
                {
                    //pg = pos * 2 + 24;
                    //if (posIndex % 2 == 0)
                    //{
                    //    pg = pos * 2 + 23;
                    //}
                    pg = db.GetDemuraPGIndex(panelID);
                }
                this.Invoke(new Action(() =>
                {
                    ControlCollection collection = tabMonitor.SelectedTab.Controls;
                    foreach (var item in collection)
                    {
                        if (item.GetType() != typeof(Label))
                            continue;
                        Label lab = (Label)item;
                        if (lab.Tag.ToString() == posIndex.ToString())
                        {
                            lab.Visible = visible;

                            lab.Text = "目标-" + pg;

                        }
                    }
                }));
            }
            catch
            {
            }
        }

        private void tabMonitor_SelectedIndexChanged(object sender, EventArgs e)
        {
            ControlCollection collection = tabMonitor.SelectedTab.Controls;
            for (int i = 0; i < lastChangeControls.Count; i++)
            {
                HTTitlePanel lastPanel = lastChangeControls[i];
                foreach (var tmp in collection)
                {
                    if (tmp.GetType() != typeof(HTTitlePanel))
                        continue;
                    HTTitlePanel tmpPanel = (HTTitlePanel)tmp;
                    if (tmpPanel.Tag.ToString().Equals(lastPanel.Tag.ToString()))
                    {
                        UpDataTargetPos(int.Parse(lastPanel.Tag.ToString()), lastPanel.TagString, true);
                        FillTitlePanel(tmpPanel, lastPanel.FillColor, lastPanel.TagString);
                        lastChangeControls[i] = tmpPanel;
                        break;
                    }
                }
                UpDataTargetPos(int.Parse(lastPanel.Tag.ToString()), lastPanel.TagString, false);
                FillTitlePanel(lastPanel, SystemColors.Control, "");
            }
        }

        void FillTitlePanel(HTTitlePanel tmpPanel, Color tmoColor, string panelId)
        {
            tmpPanel.FillColor = tmoColor;
            tmpPanel.TagString = panelId;
            toolTip1.SetToolTip(tmpPanel, panelId);

        }

        private void label21_Click(object sender, EventArgs e)
        {
            int pg = db.GetDemuraPGIndex("20210429181943_1");
        }
    }
}
