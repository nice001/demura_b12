﻿
namespace DemuraShuttleLine.Controls
{
    partial class FrmMCRFailSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbPass = new HYC.WindowsControls.HTRadioButton();
            this.rbKeyIn = new HYC.WindowsControls.HTRadioButton();
            this.htLabel1 = new HYC.WindowsControls.HTLabel();
            this.tbPanelId = new HYC.WindowsControls.TextBoxs.HTIntTextBox();
            this.btnOk = new HYC.WindowsControls.HTButton();
            this.SuspendLayout();
            // 
            // rbPass
            // 
            this.rbPass.Checked = true;
            this.rbPass.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbPass.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.rbPass.Location = new System.Drawing.Point(50, 92);
            this.rbPass.Name = "rbPass";
            this.rbPass.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.rbPass.RadioButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.rbPass.Size = new System.Drawing.Size(85, 29);
            this.rbPass.Style = HYC.WindowsControls.HTStyle.Gray;
            this.rbPass.TabIndex = 0;
            this.rbPass.Text = "By Pass";
            // 
            // rbKeyIn
            // 
            this.rbKeyIn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbKeyIn.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.rbKeyIn.Location = new System.Drawing.Point(50, 34);
            this.rbKeyIn.Name = "rbKeyIn";
            this.rbKeyIn.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.rbKeyIn.RadioButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.rbKeyIn.Size = new System.Drawing.Size(85, 29);
            this.rbKeyIn.Style = HYC.WindowsControls.HTStyle.Gray;
            this.rbKeyIn.TabIndex = 0;
            this.rbKeyIn.Text = "Key In";
            this.rbKeyIn.ValueChanged += new HYC.WindowsControls.HTRadioButton.OnValueChanged(this.rbKeyIn_ValueChanged);
            // 
            // htLabel1
            // 
            this.htLabel1.AutoSize = true;
            this.htLabel1.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htLabel1.Location = new System.Drawing.Point(146, 39);
            this.htLabel1.Name = "htLabel1";
            this.htLabel1.Size = new System.Drawing.Size(80, 20);
            this.htLabel1.TabIndex = 1;
            this.htLabel1.Text = "Panel ID：";
            this.htLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbPanelId
            // 
            this.tbPanelId.Enabled = false;
            this.tbPanelId.Location = new System.Drawing.Point(221, 38);
            this.tbPanelId.Name = "tbPanelId";
            this.tbPanelId.Size = new System.Drawing.Size(147, 21);
            this.tbPanelId.TabIndex = 2;
            this.tbPanelId.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnOk
            // 
            this.btnOk.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOk.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnOk.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnOk.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnOk.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.btnOk.Location = new System.Drawing.Point(238, 92);
            this.btnOk.Name = "btnOk";
            this.btnOk.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnOk.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnOk.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnOk.Size = new System.Drawing.Size(100, 35);
            this.btnOk.Style = HYC.WindowsControls.HTStyle.Gray;
            this.btnOk.TabIndex = 3;
            this.btnOk.Text = "确定";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // FrmMCRFailSelect
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(413, 161);
            this.ControlBox = false;
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.tbPanelId);
            this.Controls.Add(this.htLabel1);
            this.Controls.Add(this.rbKeyIn);
            this.Controls.Add(this.rbPass);
            this.Name = "FrmMCRFailSelect";
            this.Text = "扫码失败";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private HYC.WindowsControls.HTRadioButton rbPass;
        private HYC.WindowsControls.HTRadioButton rbKeyIn;
        private HYC.WindowsControls.HTLabel htLabel1;
        private HYC.WindowsControls.TextBoxs.HTIntTextBox tbPanelId;
        private HYC.WindowsControls.HTButton btnOk;
    }
}