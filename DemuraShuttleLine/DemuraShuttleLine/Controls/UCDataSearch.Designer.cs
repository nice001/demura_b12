﻿
namespace DemuraShuttleLine.Controls
{
    partial class UCDataSearch
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.htPanel1 = new HYC.WindowsControls.HTPanel();
            this.btnExport = new HYC.WindowsControls.HTButton();
            this.tpEndTime = new System.Windows.Forms.DateTimePicker();
            this.tpStartTime = new System.Windows.Forms.DateTimePicker();
            this.htLabel6 = new HYC.WindowsControls.HTLabel();
            this.htLabel4 = new HYC.WindowsControls.HTLabel();
            this.btnSearch = new HYC.WindowsControls.HTButton();
            this.htGroupBox1 = new HYC.WindowsControls.HTGroupBox();
            this.cbNG = new HYC.WindowsControls.HTCheckBox();
            this.cbOK = new HYC.WindowsControls.HTCheckBox();
            this.tbPanelId = new HYC.WindowsControls.HTTextBox();
            this.htLabel2 = new HYC.WindowsControls.HTLabel();
            this.htLabel3 = new HYC.WindowsControls.HTLabel();
            this.htLabel1 = new HYC.WindowsControls.HTLabel();
            this.dgvData = new HYC.WindowsControls.HTDataGridView();
            this.htPanel2 = new HYC.WindowsControls.HTPanel();
            this.ucPageView = new DemuraShuttleLine.Controls.UCPageView();
            this.tbTestId = new HYC.WindowsControls.HTComboBox();
            this.tbUnitId = new HYC.WindowsControls.HTComboBox();
            this.colSN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colInTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOutTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.htPanel1.SuspendLayout();
            this.htGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.htPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // htPanel1
            // 
            this.htPanel1.Controls.Add(this.tbTestId);
            this.htPanel1.Controls.Add(this.tbUnitId);
            this.htPanel1.Controls.Add(this.btnExport);
            this.htPanel1.Controls.Add(this.tpEndTime);
            this.htPanel1.Controls.Add(this.tpStartTime);
            this.htPanel1.Controls.Add(this.htLabel6);
            this.htPanel1.Controls.Add(this.htLabel4);
            this.htPanel1.Controls.Add(this.btnSearch);
            this.htPanel1.Controls.Add(this.htGroupBox1);
            this.htPanel1.Controls.Add(this.tbPanelId);
            this.htPanel1.Controls.Add(this.htLabel2);
            this.htPanel1.Controls.Add(this.htLabel3);
            this.htPanel1.Controls.Add(this.htLabel1);
            this.htPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.htPanel1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htPanel1.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htPanel1.Location = new System.Drawing.Point(0, 0);
            this.htPanel1.Name = "htPanel1";
            this.htPanel1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htPanel1.Size = new System.Drawing.Size(1404, 140);
            this.htPanel1.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htPanel1.TabIndex = 1;
            this.htPanel1.Text = null;
            // 
            // btnExport
            // 
            this.btnExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExport.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnExport.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnExport.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnExport.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.btnExport.Location = new System.Drawing.Point(1212, 46);
            this.btnExport.Name = "btnExport";
            this.btnExport.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnExport.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnExport.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnExport.Size = new System.Drawing.Size(138, 38);
            this.btnExport.Style = HYC.WindowsControls.HTStyle.Gray;
            this.btnExport.TabIndex = 12;
            this.btnExport.Text = "导出数据";
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // tpEndTime
            // 
            this.tpEndTime.CustomFormat = "yyyy/MM/dd HH:mm:ss";
            this.tpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpEndTime.Location = new System.Drawing.Point(128, 60);
            this.tpEndTime.Name = "tpEndTime";
            this.tpEndTime.Size = new System.Drawing.Size(229, 26);
            this.tpEndTime.TabIndex = 10;
            this.tpEndTime.ValueChanged += new System.EventHandler(this.tpEndTime_ValueChanged);
            // 
            // tpStartTime
            // 
            this.tpStartTime.CustomFormat = "yyyy/MM/dd HH:mm:ss";
            this.tpStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpStartTime.Location = new System.Drawing.Point(128, 15);
            this.tpStartTime.Name = "tpStartTime";
            this.tpStartTime.Size = new System.Drawing.Size(229, 26);
            this.tpStartTime.TabIndex = 11;
            this.tpStartTime.ValueChanged += new System.EventHandler(this.tpStartTime_ValueChanged);
            // 
            // htLabel6
            // 
            this.htLabel6.AutoSize = true;
            this.htLabel6.BackColor = System.Drawing.Color.Transparent;
            this.htLabel6.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htLabel6.Location = new System.Drawing.Point(400, 100);
            this.htLabel6.Name = "htLabel6";
            this.htLabel6.Size = new System.Drawing.Size(79, 20);
            this.htLabel6.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htLabel6.TabIndex = 6;
            this.htLabel6.Text = "单体编号：";
            this.htLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // htLabel4
            // 
            this.htLabel4.AutoSize = true;
            this.htLabel4.BackColor = System.Drawing.Color.Transparent;
            this.htLabel4.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htLabel4.Location = new System.Drawing.Point(400, 57);
            this.htLabel4.Name = "htLabel4";
            this.htLabel4.Size = new System.Drawing.Size(79, 20);
            this.htLabel4.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htLabel4.TabIndex = 6;
            this.htLabel4.Text = "治具编号：";
            this.htLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnSearch
            // 
            this.btnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearch.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnSearch.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnSearch.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnSearch.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.btnSearch.Location = new System.Drawing.Point(1007, 46);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnSearch.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnSearch.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnSearch.Size = new System.Drawing.Size(138, 38);
            this.btnSearch.Style = HYC.WindowsControls.HTStyle.Gray;
            this.btnSearch.TabIndex = 5;
            this.btnSearch.Text = "查询";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // htGroupBox1
            // 
            this.htGroupBox1.Controls.Add(this.cbNG);
            this.htGroupBox1.Controls.Add(this.cbOK);
            this.htGroupBox1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htGroupBox1.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htGroupBox1.Location = new System.Drawing.Point(760, 13);
            this.htGroupBox1.Name = "htGroupBox1";
            this.htGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.htGroupBox1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htGroupBox1.Size = new System.Drawing.Size(188, 107);
            this.htGroupBox1.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htGroupBox1.TabIndex = 3;
            this.htGroupBox1.Text = "检测结果";
            // 
            // cbNG
            // 
            this.cbNG.BackColor = System.Drawing.Color.Transparent;
            this.cbNG.CheckBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.cbNG.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbNG.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.cbNG.Location = new System.Drawing.Point(45, 70);
            this.cbNG.Name = "cbNG";
            this.cbNG.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.cbNG.Size = new System.Drawing.Size(104, 29);
            this.cbNG.Style = HYC.WindowsControls.HTStyle.Gray;
            this.cbNG.TabIndex = 0;
            this.cbNG.Text = "产品NG";
            // 
            // cbOK
            // 
            this.cbOK.BackColor = System.Drawing.Color.Transparent;
            this.cbOK.CheckBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.cbOK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbOK.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.cbOK.Location = new System.Drawing.Point(45, 35);
            this.cbOK.Name = "cbOK";
            this.cbOK.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.cbOK.Size = new System.Drawing.Size(104, 29);
            this.cbOK.Style = HYC.WindowsControls.HTStyle.Gray;
            this.cbOK.TabIndex = 0;
            this.cbOK.Text = "产品OK";
            // 
            // tbPanelId
            // 
            this.tbPanelId.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbPanelId.FillColor = System.Drawing.Color.White;
            this.tbPanelId.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.tbPanelId.Location = new System.Drawing.Point(489, 13);
            this.tbPanelId.Maximum = 2147483647D;
            this.tbPanelId.Minimum = -2147483648D;
            this.tbPanelId.Name = "tbPanelId";
            this.tbPanelId.Padding = new System.Windows.Forms.Padding(5);
            this.tbPanelId.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.tbPanelId.Size = new System.Drawing.Size(234, 26);
            this.tbPanelId.Style = HYC.WindowsControls.HTStyle.Gray;
            this.tbPanelId.TabIndex = 2;
            // 
            // htLabel2
            // 
            this.htLabel2.AutoSize = true;
            this.htLabel2.BackColor = System.Drawing.Color.Transparent;
            this.htLabel2.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htLabel2.Location = new System.Drawing.Point(36, 63);
            this.htLabel2.Name = "htLabel2";
            this.htLabel2.Size = new System.Drawing.Size(79, 20);
            this.htLabel2.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htLabel2.TabIndex = 1;
            this.htLabel2.Text = "结束时间：";
            this.htLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // htLabel3
            // 
            this.htLabel3.AutoSize = true;
            this.htLabel3.BackColor = System.Drawing.Color.Transparent;
            this.htLabel3.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htLabel3.Location = new System.Drawing.Point(417, 15);
            this.htLabel3.Name = "htLabel3";
            this.htLabel3.Size = new System.Drawing.Size(66, 20);
            this.htLabel3.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htLabel3.TabIndex = 1;
            this.htLabel3.Text = "产品ID：";
            this.htLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // htLabel1
            // 
            this.htLabel1.AutoSize = true;
            this.htLabel1.BackColor = System.Drawing.Color.Transparent;
            this.htLabel1.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htLabel1.Location = new System.Drawing.Point(36, 18);
            this.htLabel1.Name = "htLabel1";
            this.htLabel1.Size = new System.Drawing.Size(79, 20);
            this.htLabel1.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htLabel1.TabIndex = 1;
            this.htLabel1.Text = "开始时间：";
            this.htLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToAddRows = false;
            this.dgvData.AllowUserToDeleteRows = false;
            this.dgvData.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.dgvData.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvData.BackgroundColor = System.Drawing.Color.White;
            this.dgvData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvData.ColumnHeadersHeight = 32;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colSN,
            this.Column4,
            this.Column2,
            this.Column3,
            this.colInTime,
            this.colOutTime,
            this.Column7,
            this.Column1,
            this.Column5,
            this.Column6});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvData.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvData.EnableHeadersVisualStyles = false;
            this.dgvData.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.dgvData.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.dgvData.Location = new System.Drawing.Point(0, 0);
            this.dgvData.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.dgvData.Name = "dgvData";
            this.dgvData.ReadOnly = true;
            this.dgvData.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.dgvData.RowHeadersVisible = false;
            this.dgvData.RowHeadersWidth = 51;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            this.dgvData.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvData.RowTemplate.Height = 29;
            this.dgvData.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvData.SelectedIndex = -1;
            this.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvData.ShowGridLine = true;
            this.dgvData.Size = new System.Drawing.Size(1404, 556);
            this.dgvData.StripeOddColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.dgvData.Style = HYC.WindowsControls.HTStyle.Gray;
            this.dgvData.TabIndex = 3;
            this.dgvData.TagString = null;
            // 
            // htPanel2
            // 
            this.htPanel2.Controls.Add(this.dgvData);
            this.htPanel2.Controls.Add(this.ucPageView);
            this.htPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.htPanel2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htPanel2.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htPanel2.Location = new System.Drawing.Point(0, 140);
            this.htPanel2.Name = "htPanel2";
            this.htPanel2.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htPanel2.Size = new System.Drawing.Size(1404, 585);
            this.htPanel2.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htPanel2.TabIndex = 4;
            this.htPanel2.Text = "htPanel2";
            // 
            // ucPageView
            // 
            this.ucPageView.AutoSize = true;
            this.ucPageView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucPageView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ucPageView.Location = new System.Drawing.Point(0, 556);
            this.ucPageView.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ucPageView.Name = "ucPageView";
            this.ucPageView.PageIndex = 1;
            this.ucPageView.PageNum = 1;
            this.ucPageView.PageSize = 10;
            this.ucPageView.Size = new System.Drawing.Size(1404, 29);
            this.ucPageView.TabIndex = 5;
            this.ucPageView.TotalSize = 1;
            // 
            // tbTestId
            // 
            this.tbTestId.BackColor = System.Drawing.SystemColors.Control;
            this.tbTestId.FillColor = System.Drawing.Color.White;
            this.tbTestId.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.tbTestId.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.tbTestId.Location = new System.Drawing.Point(489, 55);
            this.tbTestId.MinimumSize = new System.Drawing.Size(63, 0);
            this.tbTestId.Name = "tbTestId";
            this.tbTestId.Padding = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.tbTestId.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.tbTestId.Size = new System.Drawing.Size(234, 28);
            this.tbTestId.Style = HYC.WindowsControls.HTStyle.Gray;
            this.tbTestId.TabIndex = 16;
            this.tbTestId.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbUnitId
            // 
            this.tbUnitId.FillColor = System.Drawing.Color.White;
            this.tbUnitId.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.tbUnitId.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.tbUnitId.Location = new System.Drawing.Point(489, 101);
            this.tbUnitId.MinimumSize = new System.Drawing.Size(63, 0);
            this.tbUnitId.Name = "tbUnitId";
            this.tbUnitId.Padding = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.tbUnitId.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.tbUnitId.Size = new System.Drawing.Size(234, 25);
            this.tbUnitId.Style = HYC.WindowsControls.HTStyle.Gray;
            this.tbUnitId.TabIndex = 15;
            this.tbUnitId.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // colSN
            // 
            this.colSN.DataPropertyName = "PANELID";
            this.colSN.HeaderText = "产品ID";
            this.colSN.MinimumWidth = 6;
            this.colSN.Name = "colSN";
            this.colSN.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "MODULETYPE";
            this.Column4.HeaderText = "产品类型";
            this.Column4.MinimumWidth = 6;
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "UnitID";
            this.Column2.HeaderText = "单体编号";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "TESTERID";
            this.Column3.HeaderText = "治具编号";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // colInTime
            // 
            this.colInTime.DataPropertyName = "STARTTIME";
            this.colInTime.HeaderText = "检测开始时间";
            this.colInTime.MinimumWidth = 6;
            this.colInTime.Name = "colInTime";
            this.colInTime.ReadOnly = true;
            // 
            // colOutTime
            // 
            this.colOutTime.DataPropertyName = "ENDTIME";
            this.colOutTime.HeaderText = "检测结束时间";
            this.colOutTime.MinimumWidth = 6;
            this.colOutTime.Name = "colOutTime";
            this.colOutTime.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "CameraID";
            this.Column7.HeaderText = "相机编号";
            this.Column7.MinimumWidth = 6;
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "PowerOnResult";
            this.Column1.HeaderText = "检测结果";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "ERRORCODE";
            this.Column5.HeaderText = "NG编号";
            this.Column5.MinimumWidth = 6;
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "ErrorMessage";
            this.Column6.FillWeight = 200F;
            this.Column6.HeaderText = "NG信息";
            this.Column6.MinimumWidth = 6;
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // UCDataSearch
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.htPanel2);
            this.Controls.Add(this.htPanel1);
            this.Name = "UCDataSearch";
            this.Size = new System.Drawing.Size(1404, 725);
            this.htPanel1.ResumeLayout(false);
            this.htPanel1.PerformLayout();
            this.htGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.htPanel2.ResumeLayout(false);
            this.htPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private HYC.WindowsControls.HTPanel htPanel1;
        private HYC.WindowsControls.HTTextBox tbPanelId;
        private HYC.WindowsControls.HTLabel htLabel2;
        private HYC.WindowsControls.HTLabel htLabel3;
        private HYC.WindowsControls.HTLabel htLabel1;
        private HYC.WindowsControls.HTButton btnSearch;
        private HYC.WindowsControls.HTGroupBox htGroupBox1;
        private HYC.WindowsControls.HTCheckBox cbNG;
        private HYC.WindowsControls.HTCheckBox cbOK;
        private HYC.WindowsControls.HTDataGridView dgvData;
        private HYC.WindowsControls.HTPanel htPanel2;
        private HYC.WindowsControls.HTLabel htLabel6;
        private HYC.WindowsControls.HTLabel htLabel4;
        private UCPageView ucPageView;
        private System.Windows.Forms.DateTimePicker tpEndTime;
        private System.Windows.Forms.DateTimePicker tpStartTime;
        private HYC.WindowsControls.HTButton btnExport;
        private HYC.WindowsControls.HTComboBox tbTestId;
        private HYC.WindowsControls.HTComboBox tbUnitId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSN;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colInTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOutTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
    }
}
