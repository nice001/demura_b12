﻿
namespace DemuraShuttleLine.Controls
{
    partial class UCDevciesStatus
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.devStatusPLC = new HYC.AutoPosition.UI.Controls.DevStatusIndicator();
            this.devStatusScaner = new HYC.AutoPosition.UI.Controls.DevStatusIndicator();
            this.devStatusUPLine = new HYC.AutoPosition.UI.Controls.DevStatusIndicator();
            this.pnlDown = new System.Windows.Forms.Panel();
            this.mlistbShow = new HYC.AutoPosition.UI.Controls.MessageListBox();
            this.imbtnStatus = new System.Windows.Forms.Button();
            this.pnlMiddle = new System.Windows.Forms.Panel();
            this.htLabel1 = new HYC.WindowsControls.HTLabel();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.timerStatus = new System.Windows.Forms.Timer(this.components);
            this.htPanel1 = new HYC.WindowsControls.HTPanel();
            this.lbRecipelID = new HYC.WindowsControls.TextBoxs.HCLabelTextBox();
            this.lbPPID = new HYC.WindowsControls.TextBoxs.HCLabelTextBox();
            this.pnlDown.SuspendLayout();
            this.pnlMiddle.SuspendLayout();
            this.htPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // devStatusPLC
            // 
            this.devStatusPLC.DevName = "PLC";
            this.devStatusPLC.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.devStatusPLC.Location = new System.Drawing.Point(0, 0);
            this.devStatusPLC.Margin = new System.Windows.Forms.Padding(0);
            this.devStatusPLC.Name = "devStatusPLC";
            this.devStatusPLC.Size = new System.Drawing.Size(283, 28);
            this.devStatusPLC.Status = false;
            this.devStatusPLC.TabIndex = 0;
            // 
            // devStatusScaner
            // 
            this.devStatusScaner.DevName = "扫码枪";
            this.devStatusScaner.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.devStatusScaner.Location = new System.Drawing.Point(0, 29);
            this.devStatusScaner.Margin = new System.Windows.Forms.Padding(0);
            this.devStatusScaner.Name = "devStatusScaner";
            this.devStatusScaner.Size = new System.Drawing.Size(283, 28);
            this.devStatusScaner.Status = false;
            this.devStatusScaner.TabIndex = 1;
            this.devStatusScaner.Visible = false;
            // 
            // devStatusUPLine
            // 
            this.devStatusUPLine.DevName = "上游Line";
            this.devStatusUPLine.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.devStatusUPLine.Location = new System.Drawing.Point(0, 58);
            this.devStatusUPLine.Margin = new System.Windows.Forms.Padding(0);
            this.devStatusUPLine.Name = "devStatusUPLine";
            this.devStatusUPLine.Size = new System.Drawing.Size(283, 28);
            this.devStatusUPLine.Status = false;
            this.devStatusUPLine.TabIndex = 2;
            this.devStatusUPLine.Visible = false;
            // 
            // pnlDown
            // 
            this.pnlDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDown.Controls.Add(this.mlistbShow);
            this.pnlDown.Controls.Add(this.imbtnStatus);
            this.pnlDown.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlDown.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.pnlDown.Location = new System.Drawing.Point(0, 448);
            this.pnlDown.Name = "pnlDown";
            this.pnlDown.Size = new System.Drawing.Size(285, 459);
            this.pnlDown.TabIndex = 5;
            // 
            // mlistbShow
            // 
            this.mlistbShow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mlistbShow.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.mlistbShow.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.mlistbShow.FormattingEnabled = true;
            this.mlistbShow.HorizontalScrollbar = true;
            this.mlistbShow.ItemHeight = 16;
            this.mlistbShow.Location = new System.Drawing.Point(0, 0);
            this.mlistbShow.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.mlistbShow.Name = "mlistbShow";
            this.mlistbShow.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            this.mlistbShow.Size = new System.Drawing.Size(283, 345);
            this.mlistbShow.TabIndex = 5;
            // 
            // imbtnStatus
            // 
            this.imbtnStatus.BackColor = System.Drawing.Color.Red;
            this.imbtnStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.imbtnStatus.Enabled = false;
            this.imbtnStatus.FlatAppearance.BorderSize = 0;
            this.imbtnStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.imbtnStatus.Font = new System.Drawing.Font("Microsoft YaHei UI", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.imbtnStatus.ForeColor = System.Drawing.Color.White;
            this.imbtnStatus.Location = new System.Drawing.Point(0, 345);
            this.imbtnStatus.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.imbtnStatus.Name = "imbtnStatus";
            this.imbtnStatus.Size = new System.Drawing.Size(283, 112);
            this.imbtnStatus.TabIndex = 6;
            this.imbtnStatus.Text = "DOWN";
            this.imbtnStatus.UseVisualStyleBackColor = false;
            // 
            // pnlMiddle
            // 
            this.pnlMiddle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMiddle.Controls.Add(this.devStatusScaner);
            this.pnlMiddle.Controls.Add(this.devStatusUPLine);
            this.pnlMiddle.Controls.Add(this.devStatusPLC);
            this.pnlMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMiddle.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.pnlMiddle.Location = new System.Drawing.Point(0, 116);
            this.pnlMiddle.Name = "pnlMiddle";
            this.pnlMiddle.Padding = new System.Windows.Forms.Padding(3);
            this.pnlMiddle.Size = new System.Drawing.Size(285, 330);
            this.pnlMiddle.TabIndex = 3;
            // 
            // htLabel1
            // 
            this.htLabel1.BackColor = System.Drawing.Color.Transparent;
            this.htLabel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.htLabel1.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.htLabel1.Location = new System.Drawing.Point(0, 87);
            this.htLabel1.Name = "htLabel1";
            this.htLabel1.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.htLabel1.Size = new System.Drawing.Size(285, 29);
            this.htLabel1.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htLabel1.TabIndex = 3;
            this.htLabel1.Text = "设备连接状态：";
            this.htLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(0, 446);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(285, 2);
            this.splitter1.TabIndex = 6;
            this.splitter1.TabStop = false;
            // 
            // timerStatus
            // 
            this.timerStatus.Interval = 1000;
            this.timerStatus.Tick += new System.EventHandler(this.timerStatus_Tick);
            // 
            // htPanel1
            // 
            this.htPanel1.Controls.Add(this.lbRecipelID);
            this.htPanel1.Controls.Add(this.lbPPID);
            this.htPanel1.Controls.Add(this.htLabel1);
            this.htPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.htPanel1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htPanel1.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htPanel1.Location = new System.Drawing.Point(0, 0);
            this.htPanel1.Name = "htPanel1";
            this.htPanel1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htPanel1.Size = new System.Drawing.Size(285, 116);
            this.htPanel1.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htPanel1.TabIndex = 3;
            this.htPanel1.Text = null;
            // 
            // lbRecipelID
            // 
            this.lbRecipelID.LabNameValue = "RecipeID";
            this.lbRecipelID.LabTextValue = "Text";
            this.lbRecipelID.Location = new System.Drawing.Point(7, 40);
            this.lbRecipelID.Margin = new System.Windows.Forms.Padding(189, 3846, 189, 3846);
            this.lbRecipelID.Name = "lbRecipelID";
            this.lbRecipelID.Size = new System.Drawing.Size(272, 34);
            this.lbRecipelID.TabIndex = 15;
            // 
            // lbPPID
            // 
            this.lbPPID.LabNameValue = "PPID";
            this.lbPPID.LabTextValue = "Text";
            this.lbPPID.Location = new System.Drawing.Point(7, 4);
            this.lbPPID.Margin = new System.Windows.Forms.Padding(145, 2568, 145, 2568);
            this.lbPPID.Name = "lbPPID";
            this.lbPPID.Size = new System.Drawing.Size(272, 34);
            this.lbPPID.TabIndex = 16;
            // 
            // UCDevciesStatus
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.pnlMiddle);
            this.Controls.Add(this.htPanel1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.pnlDown);
            this.DoubleBuffered = true;
            this.Name = "UCDevciesStatus";
            this.Size = new System.Drawing.Size(285, 907);
            this.Resize += new System.EventHandler(this.UCDevciesStatus_Resize);
            this.pnlDown.ResumeLayout(false);
            this.pnlMiddle.ResumeLayout(false);
            this.htPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private HYC.AutoPosition.UI.Controls.DevStatusIndicator devStatusPLC;
        private HYC.AutoPosition.UI.Controls.DevStatusIndicator devStatusScaner;
        private HYC.AutoPosition.UI.Controls.DevStatusIndicator devStatusUPLine;
        private System.Windows.Forms.Panel pnlDown;
        private System.Windows.Forms.Panel pnlMiddle;
        private HYC.WindowsControls.HTLabel htLabel1;
        private System.Windows.Forms.Splitter splitter1;
        private HYC.AutoPosition.UI.Controls.MessageListBox mlistbShow;
        private System.Windows.Forms.Timer timerStatus;
        private HYC.WindowsControls.HTPanel htPanel1;
        private HYC.WindowsControls.TextBoxs.HCLabelTextBox lbRecipelID;
        private HYC.WindowsControls.TextBoxs.HCLabelTextBox lbPPID;
        private System.Windows.Forms.Button imbtnStatus;
    }
}
