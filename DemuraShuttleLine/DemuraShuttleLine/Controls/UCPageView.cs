﻿using System;
using System.Windows.Forms;

namespace DemuraShuttleLine.Controls
{
    public partial class UCPageView : UserControl
    {
        public UCPageView()
        {
            InitializeComponent();
            this.Dock = DockStyle.Bottom;
            toolcmbPerNum.SelectedIndex = 0;
        }

        /// <summary>
        /// 当前页面发生改变时
        /// </summary>
        public event Action PageIndexChanged;

        /// <summary>
        /// 当前页码
        /// </summary>
        public int PageIndex
        {
            get { return int.Parse(tooltbPage.Text); }
            set
            {
                tooltbPage.Text = value.ToString();
                ButtonStatus();
            }
        }

        /// <summary>
        /// 总页数
        /// </summary>
        public int PageNum
        {
            get { return int.Parse(toollbTotalPage.Text); }
            set
            {
                toollbTotalPage.Text = value.ToString();
                ButtonStatus();
            }
        }

        /// <summary>
        /// 总条数
        /// </summary>
        public int TotalSize
        {
            get { return int.Parse(toollbTotal.Text); }
            set
            {
                PageNum = value / PageSize + (value % PageSize == 0 ? 0 : 1);
                toollbTotal.Text = value.ToString();
            }
        }

        /// <summary>
        /// 每页条数
        /// </summary>
        public int PageSize
        {
            get { return int.Parse(toolcmbPerNum.Text); }
            set { toolcmbPerNum.Text = value.ToString(); }
        }

        private void ButtonStatus()
        {
            toolbtnFirst.Enabled = true;
            toolbtnFront.Enabled = true;
            toolbtnLast.Enabled = true;
            toolbtnAfter.Enabled = true;
            if (PageIndex == 1)
            {
                toolbtnFirst.Enabled = false;
                toolbtnFront.Enabled = false;
            }
            if (PageIndex == PageNum)
            {
                toolbtnLast.Enabled = false;
                toolbtnAfter.Enabled = false;
            }
        }

        private void toolbtnFront_MouseDown(object sender, MouseEventArgs e)
        {
            toolbtnFront.Image = Properties.Resources.L;
        }

        private void toolbtnFront_MouseUp(object sender, MouseEventArgs e)
        {
            toolbtnFront.Image = Properties.Resources.L_p;
        }

        private void toolbtnFirst_MouseDown(object sender, MouseEventArgs e)
        {
            toolbtnFirst.Image = Properties.Resources.L_1;
        }

        private void toolbtnFirst_MouseUp(object sender, MouseEventArgs e)
        {
            toolbtnFirst.Image = Properties.Resources.L_p_1;
        }

        private void toolbtnAfter_MouseDown(object sender, MouseEventArgs e)
        {
            toolbtnAfter.Image = Properties.Resources.R;
        }

        private void toolbtnAfter_MouseUp(object sender, MouseEventArgs e)
        {
            toolbtnAfter.Image = Properties.Resources.R_P;
        }

        private void toolbtnLast_MouseDown(object sender, MouseEventArgs e)
        {
            toolbtnLast.Image = Properties.Resources.R_1;
        }

        private void toolbtnLast_MouseUp(object sender, MouseEventArgs e)
        {
            toolbtnLast.Image = Properties.Resources.R_p_1;
        }

        private void tooltbPage_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 8)
                e.Handled = true;
            else
            {
                e.Handled = false;
            }
        }

        private void toolbtnFront_Click(object sender, EventArgs e)
        {
            PageIndex -= 1;
            PageIndexChanged?.Invoke();
        }

        private void toolbtnAfter_Click(object sender, EventArgs e)
        {
            PageIndex += 1;
            PageIndexChanged?.Invoke();
        }

        private void toolbtnFirst_Click(object sender, EventArgs e)
        {
            PageIndex = 1;
            PageIndexChanged?.Invoke();
        }

        private void toolbtnLast_Click(object sender, EventArgs e)
        {
            PageIndex = PageNum;
            PageIndexChanged?.Invoke();
        }

        private void toolcmbPerNum_SelectedIndexChanged(object sender, EventArgs e)
        {
            PageIndex = 1;
        }

        private void tooltbPage_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tooltbPage.Text) || int.Parse(tooltbPage.Text) == 0 || int.Parse(tooltbPage.Text) > PageNum)
            {
                tooltbPage.Text = "1";
            }
        }
    }
}
