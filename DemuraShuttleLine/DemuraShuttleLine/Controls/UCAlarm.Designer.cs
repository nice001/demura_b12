﻿
namespace DemuraShuttleLine.Controls
{
    partial class UCAlarm
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.htPanel1 = new HYC.WindowsControls.HTPanel();
            this.cmbPlcId = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tpEndTime = new System.Windows.Forms.DateTimePicker();
            this.cbTop10 = new HYC.WindowsControls.HTCheckBox();
            this.tpStartTime = new System.Windows.Forms.DateTimePicker();
            this.btnExport = new HYC.WindowsControls.HTButton();
            this.btnSearch = new HYC.WindowsControls.HTButton();
            this.htGroupBox1 = new HYC.WindowsControls.HTGroupBox();
            this.cbLight = new HYC.WindowsControls.HTCheckBox();
            this.cbWeight = new HYC.WindowsControls.HTCheckBox();
            this.htLabel2 = new HYC.WindowsControls.HTLabel();
            this.htLabel1 = new HYC.WindowsControls.HTLabel();
            this.dgvData = new HYC.WindowsControls.HTDataGridView();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvStatistics = new HYC.WindowsControls.HTDataGridView();
            this.PLC编号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ucPageView = new DemuraShuttleLine.Controls.UCPageView();
            this.htPanel1.SuspendLayout();
            this.htGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStatistics)).BeginInit();
            this.SuspendLayout();
            // 
            // htPanel1
            // 
            this.htPanel1.Controls.Add(this.cmbPlcId);
            this.htPanel1.Controls.Add(this.label1);
            this.htPanel1.Controls.Add(this.tpEndTime);
            this.htPanel1.Controls.Add(this.cbTop10);
            this.htPanel1.Controls.Add(this.tpStartTime);
            this.htPanel1.Controls.Add(this.btnExport);
            this.htPanel1.Controls.Add(this.btnSearch);
            this.htPanel1.Controls.Add(this.htGroupBox1);
            this.htPanel1.Controls.Add(this.htLabel2);
            this.htPanel1.Controls.Add(this.htLabel1);
            this.htPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.htPanel1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htPanel1.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htPanel1.Location = new System.Drawing.Point(0, 0);
            this.htPanel1.Name = "htPanel1";
            this.htPanel1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htPanel1.Size = new System.Drawing.Size(1249, 120);
            this.htPanel1.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htPanel1.TabIndex = 2;
            this.htPanel1.Text = null;
            // 
            // cmbPlcId
            // 
            this.cmbPlcId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPlcId.FormattingEnabled = true;
            this.cmbPlcId.Items.AddRange(new object[] {
            "1",
            "2"});
            this.cmbPlcId.Location = new System.Drawing.Point(584, 41);
            this.cmbPlcId.Name = "cmbPlcId";
            this.cmbPlcId.Size = new System.Drawing.Size(62, 28);
            this.cmbPlcId.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(516, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "PLC编号";
            // 
            // tpEndTime
            // 
            this.tpEndTime.CustomFormat = "yyyy/MM/dd HH:mm:ss";
            this.tpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpEndTime.Location = new System.Drawing.Point(128, 72);
            this.tpEndTime.Name = "tpEndTime";
            this.tpEndTime.Size = new System.Drawing.Size(180, 26);
            this.tpEndTime.TabIndex = 10;
            // 
            // cbTop10
            // 
            this.cbTop10.BackColor = System.Drawing.Color.Transparent;
            this.cbTop10.CheckBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.cbTop10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbTop10.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.cbTop10.Location = new System.Drawing.Point(688, 40);
            this.cbTop10.Name = "cbTop10";
            this.cbTop10.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.cbTop10.Size = new System.Drawing.Size(104, 29);
            this.cbTop10.Style = HYC.WindowsControls.HTStyle.Gray;
            this.cbTop10.TabIndex = 0;
            this.cbTop10.Text = "Top10";
            this.cbTop10.ValueChanged += new HYC.WindowsControls.HTCheckBox.OnValueChanged(this.cbTop10_ValueChanged);
            // 
            // tpStartTime
            // 
            this.tpStartTime.CustomFormat = "yyyy/MM/dd HH:mm:ss";
            this.tpStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.tpStartTime.Location = new System.Drawing.Point(128, 22);
            this.tpStartTime.Name = "tpStartTime";
            this.tpStartTime.Size = new System.Drawing.Size(180, 26);
            this.tpStartTime.TabIndex = 11;
            // 
            // btnExport
            // 
            this.btnExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExport.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnExport.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnExport.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnExport.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.btnExport.Location = new System.Drawing.Point(1018, 40);
            this.btnExport.Name = "btnExport";
            this.btnExport.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnExport.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnExport.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnExport.Size = new System.Drawing.Size(138, 38);
            this.btnExport.Style = HYC.WindowsControls.HTStyle.Gray;
            this.btnExport.TabIndex = 5;
            this.btnExport.Text = "导出数据";
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSearch.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnSearch.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnSearch.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnSearch.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.btnSearch.Location = new System.Drawing.Point(826, 40);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.btnSearch.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(160)))), ((int)(((byte)(165)))));
            this.btnSearch.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(123)))), ((int)(((byte)(129)))));
            this.btnSearch.Size = new System.Drawing.Size(138, 38);
            this.btnSearch.Style = HYC.WindowsControls.HTStyle.Gray;
            this.btnSearch.TabIndex = 5;
            this.btnSearch.Text = "查询";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // htGroupBox1
            // 
            this.htGroupBox1.Controls.Add(this.cbLight);
            this.htGroupBox1.Controls.Add(this.cbWeight);
            this.htGroupBox1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.htGroupBox1.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htGroupBox1.Location = new System.Drawing.Point(331, 7);
            this.htGroupBox1.Name = "htGroupBox1";
            this.htGroupBox1.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.htGroupBox1.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.htGroupBox1.Size = new System.Drawing.Size(173, 107);
            this.htGroupBox1.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htGroupBox1.TabIndex = 3;
            this.htGroupBox1.Text = "报警等级";
            // 
            // cbLight
            // 
            this.cbLight.BackColor = System.Drawing.Color.Transparent;
            this.cbLight.CheckBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.cbLight.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbLight.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.cbLight.Location = new System.Drawing.Point(45, 68);
            this.cbLight.Name = "cbLight";
            this.cbLight.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.cbLight.Size = new System.Drawing.Size(104, 29);
            this.cbLight.Style = HYC.WindowsControls.HTStyle.Gray;
            this.cbLight.TabIndex = 0;
            this.cbLight.Text = "轻报警";
            // 
            // cbWeight
            // 
            this.cbWeight.BackColor = System.Drawing.Color.Transparent;
            this.cbWeight.CheckBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.cbWeight.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbWeight.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.cbWeight.Location = new System.Drawing.Point(45, 33);
            this.cbWeight.Name = "cbWeight";
            this.cbWeight.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.cbWeight.Size = new System.Drawing.Size(104, 29);
            this.cbWeight.Style = HYC.WindowsControls.HTStyle.Gray;
            this.cbWeight.TabIndex = 0;
            this.cbWeight.Text = "重报警";
            // 
            // htLabel2
            // 
            this.htLabel2.AutoSize = true;
            this.htLabel2.BackColor = System.Drawing.Color.Transparent;
            this.htLabel2.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htLabel2.Location = new System.Drawing.Point(36, 75);
            this.htLabel2.Name = "htLabel2";
            this.htLabel2.Size = new System.Drawing.Size(79, 20);
            this.htLabel2.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htLabel2.TabIndex = 1;
            this.htLabel2.Text = "结束时间：";
            this.htLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // htLabel1
            // 
            this.htLabel1.AutoSize = true;
            this.htLabel1.BackColor = System.Drawing.Color.Transparent;
            this.htLabel1.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.htLabel1.Location = new System.Drawing.Point(36, 25);
            this.htLabel1.Name = "htLabel1";
            this.htLabel1.Size = new System.Drawing.Size(79, 20);
            this.htLabel1.Style = HYC.WindowsControls.HTStyle.Gray;
            this.htLabel1.TabIndex = 1;
            this.htLabel1.Text = "开始时间：";
            this.htLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToAddRows = false;
            this.dgvData.AllowUserToDeleteRows = false;
            this.dgvData.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.dgvData.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvData.BackgroundColor = System.Drawing.Color.White;
            this.dgvData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvData.ColumnHeadersHeight = 32;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column8,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvData.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvData.EnableHeadersVisualStyles = false;
            this.dgvData.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.dgvData.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.dgvData.Location = new System.Drawing.Point(0, 120);
            this.dgvData.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.dgvData.Name = "dgvData";
            this.dgvData.ReadOnly = true;
            this.dgvData.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.dgvData.RowHeadersVisible = false;
            this.dgvData.RowHeadersWidth = 51;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            this.dgvData.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvData.RowTemplate.Height = 29;
            this.dgvData.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvData.SelectedIndex = -1;
            this.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvData.ShowGridLine = true;
            this.dgvData.Size = new System.Drawing.Size(1249, 636);
            this.dgvData.StripeOddColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.dgvData.Style = HYC.WindowsControls.HTStyle.Gray;
            this.dgvData.TabIndex = 6;
            this.dgvData.TagString = null;
            this.dgvData.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvData_CellFormatting);
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "EQUIPID";
            this.Column8.HeaderText = "PLC编号";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "ALARMID";
            this.Column1.FillWeight = 80F;
            this.Column1.HeaderText = "报警Code";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "LEVEL";
            this.Column2.FillWeight = 50F;
            this.Column2.HeaderText = "报警等级";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "DESCRIPTION";
            this.Column3.HeaderText = "报警描述1";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "DESCRIPTIONCHS";
            this.Column4.HeaderText = "报警描述2";
            this.Column4.MinimumWidth = 6;
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "ALARMTIME";
            this.Column5.FillWeight = 80F;
            this.Column5.HeaderText = "报警时间";
            this.Column5.MinimumWidth = 6;
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "ALARMCANCELTIME";
            this.Column6.FillWeight = 80F;
            this.Column6.HeaderText = "报警取消时间";
            this.Column6.MinimumWidth = 6;
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "ALIVETIME";
            this.Column7.FillWeight = 80F;
            this.Column7.HeaderText = "报警持续时间";
            this.Column7.MinimumWidth = 6;
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // dgvStatistics
            // 
            this.dgvStatistics.AllowUserToAddRows = false;
            this.dgvStatistics.AllowUserToDeleteRows = false;
            this.dgvStatistics.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.dgvStatistics.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvStatistics.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvStatistics.BackgroundColor = System.Drawing.Color.White;
            this.dgvStatistics.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvStatistics.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvStatistics.ColumnHeadersHeight = 32;
            this.dgvStatistics.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvStatistics.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PLC编号,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12,
            this.Column13});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvStatistics.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvStatistics.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvStatistics.EnableHeadersVisualStyles = false;
            this.dgvStatistics.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.dgvStatistics.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.dgvStatistics.Location = new System.Drawing.Point(0, 120);
            this.dgvStatistics.Name = "dgvStatistics";
            this.dgvStatistics.ReadOnly = true;
            this.dgvStatistics.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.dgvStatistics.RowHeadersVisible = false;
            this.dgvStatistics.RowHeadersWidth = 51;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            this.dgvStatistics.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvStatistics.RowTemplate.Height = 29;
            this.dgvStatistics.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvStatistics.SelectedIndex = -1;
            this.dgvStatistics.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvStatistics.ShowGridLine = true;
            this.dgvStatistics.Size = new System.Drawing.Size(1249, 636);
            this.dgvStatistics.StripeOddColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(244)))));
            this.dgvStatistics.Style = HYC.WindowsControls.HTStyle.Gray;
            this.dgvStatistics.TabIndex = 8;
            this.dgvStatistics.TagString = null;
            this.dgvStatistics.Visible = false;
            this.dgvStatistics.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvStatistics_CellFormatting);
            // 
            // PLC编号
            // 
            this.PLC编号.DataPropertyName = "EQUIPID";
            this.PLC编号.HeaderText = "PLC编号";
            this.PLC编号.Name = "PLC编号";
            this.PLC编号.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "total";
            this.Column9.FillWeight = 80F;
            this.Column9.HeaderText = "报警数量";
            this.Column9.MinimumWidth = 6;
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "ALARMID";
            this.Column10.FillWeight = 80F;
            this.Column10.HeaderText = "报警编号";
            this.Column10.MinimumWidth = 6;
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "LEVEL";
            this.Column11.FillWeight = 80F;
            this.Column11.HeaderText = "报警等级";
            this.Column11.MinimumWidth = 6;
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "DESCRIPTION";
            this.Column12.HeaderText = "报警描述1";
            this.Column12.MinimumWidth = 6;
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "DESCRIPTIONCHS";
            this.Column13.HeaderText = "报警描述2";
            this.Column13.MinimumWidth = 6;
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            // 
            // ucPageView
            // 
            this.ucPageView.AutoSize = true;
            this.ucPageView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucPageView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ucPageView.Location = new System.Drawing.Point(0, 756);
            this.ucPageView.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ucPageView.Name = "ucPageView";
            this.ucPageView.PageIndex = 1;
            this.ucPageView.PageNum = 1;
            this.ucPageView.PageSize = 10;
            this.ucPageView.Size = new System.Drawing.Size(1249, 29);
            this.ucPageView.TabIndex = 7;
            this.ucPageView.TotalSize = 1;
            // 
            // UCAlarm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.dgvStatistics);
            this.Controls.Add(this.dgvData);
            this.Controls.Add(this.ucPageView);
            this.Controls.Add(this.htPanel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UCAlarm";
            this.Size = new System.Drawing.Size(1249, 785);
            this.htPanel1.ResumeLayout(false);
            this.htPanel1.PerformLayout();
            this.htGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStatistics)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private HYC.WindowsControls.HTPanel htPanel1;
        private System.Windows.Forms.DateTimePicker tpEndTime;
        private System.Windows.Forms.DateTimePicker tpStartTime;
        private HYC.WindowsControls.HTButton btnSearch;
        private HYC.WindowsControls.HTGroupBox htGroupBox1;
        private HYC.WindowsControls.HTCheckBox cbLight;
        private HYC.WindowsControls.HTCheckBox cbWeight;
        private HYC.WindowsControls.HTLabel htLabel2;
        private HYC.WindowsControls.HTLabel htLabel1;
        private UCPageView ucPageView;
        private HYC.WindowsControls.HTButton btnExport;
        private HYC.WindowsControls.HTCheckBox cbTop10;
        private HYC.WindowsControls.HTDataGridView dgvData;
        private HYC.WindowsControls.HTDataGridView dgvStatistics;
        private System.Windows.Forms.DataGridViewTextBoxColumn PLC编号;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.ComboBox cmbPlcId;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
    }
}
