﻿using DemuraDB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace DemuraShuttleLine.Controls
{
    public partial class UCAlarm : UserControl
    {
        DemuraBaseDB db = DemuraBaseDB.GetDemuraDB();
        public UCAlarm()
        {
            InitializeComponent();
            tpStartTime.Value = DateTime.Now.AddDays(-1);
            tpEndTime.Value = DateTime.Now;
            ucPageView.PageIndexChanged += UcPageView_PageIndexChanged;
            dgvData.AutoGenerateColumns = false;
            dgvStatistics.AutoGenerateColumns = false;
            cmbPlcId.SelectedIndex = 1;
        }

        private void UcPageView_PageIndexChanged()
        {
            btnSearch_Click(btnSearch, null);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> filterWhere = new Dictionary<string, object>();
            if (cbWeight.Checked != cbLight.Checked)
            {
                if (cbWeight.Checked)
                    filterWhere.Add("LEVEL", 2);
                else
                    filterWhere.Add("LEVEL", 1);
            }
            if (!cbTop10.Checked)
                SearchList(filterWhere, int.Parse(cmbPlcId.Text));
            else
                SearchTop10(filterWhere);
        }

        private void SearchList(Dictionary<string, object> filterWhere, int plcid)
        {
            DataSet ds = db.GetAlarmInfoList(filterWhere, plcid, tpStartTime.Value, tpEndTime.Value, ucPageView.PageIndex, ucPageView.PageSize);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                ucPageView.TotalSize = ds.Tables[0].Rows.Count;
            dgvData.DataSource = ds.Tables[0];
        }

        private void SearchTop10(Dictionary<string, object> filterWhere)
        {
            DataSet ds = db.GetAlarmStatisticsTop10(filterWhere, tpStartTime.Value, tpEndTime.Value);
            dgvStatistics.DataSource = ds.Tables[0];
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            DataGridView tmpGrid;
            if (cbTop10.Checked)
                tmpGrid = dgvStatistics;
            else
                tmpGrid = dgvData;
            FileOperate.ExportGridToExcel(tmpGrid);
        }

        private void cbTop10_ValueChanged(object sender, bool value)
        {
            if (cbTop10.Checked)
            {
                dgvStatistics.Visible = true;
                dgvData.Visible = false;
                dgvData.ClearRows();
            }
            else
            {
                dgvStatistics.Visible = false;
                dgvData.Visible = true;
                dgvStatistics.ClearRows();
            }
        }

        private void dgvStatistics_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                if (e.Value.ToString() == "1")
                {
                    e.Value = "轻报警";
                }
                else if (e.Value.ToString() == "2")
                {
                    e.Value = "重报警";
                }
            }
        }

        private void dgvData_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                if (e.Value.ToString() == "1")
                {
                    e.Value = "轻报警";
                }
                else if (e.Value.ToString() == "2")
                {
                    e.Value = "重报警";
                }
            }
            else if (e.ColumnIndex == 5 || e.ColumnIndex == 6)
            {
                try
                {
                    e.Value = DateTime.Parse(e.Value.ToString()).ToString();
                }
                catch { }
            }
        }
    }
}
