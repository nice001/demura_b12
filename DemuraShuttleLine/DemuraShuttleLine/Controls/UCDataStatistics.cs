﻿using DemuraDB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.Xml;

namespace DemuraShuttleLine.Controls
{
    public partial class UCDataStatistics : UserControl
    {
        DemuraBaseDB dbManage = DemuraBaseDB.GetDemuraDB();
        static string demura_result_config = "DemuraNG.xml";
        public UCDataStatistics()
        {
            InitializeComponent();
            InitConfig();
            tpStartTime.Value = DateTime.Now.AddDays(-1);
            tpEndTime.Value = DateTime.Now;
            dgvData.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;

            dgvData.AutoGenerateColumns = true;
            dgvData.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.Beige;
            dgvData.RowsDefaultCellStyle.BackColor = System.Drawing.Color.Bisque;
            dgvData.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvData.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }

        Dictionary<string, string> ngList = new Dictionary<string, string>();
        void InitConfig()
        {
            XmlDocument document = new XmlDocument();
            document.Load(Program.configPath + demura_result_config);
            XmlNodeList list = document.SelectNodes("NGList/NGCode");
            foreach (XmlNode item in list)
            {
                if (bool.Parse(item.Attributes["isShow"].Value))
                    ngList.Add(item.InnerText, item.Attributes["name"].Value);
            }
        }

        private void rbPGID_ValueChanged(object sender, bool value)
        {
            dgvData.DataSource = null;
        }

        private void rbUnitID_ValueChanged(object sender, bool value)
        {
            dgvData.DataSource = null;
        }

        private void rbAll_ValueChanged(object sender, bool value)
        {
            dgvData.DataSource = null;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> pairs = new Dictionary<string, object>();
            DateTime startTime = tpStartTime.Value;
            DateTime endTime = tpEndTime.Value;
            TimeSpan time = endTime - startTime;
            if (time.Minutes < 0)
            {
                MessageBox.Show("请选择查询数据开始时间", "提示！！");
                return;
            }
            if (time.Days > 30)//判断天数
            {
                MessageBox.Show("数据查询时间跨度不能大于30天！", "提示");
                return;
            }

            string groupbyColumn = null;
            string onew = "UNITID";
            string strWhere = "";
            if (!string.IsNullOrWhiteSpace(tbUnitid.Text))
            {
                pairs.Add("UNITID", tbUnitid.Text);
            }
            foreach (var item in pairs)
            {
                strWhere += " and " + item.Key + "=" + item.Value;
            }
            if (strWhere != "")
            {
                if (rbPGID.Checked)
                {
                    onew = "TesterID";
                    groupbyColumn = strWhere;
                }
                else if (rbCameraID.Checked)
                {
                    onew = "CameraID";
                    groupbyColumn = strWhere;
                }
                else
                {
                    onew = "UnitID";
                    groupbyColumn = null;
                }

                dgvData.DataSource = dbManage.GetDataStatistics(ngList, groupbyColumn, onew, tpStartTime.Value, tpEndTime.Value);
            }
            else
            {
                dgvData.DataSource = dbManage.GetDataStatistics(ngList, groupbyColumn, onew, tpStartTime.Value, tpEndTime.Value);
            }
            DataTable dt;
            dgvData.AutoGenerateColumns = true;
            dgvData.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.Beige;
            dgvData.RowsDefaultCellStyle.BackColor = System.Drawing.Color.Bisque;

        }

        private void dgvData_DataSourceChanged(object sender, EventArgs e)
        {
            GridViewShow();
        }

        private void rbShowPercent_ValueChanged(object sender, bool value)
        {
            GridViewShow();
        }

        private void rbShowNum_ValueChanged(object sender, bool value)
        {
            GridViewShow();
        }
        private void rbShowPercentAndNum_ValueChanged(object sender, bool value)
        {
            GridViewShow();
        }

        List<string> fixedColumns = new List<string>() { "总数量", "单体编号", "治具编号", "相机编号" };
        Dictionary<string, string> nameLis = new Dictionary<string, string>();
        void GridViewShow()
        {
            DataGridViewColumnCollection list = dgvData.Columns;
            foreach (DataGridViewColumn item in list)
            {
                if (fixedColumns.Contains(item.DataPropertyName))
                    continue;
                if (item.HeaderText.Contains("比例"))
                {
                    if (rbShowPercent.Checked)
                        item.Visible = true;
                    else
                        item.Visible = false;
                }
                else if (item.HeaderText.Contains("比率"))
                {
                    if (rbShowPercentAndNum.Checked)
                        item.Visible = true;
                    else

                        item.Visible = false;
                }
                else
                {
                    if (rbShowNum.Checked)
                        item.Visible = true;
                    else

                        item.Visible = false;
                }
                //if (item.HeaderText.Contains("比例"))
                //{

                //    if (rbShowNum.Checked)
                //        item.Visible = false;
                //    else
                //        item.Visible = true;
                //}
                //else if (item.HeaderText.Contains("比率"))
                //{
                //    if (rbShowPercentAndNum.Checked)
                //        item.Visible = false;
                //    else

                //        item.Visible = true;
                //}
                //else
                //{
                //    if (rbShowPercent.Checked)
                //        item.Visible = false;
                //    else

                //        item.Visible = true;
                //}
            }
        }
        private void dgvData_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.Value != null && e.Value.GetType() == typeof(decimal) && rbShowPercent.Checked)
            {
                e.Value = (float)((decimal)e.Value * 100) + "%";
            }
        }

        private void tpStartTime_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (rbTimeSpan.Checked)
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void tpEndTime_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (rbTimeSpan.Checked)
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void rbDay_ValueChanged(object sender, bool value)
        {
            tpStartTime.Value = DateTime.Today.AddHours(8);
            tpEndTime.Value = DateTime.Today.AddHours(20);
        }

        private void rbNight_ValueChanged(object sender, bool value)
        {
            tpStartTime.Value = DateTime.Today.AddHours(-4);
            tpEndTime.Value = DateTime.Today.AddHours(8);
        }
        private void rbfirstDay_ValueChanged(object sender, bool value)
        {
            tpStartTime.Value = DateTime.Today.AddHours(-16);
            tpEndTime.Value = DateTime.Today.AddHours(-4);
        }
        private void rbfirstNight_ValueChanged(object sender, bool value)
        {
            tpStartTime.Value = DateTime.Today.AddHours(-28);
            tpEndTime.Value = DateTime.Today.AddHours(-16);
        }

        private void rbTimeSpan_ValueChanged(object sender, bool value)
        {
            tpStartTime.Value = DateTime.Now.AddDays(-1);
            tpEndTime.Value = DateTime.Now;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            FileOperate.ExportGridToExcel(dgvData);
        }


    }
}
