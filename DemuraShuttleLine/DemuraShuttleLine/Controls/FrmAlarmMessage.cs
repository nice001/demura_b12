﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemuraShuttleLine.Controls
{
    public partial class FrmAlarmMessage : Form
    {
        public FrmAlarmMessage()
        {
            InitializeComponent();
        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        public void ShowMessage(string mes)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() => ShowMessage(mes)));
            }
            else
            {
                label1.Text = mes;
            }
        }
    }
}
