﻿using DemuraShuttleLine.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemuraShuttleLine.Controls
{
    public partial class FrmMCRFailSelect : Form
    {

        Action<MCRResultType, string> GetMCRFailResult;
        public FrmMCRFailSelect(Action<MCRResultType, string> action)
        {
            GetMCRFailResult = action;
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (rbKeyIn.Checked)
            {
                if (string.IsNullOrWhiteSpace(tbPanelId.Text))
                {
                    MessageBox.Show("Key In情况下Panel ID不能为空！");
                    return;
                }
                GetMCRFailResult(MCRResultType.KeyIn, tbPanelId.Text.Trim());
            }
            else
                GetMCRFailResult(MCRResultType.ByPass, null);
            this.Close();
        }

        private void rbKeyIn_ValueChanged(object sender, bool value)
        {
            if (rbKeyIn.Checked)
                tbPanelId.Enabled = true;
            else
                tbPanelId.Enabled = false;
        }
    }
}
