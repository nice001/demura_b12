﻿using DemuraShuttleLine.business;
using DemuraShuttleLine.Business;
using DemuraShuttleLine.Controls;
using DemuraShuttleLine.Entity;
using HYC.AutoPosition.Events;
using HYC.AutoPosition.Interface;
using HYC.AutoPosition.UI;
using HYC.AutoPosition.UI.Entities;
using HYC.WindowsForms;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemuraShuttleLine
{
    public partial class FrmMain : FormBase
    {
        private PLCSignalProvider provider = new PLCSignalProvider();
        private PLCManage plcManage;
        FormAutoPos frmAutoPos = new FormAutoPos();
        UCParaSet ucParaSet;
        UCStatusMonitor ucStatusMonitor;
        UCDataSearch ucDataSearch;
        UCDataStatistics ucDataStatistics;
        UCAlarm ucAlarm;
        UserManager userManager;

        public FrmMain()
        {
            InitializeComponent();
            this.FormClosing += FrmMain_FormClosing;
            InitControls();
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show("确定关闭程序？", "提示", MessageBoxButtons.OKCancel);
            if (result == DialogResult.Cancel)
            {
                e.Cancel = true;
                return;
            }
            else
            {
                Process.GetCurrentProcess().Kill();
            }
        }

        void InitControls()
        {
            InitAutoPostion();
            ucParaSet = new UCParaSet();
            ucParaSet.Dock = DockStyle.Fill;
            ucParaSet.Visible = false;
            pnlHost.Controls.Add(ucParaSet);
            ucStatusMonitor = new UCStatusMonitor();
            ucStatusMonitor.Dock = DockStyle.Fill;
            ucStatusMonitor.Visible = false;
            pnlHost.Controls.Add(ucStatusMonitor);
            ucDataSearch = new UCDataSearch();
            ucDataSearch.Dock = DockStyle.Fill;
            ucDataSearch.Visible = false;
            pnlHost.Controls.Add(ucDataSearch);
            ucDataStatistics = new UCDataStatistics();
            ucDataStatistics.Dock = DockStyle.Fill;
            ucDataStatistics.Visible = false;
            pnlHost.Controls.Add(ucDataStatistics);
            ucAlarm = new UCAlarm();
            ucAlarm.Dock = DockStyle.Fill;
            ucAlarm.Visible = false;
            pnlHost.Controls.Add(ucAlarm);
        }

        /// <summary>
        /// 初始化自动对位控件
        /// </summary>
        void InitAutoPostion()
        {
            frmAutoPos.FormBorderStyle = FormBorderStyle.None;
            frmAutoPos.TopLevel = false;
            frmAutoPos.Dock = DockStyle.Fill;
            this.pnlHost.Controls.Add(frmAutoPos);
            //设置信号提供者
            frmAutoPos.SetSignalProvider(provider);
            frmAutoPos.SetLanguage("zh-CN");
            //监听事件
            frmAutoPos.GetManager().ImageReceived += FormMain_ImageReceived;
            frmAutoPos.GetUserManager().UserChanged += FormMain_UserChanged;
            frmAutoPos.RecipeSelected += FrmAutoPos_RecipeSelected;
            //显示窗体
            frmAutoPos.Show();
        }

        private void Box_ContextMenuClick(object sender, HYC.AutoPosition.UI.Controls.ImageViewBox.MenuClickEventArgs e)
        {
            Provider_AutoPosCompletedEvent(int.Parse(e.GroupName), -1, true);
        }

        private void FrmAutoPos_RecipeSelected(object sender, HYC.AutoPosition.UI.Events.RecipeSelectEventArgs e)
        {
            ucDevciesStatus.SetRecipelID(e.RecipeID);
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            InitPLC();
            ucplcTest.SetPlcManage(plcManage);
            ucStatusMonitor.SetPlcManage(plcManage);
            provider.MoveCompletedEvent += Provider_MoveCompletedEvent;
            provider.AutoPosCompletedEvent += Provider_AutoPosCompletedEvent;
            provider.CalibrationCompletedEvent += Provider_CalibrationCompletedEvent;
            provider.UpdatePlatformStatusEvent += Provider_UpdatePlatformStatusEvent;
            provider.MessageNoticeEvent += Provider_MessageNoticeEvent;

            provider.MovePLCEvent += Provider_MovePLCEvent;
            userManager = frmAutoPos.GetUserManager();
            ucDevciesStatus.SetRecipelID(frmAutoPos.GetManager().CurRecipe.RecipeID);
            ucDevciesStatus.SetPPID(frmAutoPos.GetManager().CurRecipe.PPID);
            VisableControl(ucStatusMonitor);
            var list = frmAutoPos.GetImageViewBoxes();
            foreach (var box in list)
            {
                box.ContextMenuClick += Box_ContextMenuClick;
                box.AddContextMenu("By Pass");
            }
        }

        private bool Provider_MovePLCEvent(int groupIndex, int stageIndex, double[] xyt, int[] reserve)
        {
            bool result = false;
            try
            {
                if (xyt[0] == 0 && xyt[1] == 0 && xyt[2] == 400)
                {
                    result = false;
                }
                int[] xytCount = new int[3];
                List<byte> writeData = new List<byte>();
                for (int i = 0; i < 3; i++)
                {
                    xytCount[i] = (int)(xyt[i] * 1000);
                    writeData.AddRange(BitConverter.GetBytes(xytCount[i]));
                }
                switch (groupIndex)
                {
                    case 4:
                        plcManage.WriteDatas(GlobalParameters.D_Out_UVW1_Address, writeData.ToArray());
                        break;
                    case 5:
                        plcManage.WriteDatas(GlobalParameters.D_Out_UVW2_Address, writeData.ToArray());
                        break;
                    case 6:
                        int value = 0;
                        if ((int)xyt[0] != 500)
                        {
                            value = (ushort)xyt[0];
                        }
                        plcManage.WriteDatas(GlobalParameters.D_Out_Tray_Address, BitConverter.GetBytes(value));
                        break;
                    default:
                        break;
                }
                result = true;
            }
            catch (Exception ex)
            {
                Program.logNet.WriteInfo("Provider_MovePLCEvent", ex.Message);
                result = false;
            }
            finally
            {
                //switch (groupIndex)
                //{
                //    case 4:
                //        plcManage.WriteCmd("M" + (result ? Out_UVW_Ack.Snap1OK : Out_UVW_Ack.Snap1Fail), true);
                //        break;
                //    case 5:
                //        plcManage.WriteCmd("M" + (result ? Out_UVW_Ack.Snap2OK : Out_UVW_Ack.Snap2Fail), true);
                //        break;
                //    case 6:
                //        plcManage.WriteCmd("M" + (result ? Out_Tray_Ack.SnapOK : Out_Tray_Ack.SnapFail), true);
                //        break;
                //    default:
                //        break;
                //}
            }
            return result;
        }

        private void Provider_MessageNoticeEvent(string obj)
        {
            ucDevciesStatus.ShowOperateInfo(obj);
        }

        List<int> platIndexList = new List<int>() { 0, 1, 2, 3, 4, 5 };
        private bool[] isWriting = new bool[6];
        private void Provider_UpdatePlatformStatusEvent(int platIndex, PlatformStatus status)
        {
            if (platIndexList.Contains(platIndex) && status.IsServoOn)
            {
                if (!status.IsAtHome)
                {
                    provider.StartOrgReq(platIndex);
                }
                platIndexList.Remove(platIndex);
            }
            Task.Factory.StartNew(() =>
            {
                if (isWriting[platIndex])
                    return;
                isWriting[platIndex] = true;
                switch (platIndex)
                {
                    case 0:
                        plcManage.WriteCmd("M" + UVW_A_Ack.InZeroPos1, status.IsAtHome);
                        plcManage.WriteCmd("M" + UVW_A_Ack.Ready1, status.IsServoOn);
                        if (status.IsAlarm || !status.IsServoOn)
                        {
                            plcManage.WriteCmd("M" + UVW_A_Ack.Org1Fail, false);
                        }
                        break;
                    case 1:
                        plcManage.WriteCmd("M" + UVW_A_Ack.InZeroPos2, status.IsAtHome);
                        plcManage.WriteCmd("M" + UVW_A_Ack.Ready2, status.IsServoOn);
                        if (status.IsAlarm || !status.IsServoOn)
                        {
                            plcManage.WriteCmd("M" + UVW_A_Ack.Org2Fail, false);
                        }
                        break;
                    case 2:
                        plcManage.WriteCmd("M" + UVW_B_Ack.InZeroPos1, status.IsAtHome);
                        plcManage.WriteCmd("M" + UVW_B_Ack.Ready1, status.IsServoOn);
                        if (status.IsAlarm || !status.IsServoOn)
                        {
                            plcManage.WriteCmd("M" + UVW_B_Ack.Org1Fail, false);
                        }
                        break;
                    case 3:
                        plcManage.WriteCmd("M" + UVW_B_Ack.InZeroPos2, status.IsAtHome);
                        plcManage.WriteCmd("M" + UVW_B_Ack.Ready2, status.IsServoOn);
                        if (status.IsAlarm || !status.IsServoOn)
                        {
                            plcManage.WriteCmd("M" + UVW_B_Ack.Org2Fail, false);
                        }
                        break;
                }
                isWriting[platIndex] = false;
                Thread.Sleep(50);
            });
        }

        private void Provider_CalibrationCompletedEvent(int groupIndex, int stageIndex, bool result)
        {
            try
            {
                string showMsg = "平台" + groupIndex + ";工位" + (stageIndex + 1) + "标定结果" + result.ToString();
                ucDevciesStatus.ShowOperateInfo(showMsg);
                Program.logNet.WriteInfo(showMsg);
                if (result && groupIndex == 0)
                    plcManage.WriteCmd("M" + UVW_A_Ack.Clib1Comp);
                else if (result && groupIndex == 1)
                    plcManage.WriteCmd("M" + UVW_A_Ack.Clib2Comp);
                else if (result && groupIndex == 2)
                    plcManage.WriteCmd("M" + UVW_B_Ack.Clib1Comp);
                else if (result && groupIndex == 3)
                    plcManage.WriteCmd("M" + UVW_B_Ack.Clib2Comp);
                else if (result && groupIndex == 4)
                    plcManage.WriteCmd("M" + Out_UVW_Ack.Snap1Clib1Comp);
                else if (result && groupIndex == 5)
                    plcManage.WriteCmd("M" + Out_UVW_Ack.Snap2Clib1Comp);
                               
                else if (!result && groupIndex == 0)
                    plcManage.WriteCmd("M" + UVW_A_Ack.Clib1Fail);
                else if (!result && groupIndex == 1)
                    plcManage.WriteCmd("M" + UVW_A_Ack.Clib2Fail);
                else if (!result && groupIndex == 2)
                    plcManage.WriteCmd("M" + UVW_B_Ack.Clib1Fail);
                else if (!result && groupIndex == 3)
                    plcManage.WriteCmd("M" + UVW_B_Ack.Clib2Fail);
                else if (result && groupIndex == 4)
                    plcManage.WriteCmd("M" + Out_UVW_Ack.Snap1Clib1FFail);
                else if (result && groupIndex == 5)
                    plcManage.WriteCmd("M" + Out_UVW_Ack.Snap2Clib1FFail);
            }
            catch (Exception ex)
            {
                Program.logNet.WriteInfo("CalibrationCompleted", ex.Message);
            }
        }

        private void Provider_AutoPosCompletedEvent(int groupIndex, int stageIndex, bool result)
        {
            try
            {
                string showMsg = "平台" + groupIndex + ";工位" + (stageIndex + 1) + "自动对位结果" + result.ToString();
                ucDevciesStatus.ShowOperateInfo(showMsg);
                Program.logNet.WriteInfo(showMsg);
                if (result && groupIndex == 0)
                    plcManage.WriteCmd("M" + UVW_A_Ack.ADJ1Comp);
                else if (result && groupIndex == 1)
                    plcManage.WriteCmd("M" + UVW_A_Ack.ADJ2Comp);
                else if (result && groupIndex == 2)
                    plcManage.WriteCmd("M" + UVW_B_Ack.ADJ1Comp);
                else if (result && groupIndex == 3)
                    plcManage.WriteCmd("M" + UVW_B_Ack.ADJ2Comp);
                else if(result && groupIndex==4)
                    plcManage.WriteCmd("M" + Out_UVW_Ack.Snap1OK);
                else if (result && groupIndex == 5)
                    plcManage.WriteCmd("M" + Out_UVW_Ack.Snap2OK);
                else if (result && groupIndex == 6)
                    plcManage.WriteCmd("M" + Out_Tray_Ack.SnapOK);

                else if (!result && groupIndex == 0)
                    plcManage.WriteCmd("M" + UVW_A_Ack.ADJ1Fail);
                else if (!result && groupIndex == 1)
                    plcManage.WriteCmd("M" + UVW_A_Ack.ADJ2Fail);
                else if (!result && groupIndex == 2)
                    plcManage.WriteCmd("M" + UVW_B_Ack.ADJ1Fail);
                else if (!result && groupIndex == 3)
                    plcManage.WriteCmd("M" + UVW_B_Ack.ADJ2Fail);
                else if (!result && groupIndex == 4)
                    plcManage.WriteCmd("M" + Out_UVW_Ack.Snap1Fail);
                else if (!result && groupIndex == 5)
                    plcManage.WriteCmd("M" + Out_UVW_Ack.Snap2Fail);
                else if (!result && groupIndex == 6)
                    plcManage.WriteCmd("M" + Out_Tray_Ack.SnapOK);

                //if(!result)
                // {
                //     FrmAlarmMessage frm = new FrmAlarmMessage();
                //     frm.ShowMessage($"对位平台{groupIndex + 1},对位失败,请确认！！！");
                //     frm.StartPosition = FormStartPosition.CenterScreen;
                //     frm.Show();
                // }

            }
            catch (Exception ex)
            {
                Program.logNet.WriteInfo("AutoPosCompleted", ex.Message);
            }
        }

        private void Provider_MoveCompletedEvent(int groupIndex, MoveReqType type, bool result)
        {
            try
            {
                if (type == MoveReqType.Home)
                {
                    string showMsg = "平台" + groupIndex + ";回原点结果：" + result.ToString();
                    ucDevciesStatus.ShowOperateInfo(showMsg);
                    Program.logNet.WriteInfo(showMsg);
                    string address = "";
                    switch (groupIndex)
                    {
                        case 0:
                            if (result)
                                address = "M" + UVW_A_Ack.Org1Comp;
                            else
                                address = "M" + UVW_A_Ack.Org1Fail;
                            break;
                        case 1:
                            if (result)
                                address = "M" + UVW_A_Ack.Org2Comp;
                            else
                                address = "M" + UVW_A_Ack.Org2Fail;
                            break;
                        case 2:
                            if (result)
                                address = "M" + UVW_B_Ack.Org1Comp;
                            else
                                address = "M" + UVW_B_Ack.Org1Fail;
                            break;
                        case 3:
                            if (result)
                                address = "M" + UVW_B_Ack.Org2Comp;
                            else
                                address = "M" + UVW_B_Ack.Org2Fail;
                            break;
                    }
                    plcManage.WriteCmd(address);
                }
            }
            catch (Exception ex)
            {
                Program.logNet.WriteInfo(type.ToString() + "MoveCompleted", ex.Message);
            }
        }




        void InitPLC()
        {
            try
            {
                plcManage = new PLCManage();
                plcManage.CIMServerStatus += PlcManage_CIMServerStatus;
                plcManage.ADJReqEvent += PLCManage_ADJReqEvent;
                plcManage.OrgReqEvent += PLCManage_OrgReqEvent;
                plcManage.ClibReqEvent += PLCManage_ClibReqEvent;
                plcManage.PLCStatusEvent += PlcManage_PLCStatusEvent;
                plcManage.PLCConnectEvent += PlcManage_PLCConnectEvent;
                plcManage.UnitKeepLiveEvent += PlcManage_UnitKeepLiveEvent;
                plcManage.ScanerStatusEvent += PlcManage_ScanerStatusEvent;
                plcManage.PanelExitEvent += PlcManage_PanelExitEvent;
                plcManage.PanelRemoveEvent += PlcManage_PanelRemoveEvent; 
                plcManage.CIMTerminalTextEvent += PlcManage_CIMTerminalTextEvent;
                plcManage.MCRFailEvent += PlcManage_MCRFailEvent;
                plcManage.MessageShowEvent += PlcManage_MessageShowEvent;
                plcManage.Init();
            }
            catch (Exception ex)
            {
                Program.logNet.WriteException("InitPLC", ex);
            }
        }

        private void PlcManage_PLCConnectEvent(bool status)
        {
            if (this.IsDisposed || this.Disposing)
                return;
            this.BeginInvoke(new Action(() =>
            {
                ucDevciesStatus.SetPLCConnect(status);
            }));
        }

        private void PlcManage_MessageShowEvent(string msg)
        {
            ucDevciesStatus.ShowOperateInfo(msg);
        }

        private void PlcManage_CIMServerStatus(bool obj)
        {
            ucDevciesStatus.Invoke(new Action(() => { ucDevciesStatus.SeLineStatus(obj); }));
        }

        Tuple<MCRResultType, string> MCRFailResult;
        private Tuple<MCRResultType, string> PlcManage_MCRFailEvent()
        {
            FrmMCRFailSelect frm = new FrmMCRFailSelect(GetMCRFailResult);
            frm.ShowDialog();
            return MCRFailResult;
        }

        void GetMCRFailResult(MCRResultType type, string panelId)
        {
            MCRFailResult = new Tuple<MCRResultType, string>(type, panelId);
        }

        private void PlcManage_CIMTerminalTextEvent(string text)
        {
            MessageBox.Show(text);
        }

        private void PlcManage_PanelRemoveEvent(Dictionary<int, int> removeList)
        {
            foreach (KeyValuePair<int, int> item in removeList)
            {
                string msg = AddressEntity.PanelStationList[item.Key - 96] + "位置处发生拔片操作";
                int level = item.Value;
                msg = msg + ",拔片等级：" + level;
                Program.logNet.WriteInfo("PanelRemove", msg);
                ucDevciesStatus.ShowOperateInfo(msg);
            }
        }

        private void PlcManage_PanelExitEvent(List<int> trueStatus, List<int> falseStatus)
        {
            foreach (int item in falseStatus)
            {
                string msg = AddressEntity.PanelStationList[item] + "位置处panel流出";
                Program.logNet.WriteInfo("PanelExit", msg);
                ucDevciesStatus.ShowOperateInfo(msg);
            }
            foreach (int item in trueStatus)
            {
                string msg = AddressEntity.PanelStationList[item] + "位置处panel流入";
                Program.logNet.WriteInfo("PanelExit", msg);
                ucDevciesStatus.ShowOperateInfo(msg);
            }
            ucStatusMonitor.Invoke(new Action(() => { ucStatusMonitor.UpdatePanelStatus(trueStatus, falseStatus); }));
        }

        private void PlcManage_ScanerStatusEvent(string scanNo, bool status)
        {
            if (this.IsDisposed || this.Disposing)
                return;
            this.BeginInvoke(new Action(() => { ucDevciesStatus.SetScanerStatus(scanNo, status); }));
        }

        private void PlcManage_UnitKeepLiveEvent(int unit_index)
        {
            if (this.IsDisposed || this.Disposing)
                return;
            this.BeginInvoke(new Action(() =>
            {
                ucDevciesStatus.SetUnitStatus(unit_index);
            }));
        }

        private void PlcManage_PLCStatusEvent(int status)
        {
            if (this.IsDisposed || this.Disposing)
                return;
            this.BeginInvoke(new Action(() =>
            {
                ucDevciesStatus.SetPLCStatus(status);
            }));
        }

        /// <summary>
        /// 标定请求事件
        /// </summary>
        /// <param name="manage"></param>
        /// <param name="index">平台索引</param>
        /// <param name="workNum">工位编号</param>
        private void PLCManage_ClibReqEvent(PLCManage manage, int index, ushort workNum)
        {
            //ucDevciesStatus.ShowOperateInfo("收到对位平台" + index + "标定工位" + workNum + "请求");
            provider.StartClibReq(index, workNum);
        }

        /// <summary>
        /// 回原点请求事件
        /// </summary>
        /// <param name="manage"></param>
        /// <param name="index"></param>
        private void PLCManage_OrgReqEvent(PLCManage manage, int index)
        {
            ucDevciesStatus.ShowOperateInfo("收到对位平台" + index + "回原点请求");
            provider.StartOrgReq(index);
        }

        /// <summary>
        /// 自动对位请求事件
        /// </summary>
        /// <param name="manage"></param>
        /// <param name="index"></param>
        /// <param name="entity"></param>
        private void PLCManage_ADJReqEvent(PLCManage manage, int index, ushort workNum)
        {
            //ucDevciesStatus.ShowOperateInfo("收到对位平台" + index + "对位请求");
            provider.StartAutoPos(index, workNum);
        }

        private void FormMain_UserChanged(object sender, UserEventArgs e)
        {

        }

        private void FormMain_ImageReceived(object sender, ImageReceivedArgs e)
        {

        }

        private void btnAutoPos_Click(object sender, EventArgs e)
        {
            VisableControl(frmAutoPos);
        }

        private void btnPLCTest_Click(object sender, EventArgs e)
        {
            if (userManager.IsLogin)
                VisableControl(ucplcTest);
            else
                MessageBox.Show("请登录后再进行该操作！");
        }

        private void btnParaSet_Click(object sender, EventArgs e)
        {
            if (userManager.IsLogin)
                VisableControl(ucParaSet);
            else
                MessageBox.Show("请登录后再进行该操作！");
        }

        private void btnAlarm_Click(object sender, EventArgs e)
        {
            VisableControl(ucAlarm);
        }

        private void btnDataSearch_Click(object sender, EventArgs e)
        {
            VisableControl(ucDataSearch);
        }

        private void btnDataBroadcast_Click(object sender, EventArgs e)
        {
            VisableControl(ucDataStatistics);
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("explorer.exe", Application.StartupPath + "\\log\\");
        }

        private void btnWork_Click(object sender, EventArgs e)
        {
            VisableControl(ucStatusMonitor);
        }

        void VisableControl(Control selectControl)
        {
            foreach (Control item in pnlHost.Controls)
            {
                if (item.Name == selectControl.Name)
                    selectControl.Visible = true;
                else
                    item.Visible = false;
            }
        }

        
    }
}
