﻿using DemuraShuttleLine.Entity;
using DemuraShuttleLine.Interface;
using HYC.HTFile.HTINI;
using System;
using System.Collections.Generic;

namespace DemuraShuttleLine.business
{
    public class ScanerManage
    {
        private readonly string scaner_config_name = "Scanner.ini";
        public event Action<object, bool> ScanerStatus;
        Dictionary<string, ICodeScaner> codeScaners = new Dictionary<string, ICodeScaner>();

        public void Init()
        {
            //读取扫码枪配置文件，初始化扫码枪对象
            HTINIHelper helper = new HTINIHelper(Program.configPath + scaner_config_name);
            List<string> section_names = helper.GetAllSectionNames();
            int no = 1;
            foreach (string name in section_names)
            {
                string ip = helper.Read(name, "IPAddress", "");
                int port = int.Parse(helper.Read(name, "Port", "23"));
                string scanerNo = helper.Read(name, "ScanerNo", no.ToString());
                if (string.IsNullOrEmpty(ip))
                {
                    continue;
                }
                else
                {
                    try
                    {
                        ICodeScaner codeScaner = new CodeScaner(scanerNo, ip, port);
                        codeScaner.ScanerStatus += ScanerStatus;
                        bool status = codeScaner.Connect();
                        if (status)
                            codeScaners.Add(codeScaner.ScanerNo, codeScaner);
                        else
                            Program.logNet.WriteInfo("ScanerManage", "扫码枪" + scanerNo + "网络连接失败！");
                    }
                    catch (Exception)
                    {
                        Program.logNet.WriteInfo("ScanerManage", "扫码枪" + scanerNo + "连接失败！");
                    }
                }
                no++;
            }
        }

        public string ScanCode(char sign, string scanNo = "1")
        {
            if (!codeScaners.ContainsKey(scanNo))
                throw new ArgumentException("无当前可用扫码枪");
            return codeScaners[scanNo].ScanCode(sign);
        }
    }
}
