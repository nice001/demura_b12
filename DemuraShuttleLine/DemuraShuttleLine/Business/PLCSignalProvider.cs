﻿using DemuraShuttleLine.Entity;
using HYC.AutoPosition.Interface;
using System;

namespace DemuraShuttleLine.business
{
    internal class PLCSignalProvider : ISignalProvider
    {
        public event EventHandler<RequestEventArgs> AutoPosRequest;
        public event EventHandler<RequestEventArgs> CalibrationRequest;
        public event EventHandler<MoveReqEventArgs> MoveRequest;
        public event EventHandler<SafetyEventArgs> SafetyChanged;
        public event EventHandler<RequestEventArgs> AutoTeachingRequest;
        /// <summary>
        /// 移动完成事件
        /// </summary>
        public event Action<int, MoveReqType, bool> MoveCompletedEvent;
        /// <summary>
        /// 自动对位完成事件
        /// </summary>
        public event Action<int, int, bool> AutoPosCompletedEvent;
        /// <summary>
        /// 标定完成事件
        /// </summary>
        public event Action<int, int, bool> CalibrationCompletedEvent;
        /// <summary>
        /// 更新对位平台状态事件
        /// </summary>
        public event Action<int, PlatformStatus> UpdatePlatformStatusEvent;
        /// <summary>
        /// 移动事件
        /// </summary>
        public event Func<int, int, double[], int[], bool> MovePLCEvent;

        public event Action<string> MessageNoticeEvent;
      

        public void AutoPosCompleted(int groupIndex, int stageIndex, bool result)
        {
            AutoPosCompletedEvent(groupIndex, stageIndex, result);
        }

        public void CalibrationCompleted(int groupIndex, int stageIndex, bool result)
        {
            CalibrationCompletedEvent(groupIndex, stageIndex, result);
        }

        public void GetImagesCompleted(int groupIndex, int stageIndex, int imageIndex)
        {

        }

        public void GetImagesReady(int groupIndex, int stageIndex, int imageIndex)
        {
        }


        public bool Move(int groupIndex, int stageIndex, double[] xyt, int[] reserve)
        {
            return (bool)(MovePLCEvent?.Invoke(groupIndex, stageIndex, xyt, reserve));
        }

        public bool Move(int groupIndex, double[] xyt, int[] reserve)
        {
            return true;
        }

        public void MoveCompleted(int groupIndex, MoveReqType type, bool result)
        {
            MoveCompletedEvent(groupIndex, type, result);
        }

        public void UpdateCameraStatus(int cameraIndex, bool isConnected)
        {
        }

        public void UpdatePlatformStatus(int platIndex, PlatformStatus status)
        {
            UpdatePlatformStatusEvent?.Invoke(platIndex, status);
        }

        /// <summary>
        /// 自动对位
        /// </summary>
        /// <param name="index">对位平台索引</param>
        /// <param name="entity">对位数据</param>
        public void StartAutoPos(int index, ushort workNum)
        {
            MessageNoticeEvent?.Invoke("收到对位请求平台:" + index + ",工位:" + workNum);
            RequestEventArgs e = new RequestEventArgs();
            e.StageIndex = workNum - 1; ;// ComputerStageIndex(index, workNum); //B7
            e.GroupIndex = index;
            e.DetectMode = 0;
            AutoPosRequest(this, e);
        }

        /// <summary>
        /// 回原点请求
        /// </summary>
        /// <param name="index">对位平台索引0-3</param>
        public void StartOrgReq(int index)
        {
            MoveReqEventArgs e = new MoveReqEventArgs();
            e.GroupIndex = index;
            e.MoveReqType = MoveReqType.Home;
            MoveRequest(this, e);
        }

        /// <summary>
        /// 自动标定请求
        /// </summary>
        /// <param name="index"></param>
        /// <param name="workNum"></param>
        public void StartClibReq(int index, ushort workNum)
        {
            MessageNoticeEvent?.Invoke("收到标定请求信号，平台：" + index + "；工位" + workNum);
            RequestEventArgs e = new RequestEventArgs();
            e.StageIndex = workNum - 1;//ComputerStageIndex(index, workNum); // B7
            e.GroupIndex = index;
            CalibrationRequest(this, e);
        }

        /// <summary>
        /// 计算panel编号
        /// </summary>
        /// <param name="index">对位平台编号0-3</param>
        /// <param name="workNum">工位编号上层1-5，下层6-10</param>
        private int ComputerStageIndex(int index, ushort workNum)
        {
            int panelStart = index / 2 * 20;//计算每侧panel的开始位置，因为总共分两侧，每侧20个panel
            int levelStart = (workNum - 1) % 5 * 4; //计算每一层开始位置，因为每侧5个test，每个工位有4个panel
            int panelStation = (workNum - 1) / 5 * 2 + index % 2 + 1;//每一层有两个panel，计算每一个panel的位置
            return panelStart + levelStart + panelStation - 1;//换算对应编码0-39（对位平台编码从0开始）,最后结果同状态监控页面，示例：A侧1工位编码为上层1、2，下层为3、4
        }

        /// <summary>
        /// 回原点请求
        /// </summary>
        /// <param name="index">对位平台索引0-3</param>
        public void StartMoveReq(int index)
        {
            //todo
            MoveReqEventArgs e = new MoveReqEventArgs();
            e.GroupIndex = index;
            e.MoveReqType = MoveReqType.UserPos1;
            MoveRequest(this, e);
        }

        public void AutoTeachingCompleted(int groupIndex, int stageIndex, int teachingStageIndex, bool result)
        {
            throw new NotImplementedException();
        }
    }
}