﻿using HYC.HTDB;
using System;
using System.Collections.Generic;
using System.Data;

namespace DemuraShuttleLine.Business
{
    public class DBManage
    {
        static string data_source = Properties.Settings.Default.data_source;
        static string db_name = Properties.Settings.Default.db_name;
        static string db_user = Properties.Settings.Default.db_user;
        static string db_pwd = Properties.Settings.Default.db_pwd;
        static string unit_table_name = Properties.Settings.Default.unit_table_name;

        static HTDBHelper db;
        static DBManage()
        {
            string connStr = @"Data Source=" + data_source + ";Initial Catalog=" + db_name + ";UID=" + db_user + ";PWD=" + db_pwd;
            //connStr = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=M2000DAT;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            db = HTDBHelper.CreateSQLServer();
            db.ConnectionString = connStr;
        }

        /// <summary>
        /// 插入报警
        /// </summary>
        /// <param name="alarmId">报警Code</param>
        /// <param name="unitId">PLC编号</param>
        public virtual void InsertAlarm(int alarmId, int plcId)
        {
            try
            {
                string strSql = "insert into ALARMHISTORYLIST (ALARMID,ALARMTIME,EQUIPID) values (@ALARMID,@ALARMTIME,@EQUIPID)";
                db.ExecuteNonQuery(strSql, CommandType.Text,
                    new KeyValuePair<string, object>(@"ALARMID", alarmId),
                    new KeyValuePair<string, object>(@"ALARMTIME", DateTime.Now),
                    new KeyValuePair<string, object>(@"EQUIPID", plcId));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 取消报警
        /// </summary>
        /// <param name="alarmId">报警Code</param>
        /// <param name="plcId">PLC编号</param>
        /// <param name="cancelMode">取消模式</param>
        public virtual void CancelAlarm(int alarmId, int plcId, byte cancelMode = 0)
        {
            try
            {
                string strSql = "update ALARMHISTORYLIST  set ALARMCANCLEMODE=@ALARMCANCLEMODE,ALARMCANCELTIME=GETDATE(),ALIVE=DATEDIFF(second,ALARMTIME,GETDATE()) where ALARMID=@ALARMID and EQUIPID=@EQUIPID and ALARMCANCELTIME is NULL";
                db.ExecuteNonQuery(strSql, CommandType.Text,
                    new KeyValuePair<string, object>(@"ALARMCANCLEMODE", cancelMode),
                    new KeyValuePair<string, object>(@"ALARMID", alarmId),
                    new KeyValuePair<string, object>(@"EQUIPID", plcId));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 查询报警信息
        /// </summary>
        /// <param name="filterParam">查询条件</param>
        /// <param name="plcId">PLC编号</param>
        /// <param name="startTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">显示条数</param>
        /// <returns></returns>
        public virtual DataSet GetAlarmInfoList(Dictionary<string, object> filterParam, int plcId, DateTime startTime, DateTime endTime, int pageIndex, int pageSize)
        {
            string strWhere = "";
            if (filterParam != null)
            {
                foreach (var item in filterParam)
                {
                    strWhere += " and " + item.Key + "=" + item.Value;
                }
            }
            string strSql = "select top " + pageSize + " * from(select row_number() over(order by ALARMTIME asc) as rownumber,count(1) over() as totalsize, his.*,detail.LEVEL,detail.DESCRIPTION,detail.DESCRIPTIONCHS from ALARMHISTORYLIST his left join ALARMDETAIL detail on his.ALARMID=detail.ALARMID  where  EQUIPID=" + plcId + " and ALARMTIME >='" + startTime + "' and ALARMTIME <='" + endTime + "' " + strWhere + ") temp_row where rownumber >" + ((pageIndex - 1) * pageSize);
            DataSet dataSet = db.ExecuteDataSet(strSql, CommandType.Text);
            return dataSet;
        }

        /// <summary>
        /// 查询前10报警信息
        /// </summary>
        /// <param name="filterParam">查询条件</param>
        /// <param name="startTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        public virtual DataSet GetAlarmStatisticsTop10(Dictionary<string, object> filterParam, DateTime startTime, DateTime endTime)
        {
            string strWhere = "";
            if (filterParam != null)
            {
                foreach (var item in filterParam)
                {
                    strWhere += " and " + item.Key + "=" + item.Value;
                }
            }
            string strSql = "select tmp.total,tmp.ALARMID,tmp.EQUIPID,detail.LEVEL,detail.DESCRIPTION,detail.DESCRIPTIONCHS from (select top 10 count(1) total ,his.ALARMID,his.EQUIPID from ALARMHISTORYLIST his join ALARMDETAIL detail on his.ALARMID = detail.ALARMID where ALARMTIME >='" + startTime + "' and ALARMTIME <='" + endTime + "'" + strWhere + " group  by his.ALARMID,his.EQUIPID order by total desc) tmp join ALARMDETAIL detail on tmp.ALARMID = detail.ALARMID";
            DataSet dataSet = db.ExecuteDataSet(strSql, CommandType.Text);
            return dataSet;
        }

        /// <summary>
        ///  插入检测数据
        /// </summary>
        /// <param name="param">插入的字段</param>
        public virtual void InsertDemuraData(Dictionary<string, object> param)
        {
            if (param.Count == 0)
                return;
            try
            {
                string strSql = "insert into " + unit_table_name + " (";
                foreach (KeyValuePair<string, object> item in param)
                {
                    strSql += item.Key + ",";
                }
                strSql = strSql.Remove(strSql.Length - 1);
                strSql += ") valuses (";
                foreach (KeyValuePair<string, object> item in param)
                {
                    strSql += item.Value + ",";
                }
                strSql = strSql.Remove(strSql.Length - 1);
                strSql += ")";
                db.ExecuteNonQuery(strSql, CommandType.Text);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 查询检测数据
        /// </summary>
        /// <param name="filterParam">查询条件</param>
        /// <param name="startTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">显示条数</param>
        /// <returns></returns>
        public virtual DataSet GetDemuraDatas(Dictionary<string, object> filterParam, DateTime startTime, DateTime endTime, int pageIndex, int pageSize)
        {
            string strWhere = "";
            if (filterParam != null)
            {
                foreach (var item in filterParam)
                {
                    strWhere += " and " + item.Key + "=" + item.Value;
                }
            }
            string strSql = "select top " + pageSize + " *,DATEDIFF(second,STARTTIME,ENDTIME) as CheckTime from(select row_number() over(order by ENDTIME asc) as rownumber,count(1) over() as totalsize, * from " + unit_table_name + " where ENDTIME >='" + startTime + "' and ENDTIME <='" + endTime + "' " + strWhere + ") temp_row where rownumber >" + ((pageIndex - 1) * pageSize);
            DataSet dataSet = db.ExecuteDataSet(strSql, CommandType.Text);
            return dataSet;
        }

        /// <summary>
        /// 获取数据统计信息
        /// </summary>
        /// <param name="ngList">NG列表</param>
        /// <param name="groupbyColumn">排序列</param>
        /// <param name="dtStart">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        public virtual DataSet GetDataStatistics(Dictionary<string, string> ngList, string groupbyColumn, DateTime dtStart, DateTime endTime)
        {
            string des = "";
            string searchText = "";
            if (groupbyColumn == "TESTERID")
                des = "单体编号";
            else if (groupbyColumn == "EQPID")
                des = "治具编号";
            string strSql = "select count(1) as 总数量" + (groupbyColumn != null ? "," + groupbyColumn + " as " + des : "");
            strSql += ",sum(case when RESULT=0 then 1 else 0 end) 未检测数量"; //未检测数量
            strSql += ",sum(case when RESULT = 0 then 1 else 0 end) * 1.0 / count(1) 未检测比例";
            strSql += ",sum(case when RESULT=1 then 1 else 0 end) 检测OK数量"; //检测OK数量
            strSql += ",sum(case when RESULT =1 then 1 else 0 end) * 1.0 / count(1) 检测OK比例";
            foreach (KeyValuePair<string, string> item in ngList)
            {
                strSql += ",sum(case when RESULT=" + item.Key + " then 1 else 0 end) " + item.Value;
                strSql += ",sum(case when RESULT=" + item.Key + " then 1 else 0 end) * 1.0 / count(1) " + item.Value + "比例";
                searchText += "," + item.Value + "," + item.Value + "比例";
            }
            strSql += " from " + unit_table_name + " where ENDTIME>='" + dtStart + "' and ENDTIME<='" + endTime + "'" + (groupbyColumn != null ? " group by " + groupbyColumn : "");
            strSql = "select 总数量," + (string.IsNullOrEmpty(des) ? "" : des + ",") + "未检测数量,未检测比例,检测OK数量,检测OK比例,(总数量-检测OK数量-未检测数量) as  检测NG数量,(总数量-检测OK数量-未检测数量)* 1.0 /总数量 as 检测NG比例" + searchText + " from (" + strSql + ") tmp";//检测NG总数量
            DataSet dataSet = db.ExecuteDataSet(strSql, CommandType.Text);
            return dataSet;
        }

        /// <summary>
        /// 根据panelId获取检测状态
        /// </summary>
        /// <param name="panelId"></param>
        /// <returns></returns>
        public virtual int GetDemuraStatusByPanelId(string panelId)
        {
            string strSql = "select RESULT from " + unit_table_name + " where PANELID=" + panelId;
            DataSet dataSet = db.ExecuteDataSet(strSql, CommandType.Text);
            if (dataSet.Tables.Count > 1)
            {
                return (int)dataSet.Tables[0].Rows[0]["RESULT"];
            }
            return -1;
        }
    }
}
