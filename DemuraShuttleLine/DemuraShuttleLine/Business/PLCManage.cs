﻿using CommonTool;
using DemuraClient;
using DemuraDB;
using DemuraServer;
using DemuraShuttleLine.business;
using DemuraShuttleLine.Entity;
using DemuraShuttleLine.Properties;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace DemuraShuttleLine.Business
{
    public class PLCManage
    {
        private readonly string plc_config_name = "plc.json";
        string plc_config_path;
        Dictionary<string, object> plcObjects = new Dictionary<string, object>();
        private ScanerManage scanerManage;
        private MessageServer msgServer;
        CIMClient cim_client;
        DemuraBaseDB dbManage = DemuraBaseDB.GetDemuraDB();
        int currentPlcId = Settings.Default.current_plcId;

        /// <summary>
        /// CIMServer状态
        /// </summary>
        public event Action<bool> CIMServerStatus;
        /// <summary>
        /// 对位请求事件
        /// </summary>
        public event Action<PLCManage, int, ushort> ADJReqEvent;
        /// <summary>
        /// 回原点请求事件
        /// </summary>
        public event Action<PLCManage, int> OrgReqEvent;
        /// <summary>
        /// 标定请求事件
        /// </summary>
        public event Action<PLCManage, int, ushort> ClibReqEvent;
        /// <summary>
        /// Unit保活事件
        /// </summary>
        public event Action<int> UnitKeepLiveEvent;
        /// <summary>
        /// PLC状态事件
        /// </summary>
        public event Action<int> PLCStatusEvent;
        /// <summary>
        /// PLC 连接失败
        /// </summary>
        public event Action<bool> PLCConnectEvent;
        /// <summary>
        /// 扫码枪状态事件
        /// </summary>
        public event Action<string, bool> ScanerStatusEvent;
        /// <summary>
        ///CIM消息
        /// </summary>
        public event Action<string> CIMTerminalTextEvent;
        /// <summary>
        ///扫码失败
        /// </summary>
        public event Func<Tuple<MCRResultType, string>> MCRFailEvent;
        /// <summary>
        /// Panel存在状态事件
        /// </summary>
        public event Action<List<int>, List<int>> PanelExitEvent;
        /// <summary>
        /// Panel拔片事件
        /// </summary>
        public event Action<Dictionary<int, int>> PanelRemoveEvent;
        /// <summary>
        /// 消息显示
        /// </summary>
        public event Action<string> MessageShowEvent;


        public void Init()
        {
            Task.Factory.StartNew(() =>
            {
                InitMessageServer();
            });
            plc_config_path = Program.configPath + plc_config_name;
            List<PLCBaseEntity> tmp = ReadPLCConfig();
            if (tmp.Count == 0)
                throw new ArgumentException("PLC配置格式无效，无法初始化PLC！");
            foreach (PLCBaseEntity item in tmp)
            {
                Task.Factory.StartNew(() =>
                {
                    try
                    {
                        object tmpPlc = InitPLC(item);
                        if (tmpPlc != null)
                            plcObjects.Add(item.PlcType, tmpPlc);
                    }
                    catch (Exception ex)
                    {
                        Program.logNet.WriteWarn("InitPLC", "初始化" + item.PlcType + "PLC失败，原因为：" + ex.Message);
                    }
                });
            }
            Task.Factory.StartNew(() =>
            {
                scanerManage = new ScanerManage();
                scanerManage.ScanerStatus += ScanerManage_ScanerStatus;
                scanerManage.Init();
            });

            if (Program.softRunParamaters.IsAddLine)
            {
                Task.Factory.StartNew(() =>
                {
                    try
                    {
                        cim_client = new CIMClient(Settings.Default.cim_ip);
                        cim_client.CIMServerStatus += Cim_client_CIMServerStatus;
                        cim_client.DateTimeSetEvent += Cim_client_DateTimeSetEvent;
                        cim_client.LDBuzzONEvent += Cim_client_LDBuzzONEvent;
                        cim_client.OperatorCallEvent += Cim_client_OperatorCallEvent;
                        cim_client.TerminalTextEvent += Cim_client_TerminalTextEvent;
                        cim_client.VCRReportACKEvent += Cim_client_VCRReportACKEvent;
                        cim_client.JobJudgeACKEvent += Cim_client_JobJudgeACKEvent;
                        cim_client.StartWork();
                    }
                    catch (Exception ex)
                    {
                        Program.logNet.WriteInfo("CIMConnect", "CIM连接失败，原因为：" + ex.Message);
                    }
                });
            }
        }

        private void Cim_client_CIMServerStatus(bool obj)
        {
            msgServer.SendStatus("CIM", obj);
            CIMServerStatus?.BeginInvoke(obj, null, null);
        }

        #region CIM
        private void Cim_client_JobJudgeACKEvent(int index, bool result, string panelId)
        {
            if (!result)
            {
                MessageShowEvent?.BeginInvoke("CIM上报" + panelId + "出站，响应结果为不允许", null, null);
                return;
            }
            if (index == 1)
            {
                WriteCmd("M" + TransitPlat_Ack.DVAck1);
            }
            else
            {
                WriteCmd("M" + TransitPlat_Ack.DVAck2);
            }
            MessageShowEvent?.BeginInvoke("CIM上报" + panelId + "允许出站", null, null);
        }

        private void Cim_client_VCRReportACKEvent(int no, bool result, string panelId)
        {
            HandleMCRResult(no, result, panelId);
        }

        private void Cim_client_TerminalTextEvent(string text)
        {
            CIMTerminalTextEvent?.BeginInvoke(text, null, null);
        }

        private void Cim_client_OperatorCallEvent(string text)
        {
            WriteDatas(GlobalParameters.CIM_Stauts, BitConverter.GetBytes((short)CIMStatusEnum.OperatorCallTag));
            CIMTerminalTextEvent?.BeginInvoke(text, null, null);
        }

        private void Cim_client_LDBuzzONEvent(ushort code)
        {
            WriteDatas(GlobalParameters.CIM_AlarmCode, BitConverter.GetBytes(code));
        }

        private void Cim_client_DateTimeSetEvent(string strTime)
        {
            msgServer.UpdateTime(strTime);
        }
        #endregion

        #region MessageServer
        void InitMessageServer()
        {
            msgServer = new MessageServer(Program.logNet);
            msgServer.UnitWritePanelEvent += MsgServer_WritePLCEvent;
            msgServer.UnitKeepLiveEvent += MsgServer_UnitKeepLiveEvent;
            msgServer.UnitACKEvent += MsgServer_UnitACKEvent;
            msgServer.UnitWriteDataEvent += MsgServer_UnitDataEvent;
            msgServer.UnitReadDataEvent += MsgServer_UnitReadDataEvent;
            msgServer.UnitReadPLCPanelEvent += MsgServer_UnitReadPLCPanelEvent;
            msgServer.UnitReadPanelEvent += MsgServer_UnitReadPanelEvent;
            msgServer.ReDemuraHappenEvent += MsgServer_ReDemuraHappenEvent;
            msgServer.Init();
        }

        private void MsgServer_UnitReadPanelEvent(Socket client, int panelIndex)
        {
            try
            {
                byte[] datas = ReadDatas("D" + (GlobalParameters.D_Unit_StartAddress + (panelIndex - 1) * GlobalParameters.D_Unit_Buffer_Length), GlobalParameters.D_Unit_Buffer_Length);
                ServerGlobal.logNet.WriteInfo("接收到ReadPanel请求，读数据完成");
                msgServer.SendSocketData(client, datas);
                ServerGlobal.logNet.WriteInfo("接收到ReadPanel请求，socket发送完成");
            }
            catch (Exception ex)
            {
                Program.logNet.WriteInfo("UnitReadPanel", ex.Message);
            }
        }

        private void MsgServer_ReDemuraHappenEvent(string obj)
        {
            //todo 5-12#线需要与CIM通信
        }

        private void ScanerManage_ScanerStatus(object scaner_object, bool status)
        {
            if (int.Parse(((CodeScaner)scaner_object).ScanerNo) == 1)
                WriteDatas("D" + GlobalParameters.scan1Status, BitConverter.GetBytes(status ? (short)1 : (short)2));
            else if (int.Parse(((CodeScaner)scaner_object).ScanerNo) == 2)
                WriteDatas("D" + GlobalParameters.scan2Status, BitConverter.GetBytes(status ? (short)1 : (short)2));
            ScanerStatusEvent?.Invoke(((CodeScaner)scaner_object).ScanerNo, status);
            if (cim_client != null)
                cim_client.VCRStatusChangeReport(int.Parse(((CodeScaner)scaner_object).ScanerNo), status);
        }

        private void MsgServer_UnitReadPLCPanelEvent(Socket client, int panelIndex)
        {
            try
            {
                byte[] datas = ReadDatas("D" + (GlobalParameters.D_Read_Unit_StartAddress + (panelIndex - 1) * GlobalParameters.D_Read_Unit_Buffer_Length), GlobalParameters.D_Read_Unit_Buffer_Length);
                ServerGlobal.logNet.WriteInfo("接收到ReadPLCPanel请求，读数据完成");
                msgServer.SendSocketData(client, datas);
                ServerGlobal.logNet.WriteInfo("接收到ReadPLCPanel请求，socket发送完成");
            }
            catch (Exception ex)
            {
                Program.logNet.WriteInfo("UnitReadPanel", ex.Message);
            }
        }

        private void MsgServer_UnitReadDataEvent(Socket client, int address, ushort length, string area)
        {
            try
            {
                byte[] datas;
                if (area == "D")
                    datas = ReadDatas("D" + address, length);
                else
                {
                    bool[] m_datas = ReadCmd("M" + address, length);
                    datas = TypeConvert.BoolArrayToByteArray(m_datas);
                }
                ServerGlobal.logNet.WriteInfo("接收到GetSingleData请求" + address + "读数据完成");
                msgServer.SendSocketData(client, datas);
                ServerGlobal.logNet.WriteInfo("接收到GetSingleData请求" + address + "socket发送完成");
            }
            catch (Exception ex)
            {
                Program.logNet.WriteInfo("UnitReadData", ex.Message);
            }
        }

        private void MsgServer_UnitDataEvent(JObject jObject)
        {
            int address = (int)jObject.GetValue("address");
            byte[] datas = (byte[])jObject.GetValue("data");
            string type = jObject.GetValue("type").ToString();
            int length = (int)jObject.GetValue("length");
            try
            {
                if (type == "string")
                    WriteStringDatas("D" + address, Encoding.ASCII.GetString(datas), length);
                else
                    WriteDatas("D" + address, datas);
            }
            catch (Exception ex)
            {
                Program.logNet.WriteInfo("UnitSingleData", ex.Message);
            }
        }

        private void MsgServer_UnitACKEvent(int address, bool result)
        {
            try
            {
                WriteCmd("M" + address, result);
            }
            catch (Exception ex)
            {
                Program.logNet.WriteInfo("UnitACK", ex.Message);
            }
        }

        private void MsgServer_WritePLCEvent(int panelIndex, byte[] datas)
        {
            try
            {
                int address = (panelIndex - 1) * GlobalParameters.D_Unit_Buffer_Length + GlobalParameters.D_Unit_StartAddress;
                WriteDatas("D" + address, datas);
            }
            catch (Exception ex)
            {
                Program.logNet.WriteInfo("UnitToPlc", ex.Message);
            }
        }

        Dictionary<int, bool> unitStatus = new Dictionary<int, bool>();
        private void MsgServer_UnitKeepLiveEvent(int unitIndex)
        {
            UnitKeepLiveEvent?.Invoke(unitIndex);
            if (!plcObjects.ContainsKey("status"))
                return;
            lock (unitStatus)
            {
                if (unitStatus.ContainsKey(unitIndex))
                    unitStatus[unitIndex] = !unitStatus[unitIndex];
                else
                    unitStatus.Add(unitIndex, true);
            }
            ((PLCStatusEntity)plcObjects["status"]).UpdateUnitStatus(unitIndex, unitStatus[unitIndex]);
          
        }
        #endregion

        #region PLC
        /// <summary>
        /// 读取PCL配置文件
        /// </summary>
        /// <returns></returns>
        List<PLCBaseEntity> ReadPLCConfig()
        {
            if (!File.Exists(plc_config_path))
            {
                throw new FileNotFoundException("PLC配置文件不存在！");
            }
            string str_config = File.ReadAllText(plc_config_path);
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Deserialize<List<PLCBaseEntity>>(str_config);
        }

        /// <summary>
        /// 初始化PLC
        /// </summary>
        /// <param name="plcEntity"></param>
        object InitPLC(PLCBaseEntity plcEntity)
        {
            object tmpPlc = plcEntity;
            try
            {
                switch (plcEntity.PlcType)
                {
                    case "signal"://用于信号监听
                        PLCSignalEntity signalEntity = new PLCSignalEntity();
                        signalEntity.PLCSignalEvent += SignalEntity_PLCSignalEvent;
                        signalEntity.Clone(plcEntity);
                        plcEntity = signalEntity;
                        tmpPlc = signalEntity;
                        break;
                    case "alarm"://用于报警监听
                        PLCAlarmEntity alarmEntity = new PLCAlarmEntity();
                        alarmEntity.WeightAlarmHappenEvent += AlarmHappenEvent;
                        alarmEntity.WeightAlarmOverEvent += AlarmOverEvent;
                        alarmEntity.LightAlarmHappenEvent += AlarmHappenEvent;
                        alarmEntity.LightAlarmOverEvent += AlarmOverEvent;
                        alarmEntity.Clone(plcEntity);
                        plcEntity = alarmEntity;
                        tmpPlc = alarmEntity;
                        break;
                    case "status"://用于状态监听与维护
                        PLCStatusEntity statusEntity = new PLCStatusEntity();
                        statusEntity.PLCStatusEvent += StatusEntity_PLCStatusEvent;
                        statusEntity.ConnectEvent += StatusEntity_ConnectEvent;
                        statusEntity.PanelStatusEvent += StatusEntity_PanelStatusEvent;
                        statusEntity.TimeUpdateEvent += StatusEntity_TimeUpdateEvent;
                        statusEntity.Clone(plcEntity);
                        plcEntity = statusEntity;
                        tmpPlc = statusEntity;
                        break;
                    case "main"://主要用于写数据
                        break;
                }
                plcEntity.InitPLC();
                Program.logNet.WriteInfo(plcEntity.PlcType + "PLC已连接");
                plcEntity.Start();
                return tmpPlc;
            }
            catch (Exception ex)
            {
                Program.logNet.WriteInfo(ex.Message);
                return null;
            }
        }

        private void StatusEntity_ConnectEvent(bool obj)
        {
            if (msgServer == null)
                Thread.Sleep(5000);//等待server启动，如果超过5秒没启动，表示超时失败
            if (msgServer == null)
                throw new TimeoutException("MessageServer初始化超时！");
            msgServer.UpdatePLCConnect(obj);
            /// UI界面显示
            PLCConnectEvent?.Invoke(obj);
        }

        private void StatusEntity_PanelStatusEvent(List<int> trueStatus, List<int> falseStatus)
        {
            //todo  拔片地址没给，待与PLC确认
            List<int> removeList = trueStatus.FindAll(c => { return c > 495; });//拔片地址
            Dictionary<int, int> removeDict = new Dictionary<int, int>();
            foreach (int item in removeList)
            {
                byte[] datas = ReadDatas(GlobalParameters.RemovePanelLevel, 1);
                int level = BitConverter.ToInt16(datas, 0);
                removeDict.Add(item, level);
            }
            if (removeDict.Count > 0)
            {
                PanelRemoveEvent?.Invoke(removeDict);
                if (cim_client != null)
                    foreach (int key in removeDict.Keys)
                    {
                        string panelId = GetPanelIdByPosition(key);//获取当前位置PanelID
                        cim_client.RemovedJobReport(panelId, removeDict[key]);//上报CIM拔片信息
                        MessageShowEvent?.BeginInvoke("上报CIM拔片信息" + panelId, null, null);
                    }
            }
            List<int> exitTrueList = trueStatus.FindAll(c => { return c <= 495; });//Exit地址
            List<int> exitFalseList = falseStatus.FindAll(c => { return c <= 495; });//Exit地址
            SendPanelStatus(exitTrueList, exitFalseList);
            PanelExitEvent?.Invoke(exitTrueList, exitFalseList);
        }

        private void StatusEntity_TimeUpdateEvent(string strTime)
        {
            if (msgServer == null)
                Thread.Sleep(5000);//等待server启动，如果超过5秒没启动，表示超时失败
            if (msgServer == null)
                throw new TimeoutException("MessageServer初始化超时！");
            msgServer.UpdateTime(strTime);
        }

        private void StatusEntity_PLCStatusEvent(int obj)
        {
            if (msgServer == null)
                Thread.Sleep(5000);//等待server启动，如果超过5秒没启动，表示超时失败
            if (msgServer == null)
                throw new TimeoutException("MessageServer初始化超时！");
            msgServer.UpdatePLCStatus(obj);
            if (cim_client != null)
                cim_client.MachineStatusChangeReport(obj);
            //Console.WriteLine("11111111111111111111111111111111111111111111111111111111111111111111111----------------"+obj);
            PLCStatusEvent?.Invoke(obj);
        }

        private void AlarmOverEvent(List<int> alarmCodes)
        {
            foreach (int item in alarmCodes)
            {
                try
                {
                    dbManage.CancelAlarm(item, currentPlcId);
                    if (cim_client != null)
                    {
                        DataSet ds = dbManage.GetAlarmMsg(item);
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            string alarmText = ds.Tables[0].Rows[0]["DESCRIPTION"].ToString();
                            short level = (short)ds.Tables[0].Rows[0]["LEVEL"];
                            cim_client.AlarmStatusReport(2, item, 1, level, alarmText);
                            MessageShowEvent?.BeginInvoke("上报CIM报警信息" + item, null, null);
                        }
                    }
                }
                catch (Exception ex) { Program.logNet.WriteInfo("取消报警信息失败，原因:" + ex.Message); }
            }
        }

        private void AlarmHappenEvent(List<int> alarmCodes)
        {
            foreach (int item in alarmCodes)
            {
                try
                {
                    dbManage.InsertAlarm(item, currentPlcId);
                    if (cim_client != null)
                    {
                        DataSet ds = dbManage.GetAlarmMsg(item);
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            string alarmText = ds.Tables[0].Rows[0]["DESCRIPTION"].ToString();
                            short level = (short)ds.Tables[0].Rows[0]["LEVEL"];
                            cim_client.AlarmStatusReport(1, item, 1, level, alarmText);
                            MessageShowEvent?.BeginInvoke("上报CIM报警信息" + item, null, null);
                        }
                    }
                }
                catch (Exception ex) { Program.logNet.WriteInfo("插入报警信息失败，原因:" + ex.Message); }
            }
        }

        private void SignalEntity_PLCSignalEvent(bool[] datas)
        {
            HandleSignal(datas);
        }

        /// <summary>
        /// 解析单体动作
        /// </summary>
        /// <param name="plcObject"></param>
        /// <param name="cmds"></param>
        private void HandleSignal(bool[] cmds)
        {
            CompareCmdChange(cmds, GlobalParameters.Action_Address);
            if (msgServer == null)
                Thread.Sleep(5000);//等待server启动，如果超过5秒没启动，表示超时失败
            if (msgServer == null)
                throw new TimeoutException("MessageServer初始化超时！");
            msgServer.StartAction(cmds);
        }

        //记录上一次信号状态，用于判断是否发生变化
        bool[] lastActions;

        private void CompareCmdChange(bool[] datas, int address_offer)
        {
            if (!plcObjects.ContainsKey("main"))
                return;
            List<int> indexList = new List<int>();
            List<int> overList = new List<int>();
            DataCompare.CompareBool(datas, lastActions, ref indexList, ref overList, address_offer);
            lastActions = datas;
            HandleLineSignal((PLCBaseEntity)plcObjects["main"], indexList);
        }

        /// <summary>
        /// 解析PLC信号
        /// </summary>
        /// <param name="plcObject"></param>
        /// <param name="cmds"></param>
        private void HandleLineSignal(PLCBaseEntity plcObject, List<int> cmds)
        {
            foreach (int cmd in cmds)
            {
                Program.logNet.WriteInfo("PLCCmd", "收到PLC执行信号" + cmd);
                switch (cmd)
                {
                    case InArmCmd_Req.MCR1Req://扫码第1片请求
                        HandleMCRReq(plcObject, 1);
                        break;
                    case InArmCmd_Req.MCR2Req://扫码第2片请求
                        HandleMCRReq(plcObject, 2);
                        break;
                    case UVW_A_Req.ADJ1Req://A对位平台第1片调整请求    
                        HandleADJReq(plcObject, D_UVW_Address.UVW_A1, 0);
                        break;
                    case UVW_A_Req.ADJ2Req://A对位平台第2片调整请求 
                        HandleADJReq(plcObject, D_UVW_Address.UVW_A2, 1);
                        break;
                    case UVW_A_Req.Clib1Req://A标定1请求
                        HandleClibReq(plcObject, D_UVW_Address.UVW_A1, 0);
                        break;
                    case UVW_A_Req.Clib2Req://A标定2请求
                        HandleClibReq(plcObject, D_UVW_Address.UVW_A2, 1);
                        break;
                    case UVW_A_Req.Org1Req://A对位平台第1片回原点
                        OrgReqEvent?.Invoke(this, 0);
                        break;
                    case UVW_A_Req.Org2Req://A对位平台第2片回原点
                        OrgReqEvent?.Invoke(this, 1);
                        break;
                    case UVW_B_Req.ADJ1Req://B对位平台第1片调整请求
                        HandleADJReq(plcObject, D_UVW_Address.UVW_B1, 2);
                        break;
                    case UVW_B_Req.ADJ2Req://B对位平台第2片调整请求
                        HandleADJReq(plcObject, D_UVW_Address.UVW_B2, 3);
                        break;
                    case UVW_B_Req.Clib1Req://B标定1请求
                        HandleClibReq(plcObject, D_UVW_Address.UVW_B1, 2);
                        break;
                    case UVW_B_Req.Clib2Req://B标定2请求
                        HandleClibReq(plcObject, D_UVW_Address.UVW_B2, 3);
                        break;
                    case UVW_B_Req.Org1Req://B对位平台第1片回原点
                        OrgReqEvent?.Invoke(this, 2);
                        break;
                    case UVW_B_Req.Org2Req://B对位平台第2片回原点
                        OrgReqEvent?.Invoke(this, 3);
                        break;
                    case TransitPlat_Req.DVReq1://DV1数据
                        HandleDVReq(1);
                        break;
                    case TransitPlat_Req.DVReq2://DV2数据
                        HandleDVReq(2);
                        break;
                    case Out_UVW_Req.Snap1Req://下料平台1拍照
                        HandleOutUVWReq(4,49);
                        break;
                    case Out_UVW_Req.Snap2Req://下料平台2拍照
                        HandleOutUVWReq(5,50);
                        break;
                    case Out_UVW_Req.Snap1Clib:
                        HandleOutUVWClibReq(4, 49); ///下料平台1 标定
                        break;
                    case Out_UVW_Req.Snap2Clib:
                        HandleOutUVWClibReq(5, 50); //下料平台2 标定
                        break;
                    case Out_Tray_Req.SnapReq://Tray盘拍照
                        HandleOutTrayReq(6,51);
                        break;
                    default:
                        Program.logNet.WriteWarn("PLCCmd", "暂不支持该信号M" + cmd + "！");
                        break;
                }
            }
        }

        /// <summary>
        /// 解析自动对位请求
        /// </summary>
        /// <param name="plcObject">plc对象</param>
        /// <param name="startAddress">数据起始位置</param>
        /// <param name="index">对位平台索引0-3</param>
        private void HandleADJReq(PLCBaseEntity plcObject, int startAddress, int index)
        {
            if (ADJReqEvent == null)
                return;
            byte[] datas = plcObject.ReadData("D" + startAddress, 1);
            ushort workNum = BitConverter.ToUInt16(datas, 0);
            ADJReqEvent(this, index, workNum);
        }

        private void HandleClibReq(PLCBaseEntity plcObject, int startAddress, int index)
        {
            if (ClibReqEvent == null)
                return;
            byte[] datas = plcObject.ReadData("D" + startAddress, 1);
            ushort workNum = BitConverter.ToUInt16(datas, 0);
            ClibReqEvent(this, index, workNum);
        }
        private void HandleOutUVWClibReq(int index, ushort workNum)
        {
            if (ClibReqEvent == null)
                return;
            ClibReqEvent(this, index, workNum);
        }

        /// <summary>
        /// 解析扫码请求
        /// </summary>
        /// <param name="plcObject"></param>
        private void HandleMCRReq(PLCBaseEntity plcObject, int index)
        {
            try
            {
                string oldPanelId = "";
                byte[] tmpBytes;
                if (index == 1)
                {
                    tmpBytes = plcObject.ReadData("D" + D_InArm_PLC_Address.InArm_1, 50);
                }
                else
                {
                    tmpBytes = plcObject.ReadData("D" + D_InArm_PLC_Address.InArm_2, 50);
                }
                oldPanelId = Encoding.ASCII.GetString(tmpBytes);
                if (Program.softRunParamaters.IsShieldScaner)
                {
                    HandleMCRResult(index, true, oldPanelId);
                    return;
                }
                string panelId = scanerManage.ScanCode(Program.softRunParamaters.ScanerSign);
                if (string.IsNullOrWhiteSpace(panelId))
                {
                    Tuple<MCRResultType, string> result = MCRFailEvent?.Invoke();
                    if (result.Item1 == MCRResultType.KeyIn)
                    {
                        panelId = result.Item2;
                    }
                    else
                    {
                        HandleMCRResult(index, true, oldPanelId);
                        return;
                    }
                }
                if (cim_client != null)
                {
                    cim_client.VCRReadResultReport(1, index, panelId, oldPanelId);
                    MessageShowEvent?.BeginInvoke("上报CIM扫码信息，位置:" + index + ";panelId:" + panelId, null, null);
                }
            }
            catch (Exception ex)
            {
                Program.logNet.WriteInfo("HandleMCRReq", ex.Message);
            }
        }

        /// <summary>
        /// 解析MCR扫码结果
        /// </summary>
        /// <param name="index">位置1/2</param>
        /// <param name="result">扫码结果</param>
        /// <param name="panelId">ppid</param>
        void HandleMCRResult(int index, bool result, string panelId)
        {
            if (!result)
            {
                if (index == 1)
                    WriteCmd("M" + InArmCmd_Ack.MCR1Fail);
                else
                    WriteCmd("M" + InArmCmd_Ack.MCR2Fail);
            }
            else
            {
                int address;
                if (index == 1)
                    address = D_InArm_Address.InArm_1;
                else
                    address = D_InArm_Address.InArm_2;
                InArmEntity armEntity = new InArmEntity
                {
                    PanelID = panelId,
                    PanelStatus = 3,//OK
                    DeviceStatus = 1//OnLine
                };
                WriteDatas("D" + address, TypeConvert.StructToBytes(armEntity));
                if (index == 1)
                    WriteCmd("M" + InArmCmd_Ack.MCR1OK);
                else
                    WriteCmd("M" + InArmCmd_Ack.MCR2OK);
            }
        }

        void HandleDVReq(int index)
        {
            string panelId;
            if (index == 1)
                panelId = GetPanelIdByPosition(60);
            else
                panelId = GetPanelIdByPosition(61);
            DataSet ds = dbManage.GetDemuraDataByPanelId(panelId);
            if (ds.Tables.Count > 0)
            {
                int result = (int)ds.Tables[0].Rows[0][""];
                DVInfo info = new DVInfo();
                info.pgInfo = ds.Tables[0].Rows[0]["PGNAME"].ToString();
                info.preInfo = ds.Tables[0].Rows[0]["PREPROCESSING"].ToString();
                info.lastInfo = ds.Tables[0].Rows[0]["POSTPROCESSING"].ToString();
                if (cim_client != null)
                {
                    cim_client.JobJudgeChangeReport(index, panelId, result);
                    MessageShowEvent?.BeginInvoke("上报CIM出站信息，位置:" + index + ";结果:" + result + ";panelId:" + panelId, null, null);
                }
                else
                {
                    if (index == 1)
                    {
                        WriteDatas("D" + GlobalParameters.dv1Datas, TypeConvert.StructToBytes(info));
                        WriteCmd("M" + TransitPlat_Ack.DVAck1);
                    }
                    else
                    {
                        WriteDatas("D" + GlobalParameters.dv2Datas, TypeConvert.StructToBytes(info));
                        WriteCmd("M" + TransitPlat_Ack.DVAck2);
                    }
                }
            }
            else
                Program.logNet.WriteInfo("HandleDVReq", "数据库查询" + panelId + "信息无结果！");
        }

        /// <summary>
        /// 解析下料平台自动对位请求
        /// </summary>
        /// <param name="groupIndex"> 组号</param>
        /// <param name="workNum"></param>
        private void HandleOutUVWReq(int groupIndex,ushort workNum)
        {           
            //todo
            if (ADJReqEvent == null)
                return;         
            ADJReqEvent(this, groupIndex, workNum);
        }


        /// <summary>
        /// 解析Tray满拍照请求
        /// </summary>
        /// <param name="groupIndex">组号</param>
        /// <param name="workNum"></param>
        private void HandleOutTrayReq(int groupIndex, ushort workNum)
        {
            if (ADJReqEvent == null)
                return;
            ADJReqEvent(this, groupIndex, workNum);
        }

        /// <summary>
        /// 读取M区信号
        /// </summary>
        /// <param name="address">地址</param>
        /// <param name="length">长度</param>
        /// <returns></returns>
        public bool[] ReadCmd(string address, ushort length)
        {
            if (!plcObjects.ContainsKey("main"))
                Thread.Sleep(5000);//等待PLC启动，如果超过5秒没启动，表示超时失败
            if (!plcObjects.ContainsKey("main"))
                throw new TimeoutException("PLC初始化超时！");
            return ((PLCBaseEntity)plcObjects["main"]).ReadCmd(int.Parse(address.Remove(0, 1)), length);
        }

        /// <summary>
        /// 读取指定地址数据
        /// </summary>
        /// <param name="address">地址</param>
        /// <param name="length">读取长度</param>
        /// <returns></returns>
        public byte[] ReadDatas(string address, ushort length)
        {
            if (!plcObjects.ContainsKey("main"))
                Thread.Sleep(5000);//等待PLC启动，如果超过5秒没启动，表示超时失败
            if (!plcObjects.ContainsKey("main"))
                throw new TimeoutException("PLC初始化超时！");
            return ((PLCBaseEntity)plcObjects["main"]).ReadData(address, length);
        }

        /// <summary>
        /// 写M区信号
        /// </summary>
        /// <param name="address">地址</param>
        /// <param name="result">写入结果</param>
        public void WriteCmd(string address, bool result = true)
        {
            if (!plcObjects.ContainsKey("main"))
                Thread.Sleep(5000);//等待PLC启动，如果超过5秒没启动，表示超时失败
            if (!plcObjects.ContainsKey("main"))
                throw new TimeoutException("PLC初始化超时！");
            ((PLCBaseEntity)plcObjects["main"]).WriteActionCmd(address, result);
        }

        /// <summary>
        /// 写字符数据
        /// </summary>
        /// <param name="address">地址</param>
        /// <param name="datas">数据</param>
        /// <param name="length">长度</param>
        public void WriteStringDatas(string address, string datas, int length = 0)
        {
            if (!plcObjects.ContainsKey("main"))
                Thread.Sleep(5000);//等待PLC启动，如果超过5秒没启动，表示超时失败
            if (!plcObjects.ContainsKey("main"))
                throw new TimeoutException("PLC初始化超时！");
            ((PLCBaseEntity)plcObjects["main"]).WriteStringData(address, datas, length);
        }

        /// <summary>
        /// 写数据
        /// </summary>
        /// <param name="address">地址</param>
        /// <param name="datas">数据</param>
        public void WriteDatas(string address, byte[] datas)
        {
            if (!plcObjects.ContainsKey("main"))
                Thread.Sleep(5000);//等待PLC启动，如果超过5秒没启动，表示超时失败
            if (!plcObjects.ContainsKey("main"))
                throw new TimeoutException("PLC初始化超时！");
            ((PLCBaseEntity)plcObjects["main"]).WriteData(address, datas);
        }

        /// <summary>
        /// 扫码
        /// </summary>
        /// <param name="sign">扫码字符</param>
        /// <returns></returns>
        public string ScanCode(char sign)
        {
            try
            {
                string screenNo = scanerManage.ScanCode(sign);
                return screenNo;
            }
            catch
            {
                throw new Exception("扫码枪还未初始化成功！");
            }
        }

        /// <summary>
        /// 读取指定地址的结构体类型数据
        /// </summary>
        /// <typeparam name="T">结构体类型</typeparam>
        /// <param name="address">指定地址</param>
        /// <returns></returns>
        public T ReadStruct<T>(int address)
        {
            if (!plcObjects.ContainsKey("main"))
                Thread.Sleep(5000);//等待PLC启动，如果超过5秒没启动，表示超时失败
            if (!plcObjects.ContainsKey("main"))
                throw new TimeoutException("PLC初始化超时！");
            return ((PLCBaseEntity)plcObjects["main"]).ReadDataToStuct<T>(address);
        }

        /// <summary>
        /// 发送panel状态到单体
        /// </summary>
        /// <param name="trueList"></param>
        /// <param name="falseList"></param>
        public void SendPanelStatus(List<int> trueList, List<int> falseList)
        {
            msgServer.SendPanelStatus(trueList, falseList);
        }

        /// <summary>
        ///  获取 Arm 爪子上
        /// </summary>
        /// <param name="armType"></param>
        /// <returns></returns>
        public ushort GetArmTargetPos(ArmType armType)
        {
            int addr = 0;
            switch (armType)

            {
                case ArmType.In_A:
                    addr = GlobalParameters.Arm_InA_TargetPos;
                    break;
                case ArmType.In_B:
                    addr = GlobalParameters.Arm_InB_TargetPos;
                    break;             
                default:
                    break;
            }
            byte[] datas = ReadDatas("D" + addr, 1);
            ushort workNum = BitConverter.ToUInt16(datas, 0);
            return workNum;
        }
        /// <summary>
        /// 根据位置索引获取对应位置的panelId
        /// </summary>
        /// <param name="positionId">位置索引，见AddressEntity.PanelStationList值</param>
        /// <returns></returns>
        public string GetPanelIdByPosition(int positionId)
        {
            string panelId = "";
            int address = (positionId / 4) * GlobalParameters.D_Read_Unit_Buffer_Length + (positionId % 4) * 50 + GlobalParameters.unitPanelAddress;
            byte[] bytes = ReadDatas("D" + address, 30);
            panelId = Encoding.ASCII.GetString(bytes);
            return panelId.Trim('\0');

            //string panelId;
            //if (positionId < 48) /// unit test
            //{
            //    int address = (positionId - 18) * GlobalParameters.lineOfferLength + GlobalParameters.inPanelAddress;
            //    byte[] bytes = ReadDatas("D" + address, 50);
            //    panelId = Encoding.ASCII.GetString(bytes);
            //}
            //else if (positionId >= 18 && positionId < 56)
            //{
            //    int address = (positionId - 18) * GlobalParameters.D_Read_Unit_Buffer_Length + GlobalParameters.unitPanelAddress;
            //    byte[] bytes = ReadDatas("D" + address, 50);
            //    panelId = Encoding.ASCII.GetString(bytes);
            //}
            //else
            //{
            //    int address = (positionId - 56) * GlobalParameters.lineOfferLength + GlobalParameters.outPanelAddress;
            //    byte[] bytes = ReadDatas("D" + address, 50);
            //    panelId = Encoding.ASCII.GetString(bytes);
            //}
            //return panelId;
        }

        public bool MovePlatform(int groupIndex, double[] xyt)
        {
            UVWMoveInfo info = new UVWMoveInfo();
            info.X = (ushort)xyt[0];
            info.Y = (ushort)xyt[1];
            info.R = (ushort)xyt[2];
            if (groupIndex == 4)
                WriteDatas(GlobalParameters.D_Out_UVW1_Address, TypeConvert.StructToBytes(info));
            else if (groupIndex == 5)
                WriteDatas(GlobalParameters.D_Out_UVW2_Address, TypeConvert.StructToBytes(info));
            else
                return false;
            return true;
        }

        #endregion
    }

    /// <summary>
    /// 扫码结果类型
    /// </summary>
    public enum MCRResultType
    {
        ByPass,//通过
        KeyIn//输入
    }
}
