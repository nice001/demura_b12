﻿using System;

namespace DemuraShuttleLine.Interface
{
    interface ICodeScaner
    {
        /// <summary>
        /// 扫码枪状态
        /// </summary>
        bool Status { get; set; }

        /// <summary>
        /// 扫码枪编号
        /// </summary>
        string ScanerNo { get; set; }

        /// <summary>
        /// 扫码枪IP
        /// </summary>
        string ScanerIP { get; set; }

        /// <summary>
        /// 扫码枪Port
        /// </summary>
        int ScanerPort { get; set; }

        /// <summary>
        /// 连接超时时间
        /// </summary>
        int ConnectTimeOut { get; set; }

        /// <summary>
        /// 扫码超时时间
        /// </summary>
        int ScanTimeOut { get; set; }

        /// <summary>
        /// 扫码
        /// </summary>
        /// <returns>扫码结果</returns>
        string ScanCode(char sign);

        /// <summary>
        /// 连接扫码枪
        /// </summary>
        /// <returns>连接结果</returns>
        bool Connect();

        /// <summary>
        /// 断开扫码枪连接
        /// </summary>
        /// <returns>操作结果</returns>
        void DisConnect();

        event Action<object, bool> ScanerStatus;
    }
}
