#ifndef _PAC_H
#define _PAC_H


#ifdef __cplusplus
extern	"C" {
#endif

//伺服报警代号定义PAC_GetErrorCode()

#define EC_NORMAL				0	//正常
#define EC_MOTOR_DISABLED		1	//收到运动指令，但电机未使能
#define EC_MOTOR_MOVING			2	//收到运动指令，但电机仍在运动
#define EC_WARNING_POS_ERR		3	//跟随误差报警
#define EC_FATAL_POS_ERR		4	//跟随误差故障
#define EC_OVERCURRENT			5	//驱动器过流
#define EC_DRIVER_OVERHEAT		6	//驱动器过热
#define EC_OPERA_DENY  			7	//操作不允许
#define EC_SN_ERROR				8	//序列号错误
#define EC_DRIVER_FAULT			9	//功率元件故障，保留
#define EC_CTRL_CONFLICT		10	//控制方式冲突
#define EC_SERVO_MODE			11	//伺服模式错误

//伺服模式定义PAC_GetServoMode()

#define SM_POS				0	//位置模式
#define SM_VEL				1	//速度模式
#define SM_TQ				2	//力矩模式
#define SM_SOFTLANDING		3	//软着陆模式

//输入引脚定义PAC_GetInput()

#define PIN_PULSE		0	//脉冲信号引脚
#define PIN_DIR			1	//方向信号引脚
#define PIN_ENABLE		2	//使能信号引脚
#define PIN_ACLR		3	//清除报警信号引脚

//运动方向定义
#define DIR_POSITIVE	0	//正方向
#define DIR_NEGATIVE	1	//负方向


#ifndef _DLLEXP
#define _DLLEXP __declspec(dllimport)
#endif


//函数功能：连接驱动器, 会自动搜索COM端口号
//输入参数：btAddr为驱动器地址编号，任意总线网络中，任意一个存在的地址即可！
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_AutoConnect(BYTE btAddr);

//函数功能：连接驱动器, 需手动指定COM端口号
//输入参数：btCom为串口号，btAddr为驱动器地址编号，任意总线网络中，任意一个存在的地址即可！
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_ManualConnect(BYTE btCom, BYTE btAddr);

//函数功能：断开驱动器连接
//输入参数：无
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_Disconnect(void);


//函数功能：检测驱动器是否已经连接
//输入参数：无
//返回值：  为TRUE时，表示驱动器已连接
_DLLEXP BOOL PAC_IsConnected(void);


//函数功能：设置运行速度
//输入参数：btAddr为驱动器地址，usVel为待设置的速度值，对音圈电机，单位是mm/s，对有刷直流电机，单位是rpm
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_SetVel(BYTE btAddr, USHORT usVel);

//函数功能：设置运行加速度
//输入参数：btAddr为驱动器地址，usAcc为待设置的加速度值，对音圈电机，单位是mm/s/s，对有刷直流电机，单位是rpm/s
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_SetAcc(BYTE btAddr, USHORT usAcc);

//函数功能：设置编码器反馈位置
//输入参数：btAddr为驱动器地址，nPos为待设置的坐标值
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_SetActPos(BYTE btAddr, long nPos);

//函数功能：使能或去使能电机
//输入参数：btAddr为驱动器地址，bEnable为TRUE时使能，bEnable为FALSE时去使能
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_Enable(BYTE btAddr, BOOL bEnable);

//函数功能：相对运动一段位移
//输入参数：btAddr为驱动器地址，nRelCounts为相对运动的脉冲数，
//          bWaitDone为TRUE时，等待到位后，函数才返回;bWaitDone为FALSE时, 不等待到位，函数立即返回
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_MoveRel(BYTE btAddr, long nRelCounts, BOOL bWaitDone);

//函数功能：绝对运动到指定的坐标
//输入参数：btAddr为驱动器地址，nPos为要运动到的坐标，
//          bWaitDone为TRUE时，等待到位后，函数才返回;bWaitDone为FALSE时, 不等待到位，函数立即返回
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_MoveAbs(BYTE btAddr, long nPos, BOOL bWaitDone);

//函数功能：设置伺服模式
//输入参数：btAddr为驱动器地址，ucMode为待设置的伺服模式
//          ucMode等于SM_POS时：位置模式；ucMode等于SM_VEL时：速度模式；ucMode等于SM_POS时：力矩模式。
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_SetServoMode(BYTE btAddr, BYTE ucMode);

//函数功能：速度模式下，设置命令速度
//输入参数：btAddr为驱动器地址，sVel为待设置的速度值，正数表示正转，负数表示反转
//对音圈电机，单位是mm/s，对有刷直流电机，单位是rpm
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_CmdVel(BYTE btAddr, short sVel);

//函数功能：电流模式下，设置命令电流
//输入参数：btAddr为驱动器地址，sCurrent为电流值，正数表示输出正电流，负数表示输出负电流；取值范围：-2047 ~ +2047
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_CmdCur(BYTE btAddr, short sCurrent);

//函数功能：搜索编码器的Z相(INDEX)
//输入参数：btAddr为驱动器地址，usVel为搜索速度，usAcc为搜索加速度，dir为搜索方向,0为正方向，1为负方向，bWaitDone为TRUE时，表示等待搜索完成；
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_FindIndex(BYTE btAddr, USHORT usVel, USHORT usAcc, BYTE dir, BOOL bWaitDone);

//函数功能：搜索机械硬限位
//输入参数：btAddr为驱动器地址，usCurrent为搜索电流，取值范围：-2047 ~ +2047，正负表示方向，bWaitDone为TRUE时，表示等待搜索完成；
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_FindHardLimit(BYTE btAddr, short usCurrent, BOOL bWaitDone);

/*
// 函数功能：软着陆，不再支持距离限制，请见下面的函数声明
// 输入参数：btAddr为驱动器地址，usVel为着陆速度，单位是mm/s，对有刷直流电机，单位是rpm，dir为着陆方向，0为正方向，1为负方向;
//          usCurrent为电流(力)限制值, 且着陆时维持输出此值，取值范围1~2047，
// 			usDistance为距离限制,超过此距离未着陆则函数返回FALSE, 并保持位置不动， bWaitDone为TRUE时，表示等待着陆完成；
// 返回值：  为TRUE时，函数调用成功
// 注意：    着陆完成后，若要让电机抬起，则要先调用PAC_SetServoMode函数切换到位置模式，再调用运动函数！
// _DLLEXP BOOL PAC_SoftLanding(BYTE btAddr, USHORT usVel, BYTE dir, USHORT usCurrent, USHORT usDistance, BOOL bWaitDone);
*/

//函数功能：软着陆
//输入参数：btAddr为驱动器地址，usVel为着陆速度，单位是mm/s，对有刷直流电机，单位是rpm，dir为着陆方向，0为正方向，1为负方向;
//          usCurrent为电流(力)限制值, 且着陆时维持输出此值，取值范围1~2047，
//			bWaitDone为TRUE时，表示等待着陆完成；
//返回值：  为TRUE时，函数调用成功
//注意：    着陆完成后，若要让电机抬起，则要先调用PAC_SetServoMode函数切换到位置模式，再调用运动函数！
_DLLEXP BOOL PAC_SoftLanding(BYTE btAddr, USHORT usVel, BYTE dir, USHORT usCurrent, BOOL bWaitDone);

//函数功能：读取驱动器的输入引脚信号电平
//输入参数：btAddr为驱动器地址，pin为引脚编号，详见顶部引脚定义，pbInputLevel为返回引脚电平的指针，TRUE为高电平，FALSE为低电平
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_GetInputPin(BYTE btAddr, USHORT pin, BOOL* pbInputLevel);

//函数功能：获取当前伺服模式
//输入参数：btAddr为驱动器地址，pucMode为返回的伺服模式的指针
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_GetServoMode(BYTE btAddr, BYTE *pucMode);

//函数功能：获取编码器实际位置
//输入参数：btAddr为驱动器地址，pnPos为返回的实际位置的指针
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_GetActPos(BYTE btAddr, long *pnPos);

//函数功能：获取上次的给定位置
//输入参数：btAddr为驱动器地址，pnPos为返回的给定位置的指针
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_GetRefPos(BYTE btAddr, long *pnPos);

//函数功能：获取(报警)故障代码
//输入参数：btAddr为驱动器地址，pusErrorCode为返回的错误代码的指针
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_GetErrorCode(BYTE btAddr, USHORT *pusErrorCode);

//函数功能：清除(故障)报警
//输入参数：btAddr为驱动器地址，
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_ClearError(BYTE btAddr);

//函数功能：判断是否运动到位
//输入参数：btAddr为驱动器地址，pbInPos为返回的到位状态的指针，TRUE表示到位，FALSE表示没到位
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_IsInPosition(BYTE btAddr, BOOL *pbInPos);

//函数功能：判断是否已经使能
//输入参数：btAddr为驱动器地址，pbEnabled为返回的使能状态的指针，TRUE表示已使能，FALSE表示未使能
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_IsEnabled(BYTE btAddr, BOOL* pbEnabled);

//函数功能：获取实际速度
//输入参数：btAddr为驱动器地址，psActVel为返回的实际速度的指针
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_GetActVel(BYTE btAddr, short* psActVel);

//函数功能：获取实际电流
//输入参数：btAddr为驱动器地址，pusActCur为返回的实际电流的指针，电流值的范围是：0~4095
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_GetActCur(BYTE btAddr, unsigned short* pusActCur);

//-----------------------二轴或三轴联动控制函数-----------------------------------//

//函数功能：设置要联动轴的驱动器地址，该函数必须在其它联动函数之前调用！
//输入参数：btGroup为联动组号(取值0~84)， btAddrX为X轴驱动器地址，btAddrY为Y轴驱动器地址，btAddrZ为Z轴驱动器地址, 注意，地址不能为0(广播地址)
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_LinkSetAxes(BYTE btGroup, BYTE btAddrX, BYTE btAddrY, BYTE btAddrZ);


//函数功能：使能或释放要联动的所有轴
//输入参数：btGroup为联动组号(取值0~84)，bEnable为TRUE时使能，bEnable为FALSE时释放，
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_LinkEnable(BYTE btGroup, BOOL bEnable);

//函数功能：设置联动轴的运行速度
//输入参数：btGroup为联动组号(取值0~84)，usVel为待设置的速度，对音圈电机，单位是mm/s/s，对有刷直流电机，单位是rpm
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_LinkSetVel(BYTE btGroup, USHORT usVel);

//函数功能：设置联动轴的运行加速度
//输入参数：btGroup为联动组号(取值0~84)，usAcc为待设置的加速度，对音圈电机，单位是mm/s/s，对有刷直流电机，单位是rpm/s
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_LinkSetAcc(BYTE btGroup, USHORT usAcc);

//函数功能：设置联动轴的位置坐标
//输入参数：btGroup为联动组号(取值0~84)，nPosX为X轴坐标，nPosY为Y轴坐标，nPosZ为Z轴坐标
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_LinkSetActPos(BYTE btGroup, long nPosX, long nPosY, long nPosZ);

//函数功能：联动相对运动
//输入参数：btGroup为联动组号(取值0~84)，nRelCountsX, nRelCountsY, nRelCountsZ分别为联动三轴的脉冲数
//          bWaitDone为TRUE时，等待到位后，函数才返回;bWaitDone为FALSE时, 不等待到位，函数立即返回
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_LinkMoveRel(BYTE btGroup, long nRelCountsX, long nRelCountsY, long nRelCountsZ, BOOL bWaitDone);

//函数功能：联动绝对运动
//输入参数：btGroup为联动组号(取值0~84)，nPosX, nPosY, nPosZ分别为联动三轴要运动到的位置坐标
//          bWaitDone为TRUE时，等待到位后，函数才返回;bWaitDone为FALSE时, 不等待到位，函数立即返回
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_LinkMoveAbs(BYTE btGroup, long nPosX, long nPosY, long nPosZ, BOOL bWaitDone);

//函数功能：判断联动轴是否运动到位
//输入参数：btGroup为联动组号(取值0~84)，pbInPos为返回的到位状态的指针，TRUE表示到位，FALSE表示没到位
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_LinkIsInPosition(BYTE btGroup, BOOL *pbInPos);
//------------------------------------------------------------------------------------

//-------------------以下是设置或获取PID参数的函数接口--------------------------------

//函数功能：设置比例增益Kp
//输入参数：btAddr为驱动器地址，Kp为待设置的数值
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_SetKp(BYTE btAddr, unsigned short Kp);

//函数功能：设置微分增益Kd
//输入参数：btAddr为驱动器地址，Kd为待设置的数值
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_SetKd(BYTE btAddr, unsigned short Kd);

//函数功能：设置速度前馈增益Kvff
//输入参数：btAddr为驱动器地址，Kvff为待设置的数值
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_SetKvff(BYTE btAddr, unsigned short Kvff);

//函数功能：设置积分增益Ki
//输入参数：btAddr为驱动器地址，Ki为待设置的数值
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_SetKi(BYTE btAddr, unsigned short Ki);

//函数功能：设置积分模式Kim
//输入参数：btAddr为驱动器地址，Kim为待设置的数值
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_SetKim(BYTE btAddr, unsigned short Kim);

//函数功能：设置稳态比例增益Kc
//输入参数：btAddr为驱动器地址，Kc为待设置的数值
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_SetKc(BYTE btAddr, unsigned short Kc);

//函数功能：永久保存数据/运动程序(脚本) 到驱动器
//输入参数：btAddr为驱动器地址，
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_Save(BYTE btAddr);

//函数功能：获取比例增益Kp
//输入参数：btAddr为驱动器地址，pKp为待获取的数据变量的指针
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_GetKp(BYTE btAddr, unsigned short *pKp);

//函数功能：获取微分增益Kd
//输入参数：btAddr为驱动器地址，pKd为待获取的数据变量的指针
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_GetKd(BYTE btAddr, unsigned short *pKd);

//函数功能：获取速度前馈增益Kvff
//输入参数：btAddr为驱动器地址，pKvff为待获取的数据变量的指针
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_GetKvff(BYTE btAddr, unsigned short *pKvff);

//函数功能：获取积分增益Ki
//输入参数：btAddr为驱动器地址，pKi为待获取的数据变量的指针
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_GetKi(BYTE btAddr, unsigned short *pKi);

//函数功能：获取积分模式Kim
//输入参数：btAddr为驱动器地址，pKim为待获取的数据变量的指针
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_GetKim(BYTE btAddr, unsigned short *pKim);

//函数功能：获取稳态比例增益Kc
//输入参数：btAddr为驱动器地址，pKc为待获取的数据变量的指针
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_GetKc(BYTE btAddr, unsigned short *pKc);


//////////提供一个MODBUS底层接口，方便测试, 务必谨慎调用//////////////////////////////
//函数功能：设置MODBUS线圈位
//输入参数：btAddr为驱动器地址，usCoilIndex为MODBUS线圈编号, bOnOff为线圈状态，TRUE=闭合
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_SetCoilBit(BYTE btAddr, unsigned short usCoilIndex, BOOL bOnOff);

//函数功能：获取MODBUS线圈位
//输入参数：btAddr为驱动器地址，usCoilIndex为MODBUS线圈编号, pbOnOff为线圈状态，TRUE=闭合
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_GetCoilBit(BYTE btAddr, unsigned short usCoilIndex, BOOL *pbOnOff);

//函数功能：设置MODBUS保持寄存器的值
//输入参数：btAddr为驱动器地址，usRegIndex为MODBUS保持寄存器编号, usRegValue为保持寄存器的值
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_SetHoldingReg(BYTE btAddr, unsigned short usRegIndex, unsigned short usRegValue);

//函数功能：获取MODBUS保持寄存器的值
//输入参数：btAddr为驱动器地址，usRegIndex为MODBUS保持寄存器编号, pusRegValue为待获取数据的指针
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_GetHoldingReg(BYTE btAddr, unsigned short usRegIndex, unsigned short *pusRegValue);

//函数功能：获取MODBUS输入寄存器的值
//输入参数：btAddr为驱动器地址，usRegIndex为MODBUS输入寄存器编号, pusRegValue为待获取数据的指针
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_GetInputReg(BYTE btAddr, unsigned short usRegIndex, unsigned short *pusRegValue);

//-----------------------------------------------------------------------------------------------

//-------------------------------以下是脚本下载及运行函数接口----------------------------------------------

//函数功能：清除脚本
//输入参数：btAddr为驱动器地址
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_ClearScript(BYTE btAddr);

//函数功能：运行脚本
//输入参数：btAddr为驱动器地址
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_RunScript(BYTE btAddr);

//函数功能：停止脚本，
//说明：如果有无限循环指令，则运行完循环前的最后一条指令才会停止；
//输入参数：btAddr为驱动器地址
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_StopScript(BYTE btAddr);


//函数功能：下载一条脚本指令
//输入参数：btAddr为驱动器地址，ucLine为脚本指令的行号，从0行开始，最多到99， pcCmd 为指向脚本指令的字符串的指针
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_DownloadScriptCmd(BYTE btAddr, unsigned char ucLine, char *pcCmd);


//函数功能：下载脚本文件，可以包含多条脚本指令；
//输入参数：btAddr为驱动器地址，pcFileName 为指向脚本文件路径名的字符串指针
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_DownloadScriptFile(BYTE btAddr, char *pcFileName);


//函数功能：上传脚本并保存到文件
//输入参数：btAddr为驱动器地址，pcFileName 为指向脚本文件路径名的字符串指针
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_UploadScriptFile(BYTE btAddr, char *pcFileName);


//函数功能：检测脚本是否执行完成
//说明：如果脚本包含无限循环指令，除非调用PAC_StopScript停止脚本，否则脚本一直执行
//输入参数：btAddr为驱动器地址， bScriptDone 为待获取脚本完成标志的变量指针, TRUE = 执行完成
//返回值：  为TRUE时，函数调用成功
_DLLEXP BOOL PAC_IsScriptDone(BYTE btAddr, BOOL* bScriptDone);



#ifdef __cplusplus
}
#endif
#endif
